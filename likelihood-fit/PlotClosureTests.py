import ROOT
from ROOT import gStyle, TLegend
import os
import math
import argparse
import sys
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *

parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('input_file',
                    help='input file for example: /blah/blah.root NB/ Cant use wildcards to do multiple')

def GetROOTFile(InputFile):
	File = ROOT.TFile(InputFile)
	return File

def GetTagger(Inputfile):
	Tagger = Inputfile.split("/")[-1].split("_")[3]
	return Tagger

def GetFileType(inputfile):
	FileType = inputfile.split("/")[-1].split("emu_OS_J2_")[-1]
	FileType = FileType[:-5]
	return FileType

def SaveChi2Dist(inputFile, Tagger, OutputDir, fileType, FileExt=".pdf"):
	DistName = "c_chi2"
	Chi2Dist = inputFile.Get(DistName)
	# Chi2Dist.Draw()
	Chi2Dist.SaveAs(OutputDir + DistName + "_" + fileType + "_" + Tagger + FileExt)

def SaveSFDists(inputFile, Tagger, OutputDir, fileType, WP_Array, FileExt=".pdf"):
	DirName = "sf_and_beff"
	ROOTDir = inputFile.Get(DirName)
	# Get each SF plot per WP
	for WP in WP_Array:
		SFPlotName = "c_sf_b_"
		SFDist = ROOTDir.Get(SFPlotName+WP)
		for item in SFDist.GetListOfPrimitives():
			if "TH1D" in str(item):
				# Make modifications to the histogram here
				item.SetMaximum(1.05)
				item.SetMinimum(0.95)
		SFDist.Update()
		# Do not plot it SFDist past this point, root breaks for some reason
		SFDist.SaveAs(OutputDir + SFPlotName + WP + "_" + fileType + "_" + Tagger + FileExt)

# Get the input root file
args = parser.parse_args()
input_file = args.input_file
print("input_file: " + input_file)
input_root_file = GetROOTFile(input_file)

# Create an output directory
cwd = os.getcwd() + "/"
output_dir = cwd + "ClosureTestPlots"
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

WP_array = options.WPs

# Get the dists
tagger_name = GetTagger(input_file)
file_type = GetFileType(input_file)
SaveChi2Dist(input_root_file,tagger_name, output_dir+"/", file_type)
SaveSFDists(input_root_file,tagger_name, output_dir+"/", file_type, WP_array)
