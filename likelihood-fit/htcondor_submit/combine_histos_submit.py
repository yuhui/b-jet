import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../../TTbar-b-calib-final-selection-r21/')
from options_file import *

def writeShellScript():
    # Write a bash script
    fsh = None
    jobname = "job3"
    bashFile = cwd + jobname + ".sh" 
    fsh = open(bashFile, 'w')
    fsh.write("#!/bin/bash\n")
    # Set up atlas
    fsh.write('cd ' + cwd + '../ \n')
    fsh.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n")
    fsh.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\n")
    # Setup to ensure executables work
    fsh.write("source setup.sh \n")
    fsh.write("cd build \n")
    fsh.write("make \n")
    # Have to work out of likelihood dir for executables to run
    fsh.write("cd ../ \n")
    fsh.write('mkdir -p '+ move_to_dir + ' \n')
    cmd_python = "python combine_histos_4_fits_condor.py" 
    fsh.write(cmd_python + " \n")
    # Move everything from scratch to new dir
    scratch_location = '/scratch/' + user + "/"
    # fsh.write('mv '+ scratch_location + '* '+ move_to_dir + ' \n')
    # fsh.write('rm -rf '+ scratch_location + '* \n')
    fsh.close()
    return jobname, bashFile

def writeJobSubmit(cluster_jobName):
    # Job submit file for the cluster
    fsubmit=None
    submitFile = cwd + cluster_jobName + '.submit'
    fsubmit = open(submitFile, 'w')
    fsubmit.write("#!/bin/bash\n")
    fsubmit.write("executable     = "+cwd+cluster_jobName+".sh\n")
    fsubmit.write("output         = "+cwd+cluster_jobName+".out\n")
    fsubmit.write("error          = "+cwd+cluster_jobName+".error\n")
    fsubmit.write("log            = "+cwd+cluster_jobName+".log\n")
    fsubmit.write("concurrency_limits  = " + user + ":10 \n")
    # fsubmit.write("should_transfer_files   = IF_NEEDED\n")
    fsubmit.write("universe = vanilla\n")
    fsubmit.write("when_to_transfer_output = ON_EXIT\n")
    fsubmit.write('maxjobretirementtime     =  '+str(3600*24) +'\n')
    fsubmit.write("request_memory=30000MB \n")
    fsubmit.write("queue 1\n")
    fsubmit.close()
    return submitFile

def ExecuteToCluster(submitFile, bashFile):
    # Submit Job
    subprocess.check_call(["chmod", "u+x", bashFile])
    # subprocess.check_call(["condor_submit", submitFile])
    subprocess.check_call(["condor_qsub","-lmem=3000MB", bashFile])

# 
# Main
# 

# A few imported dirs and strings
cwd = os.getcwd() + "/"
user = user=options.input_selection_dir.split("/")[2]
ttbar_calib_dir = ("/").join(options.input_selection_dir.split("/")[:-2]) + "/"
move_to_dir = options.plot_dir + 'condor_processed/'

# Write the job files and submit
job_name, bash_file = writeShellScript()
submit_file = writeJobSubmit(job_name)
ExecuteToCluster(submit_file, bash_file)