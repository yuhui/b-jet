import os
import sys
import subprocess
import shutil
sys.path.insert(0, '../../TTbar-b-calib-final-selection-r21/')
from options_file import *

print "hello world!"

#
# usage: calculate_all_fits_remote.py [-h] [-c CHANNEL] [-t TAGGER]
#                                     [-w WORKINGPOINTNAME] [-r RLTAG]
#                                     [-d DATANAME]
#
# Calculates all fits for a given Tagger, workingpoint and cahnnel.
#
# optional arguments:
#   -h, --help            show this help message and exit
#   -c CHANNEL, --channel CHANNEL
#                         Channel to run over
#   -t TAGGER, --tagger TAGGER
#                         tagger to run over
#   -w WORKINGPOINTNAME, --workingPointName WORKINGPOINTNAME
#                         workingPointName to run over
#   -r RLTAG, --rlTag RLTAG
#                         releaseTag
#   -d DATANAME, --dataName DATANAME
#                         dataName


#we have to create filestrukture first
def writeShellScript(job_dir,job_name,runOptions):
    fsh = open(job_dir+job_name+'.sh', 'w')
    fsh.write('cd '+os.getcwd()+'\n')
    fsh.write('cd ..\n')
    fsh.write("alias setupATLAS='export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;\n source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'\n")
#    fsh.write("alias setupATLAS='export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase;\n  export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK;\n  source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'\n")
    fsh.write('setupATLAS \n')
    fsh.write('lsetup "root 6.08.06-x86_64-slc6-gcc62-opt"\n')
    #f.write('lsetup "cmake 3.8.1" >> mypayload.out\n')
    fsh.write('python calculate_all_fits.py' + runOptions +'\n')
    fsh.close()  # you can omit in most cases as the destructor will call it
def writeJobSubmit(job_dir,job_name):
    fsubmit = open(job_dir+job_name+'.submit', 'w')
    cwd=os.getcwd();
    fsubmit.write("executable     = "+job_dir+job_name+".sh\n")
    fsubmit.write("universe       = vanilla\n")
    fsubmit.write("output         = "+job_dir+job_name+".out\n")
    fsubmit.write("error          = "+job_dir+job_name+".error\n")
    fsubmit.write("log            = "+job_dir+job_name+".log\n")
    fsubmit.write("should_transfer_files   = IF_NEEDED\n")
    fsubmit.write("when_to_transfer_output = ON_EXIT\n")
    fsubmit.write('requirements   = OpSysAndVer == "SL6"\n')
    fsubmit.write('+MyProject              = "af-atlas"\n')
    fsubmit.write('+RequestRuntime         =  '+str(60*60*1) +'\n')
    fsubmit.write("queue\n")
    fsubmit.close()


def submit_Fit(job_name , runOptions):
    if not os.path.exists(condor_dir):
        os.makedirs(condor_dir)
    job_dir=condor_dir + job_name+'/'
    if not os.path.exists(job_dir):
        os.makedirs(job_dir)
    else:
        shutil.rmtree(job_dir)
        os.makedirs(job_dir)
    runOptions=runOptions+" --plot_dir " +options.plot_dir
    writeShellScript(job_dir,job_name,runOptions)
    writeJobSubmit(job_dir,job_name)
    subprocess.check_call(["chmod", "u+x", job_dir+job_name+".sh"])
#    subprocess.check_call(["condor_submit", job_dir+job_name+".submit"])
    subprocess.check_call(["condor_qsub","-lmem=2000MB", job_dir+job_name+".sh"])

    print "submitted: "+ 'python calculate_all_fits.py ' + runOptions +'\n'


rlTag = "r22"
condor_dir=options.condor_dir
channels=["emu_OS_J2"] #"emu_OS_J2","emu_OS_J2_m_lj_cut"
dataName=options.data_name
taggers=["DL1dv01", "GN120220509"]#,"DL1"] #MV2c10Flip_MV2c10"
workingPointNames=["FixedCutBEff"] #"HybBEff",
# boot_strap_single_list=[
#     "nominal",
# ]
boot_strap_single_list=[]
suggested_number_of_packs=55



print "submitting fits:"
# for channel in channels:
#     for tagger in taggers:
#         for workingPointName in workingPointNames:
#             job_name="Fit_"+rlTag+"_"+channel+"_"+tagger+"_"+workingPointName+"_fconst_"+dataName
#             runOptions=" -r "+ rlTag+" -c "+channel+" -t "+tagger + " -w "+ workingPointName+ " -d "+dataName   + " --rrF"
#             submit_Fit(job_name,runOptions)

channel="emu_OS_J2"
for tagger in taggers:
    for workingPointName in workingPointNames:
        job_name="Fit_"+rlTag+"_"+channel+"_"+tagger+"_"+workingPointName+"_combined_fit"+dataName
        runOptions=" -r "+ rlTag+" -c "+channel+" -t "+tagger + " -w "+ workingPointName+ " -d "+dataName +" --combined_fit"  + " --rrF"
        submit_Fit(job_name,runOptions)
# print "submitting bootstrap fits:"
        for pack in xrange(1, suggested_number_of_packs+1):
            job_name="Fit_"+rlTag+"_"+channel+"_"+tagger+"_"+workingPointName+"_combined_fit"+dataName+"_pack_"+str(pack)
            runOptions=" -r "+ rlTag+" -c "+channel+" -t "+tagger + " -w "+ workingPointName+ " -d "+dataName +" --combined_fit"  + " --rrF" +" --run_pack "+str(pack) # +" --boot_strap"
            submit_Fit(job_name,runOptions)
print "submitting bootstrap fits:"
#calculate boostrapunc for some systematics:
for bootstrap_single in boot_strap_single_list:
    for channel in channels:
        for tagger in taggers:
            for workingPointName in workingPointNames:
                job_name="Fit_"+rlTag+"_"+channel+"_"+tagger+"_"+workingPointName+"_fconst_"+dataName+"_"+bootstrap_single
                runOptions=" -r "+ rlTag+" -c "+channel+" -t "+tagger + " -w "+ workingPointName+ " -d "+dataName  + " --boot_strap" + " --rrF" +" --boot_strap_calc_single "+bootstrap_single
                submit_Fit(job_name,runOptions)
    channel="emu_OS_J2"
    for tagger in taggers:
        for workingPointName in workingPointNames:
            job_name="Fit_"+rlTag+"_"+channel+"_"+tagger+"_"+workingPointName+"_combined_fit"+dataName+"_"+bootstrap_single
            runOptions=" -r "+ rlTag+" -c "+channel+" -t "+tagger + " -w "+ workingPointName+ " -d "+dataName +" --combined_fit" + " --boot_strap" + " --rrF"+" --boot_strap_calc_single "+bootstrap_single
            submit_Fit(job_name,runOptions)
