import os
import sys
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import subprocess
import array
import math
import argparse
import shutil
import logging
logging.basicConfig(level=logging.INFO)
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../FinalSelection/')
from options_file import *

def main():
	args = get_args()
	logging.info("Got arguments from arg parse")

	workspace_dir, fitplots_dir = SetupDirectories(args, options.plot_dir)
	fit_config = SetupFitConfig(args)

	# Configurations for the fit
	ttbar_file_to_create_model="ttbar_Sherpa221"
	ttbar_samples=[options.ttb_sample]+[sample('ttbar_PowHW7',True),sample(ttbar_file_to_create_model,True)]
	PrintTTbarInfo(ttbar_samples)

	if args.rrF:
		logging.info("You've chosen to rerun the fits")
		RerunFits(args, ttbar_samples, ttbar_file_to_create_model, fit_config)
		logging.debug("Back in py ... ")

	NomMeasurement = GetNomMeasurement(args, fitplots_dir, fit_config)
	OutputFile = CreateStressTestFile(args, ttbar_file_to_create_model, fit_config)

	# A few values for hist plotting
	Colours= GetColours()

	logging.info("Retrieving histograms required for plots ... ")
	hists=[]
	arrays=[]
	wps=["70"]
	for wp in wps:
		col=0
		ttAltHistList=[]
		for ttb_sample in ttbar_samples:
			logging.info("Running over ttbar sample:\t" + str(ttb_sample.name) + "\tsystematic_name:\t" + str(ttb_sample.systematic_name))
			if ttb_sample.systematic_name == "ttbar":
				ttb_sample.systematic_name = "nominal"

			# Read the fit plot file created at the start of the code
			StressTestPath=fitplots_dir+"/FitPlot_r21_"+options.data_name+"_"+args.tagger+"_FixedCutBEff_"+args.channel+fit_config+"_"+ttb_sample.systematic_name+"_stressTestMoMc_"+ttbar_file_to_create_model+"_nstat.root"
			logging.debug("Taking info from stress test fit file:\n" + StressTestPath)
			StressTestFile=tfile(StressTestPath)

			EffHist_Post = StressTestFile.Get("sf_and_beff/e_b_"+wp+"_Postfit").Clone("e_b_"+wp+"_Postfit_"+ttb_sample.name)
			EffHist_Post.SetLineColor(Colours[col]);
			EffHist_Post.SetMarkerColor(Colours[col]);

			OutputFile.cd()
			if ttb_sample.systematic_name==ttbar_file_to_create_model:
				logging.info("Found alternative model sample:\t" + str(ttb_sample.systematic_name))
				EffHist_Post.Write()
				# ttAltHistList.append(OutputFile.Get(EffHist_Post.GetName()))

				# Get postfit hist
				BEff_Hist_Postfit_AltModel=OutputFile.Get(EffHist_Post.GetName())

				# Get prefit hist
				BEff_Hist_Prefit_AltModel_name="e_b_"+wp+"_Prefit_"+ttb_sample.systematic_name
				BEff_Hist_Prefit_AltModel_tmp=StressTestFile.Get("sf_and_beff/e_b_"+wp+"_Prefit").Clone(BEff_Hist_Prefit_AltModel_name)
				OutputFile.cd()
				BEff_Hist_Prefit_AltModel_tmp.SetLineWidth(2)
				BEff_Hist_Prefit_AltModel_tmp.SetLineStyle(2)
				BEff_Hist_Prefit_AltModel_tmp.Write()

				# Get another prefit hist for some reason
				BEff_Hist_Prefit_AltModel=OutputFile.Get(BEff_Hist_Prefit_AltModel_name)

				col=col+1
			elif ttb_sample.systematic_name == "ttbar_PowHW7":
				logging.info("Found PowHW7 sample:\t" + str(ttb_sample.systematic_name))
				EffHist_Post.Write()

				# Create var for the HW7 hist
				EffHist_Post_HW7=OutputFile.Get(EffHist_Post.GetName())
				ttAltHistList.append(EffHist_Post_HW7)
				ROOT.SetOwnership(EffHist_Post_HW7,False)

				col=col+1
			elif ttb_sample.systematic_name == "nominal":
				EffHist_Post.SetLineColor(1);
				EffHist_Post.SetMarkerColor(1);
				EffHist_Post.Write()

				# Create a variable for the nominal postfit b-eff
				EffHist_Post_Nominal=OutputFile.Get(EffHist_Post.GetName())
			else:
				# Incase its not Alt model, PW7, ttbar nom, you end up here
				EffHist_Post.Write()
				ttAltHistList.append(OutputFile.Get(EffHist_Post.GetName()))
				ROOT.SetOwnership(EffHist_Post,False)

				col=col+1
		
		# Now we have all the histograms we need: PowHW7, 221 in ttAltHistList and
		OutputFile.cd()

		logging.info("Creating a list of sf histograms from not 221 or nominal post-fit b-eff plots")
		ttAltHistList_SF=[]
		for EffHist_Post in ttAltHistList:
			logging.debug("Creating sf for hist:\t" + str(EffHist_Post.GetName()))

			h_sf=EffHist_Post.Clone("sf_"+EffHist_Post.GetName())
			ROOT.SetOwnership(h_sf,False)

			# Subtract and divide by prefit alt model
			logging.debug("Subracting and dividing the postfit b-eff hist by prefit Alt model")
			h_sf.Add(BEff_Hist_Prefit_AltModel,-1)
			h_sf.Divide(BEff_Hist_Prefit_AltModel)

			SetStressPlotYLabel(h_sf)
			h_sf.Write()
			ttAltHistList_SF.append(h_sf)

		# Begin the plotting stage
		CreateFinalBEffPlot(OutputFile, BEff_Hist_Prefit_AltModel, BEff_Hist_Postfit_AltModel, ttAltHistList, wp)

		# Next plot
		logging.info("Creating the final SF plot ... ")

		# Setup
		c_obs = NewTCanvas("c_sf_b_"+wp+"_Postfit")		
		legend = NewTLegend()
		c_obs.cd()

		YMin = -0.07
		YMax = 0.1

		logging.debug("Subracting and dividing the nominal postfit b-eff by prefit Alt model to be used as a stat error")
		Nom_Stat_Hist=EffHist_Post_Nominal.Clone("h_mc_stat_"+EffHist_Post_Nominal.GetName())
		Nom_Stat_Hist.Add(BEff_Hist_Prefit_AltModel,-1);
		Nom_Stat_Hist.Divide(BEff_Hist_Prefit_AltModel)
		hists.append(Nom_Stat_Hist)
		ROOT.SetOwnership(Nom_Stat_Hist,False)

		# Do something with the postfit alt model b-eff hist and this becomes our "Pseudodata stat unc." in the plot
		AltModel_Stat_Hist=BEff_Hist_Postfit_AltModel.Clone("h_stat_band")
		AltModel_Stat_Hist.Reset()
		for i_bin in xrange(1,AltModel_Stat_Hist.GetNbinsX()+1):
			AltModel_Stat_Hist.SetBinContent(i_bin,BEff_Hist_Postfit_AltModel.GetBinError(i_bin)/BEff_Hist_Prefit_AltModel.GetBinContent(i_bin))
		AltModel_Stat_Hist.SetFillColor(ROOT.kGreen -8)
		AltModel_Stat_Hist.SetLineColor(ROOT.kGreen -8)
		SetStressPlotYLabel(AltModel_Stat_Hist)
		AltModel_Stat_Hist.GetYaxis().SetRangeUser(-0.2,0.2)
		AltModel_Stat_Hist_Neg=AltModel_Stat_Hist.Clone("h_stat_band_neg")
		AltModel_Stat_Hist_Neg.Scale(-1)

		# Draw pos and negative side
		AltModel_Stat_Hist.SetMinimum(YMin)
		AltModel_Stat_Hist.SetMaximum(YMax)
		AltModel_Stat_Hist.Draw("HIST")

		AltModel_Stat_Hist_Neg.SetMinimum(YMin)
		AltModel_Stat_Hist_Neg.SetMaximum(YMax)
		AltModel_Stat_Hist_Neg.Draw("HISTSAME")
		legend.AddEntry(AltModel_Stat_Hist_Neg,"Pseudodata stat unc.","F")

		logging.debug("Fetching other uncertainties for plot")
		# Load from file 
		# ttbar_mod_unc=NomMeasurement.Get("unc_summary_plots/e_b_wp_"+wp+"/e_b_"+wp+"_ttbar_modeling_syst_Error_combi_rel")
		ttbar_mod_unc=NomMeasurement.Get("unc_summary_plots/e_b_wp_"+wp+"/h_sym_ttbar_combined_"+wp)
		logging.debug("Retrieved ttbad mod unc: " + str(ttbar_mod_unc))
		ROOT.SetOwnership(ttbar_mod_unc,False)

		# mc_stat_nom=NomMeasurement.Get("other_systematics/e_b_wp_"+wp+"/e_b_"+wp+"_MC_stat_nominal_syst_Error_rel") 
		mc_stat_nom=NomMeasurement.Get("results_comulative/"+"sf_b_"+wp+"_stat_Error_rel") 
		logging.debug("Retrieved mc stat unc: " + str(mc_stat_nom))
		ROOT.SetOwnership(mc_stat_nom,False)

		Nom_Model_Tot_Unc=Nom_Stat_Hist.Clone("h_tot_unc_"+EffHist_Post_Nominal.GetName())
		logging.debug("Retrieved nom stat hist histogram: " + str(Nom_Model_Tot_Unc))
		ROOT.SetOwnership(Nom_Model_Tot_Unc,False)
		hists.append(Nom_Model_Tot_Unc)

		for i_bin in xrange(1,Nom_Stat_Hist.GetNbinsX()+1):
			Nom_Stat_Hist.SetBinError(i_bin,mc_stat_nom.GetBinContent(i_bin) )
			tot_unc=mc_stat_nom.GetBinContent(i_bin)**2+ttbar_mod_unc.GetBinContent(i_bin)**2
			Nom_Model_Tot_Unc.SetBinError(i_bin,math.sqrt(tot_unc))

		logging.debug("Created final uncertainty band")

		# Now do some TGraph shit involving all the stuff above
		a_nom_mc_stat=arrays_of_TH1(Nom_Stat_Hist, shift=-0.03)
		g_nom_mc_stat=TGraphAsymmErrors(a_nom_mc_stat.N_bins, a_nom_mc_stat.a_x_nom, a_nom_mc_stat.a_y_nom, a_nom_mc_stat.a_x_down, a_nom_mc_stat.a_x_up, a_nom_mc_stat.a_y_down, a_nom_mc_stat.a_y_up)
		g_nom_mc_stat.SetName("g_nom_mc_stat_"+wp)
		ROOT.SetOwnership(g_nom_mc_stat,False)
		g_nom_mc_stat.SetLineWidth(2)
		g_nom_mc_stat.SetFillStyle(0)
		g_nom_mc_stat.Draw("2")
		g_nom_mc_stat.Write()
		g_nom_mc_stat=OutputFile.Get(g_nom_mc_stat.GetName())

		hists.append(g_nom_mc_stat)

		a_nom_tot=arrays_of_TH1(Nom_Model_Tot_Unc,bin_size=0,shift=-0.03)
		g_nom_tot=TGraphAsymmErrors(a_nom_tot.N_bins, a_nom_tot.a_x_nom, a_nom_tot.a_y_nom, a_nom_tot.a_x_down, a_nom_tot.a_x_up, a_nom_tot.a_y_down, a_nom_tot.a_y_up)
		g_nom_tot.SetName("g_nom_tot_"+wp)
		ROOT.SetOwnership(g_nom_tot,False)
		g_nom_tot.SetLineWidth(2)
		g_nom_tot.SetMarkerStyle(20)
		g_nom_tot.Draw("P")
		g_nom_tot.Write()
		g_nom_tot=OutputFile.Get(g_nom_tot.GetName())
		hists.append(g_nom_tot)

		# Do the same stat thing but for HW7
		Hist_HW7_stat=EffHist_Post_HW7.Clone("h_mc_stat_"+EffHist_Post_HW7.GetName())
		ROOT.SetOwnership(Hist_HW7_stat,False)
		logging.debug("Subtracting and dividing HW7 postfit b-eff plot to create a stat hist")
		Hist_HW7_stat.Add(BEff_Hist_Prefit_AltModel,-1)
		Hist_HW7_stat.Divide(BEff_Hist_Prefit_AltModel)
		SetStressPlotYLabel(Hist_HW7_stat)
		h_f_HW7_tot=Hist_HW7_stat.Clone("h_tot_unc_blu"+EffHist_Post_HW7.GetName())
		ROOT.SetOwnership(h_f_HW7_tot,False)
		Hist_HW7_stat.Write()
		Hist_HW7_stat=OutputFile.Get(Hist_HW7_stat.GetName()) 

		logging.info("Creating final uncertainty histograms ...")
		# mc_stat_HW7=NomMeasurement.Get("ttbar_systematics/e_b_wp_"+wp+"/smoothing_FTAG2_ttbar_PowHW7/e_b_"+wp+"_FTAG2_ttbar_PowHW7_syst_up_Error_rel_unsmoothed") 
		# mc_stat_HW7=NomMeasurement.Get("h_mc_stat_e_b_"+wp+"_Postfit_FTAG2_ttbar_PowHW7") 
		mc_stat_HW7=NomMeasurement.Get("ttbar_systematics/e_b_wp_"+wp+"/e_b_"+wp+"_ttbar_PowHW7_syst_up_Error_rel") 
		logging.debug("Retrieved MC Stat from ttbar PW7: " + str(mc_stat_HW7))
		for i_bin in xrange(1,Hist_HW7_stat.GetNbinsX()+1):
			stat_unc=mc_stat_HW7.GetBinError(i_bin)
			Hist_HW7_stat.SetBinError(i_bin, stat_unc)
			tot_unc=stat_unc**2+ttbar_mod_unc.GetBinContent(i_bin)**2
			h_f_HW7_tot.SetBinError(i_bin,math.sqrt(tot_unc))

		for h_sf in ttAltHistList_SF:
			c_obs.cd()
			# legend.AddEntry(h_sf,h_sf.GetName().replace("sf_e_b_"+wp+"_Postfit_FTAG2_ttbar_","").replace("_nominal",""),"L")
			# This is most likely HW7 btw
			legend.AddEntry(h_sf,"Alternative model (tot. unc)","L")
			h_sf.SetMinimum(YMin)
			h_sf.SetMaximum(YMax)
			h_sf.Draw("HISTSAME")

		Nom_Model_Tot_Unc.SetMinimum(YMin)
		Nom_Model_Tot_Unc.SetMaximum(YMax)
		# legend.AddEntry(Nom_Model_Tot_Unc,Nom_Model_Tot_Unc.GetName().replace("sf_e_b_"+wp+"_ttbar_","").replace("_nominal",""),"LPE")
		legend.AddEntry(Nom_Model_Tot_Unc,"Nominal model (tot. unc)","LPE")
		Nom_Model_Tot_Unc.Draw("HISTSAME")

		# Draw ATLAS Text and TLegend
		ATLASText = DrawATLAS()
		legend.Draw()

		# Draw TLine around y=0 mark
		line1 = TLine(EffHist_Post.GetXaxis().GetXmin(),0,EffHist_Post.GetXaxis().GetXmax(),0)
		line1.SetLineWidth(2)
		line1.SetLineStyle(2)
		line1.Draw("SAME")

		OutputFile.cd()
		c_obs.RedrawAxis();
		c_obs.Write()
		c_obs.SaveAs("StressTest_FinalPlot_SF.pdf")
		logging.info("Finished plotting final SF plot!")
	logging.info("All Finished.")
	logging.info("Good luck ever modifying this code, it is a nightmare :)")

class arrays_of_TH1:
    def  __init__(self, hist,bin_size=0.02,shift=0.0):
        self._hist=hist
        l_x_nom=[]
        l_x_down=[]
        l_x_up=[]
        l_y_nom=[]
        l_y_up=[]
        l_y_down=[]
        l_zero=[]
        N_bins=hist.GetNbinsX()
        for i_bin in xrange(1,N_bins+1):
            x_center=hist.GetXaxis().GetBinCenter(i_bin)
            if not shift==0:                
                x_center=10**(math.log10(x_center)+shift)
            y_center=hist.GetBinContent(i_bin)
            y_unc=hist.GetBinError(i_bin)
            l_x_nom.append(x_center)
            if bin_size==0:
                l_x_down.append(0)
                l_x_up.append(0)
            else:
                l_x_down.append(-((10**(math.log10(x_center)-bin_size))-x_center))
                l_x_up.append((10**(math.log10(x_center)+bin_size))-x_center)
                #print x_center, math.log10(x_center),(math.log10(x_center)-bin_size),(10**(math.log10(x_center)-bin_size)),(10**(math.log10(x_center)-bin_size))-x_center
            l_y_nom.append(y_center)
            l_y_up.append(y_unc)
            l_y_down.append(y_unc)
        self.a_x_nom=array.array("d", l_x_nom)
        self.a_y_nom=array.array("d", l_y_nom)
        self.a_x_down=array.array("d", l_x_down)
        self.a_x_up=array.array("d", l_x_up)
        self.a_y_up=array.array("d", l_y_up)
        self.a_y_down=array.array("d", l_y_down)
        self.N_bins=N_bins

def SetupDirectories(Args, OutputDir):
	logging.debug("Setting up the workspace directories ... ")
	WSDir = OutputDir+"/Workspaces"
	FitPlotsDir = OutputDir+"/FitPlots"
	if Args.combined_fit:
		WSDir=WSDir+"_3SB"
		FitPlotsDir=FitPlotsDir+"_3SB"
	if not os.path.exists(WSDir):
	    os.makedirs(WSDir)
	if not os.path.exists(FitPlotsDir):
	    os.makedirs(FitPlotsDir)
	return WSDir, FitPlotsDir

def SetupFitConfig(Args):
	FitConfig='_fconst'
	if Args.combined_fit:
		FitConfig=''
	return FitConfig

def PrintTTbarInfo(ttbarSamples):
	for ttbarSamp in ttbarSamples:
		logging.debug("--------------------------------------------------------")
		logging.debug("For ttbar sample: "+str(ttbarSamp.name))
		logging.debug("With systematic: "+str(ttbarSamp.systematic))
		logging.debug("And systematic_name: "+str(ttbarSamp.systematic_name))
		logging.debug("And name short: "+str(ttbarSamp.name_short))
	logging.debug("--------------------------------------------------------")

def GetColours():
	return [ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]

def GetNomMeasurement(Args, FitPlotsDir, FitConfig):
	# Open up nominal measurement
	NomMeasurementName = FitPlotsDir+"/FinalFitPlots_r21_"+options.data_name+"_"+Args.tagger+"_FixedCutBEff_"+Args.channel+FitConfig+".root"
	logging.info("Getting nominal measurent from:\t" + NomMeasurementName)
	return tfile(NomMeasurementName) 

def CreateStressTestFile(Args, ttbarAltModel, FitConfig):
	# Create an output file for the stress test
	OutputName = "stressFits_"+ttbarAltModel+"_"+Args.tagger+"_"+Args.channel+FitConfig+"_combination.root"
	logging.info("Outputting stress test to:\t" + OutputName)
	return tfile(OutputName,"recreate")

def SetStressPlotYLabel(Hist):
	return Hist.SetYTitle("(#epsilon_{b}^{est.}-#epsilon_{b}^{true})/ #epsilon_{b}^{true}")

def NewTLegend():
	legend_x      = 0.52#0.65
	legend_y      = 0.895
	legend_width  = 0.28
	legend_height = 0.27#0.34

	Legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
	Legend.SetFillStyle(0)
	Legend.SetLineColor(0)
	Legend.SetTextFont(42)
	Legend.SetTextSize(42)
	Legend.SetTextSize(0.028)
	return Legend

def NewTCanvas(Name):
	Canvas=TCanvas(Name)

	# Canvas settings
	Canvas.SetLogx()
	Canvas.SetLeftMargin(0.15)
	Canvas.SetRightMargin(0.15)
	Canvas.SetBottomMargin(0.15)
	return Canvas

def DrawATLAS():
	latexObject = ROOT.TLatex()
	latexObject.SetTextFont(42)
	latexObject.SetTextAlign(11)
	latexObject.SetTextColor(1)

	# ATLAS Text
	latexObject.SetTextSize(0.06)
	latexObject.DrawLatexNDC(0.17, 0.83, "#scale[1.2]{#bf{#it{ATLAS}} Internal}")

	# CoM / Lumi Text
	latexObject.SetTextSize(0.056)
	comEnergy = 13
	luminosity = 139100
	latexObject.DrawLatexNDC(0.17, 0.83 - 0.06, str(comEnergy)+" TeV "+str(luminosity/1.e3) + " fb^{-1}")

	# Taggier / WP
	latexObject.SetTextSize(0.056)
	WP = 70
	luminosity = 139100
	latexObject.DrawLatexNDC(0.17, 0.83 - 0.12, "DL1r,  #epsilon_{b}="+str(WP)+"%")

	# More detail lol
	latexObject.SetTextSize(0.056)
	luminosity = 139100
	latexObject.DrawLatexNDC(0.17, 0.83 - 0.18, "Single Cut OP")
	return latexObject

def CreateFinalBEffPlot(OutputFile, PrefitAltModelBEff, PostfitAltModelBEff, ttbarHistList, WP):
	logging.info("Creating the final b-eff plot ... ")

	# Setup
	Canvas = NewTCanvas("c_e_b_"+WP+"_Postfit")
	legend = NewTLegend()
	Canvas.cd()

	# Plot range for y axis
	YMin = 0.4
	YMax = 1.2

	# Draw pre and post fit alt models and add to legend
	PostfitAltModelBEff.SetMinimum(YMin)
	PrefitAltModelBEff.SetMinimum(YMin)
	PostfitAltModelBEff.SetMaximum(YMax)
	PrefitAltModelBEff.SetMaximum(YMax)
	PostfitAltModelBEff.Draw("LPE")
	PrefitAltModelBEff.Draw("HISTSAME")
	legend.AddEntry(PostfitAltModelBEff,PostfitAltModelBEff.GetName().replace("e_b_"+WP+"_",""),"LPE")
	legend.AddEntry(PrefitAltModelBEff,PrefitAltModelBEff.GetName().replace("e_b_"+WP+"_","").replace("_nominal","").replace("FTAG2_ttbar_",""),"L")
	
	# Add remaining tt alt hists to plot
	for EffHist_Post in ttbarHistList:
		Canvas.cd()
		EffHist_Post.SetMinimum(YMin)
		EffHist_Post.SetMaximum(YMax)
		legend.AddEntry(EffHist_Post,EffHist_Post.GetName().replace("e_b_"+WP+"_",""),"L")
		EffHist_Post.Draw("HISTSAME")
	
    # Draw ATLAS Text and legend
	ATLASText = DrawATLAS()
	legend.Draw()

	OutputFile.cd()
	Canvas.RedrawAxis()
	Canvas.Write()
	Canvas.SaveAs("StressTest_FinalPlot_BEff.pdf")
	logging.info("Finished creating b-eff final plot!")

def RerunFits(Args, ttbarSamples, ttbarAltModel, FitConfig):
	logging.debug("Setting up parameters")
	s_data_lumi="{:.1f}".format(options.data_lumi/1000)
	options_for_all_fits = []
	if Args.combined_fit:
		options_for_all_fits=options_for_all_fits+["-l", s_data_lumi, "-m", "mc16ade", "--allow_neg_sf_xx" ]

	num =1
	tp = ThreadPool(num)
	logging.info("Rerunning fits ...")
	for ttbarSamp in ttbarSamples:
		if ttbarSamp.systematic_name == "ttbar":
			logging.debug("Found nominal ttbar sample, this then changes the path")
			ttbarSamp.systematic_name = "nominal"

		# Julian used this input file but I cant see how that would exist:
		# input_file=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+ttbarSamp.systematic_name+"_stressTestMoMc_"+ttbarAltModel+".root"
		# The next just takes the nominal input
		input_file=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+ttbarSamp.systematic_name+".root"
		syst_name=ttbarSamp.systematic_name+"_stressTestMoMc_"+ttbarAltModel+"_nstat"
		logging.debug("Input file to the fits is:\t" + input_file)

		logging.info("Calculating Fit with Systematic Name:\t"+str(syst_name))
		if Args.combined_fit:
			# print("./build/2jet_pt_combined_fit","-i",input_file,"-o", options.plot_dir, "-r", Args.rlTag,"-d", options.data_name, "-s",syst_name,"-c", Args.channel,"-t",Args.tagger,"-w",Args.workingPointName,"--allow_neg_sf_xx")
			p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", options.plot_dir, "-r", Args.rlTag,"-d", options.data_name, "-s",syst_name,"-c", Args.channel,"-t",Args.tagger,"-w",Args.workingPointName,"--allow_neg_sf_xx"]) #,stdout=log "--allow_neg_sf_xx"
		else:
			p=subprocess.Popen(["./build/2jet_pt_fit", "-i",input_file,"-o", options.plot_dir, "-r", Args.rlTag, "-d", options.data_name, "-s",syst_name,"-c", Args.channel,"-t",Args.tagger,"-w",Args.workingPointName])
		p.wait()
	logging.info("Finished rerunning fits")
	return 

def get_args():
	args = argparse.ArgumentParser(
	    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
	args.add_argument('-c',"--channel",default="emu_OS_J2",  #
	                    help='Channel to run over')
	args.add_argument('-t',"--tagger",default="GN120220509",
	                    help='tagger to run over')
	args.add_argument('-w',"--workingPointName",default="FixedCutBEff",
	                    help='workingPointName to run over')
	args.add_argument('-r',"--rlTag",default="r21",
	                    help='releaseTag')
	args.add_argument('--rrF', action='store_true',
	                    help='rerun all the Fits (default: false)')
	args.add_argument('-d',"--dataName",default=options.data_name,
	                    help='dataName')
	args.add_argument('--combined_fit',action='store_true',default=True,
	                    help='run with combined fit method.')
	return args.parse_args()

def tfile(path, mode='READ'):
    if not os.path.exists(path):
        if not mode=="UPDATE" and not mode=="recreate":
            raise RuntimeError("{} not found!".format(path))
    tf = ROOT.TFile.Open(path, mode)
    if tf.IsZombie():
            raise RuntimeError("Unable to open {}!".format(path))
    return tf

if __name__ == '__main__':
    main()
