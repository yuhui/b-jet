import ROOT
import os
import math
from array import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse



gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);



releaseName= '21_2_53'
ChannelName= 'emu_OS_J2'
fitConfigName= 'combined_fit'
fit_folder='emu_OS_J2_combined_fit'
taggerNames=['MV2c10']#,'DL1']
WPNames=["FixedCutBEff"]

#emu_OS_J2_m_lj_cut/FitPlots_r21_emu_OS_J2_m_lj_cut_MV2c10_FixedCutBEff/FinalFitPlots_r21_data17_MV2c10_FixedCutBEff_emu_OS_J2_m_lj_cut_fconst

# fit_folder='emu_OS_J2_m_lj_cut'
# ChannelName='emu_OS_J2_m_lj_cut'
# fitConfigName='fconst'

output_file_name="data1516_data17_unc_comparison_"+releaseName+'_'+ChannelName+'_'+fitConfigName +'.root'

output_file=ROOT.TFile(output_file_name,"recreate")                        
legend = TLegend(0.563401, 0.547085, 0.84438, 0.665919)
#legend.SetFillStyle(0)
legend.SetLineColor(0)
legend.SetBorderSize(0)
legend.SetTextFont(42)
legend.SetTextSize(0.04)

legend2 = TLegend(0.325648, 0.528027, 0.602305, 0.678251)
#legend.SetFillStyle(0)
legend2.SetLineColor(0)
legend2.SetBorderSize(0)
legend2.SetTextFont(42)
legend2.SetTextSize(0.04)
legend2.Clear();
h_dummy=ROOT.TH1I("h_dummy","hdummy",1,0,1);
h_dummy.SetFillColor(ROOT.kBlack)
h_dummy.SetLineColor(ROOT.kBlack)
legend2.AddEntry(h_dummy,"stat+syst","F")
h_dummy_2=ROOT.TH1I("h_dummy_2","hdummy",1,0,1);
h_dummy_2.SetFillColor(ROOT.kBlue+1)
h_dummy_2.SetLineColor(ROOT.kBlue+1)
legend2.AddEntry(h_dummy_2,"stat", "F")
h_dummy_3=ROOT.TH1I("h_dummy_3","hdummy",1,0,1);
h_dummy_3.SetFillColor(ROOT.kGreen -8)
h_dummy_3.SetLineColor(ROOT.kGreen -8)
legend2.AddEntry(h_dummy_3,"syst", "F")

# ATLAS label, sqrt(s), lumi
pt = TPaveText(0.228354, 0.821772, 0.348735, 0.889283, "brNDC");
pt.SetBorderSize(0);
pt.SetFillColor(0);
pt.SetTextSize(0.05);
pt.SetTextFont(72);
pt.AddText("ATLAS");

pt2 = TPaveText(0.34384, 0.820717, 0.464183, 0.890338, "brNDC");
pt2.SetBorderSize(0);
pt2.SetFillColor(0);
pt2.SetTextSize(0.05);
pt2.SetTextFont(42);
pt2.AddText("Internal");

pt3 = TPaveText(0.56447, 0.820717, 0.683381, 0.890338, "brNDC");
pt3.SetBorderSize(0);
pt3.SetFillColor(0);
pt3.SetTextSize(0.05);
pt3.SetTextFont(42);
pt3.AddText("#sqrt{s} = 13 TeV");

pt4 = TPaveText(0.570201, 0.704641, 0.689112, 0.829114, "brNDC");
pt4.SetBorderSize(0);
pt4.SetFillColor(0);
pt4.SetTextSize(0.04);
pt4.SetTextFont(42);

pt5 = TPaveText(0.501433, 0.71308, 0.620344, 0.820675, "brNDC");
pt5.SetBorderSize(0);
pt5.SetFillColor(0);
pt5.SetTextSize(0.04);
pt5.SetTextFont(42);


nice_colours=[ROOT.kRed+1, ROOT.kBlue+1, ROOT.kOrange-1, ROOT.kGray+1, ROOT.kCyan+1] #(kRed + 1), (kOrange - 1), (kBlue + 1), (kGray + 1), (kGreen - 8), (kCyan + 1), (kBlack)
for taggerName in taggerNames:
    for WPName in WPNames:
        # if taggerName=="MV2c10" and WPName=="HybBEff":
        #     continue
        output_dir= output_file.mkdir(taggerName+ "_" +WPName);
        #input_file_finalSystPlots_name="/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/r21-2-29_18-07-03_mc16a/plots-ttbar-PhPy8//FitPlots_3SB/FinalFitPlots_r21_data1516_" +taggerName+ "_" +WPName+"_emu_OS_J2.root"
        #input_file_finalSystPlots_name="~/www/b-jet-eff-ttbarPDF/180711_r21_data1516_anaTop_21_2_29_v1_4_mc16a/"+fit_folder+"/FitPlots_r21_"+ChannelName+"_" +taggerName+ "_" +WPName+"/FinalFitPlots_r21_data1516_" +taggerName+ "_" +WPName+"_"+ChannelName+"_"+fitConfigName+".root"
        input_file_finalSystPlots_name="FinalFitPlots_r21_d1516_" +taggerName+ "_" +WPName+"_emu_OS_J2.root"
        samples_to_compare_with={
            "data151617 (80.5 fb^{-1})" : "/nfs/dust/atlas/user/schmoecj/b-jet-calib-syst/rl21-2-53_25-01-19_151617_merged/plots-ttbar-PhPy8//FitPlots_3SB/FinalFitPlots_r21_d151617_" +taggerName+ "_" +WPName+"_emu_OS_J2.root",
        }
        input_file_finalSystPlots=ROOT.TFile(input_file_finalSystPlots_name,"read")
        results_dir= input_file_finalSystPlots.Get("results_comulative")
        results_dir.cd()
        hists=[]
        keyList=results_dir.GetListOfKeys ()
        for i in xrange(1, keyList.GetSize()):
            #print i,keyList.At(i).GetName()
            obj=results_dir.Get(keyList.At(i).GetName())
            className=obj.ClassName()
            oname=obj.GetName()
            if className== "TCanvas":
                if "c_sf_e_b_" in oname and oname.find("_combined_errors")>0:
                    wp=oname.replace("c_sf_e_b_","").replace("_combined_errors","")
                    canvas=obj
                    canvas.cd()
                    output_dir.cd()
                    canvas.Draw()
                    legend.Clear()
                    print "adding on Canvas:",oname, "wp: ",wp
                    i=0
                    for compare_name in samples_to_compare_with:
                        print "adding",compare_name,":",samples_to_compare_with[compare_name]
                        c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                        print "loaded file"
                        output_dir.cd()
                        if not c_file:
                            print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                        c_dir=c_file.Get("results_comulative")
                    
                        h_stat=c_dir.Get("sf_b_"+wp+"_stat_Error_rel").Clone("r_"+compare_name+"_sf_b_"+wp+"_stat_Error_rel")
                        h_syst=c_dir.Get("sf_b_"+wp+"_syst_Error_combined_rel").Clone("r_"+compare_name+"_sf_b_"+wp+"_syst_Error_combined_rel")
                        h_comb=c_dir.Get("sf_b_"+wp+"_stat_plus_syst_Error_rel").Clone("r_"+compare_name+"_sf_b_"+wp+"_stat_plus_syst_Error_rel")
                        output_dir.cd()
                        h_stat.SetDirectory(0)
                        hists.append(h_stat)
                        h_syst.SetDirectory(0)
                        hists.append(h_syst)
                        h_comb.SetDirectory(0)
                        hists.append(h_comb)
                        print "loaded hist"
                        h_stat.SetLineColor(ROOT.kBlue+1)
                        h_stat.SetMarkerSize(0)
                        h_stat.SetMarkerStyle(0)
                        h_syst.SetLineColor(ROOT.kGreen - 8)
                        h_syst.SetMarkerColor(ROOT.kGreen - 8)
                        h_syst.SetMarkerStyle(0)
                        h_stat.SetLineStyle(2+i)
                        h_syst.SetLineStyle(2+i)
                        h_comb.SetLineStyle(2+i)
                        h_comb.SetMarkerStyle(0)
                        output_dir.cd()
                        h_stat.Write()
                        h_syst.Write()
                        h_comb.Write()
                        canvas.cd()
                        h_stat.Draw("LPESAME")
                        h_syst.Draw("LPESAME")
                        h_comb.Draw("LPESAME")
                        h_comp_orig=results_dir.Get("sf_b_"+wp+"_stat_plus_syst_Error_rel").Clone()
                        legend.AddEntry(h_comp_orig, "data1516 (36.2 fb^{-1})", "lpe")
                        legend.AddEntry(h_comb,compare_name,"l")                        
                        print "added to legend"
                        c_file.Close()
                        print "closed file"
                        i=i+1
                        print  "i=",i
                    output_dir.cd()
                    canvas.cd()
                    #data_h.Draw("LPESAME")
                    legend.Draw()
                    legend2.Draw()
                    output_dir.cd()
                    canvas.Write()

output_file.Close()

print "outputfile closed."
