#include "TH1F.h"
#include "TCanvas.h"
#include "TString.h"
#include "TFile.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TROOT.h"
#include "TPad.h"

void plot_nominal_vs_final(TString fileNom, TString fileFinal,TString legOne="Nominal",TString suffix=""){


  gROOT->LoadMacro("~/CXXLibraries/PlottingStyle.cxx+");
  gROOT->ProcessLine("NormalStyle()");


  TFile* myfileOne= TFile::Open(fileNom,"READ");
  TFile* myfileTwo= TFile::Open(fileFinal,"READ");


  std::vector<TString> histogramsPlot;
  histogramsPlot.push_back("sf_and_beff/e_b_60_Postfit");
  histogramsPlot.push_back("sf_and_beff/e_b_70_Postfit");
  histogramsPlot.push_back("sf_and_beff/e_b_77_Postfit");
  histogramsPlot.push_back("sf_and_beff/e_b_85_Postfit");
  histogramsPlot.push_back("sf_and_beff/sf_b_60_Postfit");
  histogramsPlot.push_back("sf_and_beff/sf_b_70_Postfit");
  histogramsPlot.push_back("sf_and_beff/sf_b_77_Postfit");
  histogramsPlot.push_back("sf_and_beff/sf_b_85_Postfit");

  std::vector<TString> histogramsString;
  histogramsString.push_back("#epsilon = 60 %");
  histogramsString.push_back("#epsilon = 70 %");
  histogramsString.push_back("#epsilon = 77 %");
  histogramsString.push_back("#epsilon = 85 %");
  histogramsString.push_back("#epsilon = 60 %");
  histogramsString.push_back("#epsilon = 70 %");
  histogramsString.push_back("#epsilon = 77 %");
  histogramsString.push_back("#epsilon = 85 %");

  TLatex latex;
  latex.SetTextFont(42);
  latex.SetTextSize(0.04);
  TH1F* histoOne = nullptr; 
  TCanvas* histoTwo=nullptr;

  for (unsigned int i = 0 ;  i < histogramsPlot.size() ; ++i){
    TString histogram = histogramsPlot[i];
    std::cout << "Considering plot : " <<  histogram << std::endl;
    histoOne = (TH1F*) myfileOne->Get(histogram);
    TString canvasName= histogram.ReplaceAll("sf_and_beff/","c_").ReplaceAll("Postfit","nominal");

    if(canvasName.Contains("sf"))
      canvasName.ReplaceAll("sf_b","e_b_sf");
    std::cout << canvasName << std::endl;
    histoTwo = (TCanvas*)myfileTwo->Get(canvasName);

    
    histoOne->SetLineColor(kRed);
    histoOne->SetMarkerColor(kRed);
    histoOne->GetXaxis()->SetTitle("p^{b}_{T} [GeV]");
    //    histoTwo->GetXaxis()->SetTitle("p^{b}_{T} [GeV]");

    TLegend* legend=new TLegend(0.65,0.25,0.85,0.35);
    legend->SetTextFont(42);
    legend->SetTextSize(0.05);
    //    legend->SetHeader("#bf{#it{ATLAS}} Internal");
    legend->AddEntry(histoOne,legOne,"lp");
    //    legend->AddEntry(histoTwo,legTwo,"lp");
    //    legend->AddEntry(histoTwo,"Light-jet calibration applied","lp");
    histoTwo->cd();
    gPad->SetLogx(1);
    if(histogram.Contains("sf_b")){
      histoOne->GetYaxis()->SetRangeUser(0.7,1.2);
    }
    histoTwo->cd();
    histoOne->Draw("SAME");
    legend->Draw("SAME");
    // TString line;
    // if(fileOne.Contains("d1516"))
    //   line="data 2015+2016, mc16a ";
    // else if(fileOne.Contains("d17"))
    //   line="data 2017, mc16d ";
    // else if(fileOne.Contains("d18"))
    //   line="data 2018, mc16e ";
    // else
    //   line="Data Run II ";
    // if(fileOne.Contains("MV2c10"))
    //   line+=", MV2c10 tagger, ";
    // else if(fileOne.Contains("DL1"))
    //   line+=", DL1 tagger, ";
    // else
    //   line+=", Unknown tagger, ";

    // line+=histogramsString[i];
    // latex.DrawLatexNDC(0.3,0.65,line);
    histoTwo->SaveAs("data1516vs18/"+histogram+suffix+".pdf","RECREATE");
    histoTwo->SaveAs("data1516vs18/"+histogram+suffix+".png","RECREATE");


  }
    myfileTwo->Close();
}
