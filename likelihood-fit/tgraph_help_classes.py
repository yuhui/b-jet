import os
import sys
import ROOT
import math
import array

class arrays_of_TH1:
    def  __init__(self, hist,bin_size=0.017,shift=0.0):
        if hist ==None:
            print "ups"
        else:
            print "Constructed arrays_of_TH1 from hist:", hist.GetName()
        self._hist=hist
        l_x_nom=[]
        l_x_down=[]
        l_x_up=[]
        l_y_nom=[]
        l_y_up=[]
        l_y_down=[]
        l_zero=[]
        N_bins=hist.GetNbinsX()
        for i_bin in xrange(1,N_bins+2):
            x_center=hist.GetXaxis().GetBinCenter(i_bin)
            if not shift==0:                
                x_center=10**(math.log10(x_center)+shift)
            y_center=hist.GetBinContent(i_bin)
            y_unc=hist.GetBinError(i_bin)
            l_x_nom.append(x_center)
            if bin_size==0:
                l_x_down.append(0)
                l_x_up.append(0)
            else:
                l_x_down.append(-((10**(math.log10(x_center)-bin_size))-x_center))
                l_x_up.append((10**(math.log10(x_center)+bin_size))-x_center)
                #print x_center, math.log10(x_center),(math.log10(x_center)-bin_size),(10**(math.log10(x_center)-bin_size)),(10**(math.log10(x_center)-bin_size))-x_center
            l_y_nom.append(y_center)
            l_y_up.append(y_unc)
            l_y_down.append(y_unc)
        self.a_x_nom=array.array("d", l_x_nom)
        self.a_y_nom=array.array("d", l_y_nom)
        self.a_x_down=array.array("d", l_x_down)
        self.a_x_up=array.array("d", l_x_up)
        self.a_y_up=array.array("d", l_y_up)
        self.a_y_down=array.array("d", l_y_down)
        self.N_bins=N_bins



