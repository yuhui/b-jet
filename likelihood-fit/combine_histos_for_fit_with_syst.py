import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../FinalSelection/')

from options_file import *


#options.addJERUncertainties(final_fit=True)
dataName=options.data_name
outdir=options.output_dir


#do_bootstrap_global=True
do_bootstrap_global=False



# Usage:
#   ./Combine_histograms_for_fit [OPTION...]
#
#       --ttbar_sf file         Nominal ttbar file for sf computation
#       --ttbar_file file       ttbar MC file used for this variation
#       --single_top_file file  single_top MC file used for this variation
#       --diboson_file file     diboson MC file used for this variation
#       --Zjets_file file       Zjets MC file used for this variation
#       --Wjets_file file       Wjets MC file used for this variation
#       --output_file file      output_file name
#       --data_file file        data_file
#       --do_bootstrap          combine with bootstrap weights
#   -h, --help                  Print help


def combine_sample_files(data_file,ttbar_sf,ttbar_file,single_top_file,diboson_file,Zjets_file,Wjets_file,output_file,do_bootstrap=False,do_bootstrap_ttbar_only=False,save_histos_bin_mean=False):
    print("combination for outputfile:",  output_file)
    additional_options = []
    if do_bootstrap:
        if do_bootstrap_ttbar_only:
            additional_options.append("--do_bootstrap_ttbar_only")
        else:
            additional_options.append("--do_bootstrap")
    if save_histos_bin_mean:
        print( "go save_histos_bin_mean")
        additional_options.append("--save_bin_mean")
    if do_bootstrap:
        command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options )
        print( "command",command)
    command=" ".join(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options )
    print( "command",command)
    p=subprocess.Popen(["./build/Combine_histograms_for_fit", "--data_file", data_file, "--ttbar_sf", ttbar_sf, "--ttbar_file", ttbar_file, "--single_top_file", single_top_file, "--diboson_file", diboson_file, "--Zjets_file", Zjets_file, "--Wjets_file", Wjets_file, "--output_file", output_file] + additional_options) #,stdout=log
    p.wait()




if not os.path.exists(options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"):
    os.makedirs(options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/")
#data_name= outdir+dataName+"/"+dataName+"_data_combination.root"
data_name= outdir+dataName+"/"+dataName+"_nominal_combination.root"
ttb_sf_name= outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"



num_of_cores_to_use=1
tp = ThreadPool(num_of_cores_to_use)

syst="nominal"
ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst+".root"

ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root"
singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst+"_combination.root"
ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root"
Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root"
Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root"

print("FLAG #1")
tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,do_bootstrap_global,True,True))

print("FLAG #2")
# #
#combine for all tree and inner systemtaics:
syst_to_run_over=[]
syst_to_run_over+=options.inner_systematics_in_nominal_tree[:]+options.tree_systematics

if options.fake_estimation:
    syst_to_run_over.append(options.fake_estimation)

for syst_struc in syst_to_run_over:

    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_struc.systematic_name+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst_struc.systematic_name+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst_struc.systematic_name+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst_struc.systematic_name+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst_struc.systematic_name+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst_struc.systematic_name+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(syst_struc.do_bootstrap and do_bootstrap_global)))


# #combination for ttbar_systemtics:
ttbar_systematics = (
    options.ttbar_syst_samples[:] +
    options.ttbar_rad_samples +
    options.ttbar_fsr_samples
) 
for ttb_syst_sample in ttbar_systematics:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+ttb_syst_sample.systematic_name+".root"
    ttb_sample=outdir+ttb_syst_sample.name+"/"+ttb_syst_sample.name+"_"+ttb_syst_sample.systematic+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(do_bootstrap_global and ttb_syst_sample.boot_strap_available)))

# # #combination for ttbar pdf systemtics::
for syst_ttbar in options.ttbar_pdf_systematics:
    #ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_ttbar.systematic_name+".root"
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_ttbar.systematic_command+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst_ttbar.systematic_command+"_combination.root"
    #singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst_ttbar.systematic_command+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name, (syst_ttbar.do_bootstrap and do_bootstrap_global)))

## combiation for ttbar qcd systematics
for syst_ttbar in options.ttbar_qcd_systematics:
    #ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_ttbar.systematic_name+".root"
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_ttbar.systematic_command+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst_ttbar.systematic_command+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name, (syst_ttbar.do_bootstrap and do_bootstrap_global)))


# #combination for singletop_systemtics:
singletop_systematics = (
    options.singletop_syst_samples[:] +
    options.singletop_rad_samples +
    options.singletop_fsr_samples
) 

for singletop_syst_sample in singletop_systematics:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+singletop_syst_sample.systematic_name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+singletop_syst_sample.name+"/"+singletop_syst_sample.name+"_"+singletop_syst_sample.systematic+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(do_bootstrap_global and singletop_syst_sample.boot_strap_available)))

# # #combination for singletop pdf systemtics::
for syst_singletop in []: #options.singletop_pdf_systematics: PDF variations are correlated between ttbar and single top
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_singletop.systematic_name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst_singletop.systematic_command+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name, (syst_singletop.do_bootstrap and do_bootstrap_global)))


# # #combination for singletop qcd systemtics::
for syst_singletop in options.singletop_qcd_systematics:
    #ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_singletop.systematic_name+".root"
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_singletop.systematic_command+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst_singletop.systematic_command+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name, (syst_singletop.do_bootstrap and do_bootstrap_global)))


comment = ''
# *********************************************************************************8 
# # #combination for ZJets_syst_samples::
for Zjets_sample in options.ZJets_syst_samples:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+Zjets_sample.systematic_name+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+Zjets_sample.name+"/"+Zjets_sample.name+"_"+Zjets_sample.systematic+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(Zjets_sample.do_bootstrap and do_bootstrap_global)))


for weight_syst in options.ZJets_weight_mc_systematics:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+weight_syst.systematic_name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name + "_" + weight_syst.systematic_command+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(weight_syst.do_bootstrap and do_bootstrap_global)))

## Combination for QCD systematics
for weight_syst in options.ZJets_qcd_systematics:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+weight_syst.systematic_name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name + "_" + weight_syst.systematic_command+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+"nominal"+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(weight_syst.do_bootstrap and do_bootstrap_global)))

# # #combination for syst_samples_Diboson::
for syst_sample in options.Diboson_syst_samples:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_sample.systematic_name+".root"

    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+syst_sample.name+"/"+syst_sample.name+"_"+syst_sample.systematic+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(syst_sample.do_bootstrap and do_bootstrap_global)))

for weight_syst in options.Diboson_weight_mc_systematics:
    ouputfile_name=options.plot_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+weight_syst.systematic_name+".root"
    ttb_sample=outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+"nominal"+"_combination.root"
    singleTop_sample=outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+"nominal"+"_combination.root"
    ZJets_sample=outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+"nominal"+"_combination.root"
    Diboson_sample=outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+weight_syst.systematic_command+"_combination.root"
    Wjets_sample=outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+"nominal"+"_combination.root"

    tp.apply_async(combine_sample_files, (data_name,ttb_sf_name,ttb_sample,singleTop_sample,Diboson_sample,ZJets_sample,Wjets_sample,ouputfile_name,(weight_syst.do_bootstrap and do_bootstrap_global)))
# '''

print("FLAG #3")
tp.close()
print("FLAG #4")
tp.join()
print("FLAG #5")
print("Ende gut alles gut! ")
