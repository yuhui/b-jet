wps=( FixedCutBEff ) #HybBEff )
taggers=( MV2c10 DL1 ) # MV2rmu DL1 DL1r DL1rmu )


for taggerName in ${taggers[@]};
do
    for WPName in ${wps[@]};
    do
	
	# Your Nominal analysis Sample
	fileNom="/home/jhall/working_dir/bjets_ttbardilepton_PDF/likelihood-fit/build/FitResults/FitPlots_3SB/FitPlot_r21_d18_"$taggerName"_"$WPName"_emu_OS_J2_nominal.root"
	# fileNom="/home/jhall/working_dir/bjets_ttbardilepton_PDF/likelihood-fit/build/FitResults/FitPlots_3SB/FitPlot_r21_d18_"$taggerName"_"$WPName"_emu_OS_J2_nominal.root"
	
	# The comparison sample
	fileFinal="/home/jhall/working_dir/bjets_ttbardilepton_PDF/likelihood-fit/EMTopoSF/FinalFitPlots_r21_data1516_"$taggerName"_"$WPName"_emu_OS_J2.root"
	# fileFinal="/atlas/jhall/bjet_cali/finalSelection/rel_21.2.56_mc16e/plots-21.2.56_mc16e/FitPlots_3SB/FinalFitPlots_r21_data18_"$taggerName"_"$WPName"_emu_OS_J2.root"
	
	root -l -b -q plot_nominal_vs_final.cxx'("'$fileNom'","'$fileFinal'","data18","_'$taggerName'_'$WPName'")'
    done
done