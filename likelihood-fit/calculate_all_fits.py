import os
import sys
import subprocess
import argparse
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../FinalSelection/')

from options_file import *
# options.addJERUncertainties(final_fit=False) ## The FullJER uncertainties need to be defined as final 


  # ./2jet_pt_fit [OPTION...]
  #
  # -i, --input input_file     Input
  # -o, --output base_name     Final Outputfile_name will be added together:
  #                            base_name _ dataName _ rlTag _ channelName _
  #                            taggerName _ systName _ fitConfig (default: Workspace)
  # -r, --rlTag rlTag          rlTag (default: r21)
  # -d, --dataName dataName    dataName (default: d1516)
  # -t, --tagger taggerName    taggerName (default: MV2c10)
  # -w, --workingPoint name    Name of the Working point: FixedCutBEff, HybBEff
  #                            (default: FixedCutBEff)
  # -s, --syst systName        name of systematics (default: nominal)
  # -c, --channel channelName  name of channel to fit (default: emu_OS_J2)
  # -h, --help                 Print help
  #     --closure_test         take mc as data
  #     --varry_fs             varry the f_xx fractions

def work(input_file,work_dir,rlTag,syst,channel,tagger,workingPointName,additional_options=[]):
    out_name= workspace_dir+ "/Workspace_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"+ syst
    ##print( "got additional option:", additional_options
    print( "Calculating fit for Systematic: ", syst + "  " + out_name)
    s_additional_options=''
    for s_additional_options_single in additional_options:
        s_additional_options=s_additional_options+"_"+s_additional_options_single
    s_additional_options=s_additional_options.replace("-","").replace("+","")
    print( s_additional_options)
    log=open(out_name+s_additional_options+".log","w")
    if args.combined_fit:
        print( " ".join(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options))
        p=subprocess.Popen(["./build/2jet_pt_combined_fit","-i",input_file,"-o", work_dir, "-r", rlTag,"-d", dataName, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log) #,stdout=log "--allow_neg_sf_xx"
    else:
        p=subprocess.Popen(["./build/2jet_pt_fit", "-i",input_file,"-o", work_dir, "-r", rlTag, "-d", dataName, "-s",syst,"-c", channel,"-t",tagger,"-w",workingPointName]+additional_options,stdout=log)
    p.wait()

def mc_stat_calc(nominal_input_file,syst_input_file,input_dir):
    if syst_input_file=="":
        print( "calculating mc stat unc for:", nominal_input_file)
        p=subprocess.Popen(["./build/MC_stat_calc", "-i",input_dir,"-n", nominal_input_file])
    else:
        print( "calculating mc stat unc for:", syst_input_file)
        p=subprocess.Popen(["./build/MC_stat_calc", "-i",input_dir,"-n", nominal_input_file,"-s", syst_input_file])
    p.wait()



parser = argparse.ArgumentParser(
    description='Calculates all fits for a given Tagger, workingpoint and cahnnel.')
parser.add_argument('-c',"--channel",default="emu_OS_J2",
                    help='Channel to run over')
parser.add_argument('-t',"--tagger",default="GN120220509", #MV2c10
                    help='tagger to run over')
parser.add_argument('-w',"--workingPointName",default="FixedCutBEff",
                    help='workingPointName to run over')
parser.add_argument('-r',"--rlTag",default="r22",
                    help='releaseTag')
parser.add_argument('-m',"--MC_label",default="mc20a",
                    help='MC label e.g. mc20a')
parser.add_argument('-d',"--dataName",default=options.data_name,
                    help='dataName')
parser.add_argument('--rrF', action='store_true',
                    help='rerun all the Fits (default: false)')
parser.add_argument('--combined_fit',action='store_true',
                    help='run with combined fit method.')
parser.add_argument('--boot_strap',action='store_true',
                    help='run with mc stat systematics takes really long!.')
parser.add_argument("--plot_dir",default=options.plot_dir,
                    help='use in order to change working dir, usefull for combination of different years.')
parser.add_argument("--run_pack",default=0,
                    help='use to run packs of 100 fits on condor. option -1 will help you in finding the number of needed blocks')

num_of_cores_to_use = 1#8 # None  # set to the number of workers you want (it defaults to the cpu count of your machine)
args = parser.parse_args()
channel=args.channel
tagger=args.tagger
workingPointName=args.workingPointName
rlTag=args.rlTag
MC_label=args.MC_label
dataName=args.dataName
work_dir = args.plot_dir
if args.rrF:
    run_fits=range(1,1000)
if args.run_pack!=0:
    run_fits=range((int(args.run_pack)-1)*100,( int(args.run_pack))*100)

    
# syst_samples= (
#     options.ttbar_syst_samples[:] +
#     #options.ttbar_rad_samples +
#     #options.ttbar_fsr_samples +
#     # options.singletop_syst_samples +
#     # #options.singletop_rad_samples +
#     # #options.singletop_fsr_samples +
#     options.ZJets_syst_samples +
#     options.Diboson_syst_samples 
# )
syst_samples= [
    #options.ttbar_syst_samples[:] +
    #options.ttbar_rad_samples +
    #options.ttbar_fsr_samples +
    #options.singletop_syst_samples +
    #options.singletop_rad_samples +
    #options.singletop_fsr_samples +
    #options.ZJets_syst_samples #+
    #options.Diboson_syst_samples 
]
syst_samples.extend(options.ttbar_syst_samples[:])
syst_samples.extend(options.ttbar_rad_samples)
syst_samples.extend(options.ttbar_fsr_samples)
syst_samples.extend(options.singletop_syst_samples)
syst_samples.extend(options.singletop_rad_samples)
syst_samples.extend(options.singletop_fsr_samples)

systematics = [
    #options.inner_systematics_in_nominal_tree[:] +
    #options.tree_systematics + 
    #options.ttbar_pdf_systematics[:] + 
    #options.singletop_pdf_systematics +
    #options.ttbar_qcd_systematics[:] + 
    #options.singletop_qcd_systematics +
    #options.ZJets_weight_mc_systematics +
    #options.ZJets_qcd_systematics +
    #options.Diboson_weight_mc_systematics
]
systematics.extend(options.inner_systematics_in_nominal_tree[:])
systematics.extend(options.tree_systematics)

systematics.extend(options.ttbar_pdf_systematics[:]) # for debug
systematics.extend(options.ttbar_qcd_systematics[:])
systematics.extend(options.singletop_qcd_systematics[:])

#syst_samples = ()
#systematics = ()

if options.fake_estimation:
    systematics.append(options.fake_estimation)
    print( "found fake estimation.")

workspace_dir = work_dir+"/Workspaces"
fitplots_dir = work_dir+"/FitPlots"
fit_config='fconst'
s_data_lumi="{:.1f}".format(options.data_lumi/1000) 
CME=options.data_CME
options_for_all_fits=[]
if args.combined_fit:
    #options_for_all_fits=options_for_all_fits+["-l", s_data_lumi,"-m", "mc16a"]
    options_for_all_fits=options_for_all_fits+["-l", s_data_lumi, "-m", MC_label, "--allow_neg_sf_xx", "-e", CME ]
    workspace_dir=workspace_dir+"_3SB"
    fitplots_dir=fitplots_dir+"_3SB"
    fit_config=''
if not os.path.exists(workspace_dir):
    os.makedirs(workspace_dir)
if not os.path.exists(fitplots_dir):
    os.makedirs(fitplots_dir)
job_n=0
#main part of the script. Otherwise we can use it to calculate a single bootstrap untcertainty. Usefull to have shorter jobs. 
if True:
    if args.rrF:
        tp = ThreadPool(num_of_cores_to_use)
        # the nominal fit:
        print( "Nominal Fit: ")

        syst='nominal'
        input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"
        print( "input file:", input_file)
        job_n=job_n+1
        if job_n in run_fits:
            tp.apply_async(work, (input_file,work_dir,rlTag,"nominal",channel,tagger,workingPointName, options_for_all_fits[:] + ["--use_Minos"]))
            #tp.apply_async(work, (input_file,work_dir,rlTag,"nominal",channel,tagger,workingPointName, options_for_all_fits[:] ))
        job_n=job_n+1
        #a nominal version without Minos for the bootstraps
        if job_n in run_fits:        
            tp.apply_async(work, (input_file,work_dir,rlTag,syst,channel,tagger,workingPointName, options_for_all_fits[:]))    
        if args.boot_strap:
            print( "......Fit for 100 bootstraps...")
            for i_boot_strap_weight in xrange(0,100):
                #print( "Fit for bootstrap: ",i_boot_strap_weight)
                job_n=job_n+1
                if job_n in run_fits:
                    tp.apply_async(work, (input_file,work_dir,rlTag,syst,channel,tagger,workingPointName, options_for_all_fits[:] +["--boot_strap",str(i_boot_strap_weight)]))
        #.systematics the Fits:
        #............cl tests:
        print( "cl-test Fit ")
        input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"
        job_n=job_n+1
        if job_n in run_fits: 
            tp.apply_async(work, (input_file,work_dir,rlTag,"clTestMoMc",channel,tagger,workingPointName, options_for_all_fits[:] +["--closure_test"]))
        print( "cl-test Fit n_stat")
        input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"
        job_n=job_n+1
        if job_n in run_fits: 
            tp.apply_async(work, (input_file,work_dir,rlTag,"clTestMoMc_nstat",channel,tagger,workingPointName, options_for_all_fits[:] + ["--closure_test_seed","-2"," --closure_test"] ))
        #............detector systematics, pdf systematics... systematics:
        for syst in systematics:
            
            input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst.systematic_command+".root"
            job_n=job_n+1
            print( "job", job_n,"Fit for syst: ",syst.systematic_command)
            if job_n in run_fits: 
                tp.apply_async(work, (input_file,work_dir,rlTag,syst.systematic_command,channel,tagger,workingPointName,options_for_all_fits[:] ))
            if args.boot_strap and syst.do_bootstrap:
                print( "......Fit for 100 bootstraps...")
                for i_boot_strap_weight in xrange(0,100):
                    #print( "Fit for bootstrap: ",i_boot_strap_weight
                    job_n=job_n+1
                    if job_n in run_fits: 
                        tp.apply_async(work, (input_file,work_dir,rlTag, syst.systematic_command,channel,tagger,workingPointName, options_for_all_fits[:] + ["--boot_strap",str(i_boot_strap_weight)]))

        print( "Fit for syst: ", syst)
        ##input_file="/project/atlas/users/vfabiani/calib_code_1803/bjets_ttbardilepton_PDF/TTbar-b-calib-final-selection-r21/r21-2-53_180319_mc16a-e/plots-ttbar-PhPy8_data1518_2805_reweight/combination_sys_FTAG2_ttbar_PhPy8_nominal/combination_for_fit_nominal.root"
        #input_file="/project/atlas/users/vfabiani/calib_code_1803/bjets_ttbardilepton_PDF/TTbar-b-calib-final-selection-r21/r21-2-53_180319_mc16d/plots-ttbar-PhPy8_data17_2905_reweight/combination_sys_FTAG2_ttbar_PhPy8_nominal/combination_for_fit_nominal.root"
        #tp.apply_async(work, (input_file,work_dir,rlTag,"VR_trkjet_pt_mismodeling_B",channel,tagger,workingPointName,))
                        
        #............simulate misstaglight up:
        print( "Simulate misstag light: ")
        input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"
        job_n=job_n+1
        if job_n in run_fits: 
            tp.apply_async(work, (input_file, work_dir, rlTag,"misstagLight_up", channel, tagger, workingPointName, options_for_all_fits[:] + ["--pl_up",]))
        #............systematic samples:
        for syst_sample in syst_samples:
            input_file=work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+syst_sample.systematic_name+".root"
            job_n=job_n+1
            print( "job", job_n,"Fit for syst: ",syst_sample.systematic_name)
            if job_n in run_fits: 
                tp.apply_async(work, (input_file, work_dir, rlTag, syst_sample.systematic_name, channel, tagger, workingPointName,options_for_all_fits[:] ))
            if args.boot_strap and syst_sample.boot_strap_available:
                print( "......Fit for 100 bootstraps...")
                for i_boot_strap_weight in xrange(0,100):
                    #print( "Fit for bootstrap: ",i_boot_strap_weight
                    job_n=job_n+1
                    if job_n in run_fits: 
                        tp.apply_async(work, (input_file,work_dir,rlTag, syst_sample.systematic_name,channel,tagger,workingPointName, options_for_all_fits[:] + ["--boot_strap",str(i_boot_strap_weight)]))
        
        tp.close()
        tp.join()
    print( "total number of jobs: ", job_n)
    n_jobs_needed=(job_n/100)
    if job_n%100>0:
        n_jobs_needed=n_jobs_needed+1
    print( "suggested number of blocks of 100 for condor: ",  n_jobs_needed)
    if args.boot_strap and args.run_pack==0:
        print( "lets calculate mc stat unc via MC_stat_calc")
        file_name_start="FitPlot_"+ rlTag +"_" + dataName + "_" + tagger + "_" + workingPointName +"_"+ channel + "_"
        if fit_config != "":
            file_name_start=file_name_start+fit_config+"_"
        tp2 = ThreadPool(1)
        print( "nominal:")
        tp2.apply_async( mc_stat_calc,(file_name_start+"nominal","",fitplots_dir))
        for syst in systematics:
            if syst.do_bootstrap:
                print( "syst sample:: ",syst.systematic_command)
                tp2.apply_async( mc_stat_calc,(file_name_start+"nominal",file_name_start+syst.systematic_command,fitplots_dir))
        for syst_sample in syst_samples:
            if syst_sample.do_bootstrap:
                print( "syst sample:: ",syst_sample.systematic_name)
                if syst_sample.name=='ttbar_PowHW7' or syst_sample.name=='ttbar_aMcPy8' or syst_sample.name=='ttbar_PhPy8_hdamp3mtop':
                    tp2.apply_async( mc_stat_calc,(file_name_start+'ttbar_PhPy8_AF2',file_name_start+syst_sample.systematic_name,fitplots_dir))
                elif syst_sample.name=='Singletop_PowHW7' or syst_sample.name=='Singletop_aMcPy8' :
                    tp2.apply_async( mc_stat_calc,(file_name_start+'Singletop_PowPy8_AF2',file_name_start+syst_sample.systematic_name,fitplots_dir))    
                else:
                    tp2.apply_async( mc_stat_calc,(file_name_start+"nominal",file_name_start+syst_sample.systematic_name,fitplots_dir))
        tp2.close()
        tp2.join()
    if args.run_pack==0:
        syst_for_final_plot=[]  #will be a list of all systematics wich shell be included in the final plots.
        syst_for_final_plot.append("misstagLight_up")
        #syst_for_final_plot.append("clTestMoMc_nstat")
        #syst_for_final_plot.append("VR_trkjet_pt_mismodeling_B")
        for syst in systematics:
            syst_for_final_plot.append(syst.systematic_command)
        for syst_sample in syst_samples:
            syst_for_final_plot.append(syst_sample.systematic_name)
        command=" ".join(["./build/final_syst_plot", "-i", fitplots_dir ,"-r", rlTag, "-d", dataName, "-c", channel ,"-t",tagger,"-w",workingPointName, "-f", fit_config, "-l", s_data_lumi, "-n", work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"]+syst_for_final_plot)
        print( "command",command)
        p=subprocess.Popen(["./build/final_syst_plot", "-i", fitplots_dir ,"-r", rlTag, "-d", dataName, "-c", channel ,"-t",tagger,"-e", CME, "-w",workingPointName, "-f", fit_config, "-l", s_data_lumi, "-n", work_dir+"combination_sys_"+options.ttb_sample.name+"/"+"combination_for_fit_"+"nominal"+".root"]+syst_for_final_plot)
        p.wait()

