#! /usr/bin/python

from ROOT import TFile, THnSparseD, TH1

import os,sys

# Disclaimer aknue: this code is written only for the JER uncertainties when you use something more complex than simpleJER
# The standard is to use the breakdown with at least 13 NP. For this you will not just get the normal JER_NP1_up and down trees base on MC,
# but there are also variations based on pseudo data. What this code does now to get the correct histograms for the fit is the following:
#
# hist_NP1_up = hist_nominal + (hist_NP1_up_MC - hist_NP1_up_PseudoData)
#
# for each up and down variation and all NP that exist. 

# yes I know, hard coding is bad, but I have little time so I will make it more flexible later ;)

# path to the folder where all your input for the fitting part lives
FolderPath = "/lustre/fs22/group/atlas/alopezso/BTagging/Run-3/r24.2.4_June23/plots/combination_sys_ttbar_PhPy8_nominal"

ListJERNP = ["JET_JER_DataVsMC_MC16__1down",
             "JET_JER_DataVsMC_MC16__1up",
             #"JET_JER_earlyRunNoise__1down",
             #"JET_JER_earlyRunNoise__1up",
             "JET_JER_EffectiveNP_10__1down",
             "JET_JER_EffectiveNP_10__1up",
             "JET_JER_EffectiveNP_11__1down",
             "JET_JER_EffectiveNP_11__1up",
             "JET_JER_EffectiveNP_12restTerm__1down",
             "JET_JER_EffectiveNP_12restTerm__1up",
             "JET_JER_EffectiveNP_1__1down",
             "JET_JER_EffectiveNP_1__1up",
             "JET_JER_EffectiveNP_2__1down",
             "JET_JER_EffectiveNP_2__1up",
             "JET_JER_EffectiveNP_3__1down",
             "JET_JER_EffectiveNP_3__1up",
             "JET_JER_EffectiveNP_4__1down",
             "JET_JER_EffectiveNP_4__1up",
             "JET_JER_EffectiveNP_5__1down",
             "JET_JER_EffectiveNP_5__1up",
             "JET_JER_EffectiveNP_6__1down",
             "JET_JER_EffectiveNP_6__1up",
             "JET_JER_EffectiveNP_7__1down",
             "JET_JER_EffectiveNP_7__1up",
             "JET_JER_EffectiveNP_8__1down",
             "JET_JER_EffectiveNP_8__1up",
             "JET_JER_EffectiveNP_9__1down",
             "JET_JER_EffectiveNP_9__1up"
             ]
             #"JET_JER_pixel_inefficiency__1down",
             #"JET_JER_pixel_inefficiency__1up"



ListObjectsData = ["hf4_data_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff",
                   "hf4_data_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff",
                   "hf4_data_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff",
                   "hf4_data_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff", 
                   "hf4_Ncr_data_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff",
                   "hf4_Ncr_data_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff"]

# Alvaro said these ones do not have to be modified?
ListObjectsHFF = ["hff_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_light",
                  "hff_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_light",
                  "hff_MC_ttb_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_ttb_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_ttb_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_ttb_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_light",
                  "hff_MC_ttb_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_ttb_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_ttb_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_ttb_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_light",
                  "hff_MC_ttb_nominal_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_ttb_nominal_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_ttb_nominal_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_ttb_nominal_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_light",
                  "hff_MC_ttb_nominal_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_b",
                  "hff_MC_ttb_nominal_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_light",
                  "hff_MC_ttb_nominal_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_b",
                  "hff_MC_ttb_nominal_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_light"]

# from what I understand, these are the only ones to modify! 
ListObjectsToModify = ["hf4_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_bb",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_bl",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_lb",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_ll",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_bb",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_bl",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_lb",
                       "hf4_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_ll",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_bb",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_bl",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_lb",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_DL1dv01_FixedCutBEff_ll",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_bb",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_bl",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_lb",
                       "hf4_MC_combined_emu_OS_J2_m_lj_cut_hist_for_fit_GN120220509_FixedCutBEff_ll",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_bb",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_bl",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_lb",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_DL1dv01_FixedCutBEff_ll",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_bb",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_bl",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_lb",
                       "hf4_Ncr_MC_combined_emu_OS_J2_hist_for_fit_GN120220509_FixedCutBEff_ll"]


for JERNP in ListJERNP:
    input_nom = FolderPath+"/combination_for_fit_nominal.root"
    input_MC  = FolderPath+"/combination_for_fit_"+JERNP+".root"
    input_PD  = FolderPath+"/combination_for_fit_"+JERNP+"_PseudoData.root"
    output    = FolderPath+"/combination_for_fit_"+JERNP+"_final.root"
    
    file_nom  = TFile(input_nom, "READ")
    file_MC   = TFile(input_MC,  "READ")
    file_PD   = TFile(input_PD,  "READ")
    file_out  = TFile(output,    "RECREATE")

    for obj in ListObjectsToModify:

        obj_nom    = THnSparseD()
        obj_MC     = THnSparseD()
        obj_PD     = THnSparseD()
        obj_interm = THnSparseD()
        
        obj_nom = file_nom.Get(obj)
        obj_MC  = file_MC.Get(obj)
        obj_PD  = file_PD.Get(obj)

        obj_interm = obj_MC.Clone()
        obj_interm.Add(obj_PD, -1.0)
        obj_nom.Add(obj_interm, 1.0)
        
        obj_nom.Write()

    for obj in ListObjectsData:
        obj_nom = file_nom.Get(obj)
        obj_nom.Write()

    for obj in ListObjectsHFF:
        obj_nom = file_nom.Get(obj)
        obj_nom.Write()
        

    file_out.Close()
