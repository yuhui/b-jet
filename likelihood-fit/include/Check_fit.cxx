#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "Check_fit.h"
#include "RooFitResult.h"


int check_fit(TFile *f_fit, std::string file_path){
    if (!f_fit)
    {
  		std::cerr << "Input file: " << file_path << " not found ... Error 1 " << std::endl;
  		return 1;
    }
    RooFitResult *fit_Result = 0;
    fit_Result = (RooFitResult *)f_fit->Get("fitResult");
    if (!fit_Result)
    {
  		std::cerr << "no FitResult in Input file: " << file_path << " ... Error 2 " << std::endl;
  		return 2;
    }
    int fit_res_status =fit_Result->status();
    int fit_cov_qual = fit_Result->covQual();
    if (fit_res_status>0)
    {
        std::cerr << "fit_res_status not 0!: " << fit_res_status << " in "<<file_path << std::endl <<" ... returning " << fit_res_status << std::endl;
	return fit_res_status;
	// return 0;
	// Fit status (minuit)
	// status = 0    : OK
	// status = 1    : Covariance was mad  epos defined
	// status = 2    : Hesse is invalid
	// status = 3    : Edm is above max
	// status = 4    : Reached call limit
	// status = 5    : Any other failure
    }
    else if ( fit_cov_qual!= 3){
      std::cerr << "fit_Cov_qual not 3!: " << fit_cov_qual << " in "<<file_path << std::endl <<" ... returning " << fit_cov_qual+100 << std::endl;
      return fit_cov_qual+100;
    }
    else if (fit_res_status==0)
    {
	std::cout << "Fit result status is 0, well done: " << fit_res_status << std::endl; 
	return 0;
    }
    return 0;
}
