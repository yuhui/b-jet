cmake_minimum_required (VERSION 3.10)

# root librarys!:
# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT REQUIRED COMPONENTS RooFitCore RooFit Minuit) #MathCore RIO Hist Tree Net Core Graf

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})

include_directories(${CMAKE_SOURCE_DIR} ${ROOT_INCLUDE_DIRS})
add_definitions(${ROOT_CXX_FLAGS})

#for the wunderfull options:
add_library(cxxopts INTERFACE)
target_include_directories(cxxopts INTERFACE
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

ROOT_GENERATE_DICTIONARY(G__Workspace_plottingMacro_3SB Workspace_plottingMacro_3SB.h LINKDEF Workspace_plottingMacro_3SBLinkDef.h)
add_library(Workspace_plottingMacro_3SB SHARED Workspace_plottingMacro_3SB.cxx G__Workspace_plottingMacro_3SB.cxx)
target_link_libraries(Workspace_plottingMacro_3SB ${ROOT_LIBRARIES})

ROOT_GENERATE_DICTIONARY(G__Workspace_plottingMacro Workspace_plottingMacro.h LINKDEF Workspace_plottingMacroLinkDef.h)
add_library(Workspace_plottingMacro SHARED Workspace_plottingMacro.cxx G__Workspace_plottingMacro.cxx)
target_link_libraries(Workspace_plottingMacro ${ROOT_LIBRARIES})

ROOT_GENERATE_DICTIONARY(G__Check_fit Check_fit.h LINKDEF Check_fitLinkDef.h)
add_library(Check_fit SHARED Check_fit.cxx G__Check_fit.cxx)
target_link_libraries(Check_fit ${ROOT_LIBRARIES})

ROOT_GENERATE_DICTIONARY(G__SystError_plottingMacro SystError_plottingMacro.h LINKDEF SystError_plottingMacroLinkDef.h)
add_library(SystError_plottingMacro SHARED SystError_plottingMacro.cxx G__SystError_plottingMacro.cxx)
target_link_libraries(SystError_plottingMacro ${ROOT_LIBRARIES} Check_fit)




