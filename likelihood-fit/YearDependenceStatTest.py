import ROOT
from ROOT import TMatrix, gStyle
ROOT.TH1.AddDirectory(ROOT.kFALSE)
import os
import sys
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
sys.path.insert(0, '/eos/user/a/alopezso/FTAGPrerecommendations/')
from options_file import *
import argparse
import numpy as np
import datetime

# Iterator required to change seed (ROOT's crap)
TRandomSeed = datetime.datetime.now()
rdnm_no = ROOT.TRandom(int(TRandomSeed.strftime("%f")))


def GetInputFileArray(InputPath):
	ListOfFiles = os.listdir(InputPath)
	FileArray = []
	for File in ListOfFiles:
		if "Final" in File:
			FileArray.append(InputPath+File)
			print("Appended file: " + File)
	# Convert the previously unknown Arr length into a np array 
	npFileArray = np.array(FileArray)
	del FileArray
	return npFileArray

def GetNomSF(RootFile, WorkingPoint, TakeFromCanvas = False):
	# First get the canvas from file
	print("Getting nominal SF histo ... ")
	TempNomSFArr = []
	if not TakeFromCanvas:
		print("Taking the nominal SF histo directly from the file ... ")
		HistogrameName = "sf_b_" + WorkingPoint + "_Postfit"
		NomSFHisto = RootFile.Get(HistogrameName)
		NomSFHisto.SetDirectory(0)
		TempNomSFArr.append(NomSFHisto)
	else:
		print("Getting the nominal SF histo from the canvas instead ... ")
		CanvasName = "c_e_b_sf_" + WorkingPoint + "_nominal"
		ListOfPrimitivesFromSFCanvas = RootFile.Get(CanvasName).GetListOfPrimitives()
		# Next loop over it and extract histo
		print("Searching list of primitives from canvas ... ")
		FoundHisto = False
		PlotName = "sf_b_" + WorkingPoint + "_Postfit"
		for Primitive in ListOfPrimitivesFromSFCanvas:
			if PlotName in str(Primitive):
				FoundHisto = True
				print("Found histo! " + Primitive.GetName())
				NomSFHisto = Primitive
				NomSFHisto.SetDirectory(0)
				TempNomSFArr.append(NomSFHisto)
			if FoundHisto == True:
				break
	return TempNomSFArr

def GetNPStat(rootFile, workingPoint):
	print("Getting all the stat np histos ... ")
	StatNPFolder = "stats_wp_" + workingPoint
	NoStatNP = 36
	print("Looping over " + str(NoStatNP) + " nuisance parameters ... ")
	StatNPArray = []
	for NP in range(1,NoStatNP+1):
		StatNPHisto = rootFile.Get(StatNPFolder + "/" + "e_b_" + workingPoint + "_stat_np_" + str(NP) + "_Error")
		StatNPHisto.SetDirectory(0)
		StatNPArray.append(StatNPHisto)
	print("Finished adding them to an array .. \n")
	return StatNPArray

def SamplePseudoMeasurements(NPStatDict):
	# Sampling pseudo measurements
	# V. similar to cov matrix code, this time create an altered SF w/ TRandom
	PseudoMeasureDictionary = {}
	for fileKey, fileValue in NPStatDict.items():
		# fileKey is the file all the histos (fileValue) are assosciated to
		# Create cov matrix per tagger per year per WP:
		print("Working with file: " + fileKey)
		NewHistoArr = []
		for WP in WPs:
			histos = GetHistosFromWP(fileValue,WP)
			# Create a new histo with characteristics from the nominal
			new_histo_name = GetHistosFromWP(NomSFDict.get(fileKey),WP).GetName().split("Postfit")[0] + "PseudoMeasurement"
			new_histogram = ROOT.TH1D(new_histo_name, new_histo_name, no_pt_bins, pt_bin_edges)
			for pt_bin in range(1, no_pt_bins+1):
				# Create an alter SF per pt bin from np_stats
				print("Pt bin: " + str(pt_bin))
				# The summation value
				new_histo_value = 0
				for histo in histos:
					np_stat_value = histo.GetBinContent(pt_bin)
#					print("Existing bin content: " + str(histo.GetBinContent(pt_bin)))
					# Generate a new random no centred on np_stat_value
					gauss_const = rdnm_no.Gaus(0,1)
#					print("TRandom'ed value: " + str(gauss_const))
					modified_central_value = gauss_const * np_stat_value
#					print("TRandom'ed value * by the OG np_stat bin content: " + str(modified_central_value))

					# Add the new value calculated into the new histogram bin content
					new_histo_value = new_histo_value + modified_central_value
				# Get the nominal SF from the key of the np_stat key to ensure its consistent
				# We can use the GetHistosFromWP and since theres only one entry pick it out
				nomSF_value = GetHistosFromWP(NomSFDict.get(fileKey),WP).GetBinContent(pt_bin)
				print("Nominal value: " + str(nomSF_value))

				# Add it to the formula
				new_histo_value = new_histo_value + nomSF_value
				print("New varied histogram value: " + str(new_histo_value))

				# Add it to the histogram
				print("Adding the new value to a new histogram for bin: " + str(pt_bin) + " and with value: " + str(new_histo_value))
				new_histogram.SetBinContent(pt_bin, new_histo_value)

				# Finally set the error of the new bin to the error from the nominal measurement
				nomSF_error = GetHistosFromWP(NomSFDict.get(fileKey),WP).GetBinError(pt_bin)
				new_histogram.SetBinError(pt_bin,nomSF_error)
			NewHistoArr.append(new_histogram)
		print("Finished getting all the histograms for these WPs from this file! ... ")
		PseudoMeasureDictionary.update({fileKey:NewHistoArr})
	return PseudoMeasureDictionary

def BuildCovMatrixDict(NomDictionary, NPStatDictionary, Identity = False):
	# You have the option to set the cov matrix to the identity or build from stat nps
	CovMatrixDictionary = {}
	CovMatrixArr = []
	print("Beginning creating cov. matrices for all files ... ")
	for Filekey, Filevalue in NPStatDictionary.items():
		# Create cov matrix per tagger per year per WP:
		print("Working with file: " + Filekey)
		for WP in range(0, len(WPs)):
			# Next part depends on your cov. matrix choice
			if Identity == True:
				Covmatrix = CreateIdentityMatrix()
			else:
				HistoArrToUse = GetHistosFromWP(Filevalue,WPs[WP])
				Covmatrix = CreateCovMatrix(HistoArrToUse)
			CovMatrixArr.append(Covmatrix)
		CovMatrixDictionary.update({Filekey:CovMatrixArr})
		CovMatrixArr = []
	print("Finished getting all cov. matrices for all WPs per file (per tagger)")
	print("\n ------------------------------------------------ \n ")
	return CovMatrixDictionary

def BuildColVector(NominalMeasurementDictionary, PseudoMeasurementDictionary, Year2YearComparison = False):
	ColVectorDict = {}
	ColVectorArr = []
	print(NominalMeasurementDictionary)
	print("Beginning creating delta matrices for all files ... ")
	if Year2YearComparison == False:
		for PseudoKey, PseudoValue in PseudoMeasurementDictionary.items():
			print("Working with file: " + PseudoKey)
			for WP in WPs:
				# Pick out histos based on their WP
				nom_histo = GetHistosFromWP(NominalMeasurementDictionary[PseudoKey],WP)
				pseudo_histo = GetHistosFromWP(PseudoValue,WP)
				DeltaMatrix = CreateColVector(nom_histo,pseudo_histo)
				ColVectorArr.append(DeltaMatrix)
			ColVectorDict.update({PseudoKey:ColVectorArr})
			ColVectorArr = []
	else:
		# First we need to save the file names to use as keys
		ComparisonKeyDict = {}
		ComparisonKeyArr = []
		for tagger in taggers:
			print("Working with " + tagger)
			for Key in range (0, len(NominalMeasurementDictionary.keys())):
				file = NominalMeasurementDictionary.keys()[Key]
				# Need to do the comparisons one tagger at a time
				if tagger in file:
					data_year = file.split("_")[2]
					if len(data_year) < 9:
						# ensures its not a combination so now you can use the Key
						ComparisonKeyArr.append(file)
			ComparisonKeyDict.update({tagger:ComparisonKeyArr})
			ComparisonKeyArr = []
		print("Found all the files for comparison! ... ")
		NewColVectorDict = {}
		for tagger in taggers:
			# Repeat comparison per tagger
			for i in range(0, TotalNumberOfComparisons-1):
				for j in range(0,TotalNumberOfComparisons-1):
					if not ComparisonKeyDict[tagger][i] == ComparisonKeyDict[tagger][j+1]:
						# This if statement avoids comparisons between same values
						# Using i and j+1, you can now pick out the files you need for comp.
						for WP in WPs:
							# This is complicated by get inside the brackets use the tagger key on the file dict to get the file key for the main SF dict
							histo_1yr = GetHistosFromWP(NominalMeasurementDictionary[ComparisonKeyDict[tagger][i]],WP)
							histo_2yr = GetHistosFromWP(NominalMeasurementDictionary[ComparisonKeyDict[tagger][j+1]],WP)
							DeltaMatrix = CreateColVector(histo_1yr,histo_2yr)
							ColVectorArr.append(DeltaMatrix)
						# Need to create a name for the dict based on the comparison
						histo1yr = ComparisonKeyDict[tagger][i].split("_")[2]
						histo2yr = ComparisonKeyDict[tagger][j+1].split("_")[2]
						ComparisonKey = histo1yr+histo2yr
						NewColVectorDict.update({ComparisonKey:ColVectorArr})
						ColVectorArr = []
			ColVectorDict.update({tagger:NewColVectorDict})
			NewColVectorDict = {}
	print("Finished getting all delta matrices for all WPs per file (per tagger)")
	print("\n ------------------------------------------------ \n ")
	return ColVectorDict

def CalculateChi2Min(CovDict, DeltaMatrix, Year2YearComparison = False):
	Chi2MinDictionary = {}
	print("Beginning calculating chi2min for all files ... ")
	if Year2YearComparison == False:
		for CovKey, CovValue in CovDict.items():
			print("Working with file: " + CovKey)
			npChi2MinArr = np.zeros(len(WPs))
			Iter = 0
			for WP in WPs:
				print("and working point: " + WP)
				# Pick out matrices based on their WP
				cov_matrix = ROOT.TMatrix(GetMatrixFromWP(CovValue,WP))
				delta_matrix = ROOT.TMatrix(GetMatrixFromWP(DeltaMatrix[CovKey],WP))
				print("Found both matrices without failing, congrats! ... ")
				print("Now time to calculate chi2 min from the reference ... \n")
				# First calculate the r.h.s since root is s**t
				inverse_cov = cov_matrix.Invert()
				new_col_vec = ROOT.TMatrix(no_pt_bins, 1)
				new_col_vec.Mult(inverse_cov,delta_matrix)
				delta_matrix.T()
				# Repeat like before to create a new 1x1 matrix to store the scalar value from multiplication
				Chi2min_matrix = ROOT.TMatrix(1,1)
				Chi2min_matrix.Mult(delta_matrix,new_col_vec)
				Chi2min_value = Chi2min_matrix[0][0]
				print("Got it! The chi2 min value is:\t\t" + str(Chi2min_value))
				npChi2MinArr[Iter] = Chi2min_value
				Iter = Iter + 1
			Chi2MinDictionary.update({CovKey:npChi2MinArr})
		return Chi2MinDictionary
	else:
		OutputDict = {}
		for TaggerKey, TaggerDict in DeltaMatrix.items():
			print("Working with tagger: " + TaggerKey)
			for Key, Value in TaggerDict.items():
				npChi2MinArr = np.zeros(len(WPs))
				Iter1 = 0
				Iter2 = 0
				for WP in WPs:
					print("and working point: " + WP)
					# Pick out matrices based on their WP
					cov_matrix = ROOT.TMatrix(GetMatrixFromWP(CovMatrixDict[TaggerKey].values()[Iter1],WP))
					delta_matrix = ROOT.TMatrix(GetMatrixFromWP(Value,WP))
					print("Found both matrices without failing, congrats! ... ")
					print("Now time to calculate chi2 min from the reference ... \n")
					# First calculate the r.h.s since root is s**t
					inverse_cov = cov_matrix.Invert()
					new_col_vec = ROOT.TMatrix(no_pt_bins, 1)
					new_col_vec.Mult(inverse_cov,delta_matrix)
					delta_matrix.T()
					# Repeat like before to create a new 1x1 matrix to store the scalar value from multiplication
					Chi2min_matrix = ROOT.TMatrix(1,1)
					Chi2min_matrix.Mult(delta_matrix,new_col_vec)
					Chi2min_value = Chi2min_matrix[0][0]
					print("Got it! The chi2 min value is:\t\t" + str(Chi2min_value))
					npChi2MinArr[Iter2] = Chi2min_value
					Iter2 = Iter2 + 1
				Iter1 = Iter1 +1
				Chi2MinDictionary.update({Key:npChi2MinArr})
			OutputDict.update({TaggerKey:Chi2MinDictionary})
			Chi2MinDictionary = {}
	print("Finished calculating all chi2 min values for all WPs per file (per tagger)")
	print("\n ------------------------------------------------ \n ")
	return OutputDict

def CreateIdentityMatrix():
	CovMatrix = ROOT.TMatrix(no_pt_bins, no_pt_bins)
	for i in range (0, no_pt_bins):
		for j in range (0, no_pt_bins):
                        if i==j:
                                CovMatrix[i][j] = 1.0
                        else:
                                CovMatrix[i][j] = 0.0
	return CovMatrix

def CreateCovMatrix(HistogramArray):
	# Fill a (diag) cov matrix from 36_np_stats
	CovMatrix = ROOT.TMatrix(no_pt_bins, no_pt_bins)
	print("Beginning calculating covariant matrix.")
	for i, j in zip(range (0, no_pt_bins),range (0, no_pt_bins)):
		# Loop over i, j (row, col) together
		CovMatrix[i][j] = GetCijthValue(HistogramArray, i+1, j+1)
	print("Finished calculating covariant matrix. ")

	#print(CovMatrix.Print())

        print("This is your covariance matrix")
        print(CovMatrix.Print())
        
	return CovMatrix

def GetCijthValue(HistoArr, row, col):
	# Here we get the i and jth position in the cov matrix using eq 26 defined in here:
	# https://www-cdf.fnal.gov/physics/statistics/notes/cdf8661_chi2fit_w_corr_syst.pdf
	Cij = 0.0
	for Histo in HistoArr:
		# Using the notation from the paper:
		Smi = Histo.GetBinContent(row)
		Smj = Histo.GetBinContent(col)
		# NB since this is a diag matrix, row and col are the same (therefore Smi and Smj)
		# There is no error as these are stat_nps:
		# print(Histo.GetBinError(row))
		CKij = Smi * Smj
		Cij = Cij + CKij
	return Cij

def CreateColVector(FirstHisto,SecondHisto):
	# Create a col vector from the subtraction of the nom and pseudo
	ColVector = ROOT.TMatrix(no_pt_bins, 1)
	print("Beginning calculating delta matrix.")
	for i in range (0, no_pt_bins):
		ColVector[i][0] = FirstHisto.GetBinContent(i+1) - SecondHisto.GetBinContent(i+1)
	print("Finished calculating delta matrix. ")
	# ColVector.Print()
	# ColVector is our Delta matrix in the paper reference
	return ColVector

def GetHistosFromWP(DictValues, Workingpoint):
	histArr = []
	for Histgram in DictValues:
		if Workingpoint in str(Histgram).split("at")[0]:
			# Only histos corresponding to WP past this point
			histArr.append(Histgram)
	if len(histArr) == 1:
		return histArr[0]
	else:
		return histArr

def GetMatrixFromWP(MatrixDictValues, workingpoint):
	MatrixArr = []
	index_retreiver = 0
	for WP in WPs:
		# Loop over the global working points to gather an index
		if workingpoint == WP:
			return MatrixDictValues[index_retreiver]
		index_retreiver = index_retreiver + 1

def PerformChi2TestBetweenYears(HistogramDictionary):
	# Get the chi2 tests between each years
	print("Performing chi2 tests between years and saving them! ... ")
	NomChi2ValueDict = {}
	for tagger in taggers:
		# Again need to do this per tagger
		print("Tagger: " + tagger)
		NomChi2FileDict = {}
		for FileKey, FileValue in HistogramDictionary.items():
			if tagger in FileKey:
				DataYear = FileKey.split("_")[2]
				if len(DataYear) < 9:
					# The file cannot be combo(data15161718) but still smaller than data1516
					print("Found a file for chi2 test: " + FileKey.split("/")[-1])
					NomChi2FileDict.update({DataYear:FileValue})
		# Now the dates and corresponding SF histos are stored, per tagger, we can compute chi2s
		NomChi2Values = []
		for WP in range(0,len(WPs)):
			for i in range(0,len(NomChi2FileDict)-1):
				for j in range(0,len(NomChi2FileDict)-1):
					if not NomChi2FileDict.keys()[i] == NomChi2FileDict.keys()[j+1]:
						# This if statement avoids chi2 tests between same year
						Chi2Test = NomChi2FileDict.values()[i][WP].Chi2Test(NomChi2FileDict.values()[j+1][WP],"CHI2")
						print("For working point " + str(WPs[WP]) + ":")
						print("\nChi2 test between " + NomChi2FileDict.keys()[i] + " and " + NomChi2FileDict.keys()[j+1] + "\t\t\t\t" + "result: " + str(Chi2Test))
						NomChi2Values.append(Chi2Test)
		NomChi2ValueDict.update({tagger:NomChi2Values})
		print("\n ------------------------------------------------ \n ")
	# Don't forget the values are arrays of the chi2 tests for all working points
	return NomChi2ValueDict

def PerformChi2TestBetweenNomVsPsuedo(NominalDict, PseudoDict):
	# Get the chi2 test value between each a nominal and pseudo measurement
	print("Performing chi2 tests between nominal and pseudo measurements and saving them! ... ")
	NomVsPsuChi2ValueDict = {}
	for tagger in taggers:
		# Again need to do this per tagger
		print("Tagger: " + tagger)
		Chi2FileDict = {}
		for FileKey, FileValue in NominalDict.items():
			if tagger in FileKey:
				DataYear = FileKey.split("_")[2]
				if len(DataYear) < 9:
					# The file cannot be combo(data15161718) but still smaller than data1516
					# Same code as chi2 year but this time save the nominal and pseudo as an array into the dict
					print("Found a file for chi2 test: " + FileKey.split("/")[-1])
					Chi2FileDict.update({DataYear:[FileValue,PseudoDict[FileKey]]})
		# Now the dates and corresponding SF histos are stored, per tagger, we can compute chi2s
		# npChi2Values = np.zeros((1,len(WPs)*len(Chi2FileDict)), dtype = float)
		npChi2Values = np.zeros(len(WPs)*len(Chi2FileDict), dtype = float)
		counter = 0
		for WP in range(0,len(WPs)):
			for year in range(0,len(Chi2FileDict)):
				# 0 and 1 indices pick out nom and psu respectively
				Chi2Test = Chi2FileDict.values()[year][0][WP].Chi2Test(Chi2FileDict.values()[year][1][WP], "CHI2")
				print("Done chi2 for year: " + Chi2FileDict.keys()[year] + " with working point " + str(WPs[WP]) + ".")
				print("Result: " + str(Chi2Test))
				npChi2Values[counter] = Chi2Test
				counter = counter + 1
		NomVsPsuChi2ValueDict.update({tagger:npChi2Values})
		print("\n ------------------------------------------------ \n ")
	# Don't forget the values are arrays of the chi2 tests for all working points
	return NomVsPsuChi2ValueDict

def TaggerToWPDictConv(Dictionary, Tagger, Nominal = False):
	# Can only do this for one tagger (Tagger)
	print("Tagger used is: " + Tagger)
	print("The original values from the test tagger keyed on the dict:")
	print(Dictionary[Tagger])
	print("Therefore the length of the values of the dict is " + str(len(Dictionary[Tagger])))
	OutputDict = {}
	for WP in range(0, len(WPs)):
		print("Getting the values for working point, " + str(WPs[WP]))
		if Nominal == True:
			# If it just a nominal SF dict
			WPValues = np.zeros(TotalNumberOfComparisons*len(WPs))
			for Iterator in range(0, TotalNumberOfComparisons):
				index = WP+(len(WPs)*Iterator)
				WPValues[Iterator] =Dictionary[Tagger][index]
		else:
			WPValues = np.zeros(TotalNumberOfComparisons*NoOfRepeatTestValues)
			for Iterator in range(0, len(WPValues)):
				# Need to pick out the values from each WP building a starting point and then a step function
				index = WP+(len(WPs)*Iterator)
				WPValues[Iterator] =Dictionary[Tagger][index]
		OutputDict.update({WPs[WP]:WPValues})
	return OutputDict

def GetNomANDNPstatHistosAccordingToFile(InitialFileArray):
	# Pick out the files according to their taggers
	NomSFDict = {}
	NPStatDict = {}
	NomSFArr = []
	NPStatArr = []
	for tagger in taggers:
		print("Tagger: " + tagger)
		for file in range(0,len(InitialFileArray)):
			if tagger in InitialFileArray[file]:
				# Begin your analysis here
				print("Working with file: " + InitialFileArray[file].split("/")[-1])
				File = ROOT.TFile(InitialFileArray[file])
				for WP in WPs:
					# Store all histos in arrays
					NomSFArr = NomSFArr + GetNomSF(File, WP)
					NPStatArr = NPStatArr + GetNPStat(File, WP)
				NomSFDict.update({InitialFileArray[file].split("/")[-1]:NomSFArr})
				NPStatDict.update({InitialFileArray[file].split("/")[-1]:NPStatArr})
				
				# Close the file and reset arrays
				File.Close()
				NomSFArr = []
				NPStatArr = []
		print("\n ------------------------------------------------ \n ")
	return NomSFDict, NPStatDict

def CovMatrixDictToColVectorConverter(CovariantMatrixDictionary):
	OutputDict = {}
	NewCovMatrixDict = {}
	for tagger in taggers:
		for CovMatrixKey, CovMatrixValue in CovariantMatrixDictionary.items():
			if tagger in CovMatrixKey:
				data_year = CovMatrixKey.split("_")[2]
				if len(data_year) < 9:
					print(data_year)
					NewCovMatrixDict.update({data_year:CovMatrixValue})
		OutputDict.update({tagger:NewCovMatrixDict})
		NewCovMatrixDict = {}
	return OutputDict

def CreateYrHistoANDValueArr(Chi2Dictionary, TestTagger, Year2YearComparison):
	# Create a histos and arr per year tagger
	HistoValuesArray = []
	HistogramArray = []
	YearArray = []
	if Year2YearComparison == False:
		for file in Chi2Dictionary[0].keys():
		# Extract info of dataperiods from Chi2Dictionary:
			if TestTagger in file:
				DataYear = file.split("_")[2]
				if len(DataYear) < 9:
					# If its not the combination file
					print("Creating a histogram and array for " + DataYear + " for all working points: " + str(WPs))
					YearArray.append(DataYear)
					for WP in WPs:
						# Create an array per WP
						new_np_array = np.zeros(NoOfRepeatTestValues)
						HistoValuesArray.append(new_np_array)
						# Create the histo per WP
						histogram_name = "Chi2_" + DataYear + "_" + WP
						newhisto = ROOT.TH1D(histogram_name,"",no_bins,x_min_bin,x_max_bin)
						HistogramArray.append(newhisto)
		YearArray = YearArray[::-1]
		return YearArray, HistoValuesArray, HistogramArray
	else:
		for key in Chi2Dictionary[0][TestTagger].keys():
			YearArray.append(key)
			for WP in WPs:
				# Create an array per WP
				new_np_array = np.zeros(1)
				HistoValuesArray.append(new_np_array)
				# Create the histo per WP
				histogram_name = "Chi2_" + key + "_" + WP
				newhisto = ROOT.TH1D(histogram_name,"",no_bins,x_min_bin,x_max_bin)
				HistogramArray.append(newhisto)
		return YearArray, HistoValuesArray, HistogramArray

def FillNPHistoArrays(Chi2Dictionary, TestTagger, HistoValuesArray, Year2YearComparison):
	if Year2YearComparison == False:
		for sample in range (0, NoOfRepeatTestValues):
			for file in Chi2Dictionary[sample].keys():
				# Loop over the OG dict files, picking out each chi2 value
				if TestTagger in file:
					# Select only one tagger
					DataYear = file.split("_")[2]
					if len(DataYear) < 9:
						print("Working with file: " + file)
						# Next save all working points into the respective arrs
						for WP in range (0, len(WPs)):
							if DataYear == "data1516":
								YrWPIndex = WP + (len(WPs)*2)
								# Read this as: pick an array depending on Yr and WP and set its value to the corresponding WP value from the file
								HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][file][WP]
							elif DataYear == "data17":
								YrWPIndex = WP + (len(WPs)*1)
								HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][file][WP]
							elif DataYear == "data18":
								YrWPIndex = WP + (len(WPs)*0)
								HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][file][WP]
		print("Found all the values to fill the histos! ... ")
		print("Don't print it out though as it's disgusting, I've warned you.")
		return HistoValuesArray
	else:
		sample = 0
		for YearComp in Chi2Dictionary[sample][TestTagger].keys():
			print("Working with file: " + YearComp)
			# Next save all working points into the respective arrs
			for WP in range (0, len(WPs)):
				if YearComp == "data18data1516":
					YrWPIndex = WP + (len(WPs)*0)
					# Read this as: pick an array depending on Yr and WP and set its value to the corresponding WP value from the file
					HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][TestTagger][YearComp][WP]
				elif YearComp == "data18data17":
					YrWPIndex = WP + (len(WPs)*1)
					HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][TestTagger][YearComp][WP]
				elif YearComp == "data17data1516":
					YrWPIndex = WP + (len(WPs)*2)
					HistoValuesArray[YrWPIndex][sample] = Chi2Dictionary[sample][TestTagger][YearComp][WP]
	return HistoValuesArray

def CreateTestSFCompPlots(NomSFDictionary, PseudoSFDictionary, Tagger):
	# First take all info from nominal
	# Canvas
	c = ROOT.TCanvas("c", "",800,600)
	c.SetLogx()
	# ATLAS text
	atlas_text=ROOT.TLegend(0.19, 0.64, 0.64, 0.89,"")
	atlas_text.SetBorderSize(0)
	atlas_text.SetHeader("#it{#bf{ATLAS} Internal}")
	# Actual Legend
	legend = ROOT.TLegend(0.60,0.64,0.81,0.86)
	legend.SetBorderSize(0)
	legend.SetTextSize(0.04)
	# Requirements for loop
	col_iterator = 0
	WP = 1
	for FileKey, FileValue in NomSFDictionary.items():
		if Tagger in FileKey:
			# Pick only files with the (test)Tagger requested
			DataYear = FileKey.split("_")[2]
			if len(DataYear) < 9:
				# The file cannot be combo(data15161718) but still smaller than data1516
				# Now plot nominal SFs:
				print(FileKey)
				FileValue[WP].Draw("SAME")
				FileValue[WP].SetAxisRange(0.8, 1.25,"Y");
				FileValue[WP].SetLineColor(plotting_cols[col_iterator])
				FileValue[WP].SetLineWidth(3)
				FileValue[WP].SetLineStyle(1)
				legend.AddEntry(FileValue[WP],DataYear+"_nominal")

				# Also plot pseudo from the FileKey
				PseudoSFDictionary[FileKey][WP].Draw("SAME")
				PseudoSFDictionary[FileKey][WP].SetLineColor(plotting_cols[col_iterator] + 1)
				PseudoSFDictionary[FileKey][WP].SetLineWidth(3)
				PseudoSFDictionary[FileKey][WP].SetLineStyle(2)
				legend.AddEntry(PseudoSFDictionary[FileKey][WP],DataYear+"_pseudo")
	
				# Prepare for next iteration
				col_iterator = col_iterator + 1
	atlas_text.Draw()
	legend.Draw()
	title_of_plot = "/individiualSF_NomvsPseudo"
	c.SaveAs(input_filepath+title_of_plot+".pdf")
	print("Saved the plot to:\t" + input_filepath + title_of_plot + ".pdf")
	raw_input()

def PlotChi2Values(Chi2Dictionary, Complex = False, Year2YearComparison = False):
	TestWP = WPs[0]
	# Put the values into a histogram and check it out:
	c = ROOT.TCanvas("c", "",800,600)
	if Complex == False:
		Chi2Histo = ROOT.TH1D("","",30,0,15)
		for value in Chi2Dictionary[TestWP]:
			Chi2Histo.Fill(value)
		Chi2Histo.Draw()
		Chi2Histo.GetXaxis().SetTitle("\chi^2 values")
		if args.TestSampling:
			title_of_plot = "/nom_pseudo_chi2_values_test"
		else:
			title_of_plot = "/nom_pseudo_chi2_values"
		c.SaveAs(input_filepath+title_of_plot+".pdf")
		print("Saved the plot to:\t" + input_filepath + title_of_plot + ".pdf")
		raw_input()
		return 0
	else:
		print("Beginning plotting ... ")
		test_tagger = "MV2c10"
		gStyle.SetOptStat(0)
		YearArray, HistoValuesArray, HistogramArray = CreateYrHistoANDValueArr(Chi2Dictionary, test_tagger, Year2YearComparison)
		HistoValuesArray = FillNPHistoArrays(Chi2Dictionary, test_tagger, HistoValuesArray, Year2YearComparison)
		for Histogram in range (0, len(HistogramArray)):
			print("Filling " + HistogramArray[Histogram].GetName() + " with its values from the respective np array ... ")
			if Year2YearComparison == False:
				for value in range(0, NoOfRepeatTestValues):
					# Fill Histogram bin content
					HistogramArray[Histogram].Fill(HistoValuesArray[Histogram][value])
			else:
				for value in range(0, len(HistoValuesArray[0])):
					# Fill Histogram bin content
					HistogramArray[Histogram].Fill(HistoValuesArray[Histogram][value])
					print("The values of histogram: " + HistogramArray[Histogram].GetName() + " are:\n" + str(HistoValuesArray[Histogram]))
		print("Successfully filled all the histograms!")
		# Plotting
		# Legends
		atlas_text=ROOT.TLegend(0.19, 0.64, 0.64, 0.89,"")
		atlas_text.SetBorderSize(0)
		atlas_text.SetHeader("#it{#bf{ATLAS} Internal}")
		# Actual Legend
		legend = ROOT.TLegend(0.60,0.64,0.81,0.86)
		legend.SetBorderSize(0)
		legend.SetTextSize(0.04)
		for histogram in range(0, TotalNumberOfComparisons):
			if histogram == 0:
				# Histo 1
				print("Plotting histogram\t " + HistogramArray[histogram].GetName())
				HistogramArray[histogram].Draw()
				HistogramArray[histogram].SetLineColor(plotting_cols[histogram])
				HistogramArray[histogram].SetLineWidth(3)
				HistogramArray[histogram].GetXaxis().SetTitle("\chi^2 min values")
				legend.AddEntry(HistogramArray[histogram],HistogramArray[histogram].GetName())
				print("Plotted Histogram " + str(histogram + 1))
			else:
				# 2
				print("Plotting histogram\t " + HistogramArray[histogram*len(WPs)].GetName())
				HistogramArray[histogram*len(WPs)].Draw("SAME")
				HistogramArray[histogram*len(WPs)].SetLineColor(plotting_cols[histogram])
				HistogramArray[histogram*len(WPs)].SetLineWidth(3)
				legend.AddEntry(HistogramArray[histogram*len(WPs)],HistogramArray[histogram*len(WPs)].GetName())
				print("Plotted Histogram " + str(histogram + 1))
		# Extras
		atlas_text.Draw()
		legend.Draw()
		if args.TestSampling:
			title_of_plot = "/chi2_min_values_text"
		else:
			title_of_plot = "/chi2_min_values"
		c.SaveAs(input_filepath+title_of_plot+".pdf")
		print("Saved the plot to:\t" + input_filepath + title_of_plot + ".pdf")
		raw_input()
		return HistogramArray

def OutputArrayAndHistValues(HistogramArray, PseudoMeasurementFile, PseudoMeasurementKey):
	OutputArray = HistogramArray + [PseudoMeasurementFile.Get(PseudoMeasurementKey.GetName())]
	HistoValues = []
	for Histo in HistogramArray:
		print("Taking the values from " + Histo.GetName() + " and storing them seperately ... ")
		for Bin in range(0, no_bins):
			if Histo.GetBinContent(Bin+1) > 0.0:
				# Need to save the values from the yr2yr histo
				HistoValues.append(Bin+1)
	return OutputArray, HistoValues

def PlotComplexChi2(NominalFile,PseudoFile, WorkingPoint):
	Yr2YrFile = ROOT.TFile(NominalFile)
	PseudoFile = ROOT.TFile(PseudoFile)
	PseudoKeys = PseudoFile.GetListOfKeys()
	Yr2YrKeys = Yr2YrFile.GetListOfKeys()
	HistoArray = []
	CompDict = {}
	Iterator = 1
	TotalNumberOfComparisons = len(Yr2YrKeys) / len(WPs)
	Iterator2 = TotalNumberOfComparisons
	for PseuKey in PseudoKeys:
		# Key can be thought of as the nomianl sample
		NominalYear = PseuKey.GetName().split("_")[1]
		WorknPoint = PseuKey.GetName().split("_")[2]
		if WorknPoint == WorkingPoint:
			# Pick out only those that match the WP
			print("Found a nominal file for working point " + WorkingPoint + ": " + PseuKey.GetName())
			print("Going to match histograms from the year to year comparison list ... ")
			for YrKey in Yr2YrKeys:
				# We can now pick out the histo values according to the info we have picked out
				YrHistoYear = YrKey.GetName().split("_")[1]
				YrWorknPoint = YrKey.GetName().split("_")[2]
				Yr2Test = "data" + YrHistoYear.split("data")[1]
				if Yr2Test == NominalYear:
					if YrWorknPoint == WorkingPoint:
						print("Found a file that matches!\t" + YrKey.GetName())
						HistoArray.append(Yr2YrFile.Get(YrKey.GetName()))
						if len(HistoArray) > 1:
							print("Found all the values needed, now appending the corresponding pseudo file and outputting ... ")
							OutputArray, HistoValues = OutputArrayAndHistValues(HistoArray,PseudoFile, PseuKey)
							Iterator2 = Iterator2 - 1
						elif Iterator2 == (TotalNumberOfComparisons - 1):
							print("Found all the values needed, now appending the corresponding pseudo file and outputting ... ")
							OutputArray, HistoValues = OutputArrayAndHistValues(HistoArray,PseudoFile, PseuKey)
							Iterator2 = Iterator2 - 1							
		if Iterator >= len(WPs):
			# Using this Iterator to reset the lists after wp iteration
			print("Reached the end of the psuedo histos for that period ... ")
			Iterator = 0
			CompDict.update({OutputArray[-1].GetName():[OutputArray,HistoValues]})
			HistoArray = []
		Iterator = Iterator + 1

	for PlottingKey, PlottingValue in CompDict.items():
		CombinedHistogram = PlottingValue[0][0].Clone()
		# Plotting:
		c = ROOT.TCanvas("c", "",800,600)
		c.SetLogy()
		# Legends
		atlas_text=ROOT.TLegend(0.19, 0.64, 0.64, 0.89,"")
		atlas_text.SetBorderSize(0)
		atlas_text.SetHeader("#it{#bf{ATLAS} Internal}")
		# Actual Legend
		legend = ROOT.TLegend(0.60,0.64,0.81,0.86)
		legend.SetBorderSize(0)
		legend.SetTextSize(0.04)
		for Histo in range(0, len(PlottingValue[0])):
			# Now draw them
			if Histo == 0:
				PlottingValue[0][Histo].Draw()
				PlottingValue[0][Histo].SetLineColor(plotting_cols[Histo])
				legend.AddEntry(PlottingValue[0][Histo],PlottingValue[0][Histo].GetName())
			else:
				PlottingValue[0][Histo].Draw("SAME")
				PlottingValue[0][Histo].SetLineColor(plotting_cols[Histo])
				legend.AddEntry(PlottingValue[0][Histo],PlottingValue[0][Histo].GetName())
				CombinedHistogram.Add(PlottingValue[0][Histo])
		atlas_text.Draw()
		legend.Draw()
		title_of_plot = "/combined_chi2dist_values"
		c.SaveAs(input_filepath+title_of_plot+".pdf")
		print("Saved the plot to:\t" + input_filepath + title_of_plot + ".pdf")
		print("Integral from the value, " + str(min(PlottingValue[1])) + " results in a rel. probability of:\t" + str((CombinedHistogram.Integral(min(PlottingValue[1])+1,no_bins,"")-1)/NoOfRepeatTestValues))
		print("Integral from the value, " + str(max(PlottingValue[1])) + " results in a rel. probability of:\t" + str(CombinedHistogram.Integral(max(PlottingValue[1])+1,no_bins,"")/NoOfRepeatTestValues))
		raw_input()
		PseudoFile.Close()
		Yr2YrFile.Close()
	print("Finished")



parser = argparse.ArgumentParser(
    description='Various stat tests between fit results for differnet years.')
parser.add_argument('--VeryBasicChi2', action='store_true',
                    help='Do a basic chi2 test between postfit histos for all WPs and taggers')
parser.add_argument('--BasicChi2', action='store_true',
                    help='Sample pseudo measurements and compute chi2 tests like in VeryBasicChi2')
parser.add_argument('--ComplexChi2', action='store_true',
                    help='Rebuild cov matrices from np_stats and then sample and produce Chi2, need to run with and without --YearSampling')
parser.add_argument('--TestSampling', action='store_true',
                    help='Change the samplign number to something low - 1000 sampling takes a long time, blame ROOT')
parser.add_argument('--SkipSampling', action='store_true',
                    help='If you already have the file (nom_vs_pseudo_chi2min.root), you can skip the long sampling procedure, blame ROOT')
parser.add_argument('--YearSampling', action='store_true',
                    help='Perform the comlex chi2 with year to year comparisons instead of nominal vs pseudo.')

#########################################################################
# Put the files in cwd + "/TestYearDepedence/<FinalFitPlot files here>"
# Call python YearDependenceStatTest.py from /TestYearDepedence/..
#########################################################################

args = parser.parse_args()
taggers = options.taggers
WPs = options.WPs
no_pt_bins = 9
xlow_pt_bins = 0
xup_pt_bins = 600
pt_bin_edges = np.array([20.0,30.0,40.0,60.0,85.0,110.0,140.0,175.0,250.0,600.0])
plotting_cols = [221,63,209,90,93,98,225,1,2,3,4,5,6,7,8,9]
no_bins = 110
x_max_bin = no_bins
x_min_bin = 0
# Iterator required to change seed (ROOT's crap)
TRandomSeed = datetime.datetime.now()
rdnm_no = ROOT.TRandom(int(TRandomSeed.strftime("%f")))
if args.TestSampling:
	# Test value
	NoOfRepeatTestValues = 5
else:
	# Full sampling
	NoOfRepeatTestValues = 1000


# Create a directory where the files are
cwd = os.getcwd()
#input_filepath = cwd +"/TestYearDepedence/"
input_filepath="/eos/user/a/alopezso/FTAGPrerecommendations/"
if not os.path.exists(input_filepath):
	cmd = "mkdir -p " + input_filepath
	os.system(cmd)
	print("Put your files in here and rerun!")
	print("In this path: " + input_filepath)
	exit()

if args.VeryBasicChi2:
	# Do basic chi2 test between individual years
	npFileArray = GetInputFileArray(input_filepath)

	# Pick out the files according to their taggers
	NomSFDict, NPStatDict = GetNomANDNPstatHistosAccordingToFile(npFileArray)
	NomChi2Dict = PerformChi2TestBetweenYears(NomSFDict)
	# print(NomChi2Dict)
	print("Finished")

elif args.BasicChi2:
	npFileArray = GetInputFileArray(input_filepath)

	NomSFDict, NPStatDict = GetNomANDNPstatHistosAccordingToFile(npFileArray)

	PseudoMeasurementDict = SamplePseudoMeasurements(NPStatDict)

	TestTagger = taggers[0]
	TotalNumberOfComparisons = (len(NomSFDict) / len(taggers) ) -1
	# Uncomment the next line for SF comparison plots between pseudo and nominal
	# CreateTestSFCompPlots(NomSFDict, PseudoMeasurementDict, TestTagger)

	OG_NomVsPsuCompChi2Dict = PerformChi2TestBetweenNomVsPsuedo(NomSFDict, PseudoMeasurementDict)
	for i in range (1,NoOfRepeatTestValues):
		# Loop over the sampling of pseduomeasurements
		PseudoMeasurementDict = SamplePseudoMeasurements(NPStatDict)
		NomVsPsuCompChi2Dict = PerformChi2TestBetweenNomVsPsuedo(NomSFDict, PseudoMeasurementDict)
		for key, value in OG_NomVsPsuCompChi2Dict.items():
			# Add the new values to the existing numpy array
			OG_NomVsPsuCompChi2Dict[key] = np.r_[OG_NomVsPsuCompChi2Dict[key], NomVsPsuCompChi2Dict[key]]

	Chi2DictPerWP = TaggerToWPDictConv(OG_NomVsPsuCompChi2Dict, TestTagger)
	# PlotChi2Values(Chi2DictPerWP)

	NewChi2Dict = PerformChi2TestBetweenYears(NomSFDict)
	NewChi2WPDict = TaggerToWPDictConv(NewChi2Dict,TestTagger, True)
	PlotChi2Values(NewChi2WPDict)

	print("Finished")

elif args.ComplexChi2:
	print("\n ------------------------------------------------ \n ")
	print("Beginning stat test beginning now ...")
	print("\n ------------------------------------------------ \n ")
	# Global Variables for later
	Year2YearFilename = "year_vs_year_chi2min.root"
	NomVsPseuFilename = "nom_vs_pseudo_chi2min.root"
	if not args.SkipSampling:
		# If you're rerunning from fresh follow this:
		# Loop over the files you have
		npFileArray = GetInputFileArray(input_filepath)

		NomSFDict, NPStatDict = GetNomANDNPstatHistosAccordingToFile(npFileArray)
		print("Nominal measurments:")
		print(NomSFDict)
		TotalNumberOfComparisons = (len(NomSFDict) / len(taggers) ) -1

		# Build cov matrix with stat nps
		CovMatrixDict = BuildCovMatrixDict(NomSFDict, NPStatDict, False)
		Chi2Values = []
		if args.YearSampling:
			# Convert to match yr to yr col vector dict
			CovMatrixDict = CovMatrixDictToColVectorConverter(CovMatrixDict)
			# Build col vector with year to year comparisons
			ColVectorDict = BuildColVector(NomSFDict, NomSFDict, True)
			# Calculate chi2 min with year to year comparisons
			Chi2MinDict = CalculateChi2Min(CovMatrixDict, ColVectorDict, True)
			Chi2Values.append(Chi2MinDict)
			HistosPerYrPerWPArr = PlotChi2Values(Chi2Values, True, True)
			OutputFile = ROOT.TFile(Year2YearFilename,"RECREATE")
		else:
			for i in range (0,NoOfRepeatTestValues):
				# Loop over the sampling of pseduomeasurements
				PseudoMeasurementDict = SamplePseudoMeasurements(NPStatDict)

				# Before calculating eq 2 from paper need to generate a col vector from nom and pseudo values
				ColVectorDict = BuildColVector(NomSFDict, PseudoMeasurementDict)

				print("Built covariance matrices:")
				print(CovMatrixDict)
				print("Built delta matrices:")
				print(ColVectorDict)

				# Now that we have the delta and cov. matrix we can calculate Chi2min from the reference in the same way as before
				# Calculate chi2 min with nominal vs pseudo
				Chi2MinDict = CalculateChi2Min(CovMatrixDict, ColVectorDict)
				# Produce an arr of N samplings dicts which are organised by file : arr of per WP chi2 values
				Chi2Values.append(Chi2MinDict)
			print("Finished looping over pseudo measurments ... ")
			HistosPerYrPerWPArr = PlotChi2Values(Chi2Values, True, False)
			OutputFile = ROOT.TFile(NomVsPseuFilename,"RECREATE")
		for Histo in HistosPerYrPerWPArr:
			# Write all the histos out to file
			OutputFile.WriteTObject(Histo) 
		OutputFile.Close()
	else:
		PlotComplexChi2(Year2YearFilename,NomVsPseuFilename,WPs[0])

	print("Finished test.")

else:
	print("Please choose an argpase Chi2 test you would like to do")
	print("For options look at the script or called python YearDependenceStatTest.py --h")
