
folder="FitFolder"


mkdir -p $folder



for file in `ls build/`
do
    if [[ ! -d ./build/$file ]]
    then
	continue
    fi

    if [[ $file != _* ]]
    then
	continue
    fi

    cp ./build/$file/FitPlots_3SB/FitPlot_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2_nominal.root $folder/FitPlot_r21_d1516_MV2c10_FixedCutBEff_emu_OS_J2$file.root

done