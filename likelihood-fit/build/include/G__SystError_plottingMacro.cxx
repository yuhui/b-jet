// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME G__SystError_plottingMacro
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "ROOT/RConfig.hxx"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "SystError_plottingMacro.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace {
  void TriggerDictionaryInitialization_libSystError_plottingMacro_Impl() {
    static const char* headers[] = {
"SystError_plottingMacro.h",
nullptr
    };
    static const char* includePaths[] = {
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.28.08-8a690/x86_64-el9-gcc13-opt/include",
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/likelihood-fit",
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/likelihood-fit/include",
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/likelihood-fit/build/src",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.28.08-8a690/x86_64-el9-gcc13-opt/include/",
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/likelihood-fit/build/include/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libSystError_plottingMacro dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libSystError_plottingMacro dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "SystError_plottingMacro.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libSystError_plottingMacro",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libSystError_plottingMacro_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libSystError_plottingMacro_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libSystError_plottingMacro() {
  TriggerDictionaryInitialization_libSystError_plottingMacro_Impl();
}
