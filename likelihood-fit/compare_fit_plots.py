import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
import argparse



gStyle.SetPaintTextFormat("1.3f");
gStyle.SetNumberContours(12);

input_file_finalSystPlots_name="/eos/user/j/jabarr/Calibration/FTAGResults/r22.2.87_R22_Combined/plots/FitPlots_3SB/FitPlot_r22_dataR22_GN120220509_FixedCutBEff_emu_OS_J2_nominal.root"
#"/nfs/dust/atlas/user/alopezso/FTAGResults/r21.2.130_mc16e/plots/FitPlots_3SB/FitPlot_r21_data18_DL1r_FixedCutBEff_emu_OS_J2_Minos_nominal.root"
#"/afs/cern.ch/user/j/jhall/public/BTagging/BTaggingCalibration/2019_June_FinalFitPlotFiles/FinalFitPlots_r21_data15161718_DL1_FixedCutBEff_emu_OS_J2.root"

samples_to_compare_with={
    "DL1d" : "/eos/user/j/jabarr/Calibration/FTAGResults/r22.2.87_R22_Combined/plots/FitPlots_3SB/FitPlot_r22_dataR22_DL1dv01_FixedCutBEff_emu_OS_J2_nominal.root",
#"/nfs/dust/atlas/user/alopezso/FTAGResults/r22.2.62_mc16e_r22/plots/FitPlots_3SB/FitPlot_r21_dataR22_DL1r_FixedCutBEff_emu_OS_J2_Minos_nominal.root"
#"/afs/cern.ch/work/j/jhall/public/BTagging/bjets_ttbardilepton_PDF/likelihood-fit/build/FitResults/FitPlots_3SB/FitPlot_r21_d15161718_DL1_FixedCutBEff_emu_OS_J2_nominal.root",
}


input_file_finalSystPlots=ROOT.TFile(input_file_finalSystPlots_name,"read")
output_file=ROOT.TFile("/eos/user/j/jabarr/Calibration/FTAGResults/GN1_DL1d_comparison.root","recreate")
legend_x      = 0.60#0.65
legend_y      = 0.45
legend_width  = 0.28
legend_height = 0.24#0.34
legend = TLegend(legend_x, legend_y-legend_height, legend_x+legend_width, legend_y)
#legend.SetFillStyle(0)
legend.SetLineColor(0)
legend.SetBorderSize(0)
legend.SetTextFont(42)
legend.SetTextSize(0.04)


nice_colours=[ROOT.kBlue+1, ROOT.kOrange-1, ROOT.kGray+1, ROOT.kCyan+1] #(kRed + 1), (kOrange - 1), (kBlue + 1), (kGray + 1), (kGreen - 8), (kCyan + 1), (kBlack)
directory = input_file_finalSystPlots.Get("sf_and_beff")
hists=[]
keyList=directory.GetListOfKeys()#input_file_finalSystPlots.GetListOfKeys()
for i in xrange(1, keyList.GetSize()):
    obj=directory.Get(keyList.At(i).GetName())
    className=obj.ClassName()
    oname=obj.GetName()
    print i,keyList.At(i).GetName(),className
    if className=="TCanvas":
        if "c_sf_b_" in oname and not "log" in oname:# and oname.find("_nominal")>0:
            wp=oname.replace("c_sf_b_","").replace("_nominal","")
            canvas=obj
            canvas.cd()
            output_file.cd()
            canvas.Draw()
            print "adding on Canvas:",oname, "wp: ",wp
            data_h=directory.Get("sf_b_"+wp+"_Postfit").Clone()
            data_h.SetDirectory(0)
            hists.append(data_h)
            legend.Clear()
            #            legend.AddEntry(data_h,"data1516","LPE")
            i=0
            for compare_name in samples_to_compare_with:
                print "adding",compare_name,":",samples_to_compare_with[compare_name]
                c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                print "loaded file"
                output_file.cd()
                if not c_file:
                    print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                sf_hist=c_file.Get("sf_and_beff/"+"sf_b_"+wp+"_Postfit").Clone("sf_"+compare_name+"_wp_"+wp)
                sf_hist_2=c_file.Get("sf_and_beff/"+"sf_b_"+wp+"_Prefit").Clone("sf_"+compare_name+"_wp_"+wp+"_Prefit")
                output_file.cd()
                sf_hist.SetDirectory(0)
                sf_hist_2.SetDirectory(0)
                hists.append(sf_hist)
                hists.append(sf_hist_2)
                print "loaded hist"
                sf_hist.SetLineColor(nice_colours[i])
                sf_hist.SetMarkerColor(nice_colours[i])
                sf_hist_2.SetLineColor(nice_colours[i+1])
                sf_hist_2.SetMarkerColor(nice_colours[i+1])
                output_file.cd()
                sf_hist.Write()
                sf_hist_2.Write()
                canvas.cd()
                sf_hist.Draw("LPESAME")
                sf_hist_2.Draw("LPESAME")
                print "hist drawen"
                legend.AddEntry(sf_hist,compare_name+" Postfit","LPE")
                legend.AddEntry(sf_hist_2,compare_name+" Prefit","LPE")
                print "added to legend"
                c_file.Close()
                print "closed file"
                i=i+1
                print  "i=",i
            output_file.cd()
            canvas.cd()
            data_h.Draw("LPESAME")
            legend.Draw()
            output_file.cd()
            canvas.Write()
        elif "c_e_b_" in oname and not "log" in oname: # and oname.find("_nominal")>0:
            wp=oname.replace("c_e_b_","").replace("_nominal","")
            canvas=obj
            canvas.cd()
            output_file.cd()
            canvas.Draw()
            print "adding on Canvas:",oname, "wp: ",wp
            data_h=directory.Get("e_b_"+wp+"_Postfit").Clone()
            data_h.SetDirectory(0)
            hists.append(data_h)
            legend.Clear()
#            legend.AddEntry(data_h,"data1516","LPE")
            i=0
            for compare_name in samples_to_compare_with:
                print "adding",compare_name,":",samples_to_compare_with[compare_name]
                c_file=ROOT.TFile(samples_to_compare_with[compare_name],"read")
                print "loaded file"
                output_file.cd()
                if not c_file:
                    print compare_name,": ",samples_to_compare_with[compare_name],"not found!"
                sf_hist=c_file.Get("sf_and_beff/"+"e_b_"+wp+"_Postfit").Clone("e_"+compare_name+"_wp_"+wp)
                sf_hist_2=c_file.Get("sf_and_beff/"+"e_b_"+wp+"_Prefit").Clone("e_"+compare_name+"_wp_"+wp+"_Prefit")
                output_file.cd()
                sf_hist.SetDirectory(0)
                sf_hist_2.SetDirectory(0)
                hists.append(sf_hist)
                hists.append(sf_hist_2)
                print "loaded hist"
                sf_hist.SetLineColor(nice_colours[i])
                sf_hist.SetMarkerColor(nice_colours[i])
                sf_hist_2.SetLineColor(nice_colours[i+1])
                sf_hist_2.SetMarkerColor(nice_colours[i+1])
                output_file.cd()
                sf_hist.Write()
                sf_hist_2.Write()
                canvas.cd()
                sf_hist.Draw("LPESAME")
                sf_hist_2.Draw("LPESAME")
                print "hist drawen"
                legend.AddEntry(sf_hist,compare_name+" Postfit","LPE")
                legend.AddEntry(sf_hist_2,compare_name+" Prefit","LPE")
                print "added to legend"
                c_file.Close()
                print "closed file"
                i=i+1
                print  "i=",i
            output_file.cd()
            canvas.cd()
            data_h.Draw("LPESAME")
            legend.Draw()
            output_file.cd()
            canvas.Write()
output_file.Close()

print "outputfile closed."
