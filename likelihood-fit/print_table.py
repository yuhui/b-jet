import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
import sys
import subprocess
import argparse
import shutil
from multiprocessing.pool import ThreadPool
sys.path.insert(0, '../TTbar-b-calib-final-selection-r21/')
from options_file import *

flavlist=["bb","bc","bl","cb","cc","cl","lb","lc", "ll"]

class sample:
    def __init__(self,name,file_path):
        self.name = name
        self.file_path=file_path

    def append_to_list_for_table(self,row_list):
        self.r_file=ROOT.TFile(self.file_path,"read")
        for flav in flavlist:
            h_ncr=self.r_file.Get("emu_OS_J2/hf4_Ncr_emu_OS_J2_hist_for_fit_MV2c10_FixedCutBEff_"+flav).Projection(1,0)
            row_list.append([self.name+"_"+flav, h_ncr.GetBinContent(1,1),h_ncr.GetBinContent(2,1),h_ncr.GetBinContent(1,2),h_ncr.GetBinContent(2,2), h_ncr.Integral()])
        self.r_file.Close()



outdir=options.output_dir


data_fname= outdir+options.data_name+"/"+options.data_name+"_data_combination.root"

syst="nominal"
ttb_sample=sample("ttb",outdir+options.ttb_sample.name+"/"+options.ttb_sample.name+"_"+syst+"_combination.root")

singleTop_sample=sample("singleTop",outdir+options.singleTop_sample.name+"/"+options.singleTop_sample.name+"_"+syst+"_combination.root")
ZJets_sample=sample("Z+Jets",outdir+options.ZJets_sample.name+"/"+options.ZJets_sample.name+"_"+syst+"_combination.root")
Diboson_sample=sample("Diboson",outdir+options.Diboson_sample.name+"/"+options.Diboson_sample.name+"_"+syst+"_combination.root")
Wjets_sample=sample("W+Jets",outdir+options.Wjets_sample.name+"/"+options.Wjets_sample.name+"_"+syst+"_combination.root")
sample_list=[ttb_sample,singleTop_sample,ZJets_sample,Diboson_sample,Wjets_sample]

row_list=[]
title_list = ["sample", "SR","CR_HL","CR_LH","CR_HH", "SUM"]
for s in sample_list:
    s.append_to_list_for_table(row_list)

f_data=ROOT.TFile(data_fname,"read")
h_ncr_data=f_data.Get("emu_OS_J2/hf4_Ncr_emu_OS_J2_hist_for_fit_MV2c10_FixedCutBEff_data").Projection(1,0)
print "data","SR","CR_HL","CR_LH","CR_HH"
print h_ncr_data.Integral(), h_ncr_data.GetBinContent(1,1),h_ncr_data.GetBinContent(2,1),h_ncr_data.GetBinContent(1,2),h_ncr_data.GetBinContent(2,2)
row_list.append(["data", h_ncr_data.GetBinContent(1,1),h_ncr_data.GetBinContent(2,1),h_ncr_data.GetBinContent(1,2),h_ncr_data.GetBinContent(2,2), h_ncr_data.Integral()])



row_format ="{:15}"+"{:15}" * (len(title_list)-1)
print row_format.format(*title_list)
row_format ="{:15}"+"{:15.2f}" * (len(title_list)-1)
for row in row_list:
    print row_format.format( *row)

print "Ende gut alles gut! "
