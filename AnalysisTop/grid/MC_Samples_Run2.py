import TopExamples.grid

###
#FTAG2 Preliminary Nominal Samples Until DAOD_FTAG2 will be produced
###

TopExamples.grid.Add("FTAG2_ttbar_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e6348_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG2.e6337_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_FTAG2.e6337_s3681_r13167_p5981",
]
TopExamples.grid.Add("FTAG2_ttbar_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e6348_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG2.e6337_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_FTAG2.e6337_s3681_r13144_p5981",
]
TopExamples.grid.Add("FTAG2_ttbar_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e6348_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG2.e6337_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.410471.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_FTAG2.e6337_s3681_r13145_p5981",   
]

TopExamples.grid.Add("FTAG2_Singletop_PowPy8_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_FTAG2.e6527_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_FTAG2.e6527_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_FTAG2.e6671_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_FTAG2.e6671_s3681_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8547_s4231_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8547_s4231_r13144_p5981",
]
TopExamples.grid.Add("FTAG2_Singletop_PowPy8_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_FTAG2.e6527_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_FTAG2.e6527_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_FTAG2.e6671_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_FTAG2.e6671_s3681_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8547_s4231_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8547_s4231_r13167_p5981",
]
TopExamples.grid.Add("FTAG2_Singletop_PowPy8_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_FTAG2.e6527_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_FTAG2.e6527_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_FTAG2.e6671_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_FTAG2.e6671_s3681_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8547_s4231_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8547_s4231_r13145_p5981",
]

TopExamples.grid.Add("FTAG2_Zjets_Sh2211_r22_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
]
TopExamples.grid.Add("FTAG2_Zjets_Sh2211_r22_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
]
TopExamples.grid.Add("FTAG2_Zjets_Sh2211_r22_mc20e").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
]

TopExamples.grid.Add("FTAG2_Wjets_Sh2211_r22_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13167_p5981",
]

TopExamples.grid.Add("FTAG2_Wjets_Sh2211_r22_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13144_p5981",
]

TopExamples.grid.Add("FTAG2_Wjets_Sh2211_r22_mc20e").datasets = [
	"mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8351_s3681_r13145_p5981",
]

TopExamples.grid.Add("FTAG2_Diboson_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
    "mc20_13TeV:mc20_13TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8547_s3797_r13167_p5981",
]
TopExamples.grid.Add("FTAG2_Diboson_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
    "mc20_13TeV:mc20_13TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8547_s3797_r13144_p5981",
]
TopExamples.grid.Add("FTAG2_Diboson_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
    "mc20_13TeV:mc20_13TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8547_s3797_r13145_p5981",
]


###############################################################
###             TTBAR samples 
###############################################################

## Nominal
TopExamples.grid.Add("PHYS_ttbar_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_s3681_r13167_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_s3681_r13144_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_s3681_r13145_p5631",
]

## Nominal AF2 sample (for systematic comparisons) (to be checked a-tag or s-tag?)
TopExamples.grid.Add("PHYS_ttbar_fastsim_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_a899_r13167_p5631"
]
TopExamples.grid.Add("PHYS_ttbar_fastsim_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_a899_r13144_p5631"
]
TopExamples.grid.Add("PHYS_ttbar_fastsim_r22_mc20e").datasets = [
    #"mc20_13TeV:mc20_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e6348_a899_r13145_p5631"
]

## HW7 AF2 sample (for factorisation scale)
TopExamples.grid.Add("PHYS_ttbar_HW7_fastsim_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13167_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_HW7_fastsim_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13144_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_HW7_fastsim_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_PHYS.e7580_a899_r13145_p5631",
]

# PhPy8 hdamp3mtop AF2 sample (for ISR up)
TopExamples.grid.Add("PHYS_ttbar_PhPy8_hdamp3mtop_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_PHYS.e6454_a899_r13167_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_PhPy8_hdamp3mtop_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_PHYS.e6454_a899_r13144_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_PhPy8_hdamp3mtop_mc20e").datasets = [
	"mc20_13TeV:mc20_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_PHYS.e6454_a899_r13145_p5631",
]

# MadGraph sample
TopExamples.grid.Add("PHYS_ttbar_aMcAtNloPy8_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13167_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_aMcAtNloPy8_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13144_p5631",
]
TopExamples.grid.Add("PHYS_ttbar_aMcAtNloPy8_mc20e").datasets = [
	"mc20_13TeV:mc20_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_PHYS.e6762_a899_r13145_p5631",
]



###############################################################
###              Single-top samples
###############################################################

## Nominal
TopExamples.grid.Add("PHYS_Singletop_PowPy8_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13167_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13145_p5631",
]

## Nominal fast sim (for systematic comparison)
TopExamples.grid.Add("PHYS_Singletop_fastsim_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_a899_r13167_p5631",
    #"mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_a899_r13167_p5631",    
]

TopExamples.grid.Add("PHYS_Singletop_fastsim_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_a899_r13144_p5631",
    #"mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_a899_r13144_p5631",
    #"mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_a899_r13144_p5631",
    #"mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_a899_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_fastsim_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_PHYS.e6615_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_PHYS.e6615_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_a899_r13145_p5631",
    #"mc20_13TeV:mc20_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_a899_r13145_p5631",
]

## DS uncertainties 
TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.410656.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.410657.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13167_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.410656.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.410657.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.410656.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_top.deriv.DAOD_PHYS.e6615_s3681_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.410657.PowhegPythia8EvtGen_A14_Wt_DS_dilepton_antitop.deriv.DAOD_PHYS.e6615_s3681_r13145_p5631",
]

## DS uncertainties fastsim
TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_fastsim_r22_mc20a").datasets = [
    #Missing
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_fastsim_r22_mc20d").datasets = [
    #Missing
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_fastsim_r22_mc20e").datasets = [
    #Missing
]

### Madgraph uncertainties fastsim
TopExamples.grid.Add("PHYS_Singletop_MGPy8_fastsim_r22_mc20a").datasets = [
    #Missing
]

TopExamples.grid.Add("PHYS_Singletop_MGPy8_fastsim_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.412004.aMcAtNloPy8EG_tchan_NLO.deriv.DAOD_PHYS.e6888_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.412005.aMcAtNloPythia8EvtGen_A14_singletop_schan_lept.deriv.DAOD_PHYS.e6867_a899_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_MGPy8_fastsim_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.412004.aMcAtNloPy8EG_tchan_NLO.deriv.DAOD_PHYS.e6888_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.412005.aMcAtNloPythia8EvtGen_A14_singletop_schan_lept.deriv.DAOD_PHYS.e6867_a899_r13145_p5631",
]

## HW7 (for hadronization) uncertainties fastsim
TopExamples.grid.Add("PHYS_Singletop_HW7_fastsim_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.600017.PhH7EG_H7UE_716_tchan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.600018.PhH7EG_H7UE_716_tchan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.600019.PhH7EG_H7UE_716_schan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.600020.PhH7EG_H7UE_716_schan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13167_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_HW7_fastsim_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.600017.PhH7EG_H7UE_716_tchan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.600018.PhH7EG_H7UE_716_tchan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.600019.PhH7EG_H7UE_716_schan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.600020.PhH7EG_H7UE_716_schan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_HW7_fastsim_r22_mc20e").datasets = [
    "mc20_13TeV:mc20_13TeV.600017.PhH7EG_H7UE_716_tchan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.600018.PhH7EG_H7UE_716_tchan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.600019.PhH7EG_H7UE_716_schan_lept_antitop.deriv.DAOD_PHYS.e7954_a899_r13145_p5631",
    "mc20_13TeV:mc20_13TeV.600020.PhH7EG_H7UE_716_schan_lept_top.deriv.DAOD_PHYS.e7954_a899_r13145_p5631",
]


###############################################################
###              Z+jets samples 
###############################################################
TopExamples.grid.Add("PHYS_Zjets_Sh2211_r22_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
]

TopExamples.grid.Add("PHYS_Zjets_Sh2211_r22_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
]


TopExamples.grid.Add("PHYS_Zjets_Sh2211_r22_mc20e").datasets = [
	"mc20_13TeV:mc20_13TeV.700320.Sh_2211_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700321.Sh_2211_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700322.Sh_2211_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700326.Sh_2211_Ztautau_LL_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700327.Sh_2211_Ztautau_LL_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700328.Sh_2211_Ztautau_LL_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700329.Sh_2211_Ztautau_LH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700330.Sh_2211_Ztautau_LH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700331.Sh_2211_Ztautau_LH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700332.Sh_2211_Ztautau_HH_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700333.Sh_2211_Ztautau_HH_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700334.Sh_2211_Ztautau_HH_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700335.Sh_2211_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700336.Sh_2211_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700337.Sh_2211_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
]


###############################################################
###              W+jets samples 
###############################################################
TopExamples.grid.Add("PHYS_Wjets_Sh2211_r22_mc20a").datasets = [
	"mc20_13TeV:mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13167_p5631",
]

TopExamples.grid.Add("PHYS_Wjets_Sh2211_r22_mc20d").datasets = [
	"mc20_13TeV:mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13144_p5631",
]

TopExamples.grid.Add("PHYS_Wjets_Sh2211_r22_mc20e").datasets = [
	"mc20_13TeV.700338.Sh_2211_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700339.Sh_2211_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700340.Sh_2211_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700341.Sh_2211_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700342.Sh_2211_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700343.Sh_2211_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700344.Sh_2211_Wtaunu_L_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700345.Sh_2211_Wtaunu_L_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700346.Sh_2211_Wtaunu_L_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700347.Sh_2211_Wtaunu_H_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700348.Sh_2211_Wtaunu_H_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
	"mc20_13TeV:mc20_13TeV.700349.Sh_2211_Wtaunu_H_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8351_s3681_r13145_p5631",
]


###############################################################
###              Diboson samples 
###############################################################
TopExamples.grid.Add("PHYS_Diboson_r22_mc20a").datasets = [
    "mc20_13TeV:mc20_13TeV.345705.Sherpa_222_NNPDF30NNLO_ggllll_0M4l130.deriv.DAOD_PHYS.e6213_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.deriv.DAOD_PHYS.e6213_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.345718.Sherpa_222_NNPDF30NNLO_ggllvvWW.deriv.DAOD_PHYS.e6525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_PHYS.e6213_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_PHYS.e5525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_PHYS.e5525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_PHYS.e5525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_PHYS.e5525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_PHYS.e5583_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_PHYS.e5983_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_PHYS.e5525_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_PHYS.e5894_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364251.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_150.deriv.DAOD_PHYS.e5894_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364252.Sherpa_222_NNPDF30NNLO_llll_m4l300.deriv.DAOD_PHYS.e5894_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_PHYS.e5916_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_PHYS.e5916_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_PHYS.e5916_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364283.Sherpa_222_NNPDF30NNLO_lllljj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364284.Sherpa_222_NNPDF30NNLO_lllvjj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364285.Sherpa_222_NNPDF30NNLO_llvvjj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364286.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW4.deriv.DAOD_PHYS.e6055_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364287.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW6.deriv.DAOD_PHYS.e6055_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364288.Sherpa_222_NNPDF30NNLO_llll_lowMllPtComplement.deriv.DAOD_PHYS.e6096_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364289.Sherpa_222_NNPDF30NNLO_lllv_lowMllPtComplement.deriv.DAOD_PHYS.e6133_s3681_r13167_p5631",
    "mc20_13TeV:mc20_13TeV.364290.Sherpa_222_NNPDF30NNLO_llvv_lowMllPtComplement.deriv.DAOD_PHYS.e6096_s3681_r13167_p5631",
]
TopExamples.grid.Add("PHYS_Diboson_r22_mc20d").datasets = [
    "mc20_13TeV:mc20_13TeV.345705.Sherpa_222_NNPDF30NNLO_ggllll_0M4l130.deriv.DAOD_PHYS.e6213_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.345706.Sherpa_222_NNPDF30NNLO_ggllll_130M4l.deriv.DAOD_PHYS.e6213_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.345718.Sherpa_222_NNPDF30NNLO_ggllvvWW.deriv.DAOD_PHYS.e6525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.345723.Sherpa_222_NNPDF30NNLO_ggllvvZZ.deriv.DAOD_PHYS.e6213_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363355.Sherpa_221_NNPDF30NNLO_ZqqZvv.deriv.DAOD_PHYS.e5525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_PHYS.e5525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363357.Sherpa_221_NNPDF30NNLO_WqqZvv.deriv.DAOD_PHYS.e5525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_PHYS.e5525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363359.Sherpa_221_NNPDF30NNLO_WpqqWmlv.deriv.DAOD_PHYS.e5583_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363360.Sherpa_221_NNPDF30NNLO_WplvWmqq.deriv.DAOD_PHYS.e5983_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.363489.Sherpa_221_NNPDF30NNLO_WlvZqq.deriv.DAOD_PHYS.e5525_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_PHYS.e5894_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364251.Sherpa_222_NNPDF30NNLO_llll_m4l100_300_filt100_150.deriv.DAOD_PHYS.e5894_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364252.Sherpa_222_NNPDF30NNLO_llll_m4l300.deriv.DAOD_PHYS.e5894_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_PHYS.e5916_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_PHYS.e5916_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364255.Sherpa_222_NNPDF30NNLO_lvvv.deriv.DAOD_PHYS.e5916_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364283.Sherpa_222_NNPDF30NNLO_lllljj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364284.Sherpa_222_NNPDF30NNLO_lllvjj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364285.Sherpa_222_NNPDF30NNLO_llvvjj_EW6.deriv.DAOD_PHYS.e6055_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364286.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW4.deriv.DAOD_PHYS.e6055_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364287.Sherpa_222_NNPDF30NNLO_llvvjj_ss_EW6.deriv.DAOD_PHYS.e6055_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364288.Sherpa_222_NNPDF30NNLO_llll_lowMllPtComplement.deriv.DAOD_PHYS.e6096_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364289.Sherpa_222_NNPDF30NNLO_lllv_lowMllPtComplement.deriv.DAOD_PHYS.e6133_s3681_r13144_p5631",
    "mc20_13TeV:mc20_13TeV.364290.Sherpa_222_NNPDF30NNLO_llvv_lowMllPtComplement.deriv.DAOD_PHYS.e6096_s3681_r13144_p5631",
]
