import TopExamples.grid


##################################
##########     MC23a     ##########
##################################

TopExamples.grid.Add("FTAG2_Resubmit").datasets = [
	
	"mc23_13p6TeV:mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",



]




TopExamples.grid.Add("FTAG2_ttbar_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981"
]

TopExamples.grid.Add("FTAG2_ttbar_syst_mc23a").datasets = [
	# "mc23_13p6TeV:mc23_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	
	
	# "mc23_13p6TeV:mc23_13p6TeV.601399.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.601398.PhPy8EG_A14_ttbar_hdamp517p5_SingleLep.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",

	"mc23_13p6TeV:mc23_13p6TeV.601414.PhH7EG_A14_ttbar_hdamp258p75_Single_Lep.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",

	
		

]


TopExamples.grid.Add("FTAG2_Singletop_syst_mc23a").datasets = [

	"mc23_13p6TeV:mc23_13p6TeV.601624.PhPy8EG_tW_DS_dyn_dil_antitop.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601627.PhPy8EG_tW_DS_dyn_incl_antitop.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601628.PhPy8EG_tW_DS_dyn_dil_top.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601631.PhPy8EG_tW_DS_dyn_incl_top.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601456.PhPy8EG_tW_dyn_hdamp517p5_DR_dil_antitop.deriv.DAOD_FTAG2.e8551_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601458.PhPy8EG_tW_dyn_hdamp517p5_DR_dil_top.deriv.DAOD_FTAG2.e8551_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601460.PhPy8EG_tW_dyn_hdamp517p5_DR_incl_antitop.deriv.DAOD_FTAG2.e8551_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601462.PhPy8EG_tW_dyn_hdamp517p5_DR_incl_top.deriv.DAOD_FTAG2.e8551_s4162_r14622_p5981",




]



TopExamples.grid.Add("FTAG2_Diboson_syst_mc23a").datasets = [

"mc23_13p6TeV:mc23_13p6TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",

]


TopExamples.grid.Add("FTAG2_Vjet_syst_mc23a").datasets = [

"mc23_13p6TeV:mc23_13p6TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701050.Sh_2214_llvv_os.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
"mc23_13p6TeV:mc23_13p6TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",


]

TopExamples.grid.Add("FTAG2_Singletop_PowPy8_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981"
]

TopExamples.grid.Add("FTAG2_Singletop_PowPy8_DS_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.601627.PhPy8EG_tW_DS_dyn_incl_antitop.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.601631.PhPy8EG_tW_DS_dyn_incl_top.deriv.DAOD_FTAG2.e8549_s4162_r14622_p5981"
]

TopExamples.grid.Add("FTAG2_Wjets_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.700777.Sh_2214_Wenu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700778.Sh_2214_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	#"mc23_13p6TeV:mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
]

TopExamples.grid.Add("FTAG2_Zjets_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.700786.Sh_2214_Zee_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700789.Sh_2214_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700790.Sh_2214_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700792.Sh_2214_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700793.Sh_2214_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.700794.Sh_2214_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	#"mc23_13p6TeV:mc23_13p6TeV.700795.Sh_2214_Znunu_pTV2_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	#"mc23_13p6TeV:mc23_13p6TeV.700796.Sh_2214_Znunu_pTV2_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	#"mc23_13p6TeV:mc23_13p6TeV.700797.Sh_2214_Znunu_pTV2_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",

	# Don't have the Mll samples in FTAG2
	# "mc23_13p6TeV:mc23_13p6TeV.700895.Sh_2214_Zee_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700896.Sh_2214_Zee_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700897.Sh_2214_Zee_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700898.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700899.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700900.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700901.Sh_2214_Ztt_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700902.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_FTAG2.e8514_s4162_r14622_p5981",
	# "mc23_13p6TeV:mc23_13p6TeV.700903.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CVetoBVeto.derivDAOD_FTAG2.e8514_s4162_r14622_p59815",

]

TopExamples.grid.Add("FTAG2_Diboson_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.701125.Sh_2214_WlvWqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701085.Sh_2214_ZqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701055.Sh_2214_llvv_ss.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701115.Sh_2214_WlvZqq.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701040.Sh_2214_llll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701060.Sh_2214_lvvv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701090.Sh_2214_ZbbZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701105.Sh_2214_WqqZll.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701120.Sh_2214_WlvZbb.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981",
	"mc23_13p6TeV:mc23_13p6TeV.701045.Sh_2214_lllv.deriv.DAOD_FTAG2.e8543_s4162_r14622_p5981"
]



TopExamples.grid.Add("PHYS_ttbar_mc23a").datasets = [
	#add single lepton ttbar
	"mc23_13p6TeV:mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855"
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
]

#can't find dibson!

TopExamples.grid.Add("PHYS_Wjets_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.700777.Sh_2214_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700778.Sh_2214_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
]


TopExamples.grid.Add("PHYS_Zjets_mc23a").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.700786.Sh_2214_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700789.Sh_2214_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700790.Sh_2214_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700792.Sh_2214_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700793.Sh_2214_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700794.Sh_2214_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700795.Sh_2214_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700796.Sh_2214_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700797.Sh_2214_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700895.Sh_2214_Zee_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700896.Sh_2214_Zee_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700897.Sh_2214_Zee_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700898.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700899.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700900.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700901.Sh_2214_Ztt_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700902.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700903.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4162_r14622_p5855",
]


##################################
##########     MC23c     ##########
##################################

TopExamples.grid.Add("PHYS_ttbar_mc23c").datasets = [
	#add single lepton ttbar
	"mc23_13p6TeV:mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855"
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_mc23c").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
]

TopExamples.grid.Add("PHYS_Wjets_mc23c").datasets = [
	#"mc23_13p6TeV:mc23_13p6TeV.700777.Sh_2214_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700778.Sh_2214_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700779.Sh_2214_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	#"mc23_13p6TeV:mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	#"mc23_13p6TeV:mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4162_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
]

TopExamples.grid.Add("PHYS_Zjets_mc23c").datasets = [
	"mc23_13p6TeV:mc23_13p6TeV.700786.Sh_2214_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700787.Sh_2214_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700788.Sh_2214_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700789.Sh_2214_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700790.Sh_2214_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700791.Sh_2214_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700792.Sh_2214_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700793.Sh_2214_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700794.Sh_2214_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700795.Sh_2214_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700796.Sh_2214_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	"mc23_13p6TeV:mc23_13p6TeV.700797.Sh_2214_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700895.Sh_2214_Zee_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700896.Sh_2214_Zee_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700897.Sh_2214_Zee_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700898.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700899.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700900.Sh_2214_Zmumu_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700901.Sh_2214_Ztt_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700902.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
	# "mc23_13p6TeV:mc23_13p6TeV.700903.Sh_2214_Ztt_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8514_s4159_r14799_p5855",
]


##################################
##########     MC21     ##########
##################################

TopExamples.grid.Add("PHYS_ttbar_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631"
]

TopExamples.grid.Add("PHYS_ttbar_syst_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.700660.Sh_2212_ttbar_dilepton_maxHTavrgTopPT.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601415.PhH7EG_A14_ttbar_hdamp258p75_Dilep.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601399.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631"
]

TopExamples.grid.Add("PHYS_Zjets_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.700615.Sh_2212_Zee_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700616.Sh_2212_Zee_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700617.Sh_2212_Zee_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700618.Sh_2212_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700619.Sh_2212_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700620.Sh_2212_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700621.Sh_2212_Ztautau_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700622.Sh_2212_Ztautau_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700623.Sh_2212_Ztautau_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700624.Sh_2212_Znunu_pTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700625.Sh_2212_Znunu_pTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700626.Sh_2212_Znunu_pTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700697.Sh_2212_Zee_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700698.Sh_2212_Zee_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700699.Sh_2212_Zee_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700700.Sh_2212_Zmumu_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700701.Sh_2212_Zmumu_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700702.Sh_2212_Zmumu_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700703.Sh_2212_Ztt_maxHTpTV2_Mll10_40_BFilter.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700704.Sh_2212_Ztt_maxHTpTV2_Mll10_40_CFilterBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700705.Sh_2212_Ztt_maxHTpTV2_Mll10_40_CVetoBVeto.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
]

TopExamples.grid.Add("PHYS_Zjets_syst_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.601189.PhPy8EG_AZNLO_Zee.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601190.PhPy8EG_AZNLO_Zmumu.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601191.PhPy8EG_AZNLO_Ztautau.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
]

TopExamples.grid.Add("PHYS_Diboson_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.700600.Sh_2212_llll.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700601.Sh_2212_lllv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700602.Sh_2212_llvv_os.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700603.Sh_2212_llvv_ss.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700604.Sh_2212_lvvv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700605.Sh_2212_vvvv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700566.Sh_2212_WlvWqq.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700567.Sh_2212_WlvZbb.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700568.Sh_2212_WlvZqq.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700569.Sh_2212_WqqZll.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700570.Sh_2212_WqqZvv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700571.Sh_2212_ZbbZll.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700572.Sh_2212_ZqqZll.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700573.Sh_2212_ZbbZvv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700574.Sh_2212_ZqqZvv.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
]

TopExamples.grid.Add("PHYS_Wjets_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.700606.Sh_2212_Wenu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700607.Sh_2212_Wenu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700608.Sh_2212_Wenu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700609.Sh_2212_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700610.Sh_2212_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700611.Sh_2212_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700612.Sh_2212_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700613.Sh_2212_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.700614.Sh_2212_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_PHYS.e8462_s3873_r13829_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.601348.PhPy8EG_tb_lep_antitop.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601349.PhPy8EG_tb_lep_top.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601350.PhPy8EG_tqb_lep_antitop.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601351.PhPy8EG_tqb_lep_top.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	#"mc21_13p6TeV:mc21_13p6TeV.601352.PhPy8EG_tW_dyn_DR_incl_antitop.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	#"mc21_13p6TeV:mc21_13p6TeV.601355.PhPy8EG_tW_dyn_DR_incl_top.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601353.PhPy8EG_tW_dyn_DR_dil_antitop.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601354.PhPy8EG_tW_dyn_DR_dil_top.deriv.DAOD_PHYS.e8453_s3873_r13829_p5631",
]

TopExamples.grid.Add("PHYS_Singletop_PowPy8_DS_r22_mc21").datasets = [
	"mc21_13p6TeV:mc21_13p6TeV.601455.PhPy8EG_tW_DS_dil_antitop.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601457.PhPy8EG_tW_DS_dil_top.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601459.PhPy8EG_tW_DS_incl_antitop.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
	"mc21_13p6TeV:mc21_13p6TeV.601461.PhPy8EG_tW_DS_incl_top.deriv.DAOD_PHYS.e8472_s3873_r13829_p5631",
]

