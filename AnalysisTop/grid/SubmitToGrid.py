#!/usr/bin/env python
import TopExamples.grid
#import DerivationTags
import Data_Samples, Data_Samples_Run2
import MC_Samples, MC_Samples_Run2

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.gridUsername  = 'yuhui'			#Your username here!
config.suffix        = 'AT24_2024.04_v1.0'	#the last number is meant to be the date of submitting
config.excludedSites = ''				#e.g., ANALY_ABC,ANALY_XYZ -> ANALY_GLASGOW_SL6
config.noSubmit      = False
config.mergeType     = 'Default'			#'None', 'Default' or 'xAOD'
config.CMake	     = True
config.memory	     = ''
config.otherOptions  = '--allowTaskDuplication'
config.maxNFilesPerJob = '5'
#config.otherOptions  = '--extFile=./FTAG2_PRW_mc16a_18-11-12_AF.root,./FTAG2_PRW_mc16a_18-12-19_FS.root,./FTAG2_PRW_mc16d_18-11-12_FS.root,./FTAG2_PRW_mc16d_18-11-12_AF.root,./FTAG2_PRW_mc16e_18-11-30_FS.root,./FTAG2_PRW_mc16e_18-11-12_AF.root,./libTTbarBJetCalib.so,./NTUP_PILEUP.mc16a.root,./NTUP_PILEUP.mc16d.root,./NTUP_PILEUP.mc16e.root,NTUP_410472_r22.root  --nGBPerJob=8'
### DECLARE WHAT YOU WANT TO SUBMIT!
Submit1516 = False
Submit17 = False
Submit18 = False

Submit22 = True
Submit23 = False
### If your job fails/crashes because of memory issues, instead of resubmitting a new job, consider using pbook -c "retry(id,newOpts={'maxNFilesPerJob':'1'})"
###############################################################################


# Run3
if Submit23:
	config.settingsFile = 'PFlow/pflow.2023.config.mc23.txt'
	names = [
		'PHYS_ttbar_mc23c',
		'PHYS_Zjets_mc23c',
		'PHYS_Wjets_mc23c',
		'PHYS_Singletop_PowPy8_mc23c',
		'Data23',
	]

	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)

if Submit22:
	config.settingsFile = 'PFlow/pflow.2022.config.mc23.txt'
	names = [
		
		
		
		
		
		
		
		'FTAG2_ttbar_syst_mc23a',
		# 'FTAG2_Singletop_syst_mc23a',
		# 'FTAG2_Diboson_syst_mc23a',
		# 'FTAG2_Vjet_syst_mc23a',


		# 'FTAG2_Singletop_PowPy8_DS_mc23a',
		# 'FTAG2_Resubmit',	
	    # 'Data22',

		# 'FTAG2_Singletop_PowPy8_mc23a',
		# 'FTAG2_Wjets_mc23a',
		# 'FTAG2_Zjets_mc23a',
		# 'FTAG2_Diboson_mc23a',
		# 'Data22',
		# 'FTAG2_ttbar_mc23a',
	]

	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)


# Run2
if Submit18:
	config.settingsFile  = 'PFlow/pflow.run2.config.mc20.data18.txt'
	names = [
        #"FTAG2_ttbar_r22_mc20e",
        #"FTAG2_Zjets_Sh2211_r22_mc20e",
		#"FTAG2_Wjets_Sh2211_r22_mc20e",
		"FTAG2_Singletop_PowPy8_r22_mc20e",
  		#"FTAG2_Diboson_r22_mc20e",
		#"FTAG2_Data18_r22",
  
		#"PHYS_ttbar_HW7_fastsim_r22_mc20e",
		#"PHYS_Singletop_PowPy8_DS_r22_mc20e",
  		#"PHYS_ttbar_PhPy8_hdamp3mtop_mc20e",
    
		# Problematic samples↓
		#"PHYS_ttbar_fastsim_r22_mc20e",
		#"PHYS_ttbar_aMcAtNloPy8_mc20e",
		#"PHYS_Singletop_fastsim_r22_mc20e",
		#"PHYS_Singletop_MGPy8_fastsim_r22_mc20e",
		#"PHYS_Singletop_HW7_fastsim_r22_mc20e",
		
  		#"PHYS_Zjets_r22_mc20e",
		#"PHYS_Znunu_r22_mc20e",
		#"PHYS_Wjets_r22_mc20e",
		]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)

if Submit17:
	config.settingsFile  = 'PFlow/pflow.run2.config.mc20.data17.txt'
	names = [
     	#"FTAG2_ttbar_r22_mc20d",
        #"FTAG2_Zjets_Sh2211_r22_mc20d",
		#"FTAG2_Wjets_Sh2211_r22_mc20d",
		"FTAG2_Singletop_PowPy8_r22_mc20d",
  		#"FTAG2_Diboson_r22_mc20d",
		#"FTAG2_Data17_r22",
    
     	#"PHYS_ttbar_r22_mc20d",
        #"PHYS_Zjets_Sh2211_r22_mc20d",
		#"PHYS_Wjets_Sh2211_r22_mc20d",
		#"PHYS_Singletop_PowPy8_r22_mc20d",
  		#"PHYS_Diboson_r22_mc20d",
		#"Data17_r22",
  
  		#"PHYS_ttbar_HW7_fastsim_r22_mc20d",
		#"PHYS_Singletop_PowPy8_DS_r22_mc20d",
  		#"PHYS_ttbar_PhPy8_hdamp3mtop_mc20d",
  
		# Problematic samples↓
  		#"PHYS_ttbar_fastsim_r22_mc20d",
		#"PHYS_ttbar_aMcAtNloPy8_mc20d",
		#"PHYS_Singletop_fastsim_r22_mc20d",
  		#"PHYS_Singletop_MGPy8_fastsim_r22_mc20d",
		#"PHYS_Singletop_HW7_fastsim_r22_mc20d",
		
  		#"PHYS_Zjets_r22_mc20d",
		#"PHYS_Znunu_r22_mc20d",
		#"PHYS_Wjets_r22_mc20d",
		]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)

if Submit1516:
	config.settingsFile  = 'PFlow/pflow.run2.config.mc20.data1516.txt'
	names = [
    	"FTAG2_ttbar_r22_mc20a",
        "FTAG2_Zjets_Sh2211_r22_mc20a",
		"FTAG2_Wjets_Sh2211_r22_mc20a",
		"FTAG2_Singletop_PowPy8_r22_mc20a",
  		#"FTAG2_Diboson_r22_mc20a",
		#"FTAG2_Data15_r22",
  		#"FTAG2_Data16_r22",
    
     	#"PHYS_ttbar_r22_mc20a",
        #"PHYS_Zjets_Sh2211_r22_mc20a",
		#"PHYS_Wjets_Sh2211_r22_mc20a",
		#"PHYS_Singletop_PowPy8_r22_mc20a",
  		#"PHYS_Diboson_r22_mc20a",
		#"Data15_r22",
  		#"Data16_r22",
  
  		#"PHYS_ttbar_HW7_fastsim_r22_mc20a",
		#"PHYS_Singletop_PowPy8_DS_r22_mc20a",
  		#"PHYS_ttbar_PhPy8_hdamp3mtop_mc20a",
  
 		# Problematic samples↓
		#"PHYS_ttbar_fastsim_r22_mc20a",
		#"PHYS_ttbar_aMcAtNloPy8_mc20a",
		#"PHYS_Singletop_fastsim_r22_mc20a",
  		#"PHYS_Singletop_MGPy8_fastsim_r22_mc20a",
		#"PHYS_Singletop_HW7_fastsim_r22_mc20a",
  
  		#"PHYS_Zjets_r22_mc20a",
		#"PHYS_Znunu_r22_mc20a",
		#"PHYS_Wjets_r22_mc20a",
		]
	samples = TopExamples.grid.Samples(names)
	TopExamples.grid.submit(config, samples)
