import TopExamples.grid
import TopExamples.ami


###
#FTAG2 Preliminary Nominal Samples Until DAOD_FTAG2 will be produced
###

TopExamples.grid.Add('FTAG2_Data15_r22').datasets = [
    "data15_13TeV:data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_FTAG2.grp15_v01_p5981"
]

TopExamples.grid.Add('FTAG2_Data16_r22').datasets = [
    "data16_13TeV:data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_FTAG2.grp16_v01_p5981",
]

TopExamples.grid.Add('FTAG2_Data17_r22').datasets = [
    "data17_13TeV:data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_FTAG2.grp17_v01_p5981",
]

TopExamples.grid.Add('FTAG2_Data18_r22').datasets = [
    "data18_13TeV:data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_FTAG2.grp18_v02_p5981",
]



TopExamples.grid.Add('Data15_r22').datasets = [
    "data15_13TeV:data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5631"
]

TopExamples.grid.Add('Data16_r22').datasets = [
    "data16_13TeV:data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5631",
]

TopExamples.grid.Add('Data17_r22').datasets = [
    "data17_13TeV:data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5631",
]

TopExamples.grid.Add('Data18_r22').datasets = [
    "data18_13TeV:data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5631",
]
