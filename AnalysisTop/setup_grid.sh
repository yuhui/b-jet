setupATLAS

if [[ ! -f ./build ]];
then
    mkdir build
fi

cd build
acmSetup AnalysisBase,24.2.38,here
#acmSetup AnalysisBase,25.2.6,here
acm compile
cd ..

lsetup panda
lsetup rucio
lsetup pyami

if [[ ! -e /tmp/x509* ]]
then
    voms-proxy-init -voms atlas
fi