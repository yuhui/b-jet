In this Folder are configuration files for the TTBar analysis with AnalysisTop for the b-jet efficiency calibration.

In grid/ are the tools for a run on FTAG2/PHYS derivations with AnalysisTop to produce Ntuples on the Grid.

In source/TTBarBJetCalib/ is the Rootcore Package for a Customised Event Saver to save all the Data needed for calibrating the b-jet efficiency.

If you want to run this Analysis **locally**:
1. source setup_local.sh
2. mkdir run && cd run
3. create a text file (called e.g. "input.txt") that contains the full path to your input file(s)
4. create a AnalysisTop config file e.g. the one at grid/PFlow/pflow.2022.config.mc23.txt
5. top-xaod pflow.2022.config.mc23.txt input.txt

If you want to run this Analysis on the **Grid** (Panda):
1. source setup_grid.sh
2. cd grid
3. sample lists are located at MC_Samples.py and Data_Samples.py
4. run with SubmitToGrid.py
