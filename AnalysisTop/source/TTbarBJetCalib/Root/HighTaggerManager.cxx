#include "TTbarBJetCalib/ObjectManager.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"

#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;

ClassImp(top::HighTaggerManager);
namespace top {
  HighTaggerManager::HighTaggerManager() :
    m_DL1d_pb              (),
    m_DL1d_pc              (),
    m_DL1d_pu              (),
    m_GN1_pb               (),
    m_GN1_pc               (),
    m_GN1_pu               (),
    m_GN2_pb               (),
    m_GN2_pc               (),
    m_GN2_pu               ()
  { 
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void HighTaggerManager::DeclareBranches(top::TreeManager* tree)
  {
    tree->makeOutputVariable(m_DL1d_pb              , "DL1d_pb");
    tree->makeOutputVariable(m_DL1d_pc              , "DL1d_pc");
    tree->makeOutputVariable(m_DL1d_pu              , "DL1d_pu");
    tree->makeOutputVariable(m_GN1_pb              , "GN1_pb");
    tree->makeOutputVariable(m_GN1_pc              , "GN1_pc");
    tree->makeOutputVariable(m_GN1_pu              , "GN1_pu");
    tree->makeOutputVariable(m_GN2_pb              , "GN2_pb");
    tree->makeOutputVariable(m_GN2_pc              , "GN2_pc");
    tree->makeOutputVariable(m_GN2_pu              , "GN2_pu");
  }
  ///-- saveEvent - run for every systematic and every event --///
  void HighTaggerManager::Fill(const top::Event& event)
  {
    m_DL1d_pb.clear();
    m_DL1d_pc.clear();
    m_DL1d_pu.clear();
    m_GN1_pb.clear();
    m_GN1_pc.clear();
    m_GN1_pu.clear();
    m_GN2_pb.clear();
    m_GN2_pc.clear();
    m_GN2_pu.clear();

    for (const auto* const jp : event.m_jets) {
      // retrieve btagging info
      const xAOD::BTagging* btag = xAOD::BTaggingUtilities::getBTagging(*jp);
      
      if(btag){
	double pu,pc,pb;
	btag->pu("DL1dv01",pu);
	btag->pc("DL1dv01",pc);
	btag->pb("DL1dv01",pb);
	m_DL1d_pb.push_back(pb);
	m_DL1d_pc.push_back(pc);
	m_DL1d_pu.push_back(pu);

  btag->pu("GN120220509",pu);
	btag->pc("GN120220509",pc);
	btag->pb("GN120220509",pb);
	m_GN1_pb.push_back(pb);
	m_GN1_pc.push_back(pc);
	m_GN1_pu.push_back(pu);

  btag->pu("GN2v01",pu);
  btag->pc("GN2v01",pc);
  btag->pb("GN2v01",pb);
  m_GN2_pb.push_back(pb);
  m_GN2_pc.push_back(pc);
  m_GN2_pu.push_back(pu);
      }

    }
  }

  HighTaggerManager::~HighTaggerManager()
  {
  }
}
