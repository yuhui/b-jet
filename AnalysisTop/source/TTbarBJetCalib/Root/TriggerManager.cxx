#include "TTbarBJetCalib/ObjectManager.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "AthContainers/AuxElement.h"
#include <TRandom3.h>

#include <iostream>

using xAOD::IParticle;


// Loop is by default over events. TriggerManager is called from CostumEventSaver or PflowEventSaver
//CostumEventSaver needs to be activated from the job option. This works now with PFlow. 



ClassImp(top::TriggerManager);

namespace top {
  TriggerManager::TriggerManager() //:
  {
    // m_HLTJetTriggerList = {
    // "HLT_mu26_ivarmedium_j20_pf_ftf_L1MU14FCH", 
    // "HLT_mu24_ivarmedium_j20_pf_ftf_L1MU14FCH",
    // "HLT_e26_lhtight_ivarloose_j20_pf_ftf_L1eEM26M",
    // };
  }

  ///-- initialize - done once at the start of a job before the loop over events --///
  void TriggerManager::DeclareBranches(top::TreeManager* tree)
  {
    tree->makeOutputVariable(m_HLT_Pflowjet_pt        , "hlt_jet_pt");
    tree->makeOutputVariable(m_HLT_Pflowjet_eta       , "hlt_jet_eta");
    tree->makeOutputVariable(m_HLT_Pflowjet_phi       , "hlt_jet_phi");
    tree->makeOutputVariable(m_HLT_Pflowjet_e         , "hlt_jet_e");
    tree->makeOutputVariable(m_HLT_Pflowjet_m         , "hlt_jet_m");
    tree->makeOutputVariable(m_HLT_Pflowjet_is_jvt_pass       , "hlt_jet_isjvtpass");
    tree->makeOutputVariable(m_HLT_Pflowjet_jvtScore       , "hlt_jet_jvtscore");

    tree->makeOutputVariable(m_L1Jet_eta            , "L1Jet_eta");
    tree->makeOutputVariable(m_L1Jet_phi            , "L1Jet_phi");
    tree->makeOutputVariable(m_L1Jet_et8x8          , "L1Jet_et8x8");

    tree->makeOutputVariable(m_HLT_Pflowjet_DL1d_pb          , "hlt_bjet_DL1d20211216_pb");
    tree->makeOutputVariable(m_HLT_Pflowjet_DL1d_pc          , "hlt_bjet_DL1d20211216_pc");
    tree->makeOutputVariable(m_HLT_Pflowjet_DL1d_pu          , "hlt_bjet_DL1d20211216_pu");
    tree->makeOutputVariable(m_HLT_Pflowjet_GN1_pb           , "hlt_bjet_GN120220813_pb");
    tree->makeOutputVariable(m_HLT_Pflowjet_GN1_pc           , "hlt_bjet_GN120220813_pc");
    tree->makeOutputVariable(m_HLT_Pflowjet_GN1_pu           , "hlt_bjet_GN120220813_pu");
    //trigger decisions branch
    // tree->makeOutputVariable(m_JetTriggerDecisions          , "test_e140_lhloose_L1EM22VHI");

    //==== TRIGGER TOOLS INITIALIZATION ====
    //Initialize and configure trigger tools 
    m_TrigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool"); // gives us access to the meta-data
    top::check(m_TrigConfigTool->initialize(),"Failed to initialize TrigConfTool");
    ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_TrigConfigTool );
    m_TrigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->setProperty( "AcceptMultipleInstance", "1" ),"Failed to set Property for TrigDecisionTool");
    top::check(m_TrigDecisionTool->initialize(),"Failed to initialize TrigDecisionTool");

  }

  //Decorate Jvt decision for jets
  // const SG::AuxElement::Decorator<float> JvtPass("Jvt");
  SG::AuxElement::ConstAccessor<float> JvtPass{"Jvt"};

  
  ///-- saveEvent - run for every systematic and every event --///
  void TriggerManager::Fill(const top::Event& event,xAOD::TEvent* eventStore)
  {
      m_HLT_Pflowjet_pt.clear();
      m_HLT_Pflowjet_eta.clear();
      m_HLT_Pflowjet_phi.clear();
      m_HLT_Pflowjet_e.clear();
      m_HLT_Pflowjet_m.clear();
      m_HLT_Pflowjet_jvtScore.clear();
      m_HLT_Pflowjet_is_jvt_pass.clear();
      //L1 jets
      m_L1Jet_eta.clear();
      m_L1Jet_phi.clear();
      m_L1Jet_et8x8.clear();
      m_HLT_Pflowjet_DL1d_pb.clear(); 
      m_HLT_Pflowjet_DL1d_pc.clear(); 
      m_HLT_Pflowjet_DL1d_pu.clear(); 
      m_HLT_Pflowjet_GN1_pb.clear();  
      m_HLT_Pflowjet_GN1_pc.clear();  
      m_HLT_Pflowjet_GN1_pu.clear();  




    	//set lists of triggers

      //==== RETRIEVE TRIGGER LEVEL VARIABLES ====
        std::string m_TriggerChain;

      //Trigger Decisions
      // for (unsigned int itrig = 0; itrig < m_HLTJetTriggerList.size(); itrig++) {
        // m_TriggerChain = "HLT_mu24_ivarmedium_L1MU14FCH";
	      // auto cg = m_TrigDecisionTool->getChainGroup(m_TriggerChain);
        // std::cout << "cg: " << cg->isPassed() << std::endl;
        // m_JetTriggerDecisions.push_back(cg->isPassed());
      // }



      //LV1
      const xAOD::JetRoIContainer* m_L1Jets = nullptr;
      top::check(eventStore->retrieve(m_L1Jets, "LVL1JetRoIs"),"Failed to retrieve container LVL1JetRoIs");
      for(auto jptr : *m_L1Jets){
        // no pt for L1
        m_L1Jet_eta.push_back(jptr->eta());
	      m_L1Jet_phi.push_back(jptr->phi());
        m_L1Jet_et8x8.push_back(jptr->et8x8());
      }

      // Retrieve ALL hlt jet kinematics
      const xAOD::JetContainer* m_HLTJets = nullptr;
                                                  
      top::check(eventStore->retrieve(m_HLTJets, "HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf"),"Failed to retrieve container HLT_AntiKt4EMPFlowJets");
      //top::check(eventStore->retrieve(m_HLTJets, "HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf_bJets"),"Failed to retrieve container HLT_AntiKt4EMPFlowJets");
      for(auto hltjet : *m_HLTJets){
        if (hltjet->pt()>15000){
        m_HLT_Pflowjet_pt.push_back(hltjet->pt());
        m_HLT_Pflowjet_eta.push_back(hltjet->eta());
        m_HLT_Pflowjet_phi.push_back(hltjet->phi());
        m_HLT_Pflowjet_e.push_back(hltjet->e());
        m_HLT_Pflowjet_m.push_back(hltjet->m());
        // retrive jvt score through the defined accessor
        m_HLT_Pflowjet_jvtScore.push_back(JvtPass(*hltjet));
        m_HLT_Pflowjet_is_jvt_pass.push_back(JvtPass(*hltjet) > 0.5);
        }
        
      }

      const xAOD::BTaggingContainer* m_HLTBTags = nullptr;
       top::check(eventStore->retrieve(m_HLTBTags, "HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf_BTagging"),"Failed to retrieve container HLT_AntiKt4EMPFlowJet_Btagging");
      for(auto hltbjet : *m_HLTBTags){
        // 
          double pu,pc,pb;

          hltbjet->pu("DL1d20211216",pu);
	        hltbjet->pc("DL1d20211216",pc);
	        hltbjet->pb("DL1d20211216",pb);

        	m_HLT_Pflowjet_DL1d_pb.push_back(pb);
	        m_HLT_Pflowjet_DL1d_pc.push_back(pc);
	        m_HLT_Pflowjet_DL1d_pu.push_back(pu);

          hltbjet->pu("GN120220813",pu);
	        hltbjet->pc("GN120220813",pc);
	        hltbjet->pb("GN120220813",pb);
	        m_HLT_Pflowjet_GN1_pb.push_back(pb);
	        m_HLT_Pflowjet_GN1_pc.push_back(pc);
	        m_HLT_Pflowjet_GN1_pu.push_back(pu);

      }
  }
  
  TriggerManager::~TriggerManager()
  {
    delete m_TrigConfigTool;
    delete m_TrigDecisionTool;
  }
}
