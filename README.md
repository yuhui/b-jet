End to End Running Instructions (for the impatient)
-------------------------------------------------------

[AnalysisTop](AnalysisTop/):
I strongly reccommend that the ntuple production is run on the grid. First setup the package via ```source setup_grid.sh```. Data samples are stored in [Data_Samples.py](AnalysisTop/grid/Data_Samples.py) and MC within [MC_Samples.py](AnalysisTop/MC_Samples.py). These files must be updated when we wish to run with newer derivations. The AnalysisTop config file is located at [pflow.run3.config.mc21.txt](AnalysisTop/grid/PFlow/pflow.run3.config.mc21.txt). This is where the basic event selection occurs i.e. jet $p_T$ cuts, applying Good Run Lists and lepton triggets. The code can be ran using the [SubmitToGrid.py](AnalysisTop/grid/SubmitToGrid.py) script. Within this script some things you might want to update is the ```config.gridUsername``` to your own grid name and the ```config.suffix``` to include the version of AnalysisBase used and the date. This script also contains a list of boolean flags to decide which year to run, if you wish to run data from 2022 only, set ```Submit22 = True``` and for all other years, set the corresponding flag to False.

[FinalSelection](FinalSelection/):
As with the first step, you need to first run ```source setup.sh``` to setup the package. The ntuples produced by AnalysisTop need to be moved from the grid scratch disks to more permanent storage. Get in touch with the FTAG calibration team on doing so. Then, get a list of all the ntuples using ```rucio ls``` and run the ```RucioListBuilder.py``` and ```LocalFolderBuilder.py``` scripts (TODO: update and combine these scripts together). These scripts will create the correct directory structure used for the final selection. The directories will be stored in the location given in the (options_file.py)[FinalSelection/options_file.py]. Make sure to update the paths in the ```__init__``` of the ```options_container```. The final selection can be run from [submit_to_condor.py](FinalSelection/htcondor_submit/submit_to_condor.py). Note: this requires use of your x509 proxy. This can be done by running ```voms-proxy-init -voms atlas``` and this will create a proxy within the ```tmp/``` directory, copy this file to this directory. Within the submission script there are boolean flags to determine which systematics to run. To run only the nominal fit, set ```run_nominal = True``` and all other flags to False. The output of the final selection is a set of histograms for each sample that are used for the likelihood fit.

[likelihood_fit](likelihood-fit/):
The histograms from the final selection must first be combined together using the [combine_histos_for_fit_with_syst.py](likelihood-fit/combine_histos_for_fit_with_syst.py). The fit can then be ran using the [calculate_all_fits.py](likelihood-fit/calculate_all_fits.py). Note: for both of these scripts, if you are running without the systematics, parts of both scripts will need to be commented out.


Method in short
---------------
1. Select exactly two jets and 1 electron + 1 muon with opposite signs (clean in $t \bar t$ events)
2. Define $m_{lj}$ per jet as the invariant mass of the lepton + jet combinations minimizing the squared sum of both $m_{lj}$ combinations in the event. So calculate the two possible ways of combining lepton + jet and take the one which has the minimum $m_{lj1}²+m_{lj2}² $
4. Analysis Strategy (combined fit):
	- include events with $m_{lj}$ above the top mass as CR regions and extract flavor fractions here (template fit).
	- take lf-miss-tag rate and $m_{lj}$ templates from MC.
	- extract b-tagging efficiency from data in one big combined fit. 


![alt text](likelihood-fit/m_lj_region_plot.png "Region definition with m_lj of both jets in the event")