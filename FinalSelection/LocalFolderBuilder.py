import ROOT
import os
import glob
from options_file import *
import argparse

# Write the folder you want to create here
folderToCreate = options.version_tag

parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('-f',action='store',dest="folders",
                    help='Folders to search for the samples')
args = parser.parse_args()

folderToCreate=options.input_selection_dir

if options.data_name == "dataRun3" or options.data_name == "data22" or options.data_name =="data23":
    nominal={
        'Zjets_Sherpa22': [
            "*700615.Sh*", "*700616.Sh*", "*700617.Sh*", "*700618.Sh*", "*700619.Sh*",
            "*70062*.Sh*", "*70069*.Sh*", "*70070*.Sh*", "*700786*.Sh*", "*700787*.Sh*",
            "*700788*.Sh*", "*700789*.Sh*", "*70079*.Sh*",
            "*70089*.Sh*", "*70090*.Sh*"
            ],
        'Zjets_PowPy8' : ["*601189*", "*601190*", "*601191*"],
        'Wjets_Sherpa22': [
            "*700606.Sh*", "*700607.Sh*", "*700608.Sh*", "*700609.Sh*", "*700610.Sh*",
            "*700611.Sh*", "*700612.Sh*", "*700613.Sh*", "*700614.Sh*", "*70077*.Sh*",
            "*700780*.Sh*", "*700781*.Sh*", "*700782*.Sh*", "*700783*.Sh*", "*700784*.Sh*",
            "*700785*.Sh*"
            ],
        'Singletop_PowPy8': ["*60134*.PhPy8EG*", "*60135*.PhPy8EG*"],
        'Singletop_PowPy8_DS' : ["*60145*.PhPy8EG*", "*601461.PhPy8EG*"],
        'Diboson_Sherpa22': ["*700678.Sh*", "*700605.Sh*", "*700604.Sh*", "*700603.Sh*", "*700602.Sh*", "*700601.Sh*", "*700600.Sh*", "*70056*.Sh*", "*70057*.Sh*"],
        'ttbar_PhPy8_nominal' : ["*410472.PhPy8EG.*_s3681*",  "*601230.PhPy8EG.*"],
        'ttbar_Sherpa22': ["*700660*"],
        'ttbar_HW7': ["*601415*"],
        'data1516' : ["*grp15*","*grp16*"],
        'data17' : ["*grp17*"],
        'data18' : ["*grp18*"],
        'data22' : ["*grp22*"],
        'data23' : ["*grp23*"],
        "data15161718" : ["*grp15*","*grp16*","*grp17*","*grp18*"]
    }
else:
    nominal={
        # nominal
        'ttbar_PhPy8_nominal' : ["*410472.PhPy8EG.*_s3681*"],
        'Zjets_Sherpa22' : ["*70032*","*700330*","*700331*","*700332*","*700333*","*700334*","*700335*","*700336*","*700337*"],  
        'Wjets_Sherpa22' : ["*700339*","*700340*","*700341*","*700342*","*700343*","*700344*","*700345*","*700346*","*700347*","*700348*","*700349*"], 
        'Singletop_PowPy8' : ["*410648*s3681*","*410649*s3681*","*410644*s3681*","*410645*s3681*","*410658*s3681*","*410659*s3681*"], 
        'Diboson_Sherpa22' : ["*364250*","*364253*","*364254*","*36335*","*363360*","*363489*","*364255*","*36428*","*364290*","*345705*","*345706*","*345723*","*345718*","*364251*","*364252*"],
        'data1516' : ["*grp15*","*grp16*"],
        'data17'   : ["*grp17*"],
        'data18'   : ["*grp18*"],
        'dataRun2' : ["*grp15*","*grp16*","*grp17*","*grp18*"],
        
        # alternative coming soon..
    }

print("OPTIONS DATA NAME  : ", options.data_name)
input_container=args.folders.split(",")
if options.data_name == "data1516":
    mctag_list = ["r13167"]
    datatag=["*grp15*","*grp16*"]
elif options.data_name == "data17":
    mctag_list = ["r13144"]
    datatag=["*grp17*"]
elif options.data_name == "data18":
    mctag_list=["r10724"]
    datatag=["grp18"]
elif options.data_name == "data22":
    mctag_list=["r14622"]
    datatag=["grp22"]
elif options.data_name == "data23":
    mctag_list=["r14799"]
    datatag=["grp23"]
else:
    #mctag = "" 
    mctag = [] # CHANGED 

os.system("mkdir -p %s "%(folderToCreate))

for name, folders in nominal.items():
    os.system("mkdir -p %s "%(folderToCreate+"/"+name))
    globbedFolders=[]
    for folder in folders:
        for inFolder in input_container:
            if not "data" in name:
                for mctag in mctag_list:
                    globbedFolders.extend( glob.glob(inFolder+"/"+folder+"*%s*.txt"%mctag) )
            else:
                if any( x in folder for x in datatag):
                    globbedFolders.extend(glob.glob(inFolder+"/"+folder))
                else:
                    continue

    print("Sample %s has the following files "%name)
    fileIn=open(folderToCreate+"/"+name+".txt",'w')
    for myfile in globbedFolders:
        print("     %s"%myfile)
        fileIn.write(myfile.replace(".txt","")+"\n")
        os.system("mkdir -p "+folderToCreate+"/"+name+"/")
        os.system("cp  "+myfile+" "+folderToCreate+"/"+name+"/")
        #        fileIn2=open(folderToCreate+"/"+name+"/"+myfile.split('/')[-1]+".txt",'w')
        #listofFiles=glob.glob(myfile+"/*")
        #for myfile2 in listofFiles:
        #    fileIn2.write(myfile2+"\n")
        #fileIn2.close()
    fileIn.close()
