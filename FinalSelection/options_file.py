import ROOT
import os
import sys,glob,getpass,socket

#sample with plot options
class sample:
    _lastFillColor =0
    _fillColors=[ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1]
    _fillColors=_fillColors+list(range(1,50))
    def __init__(self, name, boot_strap_available = False, systematic= "nominal", name_short=""):
        self.name = name
        self.markerStyle = 1
        self.fillStyle = 1001
        self.lineColor = 1
        self.fillColor = sample._fillColors[sample._lastFillColor]
        sample._lastFillColor=sample._lastFillColor+1
        self.lineStyle = 1
        self.root_file_path = ""
        self.boot_strap_available = boot_strap_available
        self.systematic = systematic
        self.systematic_command="nominal"
        if name_short=="":
            name_short=name
        self.name_short=name_short
        if systematic=="nominal":
            self.systematic_name =name_short   #unique name of systematic in case sample is only used to calcualte it.
        else:
            self.systematic_name =name_short+"_"+systematic
            self.systematic_command=systematic
        self.do_bootstrap=boot_strap_available  
        #TODO: Update these with new ttbar DSIDs in the future once light-jet SFs are available
        if "Py8" in name:
            self.hadronization=410470 #601230
        if "Sh" in name:
            self.hadronization=410250 #700660
        if "HW7" in name:
            self.hadronization=410250 #601415

    def setHistOptions(self,hist):
        hist.SetMarkerStyle(self.markerStyle)
        hist.SetFillStyle(self.fillStyle);
        hist.SetLineColor(self.lineColor);
        hist.SetFillColor(self.fillColor);
        hist.SetLineStyle(self.lineStyle);
    def addToLegend(self,hist,legend):
        legend.AddEntry(hist,self.name[6:],"F")


class channel:
    def __init__(self,name="ee_OS_J2"):
        self.name = name

class systematic:
    def __init__(self,systematic_name="nominal",do_bootstrap=False, systematic_command=""):
        self.systematic_name = systematic_name  #unique name of systematic
        self.do_bootstrap=do_bootstrap       #if systematic appears bumpy- you can calc mc stat unc of systematic here and smooth it later
        if systematic_command == "":
            self.systematic_command = systematic_name
        else:
            self.systematic_command = systematic_command

class options_container:
    def __init__(self):
        self.name="Summary of options"
        self.AT_release = "r24.2.27"
        self.version_tag=self.AT_release+ "_mc23a/"
        #self.dust_dir="/eos/user/j/jabarr/Calibration/FTAGResults/"+self.version_tag
        self.dust_dir="/eos/user/y/yuhui/Online22/bofft/"

        self.proxy_path= "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/htcondor_submit/x509up_u146799"
        self.output_dir= self.dust_dir+"/histograms/"
        self.input_selection_dir= "/eos/user/y/yuhui/Online22/bofft/ntuple/"
        self.plot_dir= self.dust_dir+"/plots/"
        self.condor_dir= self.dust_dir+"/htc_submit/"

        # Sample list. Change names here!!!
        self.data_name = "data22"

        self.data_1516_lumi = 32988.1 + 3219.56
        self.data_17_lumi = 44307.4
        self.data_18_lumi = 59937.2
        self.data_22_lumi = 26071.4 #29049.3 #11925.1
        self.data_23_lumi = 0 # for now
        
        if self.data_name == "data1516":
            data_lumi = self.data_1516_lumi
        elif self.data_name == "data17":
            data_lumi = self.data_17_lumi
        elif self.data_name == "data18": 
            data_lumi = self.data_18_lumi
        elif self.data_name == "data22":
            data_lumi = self.data_22_lumi
        elif self.data_name == "data23":
            data_lumi = self.data_23_lumi
            self.data_CME = "13.6"
        elif self.data_name == "dataRun2":
            data_lumi = self.data_1516_lumi + self.data_17_lumi + self.data_18_lumi
            self.data_CME = "13.6"
        elif self.data_name == "dataRun3":
            data_lumi = self.data_22_lumi 
            self.data_CME = "13.6"
        else:
            raise ValueError('No such data_name:'+data_name)
        
        self.data_lumi = data_lumi
            

        self.ttb_sample=sample('ttbar_PhPy8_nominal',True,"nominal","ttbar")
        self.ttb_sample.fillColor=0
        self.singleTop_sample=sample('Singletop_PowPy8', True,"nominal","singletop");
        self.ZJets_sample=sample('Zjets_Sherpa22', True, "nominal", "Zjets");
        self.Diboson_sample=sample('Diboson_Sherpa22', True, "nominal", "Diboson");
        self.Wjets_sample=sample('Wjets_Sherpa22', True, "nominal", "Wjets")

        other_s=[self.ZJets_sample,self.Wjets_sample,self.Diboson_sample]
        self.other_samples=list(other_s)
        self.nominal_samples = [self.ZJets_sample, self.Wjets_sample, self.singleTop_sample, self.ttb_sample,self.Diboson_sample]

        for s in self.nominal_samples:
             s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

    
        #options for the combine plots makro (obsolete?)
        #the channel names. change names here!!
        self.channels=[channel('emu_OS_J2'),channel('emu_OS_J3'),channel('ee_OS_J2_aCuts'),channel('ee_OS_J3_aCuts'),channel('mumu_OS_J2_aCuts'),channel('mumu_OS_J3_aCuts')]
        self.try_cut_ratio_in_xxcon_plot=0  #=0 to have now cutraio label

        ##set plot limits
        self.plot_lim_standrad=0#[2,20e4]
        self.plot_lim_pt=0#[2,20e3]
        self.plot_lim_m=0#[2,20e3]
        self.plot_lim_mT=0#[2,20e3]

        #settings for the systematic uncertainties
        ##ttbar:
        self.ttbar_syst_samples = [
            sample('ttbar_PhPy8',True,"nominal", "ttbar_PhPy8"),
            # sample('ttbar_HW7',True,"nominal", "ttbar_HW7"),
            #sample('ttbar_Sherpa22',True,"nominal", "ttbar_Sherpa2212"),
            # sample('ttbar_HW7',True,"nominal", "ttbar_HW7"), # alternative PS and hadronisation
            # sample('ttbar_PhPy8_pthard',True,"nominal", "ttbar_PhPy8_pthard"), # alternative ME
            # sample('ttbar_PhPy8_hdamp',True,"nominal", "ttbar_PhPy8_hdamp"), # hdamp=3.0 m_top
        ]

        for s in self.ttbar_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        self.ttbar_rad_up_sample=sample('ttbar_PhPy8_nominal', True, "weight_mc_rad_UP", "ttbar_PhPy8")
        #self.ttbar_rad_up_sample=sample('FTAG2_ttbar_PhPy8_hdamp3mtop', True, "weight_mc_rad_UP", "ttbar_PhPy8_hdamp")
        self.ttbar_rad_down_sample=sample('ttbar_PhPy8_nominal', True, "weight_mc_rad_DOWN", "ttbar_PhPy8")
        self.ttbar_rad_samples=[
            self.ttbar_rad_down_sample,
            self.ttbar_rad_up_sample,
        ]
        self.ttbar_fsr_up_sample=sample('ttbar_PhPy8_nominal', True, "weight_mc_fsr_UP", "ttbar_PhPy8")
        self.ttbar_fsr_down_sample=sample('ttbar_PhPy8_nominal', True, "weight_mc_fsr_DOWN", "ttbar_PhPy8")
        self.ttbar_fsr_samples=[
            self.ttbar_fsr_down_sample,
            self.ttbar_fsr_up_sample,
        ]
        self.ttbar_pdf_systematics=[]
        self.ttbar_qcd_systematics=[]

        #for np in range(10,124): NNPDF + other samples
        #    self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_pdf4lhc_np_"+str(np), False, "weight_mc_shower_np_"+str(np)) )
    #     for np in range(124,166): ## PDF4LHC PDF uncertainties.
    #         #self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_pdf4lhc_np_"+str(np), False, "weight_mc_shower_np_"+str(np)) )
    #         self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_np_"+str(np), False, "weight_mc_shower_np_"+str(np)) )

    #         #self.ttbar_pdf_systematics=[systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_np_126", False, "weight_mc_shower_np_126")]

    #     for np in range(2,6): ## QCD uncertainties
    #         self.ttbar_qcd_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_qcd_np_"+str(np), False, "weight_mc_qcd_np_"+str(np)) )

    # #   ZJets:
    #     self.ZJets_syst_samples = [
    #         sample("Zjets_Sh",True),
        for np in range(124,167): ## PDF4LHC PDF uncertainties: baseline PDF93300 + eigen sets + alpha_S variations
            #self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_pdf4lhc_np_"+str(np), False, "weight_mc_shower_np_"+str(np)) )
            str_PDFvar="PDF93300"
            if (np>124):
                if (np-124)%2:
                    str_PDFvar_sign="DOWN" # for 93301, 93303, 93305, ...
                else:
                    str_PDFvar_sign="UP" # for 93302, 93304, 93306, ...
                str_PDFvar="PDFvar"+str(int((np-124+1)/2))+"_"+str_PDFvar_sign
            #self.ttbar_pdf_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_np_"+str(np), False, str_PDFvar) )
            self.ttbar_pdf_systematics.append( systematic("weight_mc_shower_np_"+str(np), False, str_PDFvar) )

            #self.ttbar_pdf_systematics=[systematic(self.ttb_sample.name_short + "_"  "weight_mc_shower_np_126", False, "weight_mc_shower_np_126")]

        list_ScaleVar_names=["ttbarMuF_UP", "ttbarMuF_DOWN", "ttbarMuR_UP", "ttbarMuR_DOWN"]
        list_ScaleVar_np=[2,3,4,5]
        for i_np in range(len(list_ScaleVar_np)): ## QCD Scale uncertainties
            #self.ttbar_qcd_systematics.append( systematic(self.ttb_sample.name_short + "_"  "weight_mc_qcd_np_"+str(list_ScaleVar_np[i_np]), False, list_ScaleVar_names[i_np]) )
            self.ttbar_qcd_systematics.append( systematic("weight_mc_qcd_np_"+str(list_ScaleVar_np[i_np]), False, list_ScaleVar_names[i_np]) )

    #   ZJets:
        self.ZJets_syst_samples = [
            sample("Zjets_Sh",True),
        ]
        for s in self.ZJets_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        bootstrap_high_systs=[5,6]  #-there are a few which had crazy high uncertainty, better check if they are statistical significant
        self.ZJets_weight_mc_systematics=[]
        self.ZJets_qcd_systematics=[]
        self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_crossec_5per_UP", False , "weight_crossec_5per_UP"))
        self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_crossec_5per_DOWN", False , "weight_crossec_5per_DOWN"))
        for np in range(19,322):
            if np%2 == 0:
                ## In the Sherpa 2.2.11 (to bheck for Sherpa 2.2.12), the PDF uncertainties come as ME_ONLY prefix. This only applies the uncertainty to the MatrixElement step, not the Parton Shower
                ## Use all of them
                continue 
            self.ZJets_weight_mc_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_mc_shower_np_"+str(np), np in bootstrap_high_systs , "weight_mc_shower_np_"+str(np)) )

        for np in range(5,18):
            if np%2 == 0:
                ## In the Sherpa 2.2.11 (to bheck for Sherpa 2.2.12), the PDF uncertainties come as ME_ONLY prefix. This only applies the uncertainty to the MatrixElement step, not the Parton Shower
                ## Use all of them
                continue 
            self.ZJets_qcd_systematics.append(systematic(self.ZJets_sample.name_short + "_" + "weight_mc_qcd_np_"+str(np), np in bootstrap_high_systs , "weight_mc_qcd_np_"+str(np)) )
   

    #   Diboson:
        self.Diboson_syst_samples = [
            sample("FTAG2_Diboson_Sh",True), #TODO: Need PowPy8 sample
        ]
        for s in self.Diboson_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        #weight systematics  
        bootstrap_high_systs=[]  #-there are a few which had crazy high uncertainty, better check if they are statistical significant
        self.Diboson_weight_mc_systematics=[]
        self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_crossec_6per_UP", False , "weight_crossec_6per_UP"))
        self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_crossec_6per_DOWN", False , "weight_crossec_6per_DOWN"))
        # for np in range(4,113):
        #     if np != 36 or np != 51:
        #         self.Diboson_weight_mc_systematics.append(systematic(self.Diboson_sample.name_short + "_" + "weight_mc_shower_np_"+str(np), np in bootstrap_high_systs , "weight_mc_shower_np_"+str(np)) )
   

    #   singletop:
        self.singletop_syst_samples = [
            sample('Singletop_PowPy8sy', True, "nominal", "Singletop_PowPy8_DS"),
        ]
        for s in self.singletop_syst_samples:
            s.root_file_path=self.output_dir+s.name+"/"+s.name+"_nominal_combination.root"

        self.singletop_rad_up_sample=sample('Singletop_PowPy8', True, "weight_mc_rad_UP", "Singletop_PhPy8")
        self.singletop_rad_down_sample=sample('Singletop_PowPy8', True, "weight_mc_rad_DOWN", "Singletop_PhPy8")

        self.singletop_rad_samples=[
            self.singletop_rad_down_sample,
            self.singletop_rad_up_sample,
        ]
        self.singletop_fsr_up_sample=sample('Singletop_PowPy8', True, "weight_mc_fsr_UP", "Singletop_PhPy8")
        self.singletop_fsr_down_sample=sample('Singletop_PowPy8', True, "weight_mc_fsr_DOWN", "Singletop_PhPy8")
        self.singletop_fsr_samples=[
            self.singletop_fsr_down_sample,
            self.singletop_fsr_up_sample,
        ]

        bootstrap_high_systs=[]
        self.singletop_pdf_systematics=[]
        self.singletop_qcd_systematics=[]

        for np in range(122,165): # PDF4LHC, correlated between ttbar and single top
            str_PDFvar="PDF93300"
            if (np>122):
                if (np-122)%2:
                    str_PDFvar_sign="DOWN" # for 93301, 93303, 93305, ...
                else:
                    str_PDFvar_sign="UP" # for 93302, 93304, 93306, ...
                str_PDFvar="PDFvar"+str(int((np-122+1)/2))+"_"+str_PDFvar_sign
            #self.singletop_pdf_systematics.append(systematic(self.singleTop_sample.name_short +"_"+"weight_mc_shower_np_"+str(np), np in bootstrap_high_systs, str_PDFvar) )
            self.singletop_pdf_systematics.append(systematic("weight_mc_shower_np_"+str(np), np in bootstrap_high_systs, str_PDFvar) )

        list_ScaleVar_names=["SingletopMuF_UP", "SingletopMuF_DOWN", "SingletopMuR_UP", "SingletopMuR_DOWN"]
        list_ScaleVar_np=[6,5,3,2]
        for i_np in range(len(list_ScaleVar_np)):
            #self.singletop_qcd_systematics.append(systematic(self.singleTop_sample.name_short +"_"+"weight_mc_qcd_np_"+str(list_ScaleVar_np[i_np]), list_ScaleVar_np[i_np] in bootstrap_high_systs, list_ScaleVar_names[i_np]) )
            self.singletop_qcd_systematics.append(systematic("weight_mc_qcd_np_"+str(list_ScaleVar_np[i_np]), list_ScaleVar_np[i_np] in bootstrap_high_systs, list_ScaleVar_names[i_np]) )
   
   
        # detector, pileup, leptons
        self.fake_estimation = systematic("correctFakes")
        #systematics with weights in the nominal tree:
        self.inner_systematics_in_nominal_tree = [
            systematic("weight_pileup_UP"),
            systematic("weight_pileup_DOWN"),
            systematic("weight_trigger_EL_SF_Trigger_UP"),
            systematic("weight_trigger_EL_SF_Trigger_DOWN"),
            systematic("weight_leptonSF_EL_SF_Reco_UP"),
            systematic("weight_leptonSF_EL_SF_Reco_DOWN"),
            systematic("weight_leptonSF_EL_SF_ID_UP"),
            systematic("weight_leptonSF_EL_SF_ID_DOWN"),
            systematic("weight_leptonSF_EL_SF_Isol_UP"),
            systematic("weight_leptonSF_EL_SF_Isol_DOWN"),

            systematic("weight_trigger_MU_SF_STAT_UP"),
            systematic("weight_trigger_MU_SF_STAT_DOWN"),
            systematic("weight_trigger_MU_SF_SYST_UP"),
            systematic("weight_trigger_MU_SF_SYST_DOWN"),

            systematic("weight_leptonSF_MU_SF_ID_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP"),
            systematic("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN"),
            systematic("weight_leptonSF_MU_SF_Isol_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_Isol_STAT_DOWN"),

            systematic("weight_leptonSF_MU_SF_Isol_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_Isol_SYST_DOWN"),
            systematic("weight_leptonSF_MU_SF_TTVA_STAT_UP"),
            systematic("weight_leptonSF_MU_SF_TTVA_STAT_DOWN"),
            systematic("weight_leptonSF_MU_SF_TTVA_SYST_UP"),
            systematic("weight_leptonSF_MU_SF_TTVA_SYST_DOWN"),

            # systematic("weight_jvt_UP"), # The fit does not converge with those negatively weighted events
            # systematic("weight_jvt_DOWN"), # The fit does not converge with those negatively weighted events

            #systematic("FT_EFF_Eigen_C_0__1down"),
            #systematic("FT_EFF_Eigen_C_0__1up"),
            #systematic("FT_EFF_Eigen_C_1__1down"),
            #systematic("FT_EFF_Eigen_C_1__1up"),

            # systematic("FT_EFF_Eigen_Light_0__1down"),
            # systematic("FT_EFF_Eigen_Light_0__1up"),
            # systematic("FT_EFF_Eigen_Light_1__1down"),
            # systematic("FT_EFF_Eigen_Light_1__1up"),

            # systematic("misstagLight_1up"),
            # systematic("misstagLight_1down"),
        ]

        #systematics with own tree:
        self.tree_systematics = [
            systematic("EG_RESOLUTION_ALL__1down"),
            systematic("EG_RESOLUTION_ALL__1up"),
            systematic("EG_SCALE_AF2__1down"),
            systematic("EG_SCALE_AF2__1up"),
            systematic("EG_SCALE_ALL__1down"),
            systematic("EG_SCALE_ALL__1up"),
            systematic("JET_EtaIntercalibration_NonClosure_PreRec__1down"),
            systematic("JET_EtaIntercalibration_NonClosure_PreRec__1up"),
            systematic("JET_GroupedNP_1__1down"),
            systematic("JET_GroupedNP_1__1up"),
            systematic("JET_GroupedNP_2__1down"),
            systematic("JET_GroupedNP_2__1up"),
            systematic("JET_GroupedNP_3__1down"),
            systematic("JET_GroupedNP_3__1up"),
            systematic("JET_InSitu_NonClosure_PreRec__1down"),
            systematic("JET_InSitu_NonClosure_PreRec__1up"),
            systematic("JET_JESUnc_Noise_PreRec__1down"),
            systematic("JET_JESUnc_Noise_PreRec__1up"),
            systematic("JET_JESUnc_VertexingAlg_PreRec__1down"),
            systematic("JET_JESUnc_VertexingAlg_PreRec__1up"),
            systematic("JET_JESUnc_mc20vsmc21_MC21_PreRec__1down"),
            systematic("JET_JESUnc_mc20vsmc21_MC21_PreRec__1up"),
            systematic("JET_RelativeNonClosure_MC21__1down"),
            systematic("JET_RelativeNonClosure_MC21__1up"),
            systematic("MET_SoftTrk_ResoPara"),
            systematic("MET_SoftTrk_ResoPerp"),
            systematic("MET_SoftTrk_Scale__1down"),
            systematic("MET_SoftTrk_Scale__1up"),
            systematic("MUON_CB__1down"),
            systematic("MUON_CB__1up"),
            systematic("MUON_SAGITTA_DATASTAT__1down"),
            systematic("MUON_SAGITTA_DATASTAT__1up"),
            systematic("MUON_SAGITTA_GLOBAL__1down"),
            systematic("MUON_SAGITTA_GLOBAL__1up"),
            systematic("MUON_SAGITTA_PTEXTRA__1down"),
            systematic("MUON_SAGITTA_PTEXTRA__1up"),
            systematic("MUON_SAGITTA_RESBIAS__1down"),
            systematic("MUON_SAGITTA_RESBIAS__1up"),
            systematic("MUON_SCALE__1down"),
            systematic("MUON_SCALE__1up"),

            systematic("JET_JERUnc_Noise_PreRec__1down"),
            systematic("JET_JERUnc_Noise_PreRec__1up"),
            systematic("JET_JERUnc_mc20vsmc21_MC21_PreRec__1down"),
            systematic("JET_JERUnc_mc20vsmc21_MC21_PreRec__1up"),
            systematic("JET_JER_DataVsMC_MC16__1down"),
            systematic("JET_JER_DataVsMC_MC16__1up"),

            systematic("JET_JER_EffectiveNP_1__1down", True),
            systematic("JET_JER_EffectiveNP_1__1up", True),
            systematic("JET_JER_EffectiveNP_2__1down", True),
            systematic("JET_JER_EffectiveNP_2__1up", True),
            systematic("JET_JER_EffectiveNP_3__1down", True),
            systematic("JET_JER_EffectiveNP_3__1up", True),
            systematic("JET_JER_EffectiveNP_4__1down", True),
            systematic("JET_JER_EffectiveNP_4__1up", True),
            systematic("JET_JER_EffectiveNP_5__1down", True),
            systematic("JET_JER_EffectiveNP_5__1up", True),
            systematic("JET_JER_EffectiveNP_6__1down", True),
            systematic("JET_JER_EffectiveNP_6__1up", True),
            systematic("JET_JER_EffectiveNP_7restTerm__1down", True),
            systematic("JET_JER_EffectiveNP_7restTerm__1up", True),

            ]

options=options_container()
