import ROOT
import os
from options_file import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import TChain, TSelector, TTree, TCanvas, TH1F
import argparse,sys
import shutil

## Parallelisation 

from functools import partial
import multiprocessing as mp
import subprocess


def getDsid(fileName):
    dsid=fileName.split('/')[-1].split(".")[2]

    if (len(dsid) != 6) and (len(dsid) != 8):
        print('Dsid in Filename not Found! Filename:', fileName, "dsid:",  dsid)
        raise ValueError('Dsid in Filename not Found! Filename:'+fileName)
    return int(dsid)


def getWeight_lumi(dsid, sumWeightsChain, isyst, fline):
    print("calculating Sum of weights")
    sWentries = sumWeightsChain.GetEntries()
    n_generated = 0

    for jentry in range( sWentries ):
        sumWeightsChain.GetEntry( jentry )

        # TODO: Don't check hard-coded DSIDs, use LocalFolderBuilder nominal dictionary
        # case 1: ttbar QCD ISR UP
        if args.isyst=="weight_mc_rad_UP" and "6012" in str(dsid): ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
            print("using different generator weights!: ", 6, 167, "due to ", "weight_mc_rad_UP in ttbar")
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(6)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(167)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(1) # 1 (nominal) seems to match the MC weight?
        # case 2: ttbar QCD ISR DOWN
        elif args.isyst=="weight_mc_rad_DOWN" and "6012" in str(dsid): ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
            print("using different generator weights!: ", 7, 168,  "due to ", "weight_mc_rad_DOWN in ttbar")
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(7)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(168)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(1) # 1 (nominal) seems to match the MC weight?
        # case 3: single top ISR UP
        elif args.isyst=="weight_mc_rad_UP" and ("60135" in str(dsid) or "601348" in str(dsid) or "601349" in str(dsid)): ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
            print("using different generator weights!: ", 4, 165,  "due to ", "weight_mc_rad_UP in single top")
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(4)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(165)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(1) # 1 (nominal) seems to match the MC weight?
        # case 4: single top ISR DOWN
        elif args.isyst=="weight_mc_rad_DOWN" and ("60135" in str(dsid) or "601348" in str(dsid) or "601349" in str(dsid)): ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
            print("using different generator weights!: ", 7, 166,  "due to ", "weight_mc_rad_DOWN in single top")
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(7)*sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(166)/sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(1) # 1 (nominal) seems to match the MC weight?

        elif "weight_mc_shower_np_" in args.isyst and dsid >=364100 and dsid <=364113:   #TODO: Update these DSIDs
            gen_weight = int(args.isyst.replace("weight_mc_shower_np_",""))
            if gen_weight >10:
                print("we are in a dangerous region Z->mumu sample has no pdf weights for now!")
                if gen_weight<111:
                    n_generated +=  sumWeightsChain.totalEventsWeighted
                    args.isyst="nominal"
                else:
                    gen_weight =gen_weight - 100
                    print("using different generator weights!: ", gen_weight,  "due to ", args.isyst)
                    n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(gen_weight)
                    args.isyst="weight_mc_shower_np_"+str(gen_weight)
            else:
                gen_weight = int(args.isyst.replace("weight_mc_shower_np_",""))
                print("using different generator weights!: ", gen_weight,  "due to ", args.isyst)
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(gen_weight)

        elif args.isyst=="weight_mc_fsr_UP":
            if "singletop" in args.input_file or "Singletop" in args.input_file: ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
                #n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(147)
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(170)
            else: # TODO: This is for ttbar then?   
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(172) #198
        elif args.isyst=="weight_mc_fsr_DOWN":
            if "singletop" in args.input_file or "Singletop" in args.input_file: ## To check the correct numbers, you should setup an AthDerivation release and use checkMetaSG.py on the treated file
                #n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(148)
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(171)
            else:  
                n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(173) #199
        elif "weight_mc_shower_np_" in args.isyst:
            gen_weight = int(args.isyst.replace("weight_mc_shower_np_",""))
            print("using generator weights : ", gen_weight)
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(gen_weight)

        elif "weight_mc_qcd_np_" in args.isyst: 
            gen_weight = int(args.isyst.replace("weight_mc_qcd_np_",""))
            print("using generator weights : ", gen_weight)
            n_generated += sumWeightsChain.totalEventsWeighted_mc_generator_weights.at(gen_weight)

        else:
            n_generated +=  sumWeightsChain.totalEventsWeighted
    print("sum_of_weights: " + str(n_generated))
    kfac=-1
    crossec=-1
        #calculatin w_lumi
        #from anna top mc 15: 410000  crossection: 377.9932 kfaktor: 1.1949   pythia  kfaktor needid to crrect for NNlo
    # if options.data_name == "dataRun3":
    #     PathToXSection = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC21-13p6TeV.data"
    # elif options.data_name == "data22":
        #PathToXSection = "/afs/cern.ch/user/j/jabarr/bjets_ttbardilepton_PDF/AnalysisTop/grid/MyXSection-MC21-13p6TeV.data"
        #PathToXSection= "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC21-13p6TeV.data"
    PathToXSection= "/afs/cern.ch/user/y/yuhui/private/online22/yuhui/bjets_ttbardilepton_PDF-dev-run3/AnalysisTop/grid/MyXSection-MC23-13p6TeV.data"
    # else:
    #     PathToXSection = "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/AnalysisTop/TopDataPreparation/XSection-MC16-13TeV.data"
    print("PathToXSection : %s"%PathToXSection)    
    print("DSID %s" %dsid)
    with open(PathToXSection,"r") as f:
        for line in f:
            if str(dsid) in line:
                spli=line.split()
                print("SPLI :%s"%spli)

                if not spli[0] == "#":
                    kfac=float(spli[2])
                    crossec=float(spli[1])
    print("kfac : %s"%kfac)                
    if kfac == -1:
        print("kfac not found!")
        print("were looking for dsid", dsid)
        raise kfacNotFound('kfac in XSection-MC16-13TeV.data not Found!')
    if "weight_crossec_" in args.isyst:
        
        par=args.isyst.replace("weight_crossec_","")
        if "per_UP" in par:
            var_in_percent=float(par.replace("per_UP",""))
            crossec=crossec*(1+var_in_percent/100)
            print("variated crossec by", var_in_percent, "'%' up." )
        elif "per_DOWN" in par:
            var_in_percent=float(par.replace("per_DOWN",""))
            crossec=crossec*(1-var_in_percent/100)
            print("variated crossec by", var_in_percent, "'%' up." )
        else:
            print("error with:", args.isyst, "could not find crossec variation.")
        args.isyst="nominal"
    lumi_mc = n_generated/(crossec * kfac)

    #lumi_data = options.data_lumi
    #if args.combine_datasets:
        #lumi_data = options.data_lumi
    #else:
    if "r13167" in fline:
        lumi_data = options.data_1516_lumi
        print("data year is 2015 + 2016")
    elif "r13144" in fline:
        lumi_data = options.data_17_lumi
        print("data year is 2017")
    elif "r13145" in fline:
        lumi_data = options.data_18_lumi
        print("data year is 2018")
    elif "r14622" in fline:
        lumi_data = options.data_22_lumi
        print("data year is 2022")
    elif "r14799" in fline:
        lumi_data = options.data_23_lumi
        print("data year is 2023")
    else:
        raise ValueError(fline + 'includes unidentified rtag!')

    w_lumi = lumi_data/lumi_mc
    print("crossec "+str(crossec)+"; kfac "+str(kfac)+"; sum_of_weights: "+ str(n_generated)+"; w_lumi: ",str(w_lumi))

    return w_lumi



def runOverFile(inFile, outdir, outputFilename, args, process_list):
    elistsOut=outdir+outputFilename
    print("running over inFile: ", inFile)
    w_lumi=1
    if not (args.data):
        sumWeightsChain = TChain("sumWeights")
        fn=0
        with open(inFile) as f:
            for line in f:
                fn=fn+1
                sumWeightsChain.AddFile(line[:-1])
                if(args.test and (fn>1)):
                    print("running in test mode!")
                    break
        dsid=getDsid(fline)
        print("DSID %s" %dsid)
        w_lumi=getWeight_lumi(dsid,sumWeightsChain, args.isyst, fline)
    #running the TSelector: doing the event Selection
    #combine the AnalysisTopCutflow :
    h_anaTopCutflow = 0
    fn=0
    with open(inFile) as f:
        for line in f:
            fn=fn+1

            if(args.test and (fn>1)):
                print("running in test mode!")
                break

    print("\033[94m Re-running the Event Selection for file : %s \033[0m"%inFile)
    log=None;
    if args.log:
        log=open(elistsOut+".log","w")

    cmd=shutil.which("RunTSelectorMC")
    print("COMMAND : ",cmd)

    if cmd == "":
        print("\033[91m Cannot find the RunTSelectorMC. Did you forget to run source ./TSelectors/build/x86_64-centos7-gcc11-opt/setup.sh ? \033[0m")
        sys.exit(-1)


    additional_options=[]
    if args.save_fit_input:
        additional_options.append("--save_fit_input")
    if args.use_pt_bins_as_eta:
        additional_options.append("--use_pt_bins_as_eta")
    if args.run_fakes:
        additional_options.append("--run_fakes")
    if args.save_bin_means_for_sr_histo:
        additional_options.append("--save_bin_means_for_sr_histo")        
    if args.run_on_VR:
        additional_options.append("--run_on_VR")        
    if args.data:
        print("running on data:")
        additional_options.append("--data")
        additional_options.append("-t nominal")
        p=subprocess.Popen([cmd,"-i", inFile,"-o",elistsOut, "-l" ,"1"]+additional_options,stdout=log)
    else:
        print("running on MC "+ args.tsyst +" "+ args.isyst + ": ")
        if args.jet_collection:
            additional_options.append("--jet_collection %s"%args.jet_collection)
        if args.jet_collection_lf:
            additional_options.append("--jet_collection_lf %s"%args.jet_collection_lf)

        if args.apply_lf_calib:
            additional_options.append("--apply_lf_calib")
        if args.boot_strap:
            additional_options.append("--boot_strap")

    if args.correctFakes:
            p=subprocess.Popen([cmd, "-l",str(w_lumi),"-i", inFile,"-o",elistsOut,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization,"-f",NPLeptonsFile]+additional_options, stdout=log) #,"-n",NormfactorsFile
    else:
            p=subprocess.Popen([cmd, "-l",str(w_lumi),"-i", inFile,"-o",elistsOut,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization]+additional_options, stdout=log) #,"-n",NormfactorsFile

    process_list.append(p)
    if not args.runParallel:
        p.wait()
    print("back in py")



# def runSample(outdir,args,sampleFiles):
    
#     total_number_of_dsids=0
#     samples=[]
#     for sample in open(sampleFiles,"r"):
#         # get the name of the file without the namespace:
#         inDir=sampleFiles[:-4]   #subtracting '.root'
        
#         name_space_parts=sample.split(":")
#         if len(name_space_parts) > 1 :
#             flineWithoutNameSpace=name_space_parts[-1]
#         else:
#             flineWithoutNameSpace=name_space_parts[0]
#         flineWithoutNameSpace=flineWithoutNameSpace[0:-1].replace(' ','')
#         filesForChainFileName=flineWithoutNameSpace.split("/")[-1]+".txt"
#         inputFileName = inDir+'/'+filesForChainFileName
#         outputFilename=outdir+"/"+filesForChainFileName[0:-16]+ "_" + systematic_name+ "_elist.root"
        
#         ## Get the dsid from the sample name 
#         w_lumi=1
#         dsid=-1
#         if not args.data:
#             dsid=getDsid(inputFileName)
#             sumWeightsChain = TChain("sumWeights")

#             with open(inputFileName) as f:
#                 for line in f:
#                     sumWeightsChain.AddFile(line[:-1])
#                     if(args.test and (total_number_of_dsids>1)):
#                         print("running in test mode!")
#                         break

#             w_lumi=getWeight_lumi(dsid, sumWeightsChain, args.isyst, sample)
#             total_number_of_dsids+=1
#         samples.append((dsid,w_lumi,inputFileName,outputFilename))
                
#     if args.rrS:
#         nCores=4
#         filledFunc = partial(runSingleFile,args,outputFilename,outdir)
#         results=None
#         with mp.Pool(processes = nCores, maxtasksperchild = 1) as pool:
#             results = pool.map(filledFunc,samples)
            
#         print("The following warnings were issued: ")
#         for i,res in enumerate(results):
#             print("Job %d: "%i+res)
#     return total_number_of_dsids
def runSample(outdir,args,sampleFiles):
    
    total_number_of_dsids=0
    samples=[]
    for sample in open(sampleFiles,"r"):
        # get the name of the file without the namespace:
        inDir=sampleFiles[:-4]   #subtracting '.root'
        
        name_space_parts=sample.split(":")
        if len(name_space_parts) > 1 :
            flineWithoutNameSpace=name_space_parts[-1]
        else:
            flineWithoutNameSpace=name_space_parts[0]
        flineWithoutNameSpace=flineWithoutNameSpace[0:-1].replace(' ','')
        filesForChainFileName=flineWithoutNameSpace.split("/")[-1]+".txt"
        inputFileName = inDir+'/'+filesForChainFileName
        outputFilename=outdir+"/"+filesForChainFileName[0:-16]+ "_" + systematic_name+ "_elist.root"
        
        ## Get the dsid from the sample name 
        w_lumi=1
        dsid=-1
        if not args.data:
            dsid=getDsid(inputFileName)
            sumWeightsChain = TChain("sumWeights")

            with open(inputFileName) as f:
                for line in f:
                    sumWeightsChain.AddFile(line[:-1])
                    if(args.test and (total_number_of_dsids>1)):
                        print("running in test mode!")
                        break

            w_lumi=getWeight_lumi(dsid, sumWeightsChain, args.isyst, sample)
            total_number_of_dsids+=1
        samples.append((dsid,w_lumi,inputFileName,outputFilename))
                
    if args.rrS:
        nCores=4
        filledFunc = partial(runSingleFile,args)
        results=None
        with mp.Pool(processes = nCores, maxtasksperchild = 1) as pool:
            results = pool.map(filledFunc,samples)
            
        print("The following warnings were issued: ")
        for i,res in enumerate(results):
            print("Job %d: "%i+res)
    return total_number_of_dsids



def runSingleFile(args, inFile):

    DSID=inFile[0]
    lumi_weight = inFile[1]
    inputfile = inFile[2]
    outFileName = inFile[3]
    #running the TSelector: doing the event Selection
    #combine the AnalysisTopCutflow :
    print("\033[94m Re-running the Event Selection for file : %s \033[0m"%inputfile)
    log=None;

    cmd=shutil.which("RunTSelectorMC")    
    if cmd == "":
        print("\033[91m Cannot find the RunTSelectorMC. Did you forget to run source ./TSelectors/build/x86_64-centos7-gcc11-opt/setup.sh ? \033[0m")
        sys.exit(-1)
    additional_options=[]
    if args.save_fit_input:
        additional_options.append("--save_fit_input")
    if args.use_pt_bins_as_eta:
        additional_options.append("--use_pt_bins_as_eta")
    if args.run_fakes:
        additional_options.append("--run_fakes")
    if args.save_bin_means_for_sr_histo:
        additional_options.append("--save_bin_means_for_sr_histo")        
    if args.run_on_VR:
        additional_options.append("--run_on_VR")        
    if args.data:
        print("running on data:")
        additional_options.append("--data")
        additional_options.append("-t nominal")
        results=os.popen(" ".join([cmd,"-i", inputfile,"-o",outFileName, "-l" ,"1"]+additional_options)).readlines()
    else:
        print("running on MC "+ args.tsyst +" "+ args.isyst + ": ")
        if args.jet_collection:
            additional_options.append("--jet_collection %s"%args.jet_collection)
        if args.jet_collection_lf:
            additional_options.append("--jet_collection_lf %s"%args.jet_collection_lf)

        if args.apply_lf_calib:
            additional_options.append("--apply_lf_calib")
        if args.boot_strap:
            additional_options.append("--boot_strap")

    if args.correctFakes:
        results=os.popen(" ".join([cmd, "-l",str(lumi_weight),"-i", inputfile,"-o",outFileName,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization,"-f",NPLeptonsFile]+additional_options)).readlines()
    else:
        results=os.popen(" ".join([cmd, "-l",str(lumi_weight),"-i", inputfile,"-o",outFileName,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization]+additional_options)).readlines()

    for resline in results:
        print(resline.split("\n")[0])
        if any(x in resline for x in ["Break","illegal"]):
            return "\033[91m Job failed : %s \033[0m"%resline.split("\n")[0]
    return "\033[92m Job ran successfully \033[0m"


# def runSingleFile(args, outputFilename,outdir,inFile):
#     elistsOut=outputFilename
#     DSID=inFile[0]
#     lumi_weight = inFile[1]
#     inputfile = inFile[2]
#     outFileName = inFile[3]
#     #running the TSelector: doing the event Selection
#     #combine the AnalysisTopCutflow :
#     print("\033[94m Re-running the Event Selection for file : %s \033[0m"%inputfile)
#     log=None;
#     if args.log:
#         log=open(elistsOut+".log","w")
#     cmd=shutil.which("RunTSelectorMC")    
#     if cmd == "":
#         print("\033[91m Cannot find the RunTSelectorMC. Did you forget to run source ./TSelectors/build/x86_64-centos7-gcc11-opt/setup.sh ? \033[0m")
#         sys.exit(-1)
#     additional_options=[]
#     if args.save_fit_input:
#         additional_options.append("--save_fit_input")
#     if args.use_pt_bins_as_eta:
#         additional_options.append("--use_pt_bins_as_eta")
#     if args.run_fakes:
#         additional_options.append("--run_fakes")
#     if args.save_bin_means_for_sr_histo:
#         additional_options.append("--save_bin_means_for_sr_histo")        
#     if args.run_on_VR:
#         additional_options.append("--run_on_VR")        
#     if args.data:
#         print("running on data:")
#         additional_options.append("--data")
#         additional_options.append("-t nominal")
#         cmd_with_args = " ".join([cmd,"-i", inputfile,"-o",outFileName, "-l" ,"1"]+additional_options)
#         proc = os.popen(cmd_with_args)
#         results = proc.readlines()

#         log.writelines(results)  
#         proc.close()
        
#     else:
#         print("running on MC "+ args.tsyst +" "+ args.isyst + ": ")
#         if args.jet_collection:
#             additional_options.append("--jet_collection %s"%args.jet_collection)
#         if args.jet_collection_lf:
#             additional_options.append("--jet_collection_lf %s"%args.jet_collection_lf)

#         if args.apply_lf_calib:
#             additional_options.append("--apply_lf_calib")
#         if args.boot_strap:
#             additional_options.append("--boot_strap")
    
#     if args.correctFakes:
#         cmd_with_args = " ".join([cmd, "-l", str(lumi_weight), "-i", inputfile, "-o", outFileName, "-t", args.tsyst, "-w", args.isyst, "--hadronization", args.hadronization, "-f", NPLeptonsFile] + additional_options)
#         proc = os.popen(cmd_with_args)
#         results = proc.readlines()
#         log.writelines(results) 
#         proc.close()
        
#     else:
#         cmd_with_args = " ".join([cmd, "-l", str(lumi_weight), "-i", inputfile, "-o", outFileName, "-t", args.tsyst, "-w", args.isyst, "--hadronization", args.hadronization] + additional_options)
#         proc = os.popen(cmd_with_args)
#         results = proc.readlines()
#         log.writelines(results) 
#         proc.close()
        


#     # if args.correctFakes:
#     #     results=os.popen(" ".join([cmd, "-l",str(lumi_weight),"-i", inputfile,"-o",outFileName,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization,"-f",NPLeptonsFile]+additional_options),stdout=log).readlines()
#     # else:
#     #     results=os.popen(" ".join([cmd, "-l",str(lumi_weight),"-i", inputfile,"-o",outFileName,"-t",args.tsyst,"-w",args.isyst,"--hadronization",args.hadronization]+additional_options),stdout=log).readlines()

#     for resline in results:
#         print(resline.split("\n")[0])
#         if any(x in resline for x in ["Break","illegal"]):
#             return "\033[91m Job failed : %s \033[0m"%resline.split("\n")[0]
#     return "\033[92m Job ran successfully \033[0m"

def DrawCutflow(name,outdir, elists):
    print("**************************************")
    print("Drawing Cutflow: ")
    cutFlow = elists.Get(name)
    c = TCanvas()
    cutFlow.Draw()
    outputfile = outdir + "cutflow/"
    if not os.path.exists(outputfile):
        os.makedirs(outputfile)
    c.SaveAs((outputfile + name.replace("/","_") + ".png"))
    print("Events in first bin: " + str(cutFlow.GetBinContent(1)))
    print("Events in 3 bin channel: " + str(cutFlow.GetBinContent(3)))
    print("Events in 4 bin sign: " + str(cutFlow.GetBinContent(4)))
    print("Events in 5 bin 2 or 3 jets: " + str(cutFlow.GetBinContent(5)))
    print("Events in 6 bin 2 jets: " + str(cutFlow.GetBinContent(6)))
    print("Events in 7 bin MET: " + str(cutFlow.GetBinContent(7)))
    print("Events in 9 bin mll: " + str(cutFlow.GetBinContent(9)))



# in main:
parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('input_file',
                    help='input file for example: ../selecting_inputs/user.jschmoec.410000.PowhegPythiaEvtGen.DAOD_FTAG2.e3698_s2608_s2183_r7725_r7676_p2669.anaTopStyle-17-06-06_output.root.txt')
parser.add_argument('--rrS', action='store_true',
                    help='rerun the Event Selection (default: false)')
parser.add_argument('--data', action='store_true',
                    help='the input is data (default: false)')
parser.add_argument('--save_fit_input', action='store_true',
                    help='Save the histogras for the fit,takes longer and uses more space.')
parser.add_argument('--test', action='store_true',
                    help='only run on two files for testing (default: false)')
parser.add_argument('--dPlots', action='store_true',
                    help='draw Comparison Plots, takes a while. (default: false)')
parser.add_argument('--sFile', action='store_true',
                    help='The input is a single File and not a file with Folders as usual (default: false)')
parser.add_argument('--boot_strap', action='store_true',
                    help='Run over Bootstrapweights -takes much more time and space, but neccessary for mc-stat unc.')
parser.add_argument('-t',"--tsyst",default="nominal",
                    help='name of systematic tree to run over. default=nominal.')
parser.add_argument('-w',"--isyst",default="nominal",
                    help='only valid if you run on nominal. You can specify inner systematics here like weight_leptonSF_EL_SF_Trigger_UP(weights)')
parser.add_argument("--output_cmd",default="nominal",
                    help='only valid if you run on nominal. This argument is introduced to harmonise the naming for modelling systematics.')
parser.add_argument('--correctFakes', action='store_true',
                    help='Scale factors are applied to correct weights of SS events with a non-prompt electron - have to be generated first! (default: false)')
parser.add_argument('--run_fakes', action='store_true',
                    help='Generate  fake scale factors (default: false).')
parser.add_argument('--apply_lf_calib', action='store_true',
                    help='apply calibration for lf-jets. Not fully supportet jet, since not all taggers hafe pseudo continious lf-sf available.' )
parser.add_argument('--jet_collection', action='store',default='AntiKt4EMPFlowJets',
                    help='Jet collection that is being considered in the analysis.')
parser.add_argument('--jet_collection_lf', action='store',default='AntiKt4EMPFlowJets',
                    help='Jet collection that is considered to get the efficiency scale factors for light jets when option --apply_lf_calib is used.')
parser.add_argument('--hadronization', default="410470",
                    help='hadronization model used for the Generator. needed for MC/MC sf. https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalib2017#MC_MC_Scale_Factors_for_Analysis')
parser.add_argument('--log', action='store_true',
                    help='Create log files, nice option for batch submission! (default: false)')
parser.add_argument('--runParallel', action='store_true',
                    help='if you want to run all jobs in parallel. (default: false)')
parser.add_argument('--use_pt_bins_as_eta', action='store_true',
                    help='you can tweak the pt bins to hold eta. Check if sf are flat in eta.  (default: false)')
parser.add_argument('--save_bin_means_for_sr_histo', action='store_true',
                    help='save bin means in the sr- nice for plots with vaariable bin means at the end  (default: false)')
parser.add_argument('--nominal_only', action='store_true',
                    help='only save emu histograms (default: false)')
parser.add_argument('--run_on_VR', action='store_true',
                    help='run calibration on Variable-R track jets (default: false)')
parser.add_argument('--combine_datasets',action='store_true',
                    help='Want to create a dir to run over all your data not just one year!')

args = parser.parse_args()
inFile = args.input_file
if (args.tsyst != "nominal") & (args.isyst != "nominal"):
    print("you can only provide inner_systematic when you run over nominal!")

outFolderName = options.output_dir

# getting the ouputfilename
inpath, inFileName = os.path.split(inFile)
sampleName = inFileName[:-4]
if args.sFile:
    sampleName = inFileName[:-9]
if args.test:
    sampleName = sampleName + "_test"
NPLeptonsFile=""
if args.correctFakes:
    NPLeptonsFile = outFolderName + "NPLeptons/compare_1PL_DatavsMC_nominal.root"

outdir = outFolderName + sampleName + "/"
if not os.path.exists(outdir):
    os.makedirs(outdir)
    
if (args.tsyst=='nominal') & (args.isyst != ""):
    if args.data:
        systematic_name='data'
        systematic_name = 'nominal'
    elif args.correctFakes:
        systematic_name='correctFakes'
    else:
        #systematic_name=args.isyst # we run with different weights (an inner systematic)
        #systematic_name=args.isyst # we run with different weights (an inner systematic)
        systematic_name=args.output_cmd # we run with different weights (an inner systematic) Use "output_cmd" to assign a new name to the output file, if applicable
else:
    systematic_name=args.tsyst # we run over a different tree (a tree systematic)
    args.isyst="nominal"


process_list=[]


if args.sFile:
    fn = 0
    inFile  =inFileName
    runOverFile(inFile, outdir, sampleName, args,process_list)
    fn = fn + 1
else:

    fn = runSample(outdir,args,inFile)
    print("In " + str(fn) + " Input Containers")
    #creating a histogram with the Lumi_weight:
    if not args.data:
        w_lumi_hist=TH1F('w_lumi_hist','Lumi_weight per disd.',fn,0,fn)
    #Doing the Combination for the full sample:
    fn=0
    out_files=""
    print("verify that the sample is there")
    with open(inFile) as f:
        for fline in f:
            # get the name of the file without the namespace:
            name_space_parts=fline.split(":")
            if len(name_space_parts) > 1 :
                flineWithoutNameSpace=name_space_parts[1]
            else:
                flineWithoutNameSpace=name_space_parts[0]
            flineWithoutNameSpace=flineWithoutNameSpace[0:-1].replace(' ','')
            filesForChainFileName=flineWithoutNameSpace.split('/')[-1]+".txt"
            outputFilename=filesForChainFileName[0:-16]+ "_" + systematic_name+ "_elist.root"
            single_output_file_path=outdir+outputFilename
            # if args.log:
            #     #lets check the log file by the last line:
            #     print("    checking:",single_output_file_path+".log")
            #     with open(single_output_file_path+".log", 'r') as fh:
            #         lines = fh.readlines()
            #         last_line = lines[-1] if lines else ""
            #         print("last_line: ",last_line)
            #         if "deleted chain" in last_line:
            #             print("succes! passed logtest!")
            #         else:
            #             print("logtest failed!")
            #             print("last_line: ",last_line)
            #             print("check: ",single_output_file_path+".log")
            #             fh.close()
            #             print("lets try again: ",single_output_file_path)
            #             command="mv "+single_output_file_path+".log"+" "+single_output_file_path+"_1attempt_"+".log"
            #             print(command)
            #             os.system(command)
            #             process_list=[]
            #             runOverFile(inDir+'/'+filesForChainFileName,outdir,outputFilename, args, process_list)
            #             for p in process_list:
            #                 p.wait()
            #             with open(single_output_file_path+".log", 'r') as fh2:
            #                 lines = fh2.readlines()
            #                 last_line = lines[-1] if lines else ""
            #                 print("last_line: ",last_line)
            #                 if "deleted chain" in last_line:
            #                     print("succes! passed logtest!")
            #                 else:
            #                     print("logtest failed again!")
            #                     print("last_line: ",last_line)
            #                     print("check: ",single_output_file_path+".log")
            #                     exit(3)

            #lets check if we can open correctly the outpath:
            try:
                print("CHECKING IF FILE EXISTS: ",single_output_file_path)
                print("EXISTANCE", os.path.exists(single_output_file_path))
                single_output_file = ROOT.TFile(single_output_file_path, "read")
            except:
                raise ValueError(f"Failed to open {single_output_file_path}")

            h_emu_OS_J2_CutFlow=single_output_file.Get("h_emu_OS_J2_CutFlow")
            out_files=out_files+single_output_file_path+" "
            fn = fn + 1
            single_output_file.Close()
            #filling the histo with the applied lumi weight:
            if not args.data:
                single_output_file = ROOT.TFile(single_output_file_path, "read")
                w_lumi_hist.AddBinContent(fn,single_output_file.Get("applied_lumi_weight")[0])
                rff = fline.split(":", 1)[1]
                dsid=getDsid(rff)
                w_lumi_hist.GetXaxis().SetBinLabel(fn,str(dsid))
                single_output_file.Close();

    print("combining output files: ")
    combiName=sampleName+ "_" + systematic_name +"_combination.root"
    command="hadd -f "+outdir+combiName+" "+out_files
    os.system(command)
    if not args.data:
        c = TCanvas()
        outfile= ROOT.TFile(outdir+combiName,"update")
        w_lumi_hist.Write()
        w_lumi_hist.SetMaximum(3)
        w_lumi_hist.Draw()
        outfile.Close()

    #lets remove the elist files to save some disc space:
    command="rm -rf "+out_files
    os.system(command)


if args.dPlots:
    print("Drawing Cutflows:")
    f_comb = ROOT.TFile(outdir+combiName, "read")
    p_outdir = outdir + "ComparisonPlots/"
    if not os.path.exists(p_outdir):
        os.makedirs(p_outdir)
    c = TCanvas()
    jet_multiplicity = f_comb.Get("h_jet_multiplicity")
    jet_multiplicity.Draw()
    c.SaveAs(p_outdir + "h_jet_multiplicity.png")
    c.SaveAs(p_outdir + "h_jet_multiplicity.pdf")

    DrawCutflow("ee_OS_J2_aCuts/h_ee_OS_J2_aCuts_CutFlow", p_outdir ,f_comb)
    DrawCutflow("ee_OS_J3_aCuts/h_ee_OS_J3_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J2_aCuts/h_mumu_OS_J2_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J3_aCuts/h_mumu_OS_J3_aCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("emu_OS_J2/h_emu_OS_J2_CutFlow", p_outdir, f_comb)
    DrawCutflow("emu_OS_J3/h_emu_OS_J3_CutFlow", p_outdir, f_comb)
    DrawCutflow("ee_OS_J2_bCuts/h_ee_OS_J2_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("ee_OS_J3_bCuts/h_ee_OS_J3_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J2_bCuts/h_mumu_OS_J2_bCuts_CutFlow", p_outdir, f_comb)
    DrawCutflow("mumu_OS_J3_bCuts/h_mumu_OS_J3_bCuts_CutFlow", p_outdir, f_comb)
    f_comb.Close()

print("End.")
