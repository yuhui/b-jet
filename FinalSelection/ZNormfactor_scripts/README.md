These scripts can be used to derive a correction factor for the Z+jets normalisation (and ttbar in principle too).

It works like this:
1. You run the finalSelection over your samples defined in options_file.py
	- either nominal or systematics
2. getZNormfactors.py extracts Data/MC scale factors in a Z+jets control region for emu_OS_J2
	- use option -t or -z to specify ttbar or Z+jets alternative samples, respectively
	- use option -s to specify a systematic in your nominal sample
	- use option -a to directly apply the scale factors, see below
3. apply_ZNormfactors.py applies the correction factor to the samples you want
	- ttbar samples get the ttbar SF, Z+jets samples get the Z+jets SF, all others are ignored
	- apply_ZNormfactors.py can be called directly with "python apply_ZNormfactors.py -n /path/to/normfile.root file1 file2 ..."
	- it can also be called indirectly by using "python getZNormfactors.py -a" which applies the SF (+UP/DOWN variations) to your nominal Z sample
	- finally, use "python getZNormfactors_syst.py" to generate AND apply these SF for ALL your systematic samples
