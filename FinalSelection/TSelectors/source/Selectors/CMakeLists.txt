# Declare the name of this package:
atlas_subdir(Selectors None)

# This package uses ROOT:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO Net Graf Graf3d Gpad Rint Postscript Matrix Physics Thread Gui pthread m MathMore TreePlayer TreeViewer )

# Custom definitions needed for this package:
add_definitions( -g )

# Generate a CINT dictionary source file:
atlas_add_root_dictionary( libSelectorMC _cintDictSource
                           ROOT_HEADERS libSelectorMC/*.h Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

atlas_add_library( libSelectorMC libSelectorMC/*.cxx ${_cintDictSource}
                   PUBLIC_HEADERS libSelectorMC
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES TopAnalysis xAODBTaggingEfficiencyLib ${ROOT_LIBRARIES}
                )
                    
atlas_add_executable (RunTSelectorMC macros/runTSelectorMC.cxx INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} LINK_LIBRARIES ${ROOT_LIBRARIES} libSelectorMC)
