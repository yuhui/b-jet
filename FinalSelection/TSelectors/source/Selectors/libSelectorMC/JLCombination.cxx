// JLCombination.cxx

#include "JLCombination.h"

ClassImp(JL_Combination)

JL_Combination::JL_Combination(std::string name, int n_jets, bool data, std::vector<std::string> eflavours){
	this->n_jets = n_jets;
	jet1_combination = new XX_Container(name + "_jet1c", n_jets, data, eflavours, 0.0);
	jet2_combination = new XX_Container(name + "_jet2c", n_jets, data, eflavours, 0.0);
}

JL_Combination::~JL_Combination()
{
}

void JL_Combination::Write()
{
	if(this->n_jets == 2) {
		jet1_combination->Write();
		jet2_combination->Write();
	}
}
