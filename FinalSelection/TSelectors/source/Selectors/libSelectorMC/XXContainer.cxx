// XXContainer.cxx

#include "XXContainer.h"

ClassImp(XX_Container)

XX_Container::XX_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours, double xx_m_start)
{
	xx_m = new Variable_Container(name + "_m", n_jets, 40, xx_m_start, xx_m_start + 800., data, eflavours);
	xx_mT = new Variable_Container(name + "_mT", n_jets, 40, 0, 400, data, eflavours);
	xx_pT = new Variable_Container(name + "_pT", n_jets, 60, 0, 600, data, eflavours);
	xx_d_eta = new Variable_Container(name + "_d_eta", n_jets, 40, -5, 5, data, eflavours);
	xx_d_phi = new Variable_Container(name + "_d_phi", n_jets, 28, -3.5, 3.5, data, eflavours);
	xx_d_R = new Variable_Container(name + "_d_R", n_jets, 20, 0, 5, data, eflavours);
}

XX_Container::~XX_Container()
{
}

void XX_Container::Write()
{
	xx_m->Write();
	xx_mT->Write();
	xx_pT->Write();
	xx_d_eta->Write();
	xx_d_phi->Write();
	xx_d_R->Write();
}

void XX_Container::addEvent(TLorentzVector first4vec, TLorentzVector sec4vec, double weight, std::vector<int> *jet_truthflav)
{
	xx_m->addEvent((first4vec + sec4vec).M() / 1000, weight, jet_truthflav);
	xx_mT->addEvent((first4vec + sec4vec).Mt() / 1000, weight, jet_truthflav);
	xx_pT->addEvent((first4vec + sec4vec).Pt() / 1000, weight, jet_truthflav);
	xx_d_eta->addEvent((first4vec.Eta() - sec4vec.Eta()), weight, jet_truthflav);
	xx_d_phi->addEvent(first4vec.DeltaPhi(sec4vec), weight, jet_truthflav);
	xx_d_R->addEvent(first4vec.DeltaR(sec4vec), weight, jet_truthflav);
}
