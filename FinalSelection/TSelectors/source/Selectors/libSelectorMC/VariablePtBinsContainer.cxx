// VariablePtBinsContainer.cxx

#include "VariablePtBinsContainer.h"

ClassImp(Variable_ptBins_Container)

Variable_ptBins_Container::Variable_ptBins_Container(std::string name, int n_jets, int nxbins, const double *xbins, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	this->data = data;
	if (data)
	{
		eflavours.clear();
		eflavours.push_back("data");
	}
	if (n_jets == 2)
	{
		for (const auto &eventFlav : eflavours)
		{
			histograms[eventFlav] = new TH3F(("h_pt_" + name + "_" + eventFlav).c_str(), (name + " with Jet Flavours: " + eventFlav).c_str(), nxbins, xbins, NBINS_pt, pt_bins, NBINS_pt, pt_bins);
			histograms[eventFlav]->Sumw2();
			histograms[eventFlav]->GetXaxis()->SetTitle(name.c_str());
			histograms[eventFlav]->GetYaxis()->SetTitle("jet1_pT");
			histograms[eventFlav]->GetZaxis()->SetTitle("jet2_pT");
		}
	}
}

Variable_ptBins_Container::~Variable_ptBins_Container()
{
}

void Variable_ptBins_Container::Write()
{
	for (const auto &x : histograms)
		x.second->Write();
}

void Variable_ptBins_Container::addEvent(double value, std::vector<float> *jet_pt, double weight, std::vector<int> *jet_truthflav)
{ //defing string flavour "bb","bl"...:
	std::string eventFlav = getEventFlav(jet_truthflav);
	//adding event
	if (jet_pt->size() == 2){
		histograms[eventFlav]->Fill(value, jet_pt->at(0) / 1000., jet_pt->at(1) / 1000., weight);
	}
	// else
	// 	std::cout << "jet_pt->size() for Variable_ptBins_Container wrong!!!: " << jet_pt->size() << std::endl;
}

std::string Variable_ptBins_Container::getEventFlav(std::vector<int> *jet_truthflav)
{
	std::string eventFlav = "";
	if (this->data)
	{
		eventFlav = "data";
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			if (jet_truthflav->at(i) == 4)
				eventFlav = eventFlav + "c";
			else if (jet_truthflav->at(i) == 5)
				eventFlav = eventFlav + "b";
			else
				eventFlav = eventFlav + "l"; //this is also tau jets!
		}
	}
	return eventFlav;
}
