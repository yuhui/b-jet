// VariablePtBinsCorrelationContainer.cxx

#include "VariablePtBinsCorrelationContainer.h"

ClassImp(Variable_ptBins_Correlation_Container)

Variable_ptBins_Correlation_Container::Variable_ptBins_Correlation_Container(std::string name, int n_jets, std::string x_name, int nxbins, const double *xbins, std::string y_name, int nybins, const double *ybins, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	bins[0] = nxbins;
	bins[1] = nybins;
	this->data = data;
	if (data)
	{
		eflavours.clear();
		eflavours.push_back("data");
	}
	if (n_jets == 2)
	{
		for (const auto &eventFlav : eflavours)
		{
			histograms[eventFlav]=new THnSparseD(("h_pt_" + name + "_" + "correlation_" + x_name + "_and_" + y_name + "_" + eventFlav).c_str(), (name + " correlation between" + x_name + " and " + y_name + eventFlav).c_str(), 4, bins, NULL, NULL);
			histograms[eventFlav]->SetBinEdges(0, xbins);
			histograms[eventFlav]->SetBinEdges(1, ybins);
			histograms[eventFlav]->SetBinEdges(2, pt_bins);
			histograms[eventFlav]->SetBinEdges(3, pt_bins);
			histograms[eventFlav]->Sumw2();
			histograms[eventFlav]->GetAxis(0)->SetTitle(x_name.c_str());
			histograms[eventFlav]->GetAxis(1)->SetTitle(y_name.c_str());
			histograms[eventFlav]->GetAxis(2)->SetTitle("jet1_pT");
			histograms[eventFlav]->GetAxis(3)->SetTitle("jet2_pT");
		}
	}
}

Variable_ptBins_Correlation_Container::~Variable_ptBins_Correlation_Container()
{
  for( auto histo : histograms)
    delete histo.second;
}

void Variable_ptBins_Correlation_Container::Write()
{
	for (const auto &x : histograms)
		x.second->Write();
}

void Variable_ptBins_Correlation_Container::addEvent(double xvalue, double yvalue, std::vector<float> *jet_pt, double weight, std::vector<int> *jet_truthflav)
{ //defing string flavour "bb","bl"...:
	std::string eventFlav = getEventFlav(jet_truthflav);
	//adding event
	if (jet_pt->size() == 2)
	{
		Double_t where_vector[4] = {Double_t(xvalue), Double_t(yvalue), jet_pt->at(0) / 1000., jet_pt->at(1) / 1000.};
		histograms[eventFlav]->Fill(where_vector, weight);
	}
	else
	{
		// std::cout << "jet_pt->size() for Variable_ptBins_Container wrong!!!: " << jet_pt->size() << std::endl;
	}
}

std::string Variable_ptBins_Correlation_Container::getEventFlav(std::vector<int> *jet_truthflav)
{
	std::string eventFlav = "";
	if (data)
	{
		eventFlav = "data";
	}
	else
	{
		for (int i = 0; i < 2; i++)
		{
			if (jet_truthflav->at(i) == 4)
				eventFlav = eventFlav + "c";
			else if (jet_truthflav->at(i) == 5)
				eventFlav = eventFlav + "b";
			else
				eventFlav = eventFlav + "l"; //this is also tau jets!
		}
	}
	return eventFlav;
}
