// finalSelectionMC.cxx

// Can use via:
// root> T->Process("finalSelectionMC.C")

#include "TreeReader.h"

using namespace std;
// using CP::SystematicCode;

ClassImp(TreeReader)

void TreeReader::initialize_m_ljs()
{
	//lets try the lj combi cut
	TLorentzVector lep_4vec[2];
	TLorentzVector jet_4vec[3];

	//clear the vectors in the this:
	this->jet_m_jl->clear();
	this->lepton_m_lj->clear();
	if (this->jet_pt->size() != 2)
		return;
	for (uint i = 0; i < this->jet_pt->size(); i++)
		jet_4vec[i].SetPtEtaPhiE(this->jet_pt->at(i), this->jet_eta->at(i), this->jet_phi->at(i), this->jet_e->at(i));
	if ((this->el_pt->size() == 2) && (this->mu_pt->size() == 0))
	{
		lep_4vec[0].SetPtEtaPhiM(this->el_pt->at(0), this->el_eta->at(0), this->el_phi->at(0), 0);
		lep_4vec[1].SetPtEtaPhiM(this->el_pt->at(1), this->el_eta->at(1), this->el_phi->at(1), 0);
	}
	else if ((this->el_pt->size() == 1) && (this->mu_pt->size() == 1))
	{
		lep_4vec[0].SetPtEtaPhiM(this->el_pt->at(0), this->el_eta->at(0), this->el_phi->at(0), 0);
		lep_4vec[1].SetPtEtaPhiM(this->mu_pt->at(0), this->mu_eta->at(0), this->mu_phi->at(0), 0);
	}
	else if ((this->el_pt->size() == 0) && (this->mu_pt->size() == 2))
	{
		lep_4vec[0].SetPtEtaPhiM(this->mu_pt->at(0), this->mu_eta->at(0), this->mu_phi->at(0), 0);
		lep_4vec[1].SetPtEtaPhiM(this->mu_pt->at(1), this->mu_eta->at(1), this->mu_phi->at(1), 0);
	}
	double m_l1_j1, m_l2_j2, m_l1_j2, m_l2_j1;
	m_l1_j1 = (lep_4vec[0] + jet_4vec[0]).M2();
	m_l1_j2 = (lep_4vec[0] + jet_4vec[1]).M2();
	m_l2_j1 = (lep_4vec[1] + jet_4vec[0]).M2();
	m_l2_j2 = (lep_4vec[1] + jet_4vec[1]).M2();
	if ((m_l1_j1 + m_l2_j2) < (m_l1_j2 + m_l2_j1))
	{
		//so m_l1_j1 and m_l2_j2 right combinations
		this->jet_m_jl->push_back(sqrt(m_l1_j1));
		this->jet_m_jl->push_back(sqrt(m_l2_j2));
		this->lepton_m_lj->push_back(sqrt(m_l1_j1));
		this->lepton_m_lj->push_back(sqrt(m_l2_j2));
	}
	else
	{
		//so m_l1_j2 and m_l2_j1 right combinations. Be carefull with the order now!
		this->jet_m_jl->push_back(sqrt(m_l2_j1));
		this->jet_m_jl->push_back(sqrt(m_l1_j2));
		this->lepton_m_lj->push_back(sqrt(m_l1_j2));
		this->lepton_m_lj->push_back(sqrt(m_l2_j1));
	}
	// std::cout << "initialized jet_m_jl " << this->jet_m_jl->size() << " lepton_m_lj " << this->lepton_m_lj->size() << std::endl;
};

TreeReader::TreeReader() : fChain(0)
{
}

TreeReader::~TreeReader()
{
}

void TreeReader::Begin()
{
}

void TreeReader::SlaveBegin()
{
	// The SlaveBegin() function is called after the Begin() function.
	// When running with PROOF SlaveBegin() is called on each slave server.
	// The tree argument is deprecated (on PROOF 0 is passed).

	TString option = GetOption();
}

Bool_t TreeReader::Process(Long64_t entry)
{
	// The Process() function is called for each entry in the tree (or possibly
	// keyed object in the case of PROOF) to be processed. The entry argument
	// specifies which entry in the currently loaded tree is to be processed.
	// It can be passed to either finalSelectionMC::GetEntry() or TBranch::GetEntry()
	// to read either all or the required parts of the data. When processing
	// keyed objects with PROOF, the object is already loaded and is available
	// via the fObject pointer.
	//
	// This function should contain the "body" of the analysis. It can contain
	// simple or elaborate selection criteria, run algorithms on the data
	// of the event and typically fill histograms.
	//
	// The processing can be stopped by calling Abort().
	//
	// Use fStatus to set the return value of TTree::Process().
	//
	// The return value is currently not used.

	fChain->GetTree()->GetEntry(entry);
	this->jet_m_jl->clear();
	this->lepton_m_lj->clear();
	if(this->el_pt->size() + this->mu_pt->size() >=2 )
	  this->initialize_m_ljs();

	return kTRUE;
}
