// JLMinMSumCombination.h

#pragma once
#include "JLCombination.h"
#include <TLorentzVector.h>
#include <string>

class JL_min_m_sum_Combination : public JL_Combination
{
 public:
  int n_jets  = 0;
  bool data;
  std::vector<std::string> eflavours;


public:
	// Constructor
  JL_min_m_sum_Combination(std::string name, int n_jets, bool data, std::vector<std::string> eflavours);
  
  // Destructor
  ~JL_min_m_sum_Combination();
  
  // Writes
  void Write();
  
  // Adds an event to something
  void addEvent(TLorentzVector *lep_4vecArray, TLorentzVector *jet_4vecArray, double weight, std::vector<int> *jet_truthflav);
  
  ClassDef(JL_min_m_sum_Combination,1);
};
