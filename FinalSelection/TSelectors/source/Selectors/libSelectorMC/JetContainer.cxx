// JetContainer.cxx

#include "JetContainer.h"

ClassImp(jet_Container)

jet_Container::jet_Container(std::string name, int n_jets, bool data, std::vector<std::string> eflavours)
{
	this->name = name;
	this->n_jets = n_jets;
	this->m_data = data;
	jet_pt = new Variable_Container(name + "_pt", n_jets, 58, 20, 600, m_data, eflavours);
	if (n_jets == 2)

	{
		double m_jl_bins [29];
		double first_bin =20;
		for (int i=0;i<20; i++)
		{
			m_jl_bins[i]=first_bin;
			first_bin=first_bin+20;
		}
		m_jl_bins[20]=430;
		m_jl_bins[21]=460;
		m_jl_bins[22]=490;
		m_jl_bins[23]=520;
		m_jl_bins[24]=550;
		m_jl_bins[25]=600;
		m_jl_bins[26]=650;
		m_jl_bins[27]=700;
		m_jl_bins[28]=800;
		jet_m_jl = new Variable_Container(name + "_m_jl", n_jets, 28,m_jl_bins, m_data, eflavours);
		jet_HLTDL1d = new Variable_Container(name + "_HLTDL1d", n_jets, 56, -10, 18, m_data, eflavours);
		jet_HLTGN1 = new Variable_Container(name + "_HLTGN1", n_jets, 56, -10, 18, m_data, eflavours);
	}
	jet_eta = new Variable_Container(name + "_eta", n_jets, 24, -2.5, 2.5, m_data, eflavours);
	jet_eta_pt_gt_60 = new Variable_Container(name + "_eta_pt_gt_60", n_jets, 24, -2.5, 2.5, m_data, eflavours);
	jet_phi = new Variable_Container(name + "_phi", n_jets, 28, -3.5, 3.5, m_data, eflavours);

	jet_DL1d = new Variable_Container(name + "_DL1d", n_jets, 56, -10, 18, m_data, eflavours);
	jet_GN1 = new Variable_Container(name + "_GN1", n_jets, 56, -10, 18, m_data, eflavours);
	jet_GN2 = new Variable_Container(name + "_GN2", n_jets, 56, -10, 18, m_data, eflavours);

	if (!m_data)
	{
		HadronConeExclExtendedTruthLabelID = new Variable_Container(name + "_HadronConeExclExtendedTruthLabelID", n_jets, 56, 0, 55, m_data, eflavours);
		double hadron_bins [8] ={0,4,5,15,44,54,55,56};
		jet_HadronConeExclExtendedTruthLabelID_pt = new Variable_ptBins_Container(name + "_HadronConeExclExtendedTruthLabelID_pt", n_jets, 7, hadron_bins, m_data, eflavours);
	}
}

jet_Container::~jet_Container()
{
}

void jet_Container::Write()
{
	jet_pt->Write();
	if (this->n_jets == 2)
	{
		jet_m_jl->Write();
	}
	jet_eta->Write();
	jet_eta_pt_gt_60->Write();
	jet_phi->Write();
	jet_DL1d->Write();
	jet_GN1->Write();
	jet_GN2->Write();
	if (this->n_jets == 2)
	{
		
	jet_HLTDL1d->Write();
	jet_HLTGN1->Write();
	}

	if (!m_data)
	{
		HadronConeExclExtendedTruthLabelID->Write();
		jet_HadronConeExclExtendedTruthLabelID_pt->Write();
	}
}

std::vector<float> jet_Container::DiscriminantHand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction)                 
{

  std::vector<float> Discriminant;

  for(unsigned int i = 0 ; i < pb.size() ; ++i){
    float disc=TMath::Log(pb[i]/( fraction*pc[i] + (1-fraction)*pu[i] ));
    Discriminant.push_back(disc);
  }
  return Discriminant;
}
// std::vector<float> jet_Container::DiscriminantHand(std::vector<float> pb,std::vector<float> pc,std::vector<float> pu, double fraction_c, double fraction_tau)                 
// {
//   std::vector<float> Discriminant;
//   for(unsigned int i = 0 ; i < pb.size() ; ++i){
//     float disc = -999.;
//     if (fraction_tau == -999.) {
//       disc = TMath::Log(pb[i]/( fraction_c*pc[i] + (1-fraction_c)*pu[i] ));
//     }
//     else {
//       float ptau = 1. - pb[i] - pc[i] - pu[i];
//       disc = TMath::Log(pb[i]/( fraction_c*pc[i] + fraction_tau*ptau + (1-fraction_c-fraction_tau)*pu[i] ));
//     }
//     Discriminant.push_back(disc);
//   }
//   return Discriminant;
// }

void jet_Container::addJet(int jet_n, TreeReader *selector, double weight)
{
  std::vector<float> GN1 = DiscriminantHand(*selector->jet_GN1_pb,*selector->jet_GN1_pc,*selector->jet_GN1_pu,0.05);
  std::vector<float> GN2 = DiscriminantHand(*selector->jet_GN2_pb,*selector->jet_GN2_pc,*selector->jet_GN2_pu,0.1);
  std::vector<float> DL1d = DiscriminantHand(*selector->jet_DL1d_pb,*selector->jet_DL1d_pc,*selector->jet_DL1d_pu,0.018);
  std::vector<float> HLTGN1;
  std::vector<float> HLTDL1d;
  std::vector<float> GN11(GN1.begin(), GN1.begin() + 2);
    std::vector<float> closest;
  if ((this->n_jets == 2)&&((selector->hlt_bjet_GN120220813_pb->size())>= 2))
  {
  HLTGN1 = DiscriminantHand(*selector->hlt_bjet_GN120220813_pb,*selector->hlt_bjet_GN120220813_pc,*selector->hlt_bjet_GN120220813_pu,0.018);
  //std::vector<float> GN2 = DiscriminantHand(*selector->jet_GN2_pb,*selector->jet_GN2_pc,*selector->jet_GN2_pu,0.1);
  //HLTDL1d = DiscriminantHand(*selector->hlt_bjet_DL1d20211216_pb,*selector->hlt_bjet_DL1d20211216_pc,*selector->hlt_bjet_DL1d20211216_pu,0.018);
  HLTDL1d =DiscriminantHand(*selector->hlt_bjet_GN120220813_pb,*selector->hlt_bjet_GN120220813_pc,*selector->hlt_bjet_GN120220813_pu,0.005);
      float minDist = std::numeric_limits<float>::max();
    float secondMinDist = std::numeric_limits<float>::max();
    float closest1, closest2;

    // Find the two closest elements
    for (float element : HLTGN1) {
        float dist1 = std::abs(element - GN11[0]);
        float dist2 = std::abs(element - GN11[1]);

        // Find the two closest elements
        if (dist1 < minDist) {
            closest1 = element;
            minDist = dist1;
        }
        if (dist2 < secondMinDist) {
            closest2 = element;
            secondMinDist = dist2;
        }
    }

    // Remove all but the two closest elements
    HLTGN1.clear();
    HLTGN1.push_back(closest1);
    HLTGN1.push_back(closest2);
//   std::partial_sort(HLTGN1.begin(), HLTGN1.begin() + 2, HLTGN1.end(), std::greater<float>());
//   HLTGN1.erase(HLTGN1.begin() + 2, HLTGN1.end());
//   std::sort(HLTGN1.begin(), HLTGN1.end(), [&GN11](float a, float b) {
//   float targetA = GN11[0];
//   float targetB = GN11[1];
//   return std::abs(a - targetA) < std::abs(b - targetB);
//     });
  
 }


  jet_pt->addEvent(selector->jet_pt->at(jet_n) / 1000, weight, selector->jet_truthflav);
  if (this->n_jets == 2&&selector->jet_pt->at(jet_n) > 60000)
    jet_eta_pt_gt_60->addEvent(selector->jet_eta->at(jet_n), weight, selector->jet_truthflav);
  jet_eta->addEvent(selector->jet_eta->at(jet_n), weight, selector->jet_truthflav);
  jet_phi->addEvent(selector->jet_phi->at(jet_n), weight, selector->jet_truthflav);

  jet_GN1->addEvent(GN1.at(jet_n), weight, selector->jet_truthflav);
  jet_GN2->addEvent(GN2.at(jet_n), weight, selector->jet_truthflav);
  jet_DL1d->addEvent(DL1d.at(jet_n), weight, selector->jet_truthflav);
  if ((this->n_jets == 2)&&((selector->hlt_bjet_GN120220813_pb->size())>= 2))
  {
	jet_HLTGN1->addEvent(HLTGN1.at(jet_n), weight, selector->jet_truthflav);
    jet_HLTDL1d->addEvent(HLTDL1d.at(jet_n), weight, selector->jet_truthflav);

  }
  if (!m_data)
    HadronConeExclExtendedTruthLabelID->addEvent(selector->HadronConeExclExtendedTruthLabelID->at(jet_n), weight, selector->jet_truthflav);
  if (this->n_jets == 2)
    {
      jet_m_jl->addEvent(selector->jet_m_jl->at(jet_n) / 1000, weight, selector->jet_truthflav);
      if (!m_data)
	jet_HadronConeExclExtendedTruthLabelID_pt->addEvent(selector->HadronConeExclExtendedTruthLabelID->at(jet_n), selector->jet_pt, weight, selector->jet_truthflav);
    }
}

double* jet_Container::getDArray(int bins, double lower, double upper)
{
  double interval = (upper - lower) / bins;
  double *ar = new double[bins + 1];

  for (int bin = 0; bin <= bins; bin++)
    ar[bin] = lower + bin * interval;
  return ar;
}
