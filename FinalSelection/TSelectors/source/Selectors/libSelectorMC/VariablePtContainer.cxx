// // VariablePtContainer.cxx

#include "VariablePtContainer.h"

ClassImp(Variable_pt_Container)

Variable_pt_Container::Variable_pt_Container(std::string name, int n_jets, int bins, double xmin, double xmax,int ybins, double ymin, double ymax, bool data, std::vector<std::string> eflavours)
{
    this->name = name;
    this->data = data;
    if (data)
    {
        eflavours.clear();
        eflavours.push_back("data");
    }
    for (const auto &eventFlav : eflavours)
    {
        histograms[eventFlav] = new TH2F(("h_" + name + "_" + eventFlav).c_str(), (name + " with Jet Flavours: " + eventFlav).c_str(), bins, xmin, xmax, ybins, ymin, ymax);
        //cout<<"created: "<< ("h_" + name + "_" + flav) <<endl;
        histograms[eventFlav]->Sumw2();
    }
}

Variable_pt_Container::~Variable_pt_Container()
{
}

void Variable_pt_Container::Write()
{
    for (const auto &x : histograms)
        x.second->Write();
}

void Variable_pt_Container::addEvent(double value, float pt,double weight, std::vector<int> *jet_truthflav)
{ //defing string flavour "bb","bl"...:
    std::string eventFlav = getEventFlav(jet_truthflav);
    // cout<<"filling: "<< ("h_" + this->name + "_" + eventFlav) <<endl;
    //adding event
    histograms[eventFlav]->Fill(value, pt/1000. ,weight);
}

std::string Variable_pt_Container::getEventFlav(std::vector<int> *jet_truthflav)
{
    std::string eventFlav = "";
    if (data)
    {
        eventFlav = "data";
    }
    else
    {
        for (int i = 0; i < 2; i++)
        {
            if (jet_truthflav->at(i) == 4)
                eventFlav = eventFlav + "c";
            else if (jet_truthflav->at(i) == 5)
                eventFlav = eventFlav + "b";
            else
                eventFlav = eventFlav + "l"; //this is also tau jets!
        }
    }
    return eventFlav;
}
