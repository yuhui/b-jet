// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME libSelectorMCCintDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "ROOT/RConfig.hxx"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Header files passed as explicit arguments
#include "libSelectorMC/BTagToolContainer.h"
#include "libSelectorMC/DilepChannel.h"
#include "libSelectorMC/HistsForFitContainer.h"
#include "libSelectorMC/JLCombination.h"
#include "libSelectorMC/JLMinMSubCombination.h"
#include "libSelectorMC/JLMinMSumCombination.h"
#include "libSelectorMC/JLdRCombination.h"
#include "libSelectorMC/JetContainer.h"
#include "libSelectorMC/PtBinMeanContainer.h"
#include "libSelectorMC/PtBinSingle.h"
#include "libSelectorMC/TreeReader.h"
#include "libSelectorMC/VariableContainer.h"
#include "libSelectorMC/VariablePtBinsContainer.h"
#include "libSelectorMC/VariablePtBinsCorrelationContainer.h"
#include "libSelectorMC/VariablePtContainer.h"
#include "libSelectorMC/XXContainer.h"
#include "libSelectorMC/finalSelectionMC.h"
#include "libSelectorMC/finalSelectionMC.h"
#include "libSelectorMC/DilepChannel.h"
#include "libSelectorMC/TreeReader.h"
#include "libSelectorMC/HistsForFitContainer.h"
#include "libSelectorMC/BTagToolContainer.h"
#include "libSelectorMC/PtBinMeanContainer.h"
#include "libSelectorMC/PtBinSingle.h"
#include "libSelectorMC/JetContainer.h"
#include "libSelectorMC/VariableContainer.h"
#include "libSelectorMC/VariablePtBinsContainer.h"
#include "libSelectorMC/VariablePtBinsCorrelationContainer.h"
#include "libSelectorMC/VariablePtContainer.h"
#include "libSelectorMC/XXContainer.h"
#include "libSelectorMC/JLCombination.h"
#include "libSelectorMC/JLMinMSubCombination.h"
#include "libSelectorMC/JLMinMSumCombination.h"
#include "libSelectorMC/JLdRCombination.h"

// Header files passed via #pragma extra_include

// The generated code does not explicitly qualify STL entities
namespace std {} using namespace std;

namespace ROOT {
   static void delete_Variable_Container(void *p);
   static void deleteArray_Variable_Container(void *p);
   static void destruct_Variable_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Variable_Container*)
   {
      ::Variable_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Variable_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Variable_Container", ::Variable_Container::Class_Version(), "libSelectorMC/VariableContainer.h", 11,
                  typeid(::Variable_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Variable_Container::Dictionary, isa_proxy, 4,
                  sizeof(::Variable_Container) );
      instance.SetDelete(&delete_Variable_Container);
      instance.SetDeleteArray(&deleteArray_Variable_Container);
      instance.SetDestructor(&destruct_Variable_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Variable_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Variable_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Variable_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Variable_ptBins_Container(void *p);
   static void deleteArray_Variable_ptBins_Container(void *p);
   static void destruct_Variable_ptBins_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Variable_ptBins_Container*)
   {
      ::Variable_ptBins_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Variable_ptBins_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Variable_ptBins_Container", ::Variable_ptBins_Container::Class_Version(), "libSelectorMC/VariablePtBinsContainer.h", 8,
                  typeid(::Variable_ptBins_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Variable_ptBins_Container::Dictionary, isa_proxy, 4,
                  sizeof(::Variable_ptBins_Container) );
      instance.SetDelete(&delete_Variable_ptBins_Container);
      instance.SetDeleteArray(&deleteArray_Variable_ptBins_Container);
      instance.SetDestructor(&destruct_Variable_ptBins_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Variable_ptBins_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Variable_ptBins_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Variable_ptBins_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Variable_ptBins_Correlation_Container(void *p);
   static void deleteArray_Variable_ptBins_Correlation_Container(void *p);
   static void destruct_Variable_ptBins_Correlation_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Variable_ptBins_Correlation_Container*)
   {
      ::Variable_ptBins_Correlation_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Variable_ptBins_Correlation_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Variable_ptBins_Correlation_Container", ::Variable_ptBins_Correlation_Container::Class_Version(), "libSelectorMC/VariablePtBinsCorrelationContainer.h", 13,
                  typeid(::Variable_ptBins_Correlation_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Variable_ptBins_Correlation_Container::Dictionary, isa_proxy, 4,
                  sizeof(::Variable_ptBins_Correlation_Container) );
      instance.SetDelete(&delete_Variable_ptBins_Correlation_Container);
      instance.SetDeleteArray(&deleteArray_Variable_ptBins_Correlation_Container);
      instance.SetDestructor(&destruct_Variable_ptBins_Correlation_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Variable_ptBins_Correlation_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Variable_ptBins_Correlation_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Variable_ptBins_Correlation_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_TreeReader(void *p = nullptr);
   static void *newArray_TreeReader(Long_t size, void *p);
   static void delete_TreeReader(void *p);
   static void deleteArray_TreeReader(void *p);
   static void destruct_TreeReader(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeReader*)
   {
      ::TreeReader *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TreeReader >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("TreeReader", ::TreeReader::Class_Version(), "libSelectorMC/TreeReader.h", 36,
                  typeid(::TreeReader), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TreeReader::Dictionary, isa_proxy, 4,
                  sizeof(::TreeReader) );
      instance.SetNew(&new_TreeReader);
      instance.SetNewArray(&newArray_TreeReader);
      instance.SetDelete(&delete_TreeReader);
      instance.SetDeleteArray(&deleteArray_TreeReader);
      instance.SetDestructor(&destruct_TreeReader);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeReader*)
   {
      return GenerateInitInstanceLocal(static_cast<::TreeReader*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::TreeReader*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_jet_Container(void *p);
   static void deleteArray_jet_Container(void *p);
   static void destruct_jet_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::jet_Container*)
   {
      ::jet_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::jet_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("jet_Container", ::jet_Container::Class_Version(), "libSelectorMC/JetContainer.h", 10,
                  typeid(::jet_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::jet_Container::Dictionary, isa_proxy, 4,
                  sizeof(::jet_Container) );
      instance.SetDelete(&delete_jet_Container);
      instance.SetDeleteArray(&deleteArray_jet_Container);
      instance.SetDestructor(&destruct_jet_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::jet_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::jet_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::jet_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_BtagTool_Container(void *p);
   static void deleteArray_BtagTool_Container(void *p);
   static void destruct_BtagTool_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::BtagTool_Container*)
   {
      ::BtagTool_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::BtagTool_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("BtagTool_Container", ::BtagTool_Container::Class_Version(), "libSelectorMC/BTagToolContainer.h", 12,
                  typeid(::BtagTool_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::BtagTool_Container::Dictionary, isa_proxy, 4,
                  sizeof(::BtagTool_Container) );
      instance.SetDelete(&delete_BtagTool_Container);
      instance.SetDeleteArray(&deleteArray_BtagTool_Container);
      instance.SetDestructor(&destruct_BtagTool_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::BtagTool_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::BtagTool_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::BtagTool_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Pt_bin_single(void *p);
   static void deleteArray_Pt_bin_single(void *p);
   static void destruct_Pt_bin_single(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Pt_bin_single*)
   {
      ::Pt_bin_single *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Pt_bin_single >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Pt_bin_single", ::Pt_bin_single::Class_Version(), "libSelectorMC/PtBinSingle.h", 13,
                  typeid(::Pt_bin_single), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Pt_bin_single::Dictionary, isa_proxy, 4,
                  sizeof(::Pt_bin_single) );
      instance.SetDelete(&delete_Pt_bin_single);
      instance.SetDeleteArray(&deleteArray_Pt_bin_single);
      instance.SetDestructor(&destruct_Pt_bin_single);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Pt_bin_single*)
   {
      return GenerateInitInstanceLocal(static_cast<::Pt_bin_single*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Pt_bin_single*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Pt_bin_mean_container(void *p);
   static void deleteArray_Pt_bin_mean_container(void *p);
   static void destruct_Pt_bin_mean_container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Pt_bin_mean_container*)
   {
      ::Pt_bin_mean_container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Pt_bin_mean_container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Pt_bin_mean_container", ::Pt_bin_mean_container::Class_Version(), "libSelectorMC/PtBinMeanContainer.h", 11,
                  typeid(::Pt_bin_mean_container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Pt_bin_mean_container::Dictionary, isa_proxy, 4,
                  sizeof(::Pt_bin_mean_container) );
      instance.SetDelete(&delete_Pt_bin_mean_container);
      instance.SetDeleteArray(&deleteArray_Pt_bin_mean_container);
      instance.SetDestructor(&destruct_Pt_bin_mean_container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Pt_bin_mean_container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Pt_bin_mean_container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Pt_bin_mean_container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Hist_for_fit_Container(void *p);
   static void deleteArray_Hist_for_fit_Container(void *p);
   static void destruct_Hist_for_fit_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hist_for_fit_Container*)
   {
      ::Hist_for_fit_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hist_for_fit_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Hist_for_fit_Container", ::Hist_for_fit_Container::Class_Version(), "libSelectorMC/HistsForFitContainer.h", 18,
                  typeid(::Hist_for_fit_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hist_for_fit_Container::Dictionary, isa_proxy, 4,
                  sizeof(::Hist_for_fit_Container) );
      instance.SetDelete(&delete_Hist_for_fit_Container);
      instance.SetDeleteArray(&deleteArray_Hist_for_fit_Container);
      instance.SetDestructor(&destruct_Hist_for_fit_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hist_for_fit_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Hist_for_fit_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Hist_for_fit_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_XX_Container(void *p);
   static void deleteArray_XX_Container(void *p);
   static void destruct_XX_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::XX_Container*)
   {
      ::XX_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::XX_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("XX_Container", ::XX_Container::Class_Version(), "libSelectorMC/XXContainer.h", 10,
                  typeid(::XX_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::XX_Container::Dictionary, isa_proxy, 4,
                  sizeof(::XX_Container) );
      instance.SetDelete(&delete_XX_Container);
      instance.SetDeleteArray(&deleteArray_XX_Container);
      instance.SetDestructor(&destruct_XX_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::XX_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::XX_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::XX_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_JL_Combination(void *p);
   static void deleteArray_JL_Combination(void *p);
   static void destruct_JL_Combination(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JL_Combination*)
   {
      ::JL_Combination *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JL_Combination >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("JL_Combination", ::JL_Combination::Class_Version(), "libSelectorMC/JLCombination.h", 8,
                  typeid(::JL_Combination), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JL_Combination::Dictionary, isa_proxy, 4,
                  sizeof(::JL_Combination) );
      instance.SetDelete(&delete_JL_Combination);
      instance.SetDeleteArray(&deleteArray_JL_Combination);
      instance.SetDestructor(&destruct_JL_Combination);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JL_Combination*)
   {
      return GenerateInitInstanceLocal(static_cast<::JL_Combination*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::JL_Combination*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_JL_min_m_sum_Combination(void *p);
   static void deleteArray_JL_min_m_sum_Combination(void *p);
   static void destruct_JL_min_m_sum_Combination(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JL_min_m_sum_Combination*)
   {
      ::JL_min_m_sum_Combination *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JL_min_m_sum_Combination >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("JL_min_m_sum_Combination", ::JL_min_m_sum_Combination::Class_Version(), "libSelectorMC/JLMinMSumCombination.h", 8,
                  typeid(::JL_min_m_sum_Combination), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JL_min_m_sum_Combination::Dictionary, isa_proxy, 4,
                  sizeof(::JL_min_m_sum_Combination) );
      instance.SetDelete(&delete_JL_min_m_sum_Combination);
      instance.SetDeleteArray(&deleteArray_JL_min_m_sum_Combination);
      instance.SetDestructor(&destruct_JL_min_m_sum_Combination);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JL_min_m_sum_Combination*)
   {
      return GenerateInitInstanceLocal(static_cast<::JL_min_m_sum_Combination*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::JL_min_m_sum_Combination*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_JL_min_m_sub_Combination(void *p);
   static void deleteArray_JL_min_m_sub_Combination(void *p);
   static void destruct_JL_min_m_sub_Combination(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JL_min_m_sub_Combination*)
   {
      ::JL_min_m_sub_Combination *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JL_min_m_sub_Combination >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("JL_min_m_sub_Combination", ::JL_min_m_sub_Combination::Class_Version(), "libSelectorMC/JLMinMSubCombination.h", 9,
                  typeid(::JL_min_m_sub_Combination), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JL_min_m_sub_Combination::Dictionary, isa_proxy, 4,
                  sizeof(::JL_min_m_sub_Combination) );
      instance.SetDelete(&delete_JL_min_m_sub_Combination);
      instance.SetDeleteArray(&deleteArray_JL_min_m_sub_Combination);
      instance.SetDestructor(&destruct_JL_min_m_sub_Combination);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JL_min_m_sub_Combination*)
   {
      return GenerateInitInstanceLocal(static_cast<::JL_min_m_sub_Combination*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::JL_min_m_sub_Combination*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_JL_closest_dR_Combination(void *p);
   static void deleteArray_JL_closest_dR_Combination(void *p);
   static void destruct_JL_closest_dR_Combination(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::JL_closest_dR_Combination*)
   {
      ::JL_closest_dR_Combination *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::JL_closest_dR_Combination >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("JL_closest_dR_Combination", ::JL_closest_dR_Combination::Class_Version(), "libSelectorMC/JLdRCombination.h", 9,
                  typeid(::JL_closest_dR_Combination), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::JL_closest_dR_Combination::Dictionary, isa_proxy, 4,
                  sizeof(::JL_closest_dR_Combination) );
      instance.SetDelete(&delete_JL_closest_dR_Combination);
      instance.SetDeleteArray(&deleteArray_JL_closest_dR_Combination);
      instance.SetDestructor(&destruct_JL_closest_dR_Combination);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::JL_closest_dR_Combination*)
   {
      return GenerateInitInstanceLocal(static_cast<::JL_closest_dR_Combination*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::JL_closest_dR_Combination*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_Variable_pt_Container(void *p);
   static void deleteArray_Variable_pt_Container(void *p);
   static void destruct_Variable_pt_Container(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Variable_pt_Container*)
   {
      ::Variable_pt_Container *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Variable_pt_Container >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("Variable_pt_Container", ::Variable_pt_Container::Class_Version(), "libSelectorMC/VariablePtContainer.h", 10,
                  typeid(::Variable_pt_Container), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Variable_pt_Container::Dictionary, isa_proxy, 4,
                  sizeof(::Variable_pt_Container) );
      instance.SetDelete(&delete_Variable_pt_Container);
      instance.SetDeleteArray(&deleteArray_Variable_pt_Container);
      instance.SetDestructor(&destruct_Variable_pt_Container);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Variable_pt_Container*)
   {
      return GenerateInitInstanceLocal(static_cast<::Variable_pt_Container*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::Variable_pt_Container*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_DiLepChannel(void *p);
   static void deleteArray_DiLepChannel(void *p);
   static void destruct_DiLepChannel(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DiLepChannel*)
   {
      ::DiLepChannel *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::DiLepChannel >(nullptr);
      static ::ROOT::TGenericClassInfo 
         instance("DiLepChannel", ::DiLepChannel::Class_Version(), "libSelectorMC/DilepChannel.h", 15,
                  typeid(::DiLepChannel), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::DiLepChannel::Dictionary, isa_proxy, 4,
                  sizeof(::DiLepChannel) );
      instance.SetDelete(&delete_DiLepChannel);
      instance.SetDeleteArray(&deleteArray_DiLepChannel);
      instance.SetDestructor(&destruct_DiLepChannel);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DiLepChannel*)
   {
      return GenerateInitInstanceLocal(static_cast<::DiLepChannel*>(nullptr));
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const ::DiLepChannel*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr Variable_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Variable_Container::Class_Name()
{
   return "Variable_Container";
}

//______________________________________________________________________________
const char *Variable_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Variable_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Variable_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Variable_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Variable_ptBins_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Variable_ptBins_Container::Class_Name()
{
   return "Variable_ptBins_Container";
}

//______________________________________________________________________________
const char *Variable_ptBins_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Variable_ptBins_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Variable_ptBins_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Variable_ptBins_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Variable_ptBins_Correlation_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Variable_ptBins_Correlation_Container::Class_Name()
{
   return "Variable_ptBins_Correlation_Container";
}

//______________________________________________________________________________
const char *Variable_ptBins_Correlation_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Correlation_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Variable_ptBins_Correlation_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Correlation_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Variable_ptBins_Correlation_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Correlation_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Variable_ptBins_Correlation_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_ptBins_Correlation_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr TreeReader::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *TreeReader::Class_Name()
{
   return "TreeReader";
}

//______________________________________________________________________________
const char *TreeReader::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TreeReader*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int TreeReader::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TreeReader*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TreeReader::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TreeReader*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TreeReader::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TreeReader*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr jet_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *jet_Container::Class_Name()
{
   return "jet_Container";
}

//______________________________________________________________________________
const char *jet_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::jet_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int jet_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::jet_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *jet_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::jet_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *jet_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::jet_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr BtagTool_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *BtagTool_Container::Class_Name()
{
   return "BtagTool_Container";
}

//______________________________________________________________________________
const char *BtagTool_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BtagTool_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int BtagTool_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::BtagTool_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *BtagTool_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BtagTool_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *BtagTool_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::BtagTool_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Pt_bin_single::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Pt_bin_single::Class_Name()
{
   return "Pt_bin_single";
}

//______________________________________________________________________________
const char *Pt_bin_single::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_single*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Pt_bin_single::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_single*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Pt_bin_single::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_single*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Pt_bin_single::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_single*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Pt_bin_mean_container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Pt_bin_mean_container::Class_Name()
{
   return "Pt_bin_mean_container";
}

//______________________________________________________________________________
const char *Pt_bin_mean_container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_mean_container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Pt_bin_mean_container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_mean_container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Pt_bin_mean_container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_mean_container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Pt_bin_mean_container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Pt_bin_mean_container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Hist_for_fit_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Hist_for_fit_Container::Class_Name()
{
   return "Hist_for_fit_Container";
}

//______________________________________________________________________________
const char *Hist_for_fit_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hist_for_fit_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Hist_for_fit_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hist_for_fit_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Hist_for_fit_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hist_for_fit_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Hist_for_fit_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hist_for_fit_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr XX_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *XX_Container::Class_Name()
{
   return "XX_Container";
}

//______________________________________________________________________________
const char *XX_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::XX_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int XX_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::XX_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *XX_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::XX_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *XX_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::XX_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JL_Combination::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *JL_Combination::Class_Name()
{
   return "JL_Combination";
}

//______________________________________________________________________________
const char *JL_Combination::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_Combination*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int JL_Combination::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_Combination*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JL_Combination::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_Combination*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JL_Combination::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_Combination*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JL_min_m_sum_Combination::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *JL_min_m_sum_Combination::Class_Name()
{
   return "JL_min_m_sum_Combination";
}

//______________________________________________________________________________
const char *JL_min_m_sum_Combination::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sum_Combination*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int JL_min_m_sum_Combination::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sum_Combination*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JL_min_m_sum_Combination::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sum_Combination*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JL_min_m_sum_Combination::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sum_Combination*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JL_min_m_sub_Combination::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *JL_min_m_sub_Combination::Class_Name()
{
   return "JL_min_m_sub_Combination";
}

//______________________________________________________________________________
const char *JL_min_m_sub_Combination::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sub_Combination*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int JL_min_m_sub_Combination::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sub_Combination*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JL_min_m_sub_Combination::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sub_Combination*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JL_min_m_sub_Combination::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_min_m_sub_Combination*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr JL_closest_dR_Combination::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *JL_closest_dR_Combination::Class_Name()
{
   return "JL_closest_dR_Combination";
}

//______________________________________________________________________________
const char *JL_closest_dR_Combination::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_closest_dR_Combination*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int JL_closest_dR_Combination::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::JL_closest_dR_Combination*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *JL_closest_dR_Combination::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_closest_dR_Combination*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *JL_closest_dR_Combination::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::JL_closest_dR_Combination*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr Variable_pt_Container::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *Variable_pt_Container::Class_Name()
{
   return "Variable_pt_Container";
}

//______________________________________________________________________________
const char *Variable_pt_Container::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_pt_Container*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int Variable_pt_Container::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Variable_pt_Container*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Variable_pt_Container::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_pt_Container*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Variable_pt_Container::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Variable_pt_Container*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr DiLepChannel::fgIsA(nullptr);  // static to hold class pointer

//______________________________________________________________________________
const char *DiLepChannel::Class_Name()
{
   return "DiLepChannel";
}

//______________________________________________________________________________
const char *DiLepChannel::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DiLepChannel*)nullptr)->GetImplFileName();
}

//______________________________________________________________________________
int DiLepChannel::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::DiLepChannel*)nullptr)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *DiLepChannel::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DiLepChannel*)nullptr)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *DiLepChannel::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::DiLepChannel*)nullptr)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void Variable_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Variable_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Variable_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Variable_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Variable_Container(void *p) {
      delete (static_cast<::Variable_Container*>(p));
   }
   static void deleteArray_Variable_Container(void *p) {
      delete [] (static_cast<::Variable_Container*>(p));
   }
   static void destruct_Variable_Container(void *p) {
      typedef ::Variable_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Variable_Container

//______________________________________________________________________________
void Variable_ptBins_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Variable_ptBins_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Variable_ptBins_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Variable_ptBins_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Variable_ptBins_Container(void *p) {
      delete (static_cast<::Variable_ptBins_Container*>(p));
   }
   static void deleteArray_Variable_ptBins_Container(void *p) {
      delete [] (static_cast<::Variable_ptBins_Container*>(p));
   }
   static void destruct_Variable_ptBins_Container(void *p) {
      typedef ::Variable_ptBins_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Variable_ptBins_Container

//______________________________________________________________________________
void Variable_ptBins_Correlation_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Variable_ptBins_Correlation_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Variable_ptBins_Correlation_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Variable_ptBins_Correlation_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Variable_ptBins_Correlation_Container(void *p) {
      delete (static_cast<::Variable_ptBins_Correlation_Container*>(p));
   }
   static void deleteArray_Variable_ptBins_Correlation_Container(void *p) {
      delete [] (static_cast<::Variable_ptBins_Correlation_Container*>(p));
   }
   static void destruct_Variable_ptBins_Correlation_Container(void *p) {
      typedef ::Variable_ptBins_Correlation_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Variable_ptBins_Correlation_Container

//______________________________________________________________________________
void TreeReader::Streamer(TBuffer &R__b)
{
   // Stream an object of class TreeReader.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TreeReader::Class(),this);
   } else {
      R__b.WriteClassBuffer(TreeReader::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TreeReader(void *p) {
      return  p ? new(p) ::TreeReader : new ::TreeReader;
   }
   static void *newArray_TreeReader(Long_t nElements, void *p) {
      return p ? new(p) ::TreeReader[nElements] : new ::TreeReader[nElements];
   }
   // Wrapper around operator delete
   static void delete_TreeReader(void *p) {
      delete (static_cast<::TreeReader*>(p));
   }
   static void deleteArray_TreeReader(void *p) {
      delete [] (static_cast<::TreeReader*>(p));
   }
   static void destruct_TreeReader(void *p) {
      typedef ::TreeReader current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::TreeReader

//______________________________________________________________________________
void jet_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class jet_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(jet_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(jet_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_jet_Container(void *p) {
      delete (static_cast<::jet_Container*>(p));
   }
   static void deleteArray_jet_Container(void *p) {
      delete [] (static_cast<::jet_Container*>(p));
   }
   static void destruct_jet_Container(void *p) {
      typedef ::jet_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::jet_Container

//______________________________________________________________________________
void BtagTool_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class BtagTool_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(BtagTool_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(BtagTool_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_BtagTool_Container(void *p) {
      delete (static_cast<::BtagTool_Container*>(p));
   }
   static void deleteArray_BtagTool_Container(void *p) {
      delete [] (static_cast<::BtagTool_Container*>(p));
   }
   static void destruct_BtagTool_Container(void *p) {
      typedef ::BtagTool_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::BtagTool_Container

//______________________________________________________________________________
void Pt_bin_single::Streamer(TBuffer &R__b)
{
   // Stream an object of class Pt_bin_single.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Pt_bin_single::Class(),this);
   } else {
      R__b.WriteClassBuffer(Pt_bin_single::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Pt_bin_single(void *p) {
      delete (static_cast<::Pt_bin_single*>(p));
   }
   static void deleteArray_Pt_bin_single(void *p) {
      delete [] (static_cast<::Pt_bin_single*>(p));
   }
   static void destruct_Pt_bin_single(void *p) {
      typedef ::Pt_bin_single current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Pt_bin_single

//______________________________________________________________________________
void Pt_bin_mean_container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Pt_bin_mean_container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Pt_bin_mean_container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Pt_bin_mean_container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Pt_bin_mean_container(void *p) {
      delete (static_cast<::Pt_bin_mean_container*>(p));
   }
   static void deleteArray_Pt_bin_mean_container(void *p) {
      delete [] (static_cast<::Pt_bin_mean_container*>(p));
   }
   static void destruct_Pt_bin_mean_container(void *p) {
      typedef ::Pt_bin_mean_container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Pt_bin_mean_container

//______________________________________________________________________________
void Hist_for_fit_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hist_for_fit_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hist_for_fit_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hist_for_fit_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Hist_for_fit_Container(void *p) {
      delete (static_cast<::Hist_for_fit_Container*>(p));
   }
   static void deleteArray_Hist_for_fit_Container(void *p) {
      delete [] (static_cast<::Hist_for_fit_Container*>(p));
   }
   static void destruct_Hist_for_fit_Container(void *p) {
      typedef ::Hist_for_fit_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Hist_for_fit_Container

//______________________________________________________________________________
void XX_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class XX_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(XX_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(XX_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_XX_Container(void *p) {
      delete (static_cast<::XX_Container*>(p));
   }
   static void deleteArray_XX_Container(void *p) {
      delete [] (static_cast<::XX_Container*>(p));
   }
   static void destruct_XX_Container(void *p) {
      typedef ::XX_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::XX_Container

//______________________________________________________________________________
void JL_Combination::Streamer(TBuffer &R__b)
{
   // Stream an object of class JL_Combination.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JL_Combination::Class(),this);
   } else {
      R__b.WriteClassBuffer(JL_Combination::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JL_Combination(void *p) {
      delete (static_cast<::JL_Combination*>(p));
   }
   static void deleteArray_JL_Combination(void *p) {
      delete [] (static_cast<::JL_Combination*>(p));
   }
   static void destruct_JL_Combination(void *p) {
      typedef ::JL_Combination current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::JL_Combination

//______________________________________________________________________________
void JL_min_m_sum_Combination::Streamer(TBuffer &R__b)
{
   // Stream an object of class JL_min_m_sum_Combination.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JL_min_m_sum_Combination::Class(),this);
   } else {
      R__b.WriteClassBuffer(JL_min_m_sum_Combination::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JL_min_m_sum_Combination(void *p) {
      delete (static_cast<::JL_min_m_sum_Combination*>(p));
   }
   static void deleteArray_JL_min_m_sum_Combination(void *p) {
      delete [] (static_cast<::JL_min_m_sum_Combination*>(p));
   }
   static void destruct_JL_min_m_sum_Combination(void *p) {
      typedef ::JL_min_m_sum_Combination current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::JL_min_m_sum_Combination

//______________________________________________________________________________
void JL_min_m_sub_Combination::Streamer(TBuffer &R__b)
{
   // Stream an object of class JL_min_m_sub_Combination.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JL_min_m_sub_Combination::Class(),this);
   } else {
      R__b.WriteClassBuffer(JL_min_m_sub_Combination::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JL_min_m_sub_Combination(void *p) {
      delete (static_cast<::JL_min_m_sub_Combination*>(p));
   }
   static void deleteArray_JL_min_m_sub_Combination(void *p) {
      delete [] (static_cast<::JL_min_m_sub_Combination*>(p));
   }
   static void destruct_JL_min_m_sub_Combination(void *p) {
      typedef ::JL_min_m_sub_Combination current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::JL_min_m_sub_Combination

//______________________________________________________________________________
void JL_closest_dR_Combination::Streamer(TBuffer &R__b)
{
   // Stream an object of class JL_closest_dR_Combination.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(JL_closest_dR_Combination::Class(),this);
   } else {
      R__b.WriteClassBuffer(JL_closest_dR_Combination::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_JL_closest_dR_Combination(void *p) {
      delete (static_cast<::JL_closest_dR_Combination*>(p));
   }
   static void deleteArray_JL_closest_dR_Combination(void *p) {
      delete [] (static_cast<::JL_closest_dR_Combination*>(p));
   }
   static void destruct_JL_closest_dR_Combination(void *p) {
      typedef ::JL_closest_dR_Combination current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::JL_closest_dR_Combination

//______________________________________________________________________________
void Variable_pt_Container::Streamer(TBuffer &R__b)
{
   // Stream an object of class Variable_pt_Container.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Variable_pt_Container::Class(),this);
   } else {
      R__b.WriteClassBuffer(Variable_pt_Container::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Variable_pt_Container(void *p) {
      delete (static_cast<::Variable_pt_Container*>(p));
   }
   static void deleteArray_Variable_pt_Container(void *p) {
      delete [] (static_cast<::Variable_pt_Container*>(p));
   }
   static void destruct_Variable_pt_Container(void *p) {
      typedef ::Variable_pt_Container current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::Variable_pt_Container

//______________________________________________________________________________
void DiLepChannel::Streamer(TBuffer &R__b)
{
   // Stream an object of class DiLepChannel.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(DiLepChannel::Class(),this);
   } else {
      R__b.WriteClassBuffer(DiLepChannel::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DiLepChannel(void *p) {
      delete (static_cast<::DiLepChannel*>(p));
   }
   static void deleteArray_DiLepChannel(void *p) {
      delete [] (static_cast<::DiLepChannel*>(p));
   }
   static void destruct_DiLepChannel(void *p) {
      typedef ::DiLepChannel current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class ::DiLepChannel

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = nullptr);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 423,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<string>","std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<string>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<string>*>(nullptr))->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete (static_cast<vector<string>*>(p));
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] (static_cast<vector<string>*>(p));
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace ROOT {
   static TClass *vectorlEjet_ContainermUgR_Dictionary();
   static void vectorlEjet_ContainermUgR_TClassManip(TClass*);
   static void *new_vectorlEjet_ContainermUgR(void *p = nullptr);
   static void *newArray_vectorlEjet_ContainermUgR(Long_t size, void *p);
   static void delete_vectorlEjet_ContainermUgR(void *p);
   static void deleteArray_vectorlEjet_ContainermUgR(void *p);
   static void destruct_vectorlEjet_ContainermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<jet_Container*>*)
   {
      vector<jet_Container*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<jet_Container*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<jet_Container*>", -2, "vector", 423,
                  typeid(vector<jet_Container*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEjet_ContainermUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<jet_Container*>) );
      instance.SetNew(&new_vectorlEjet_ContainermUgR);
      instance.SetNewArray(&newArray_vectorlEjet_ContainermUgR);
      instance.SetDelete(&delete_vectorlEjet_ContainermUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEjet_ContainermUgR);
      instance.SetDestructor(&destruct_vectorlEjet_ContainermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<jet_Container*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<jet_Container*>","std::vector<jet_Container*, std::allocator<jet_Container*> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<jet_Container*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEjet_ContainermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<jet_Container*>*>(nullptr))->GetClass();
      vectorlEjet_ContainermUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEjet_ContainermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEjet_ContainermUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<jet_Container*> : new vector<jet_Container*>;
   }
   static void *newArray_vectorlEjet_ContainermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<jet_Container*>[nElements] : new vector<jet_Container*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEjet_ContainermUgR(void *p) {
      delete (static_cast<vector<jet_Container*>*>(p));
   }
   static void deleteArray_vectorlEjet_ContainermUgR(void *p) {
      delete [] (static_cast<vector<jet_Container*>*>(p));
   }
   static void destruct_vectorlEjet_ContainermUgR(void *p) {
      typedef vector<jet_Container*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<jet_Container*>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = nullptr);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 423,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<int>","std::vector<int, std::allocator<int> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<int>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<int>*>(nullptr))->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete (static_cast<vector<int>*>(p));
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] (static_cast<vector<int>*>(p));
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = nullptr);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 423,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<float>","std::vector<float, std::allocator<float> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<float>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<float>*>(nullptr))->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete (static_cast<vector<float>*>(p));
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] (static_cast<vector<float>*>(p));
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = nullptr);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 423,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<double>","std::vector<double, std::allocator<double> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<double>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<double>*>(nullptr))->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete (static_cast<vector<double>*>(p));
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] (static_cast<vector<double>*>(p));
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlEchargR_Dictionary();
   static void vectorlEchargR_TClassManip(TClass*);
   static void *new_vectorlEchargR(void *p = nullptr);
   static void *newArray_vectorlEchargR(Long_t size, void *p);
   static void delete_vectorlEchargR(void *p);
   static void deleteArray_vectorlEchargR(void *p);
   static void destruct_vectorlEchargR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<char>*)
   {
      vector<char> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<char>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<char>", -2, "vector", 423,
                  typeid(vector<char>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEchargR_Dictionary, isa_proxy, 0,
                  sizeof(vector<char>) );
      instance.SetNew(&new_vectorlEchargR);
      instance.SetNewArray(&newArray_vectorlEchargR);
      instance.SetDelete(&delete_vectorlEchargR);
      instance.SetDeleteArray(&deleteArray_vectorlEchargR);
      instance.SetDestructor(&destruct_vectorlEchargR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<char> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<char>","std::vector<char, std::allocator<char> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<char>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEchargR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<char>*>(nullptr))->GetClass();
      vectorlEchargR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEchargR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEchargR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char> : new vector<char>;
   }
   static void *newArray_vectorlEchargR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<char>[nElements] : new vector<char>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEchargR(void *p) {
      delete (static_cast<vector<char>*>(p));
   }
   static void deleteArray_vectorlEchargR(void *p) {
      delete [] (static_cast<vector<char>*>(p));
   }
   static void destruct_vectorlEchargR(void *p) {
      typedef vector<char> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<char>

namespace ROOT {
   static TClass *vectorlEboolgR_Dictionary();
   static void vectorlEboolgR_TClassManip(TClass*);
   static void *new_vectorlEboolgR(void *p = nullptr);
   static void *newArray_vectorlEboolgR(Long_t size, void *p);
   static void delete_vectorlEboolgR(void *p);
   static void deleteArray_vectorlEboolgR(void *p);
   static void destruct_vectorlEboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<bool>*)
   {
      vector<bool> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<bool>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<bool>", -2, "vector", 702,
                  typeid(vector<bool>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEboolgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<bool>) );
      instance.SetNew(&new_vectorlEboolgR);
      instance.SetNewArray(&newArray_vectorlEboolgR);
      instance.SetDelete(&delete_vectorlEboolgR);
      instance.SetDeleteArray(&deleteArray_vectorlEboolgR);
      instance.SetDestructor(&destruct_vectorlEboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<bool> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<bool>","std::vector<bool, std::allocator<bool> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<bool>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<bool>*>(nullptr))->GetClass();
      vectorlEboolgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEboolgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool> : new vector<bool>;
   }
   static void *newArray_vectorlEboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<bool>[nElements] : new vector<bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEboolgR(void *p) {
      delete (static_cast<vector<bool>*>(p));
   }
   static void deleteArray_vectorlEboolgR(void *p) {
      delete [] (static_cast<vector<bool>*>(p));
   }
   static void destruct_vectorlEboolgR(void *p) {
      typedef vector<bool> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<bool>

namespace ROOT {
   static TClass *vectorlEVariable_ContainermUgR_Dictionary();
   static void vectorlEVariable_ContainermUgR_TClassManip(TClass*);
   static void *new_vectorlEVariable_ContainermUgR(void *p = nullptr);
   static void *newArray_vectorlEVariable_ContainermUgR(Long_t size, void *p);
   static void delete_vectorlEVariable_ContainermUgR(void *p);
   static void deleteArray_vectorlEVariable_ContainermUgR(void *p);
   static void destruct_vectorlEVariable_ContainermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Variable_Container*>*)
   {
      vector<Variable_Container*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Variable_Container*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Variable_Container*>", -2, "vector", 423,
                  typeid(vector<Variable_Container*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEVariable_ContainermUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Variable_Container*>) );
      instance.SetNew(&new_vectorlEVariable_ContainermUgR);
      instance.SetNewArray(&newArray_vectorlEVariable_ContainermUgR);
      instance.SetDelete(&delete_vectorlEVariable_ContainermUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEVariable_ContainermUgR);
      instance.SetDestructor(&destruct_vectorlEVariable_ContainermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Variable_Container*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<Variable_Container*>","std::vector<Variable_Container*, std::allocator<Variable_Container*> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<Variable_Container*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEVariable_ContainermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<Variable_Container*>*>(nullptr))->GetClass();
      vectorlEVariable_ContainermUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEVariable_ContainermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEVariable_ContainermUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Variable_Container*> : new vector<Variable_Container*>;
   }
   static void *newArray_vectorlEVariable_ContainermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Variable_Container*>[nElements] : new vector<Variable_Container*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEVariable_ContainermUgR(void *p) {
      delete (static_cast<vector<Variable_Container*>*>(p));
   }
   static void deleteArray_vectorlEVariable_ContainermUgR(void *p) {
      delete [] (static_cast<vector<Variable_Container*>*>(p));
   }
   static void destruct_vectorlEVariable_ContainermUgR(void *p) {
      typedef vector<Variable_Container*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<Variable_Container*>

namespace ROOT {
   static TClass *vectorlETHnSparseTlETArrayDgRmUgR_Dictionary();
   static void vectorlETHnSparseTlETArrayDgRmUgR_TClassManip(TClass*);
   static void *new_vectorlETHnSparseTlETArrayDgRmUgR(void *p = nullptr);
   static void *newArray_vectorlETHnSparseTlETArrayDgRmUgR(Long_t size, void *p);
   static void delete_vectorlETHnSparseTlETArrayDgRmUgR(void *p);
   static void deleteArray_vectorlETHnSparseTlETArrayDgRmUgR(void *p);
   static void destruct_vectorlETHnSparseTlETArrayDgRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<THnSparseT<TArrayD>*>*)
   {
      vector<THnSparseT<TArrayD>*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<THnSparseT<TArrayD>*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<THnSparseT<TArrayD>*>", -2, "vector", 423,
                  typeid(vector<THnSparseT<TArrayD>*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETHnSparseTlETArrayDgRmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<THnSparseT<TArrayD>*>) );
      instance.SetNew(&new_vectorlETHnSparseTlETArrayDgRmUgR);
      instance.SetNewArray(&newArray_vectorlETHnSparseTlETArrayDgRmUgR);
      instance.SetDelete(&delete_vectorlETHnSparseTlETArrayDgRmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETHnSparseTlETArrayDgRmUgR);
      instance.SetDestructor(&destruct_vectorlETHnSparseTlETArrayDgRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<THnSparseT<TArrayD>*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<THnSparseT<TArrayD>*>","std::vector<THnSparseT<TArrayD>*, std::allocator<THnSparseT<TArrayD>*> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<THnSparseT<TArrayD>*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETHnSparseTlETArrayDgRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<THnSparseT<TArrayD>*>*>(nullptr))->GetClass();
      vectorlETHnSparseTlETArrayDgRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETHnSparseTlETArrayDgRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETHnSparseTlETArrayDgRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<THnSparseT<TArrayD>*> : new vector<THnSparseT<TArrayD>*>;
   }
   static void *newArray_vectorlETHnSparseTlETArrayDgRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<THnSparseT<TArrayD>*>[nElements] : new vector<THnSparseT<TArrayD>*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETHnSparseTlETArrayDgRmUgR(void *p) {
      delete (static_cast<vector<THnSparseT<TArrayD>*>*>(p));
   }
   static void deleteArray_vectorlETHnSparseTlETArrayDgRmUgR(void *p) {
      delete [] (static_cast<vector<THnSparseT<TArrayD>*>*>(p));
   }
   static void destruct_vectorlETHnSparseTlETArrayDgRmUgR(void *p) {
      typedef vector<THnSparseT<TArrayD>*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<THnSparseT<TArrayD>*>

namespace ROOT {
   static TClass *vectorlEPt_bin_singlemUgR_Dictionary();
   static void vectorlEPt_bin_singlemUgR_TClassManip(TClass*);
   static void *new_vectorlEPt_bin_singlemUgR(void *p = nullptr);
   static void *newArray_vectorlEPt_bin_singlemUgR(Long_t size, void *p);
   static void delete_vectorlEPt_bin_singlemUgR(void *p);
   static void deleteArray_vectorlEPt_bin_singlemUgR(void *p);
   static void destruct_vectorlEPt_bin_singlemUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Pt_bin_single*>*)
   {
      vector<Pt_bin_single*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Pt_bin_single*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Pt_bin_single*>", -2, "vector", 423,
                  typeid(vector<Pt_bin_single*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEPt_bin_singlemUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Pt_bin_single*>) );
      instance.SetNew(&new_vectorlEPt_bin_singlemUgR);
      instance.SetNewArray(&newArray_vectorlEPt_bin_singlemUgR);
      instance.SetDelete(&delete_vectorlEPt_bin_singlemUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEPt_bin_singlemUgR);
      instance.SetDestructor(&destruct_vectorlEPt_bin_singlemUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Pt_bin_single*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<Pt_bin_single*>","std::vector<Pt_bin_single*, std::allocator<Pt_bin_single*> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<Pt_bin_single*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEPt_bin_singlemUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<Pt_bin_single*>*>(nullptr))->GetClass();
      vectorlEPt_bin_singlemUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEPt_bin_singlemUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEPt_bin_singlemUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Pt_bin_single*> : new vector<Pt_bin_single*>;
   }
   static void *newArray_vectorlEPt_bin_singlemUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Pt_bin_single*>[nElements] : new vector<Pt_bin_single*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEPt_bin_singlemUgR(void *p) {
      delete (static_cast<vector<Pt_bin_single*>*>(p));
   }
   static void deleteArray_vectorlEPt_bin_singlemUgR(void *p) {
      delete [] (static_cast<vector<Pt_bin_single*>*>(p));
   }
   static void destruct_vectorlEPt_bin_singlemUgR(void *p) {
      typedef vector<Pt_bin_single*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<Pt_bin_single*>

namespace ROOT {
   static TClass *vectorlEBTaggingSelectionToolmUgR_Dictionary();
   static void vectorlEBTaggingSelectionToolmUgR_TClassManip(TClass*);
   static void *new_vectorlEBTaggingSelectionToolmUgR(void *p = nullptr);
   static void *newArray_vectorlEBTaggingSelectionToolmUgR(Long_t size, void *p);
   static void delete_vectorlEBTaggingSelectionToolmUgR(void *p);
   static void deleteArray_vectorlEBTaggingSelectionToolmUgR(void *p);
   static void destruct_vectorlEBTaggingSelectionToolmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<BTaggingSelectionTool*>*)
   {
      vector<BTaggingSelectionTool*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<BTaggingSelectionTool*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<BTaggingSelectionTool*>", -2, "vector", 423,
                  typeid(vector<BTaggingSelectionTool*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEBTaggingSelectionToolmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<BTaggingSelectionTool*>) );
      instance.SetNew(&new_vectorlEBTaggingSelectionToolmUgR);
      instance.SetNewArray(&newArray_vectorlEBTaggingSelectionToolmUgR);
      instance.SetDelete(&delete_vectorlEBTaggingSelectionToolmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEBTaggingSelectionToolmUgR);
      instance.SetDestructor(&destruct_vectorlEBTaggingSelectionToolmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<BTaggingSelectionTool*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("vector<BTaggingSelectionTool*>","std::vector<BTaggingSelectionTool*, std::allocator<BTaggingSelectionTool*> >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const vector<BTaggingSelectionTool*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEBTaggingSelectionToolmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const vector<BTaggingSelectionTool*>*>(nullptr))->GetClass();
      vectorlEBTaggingSelectionToolmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEBTaggingSelectionToolmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEBTaggingSelectionToolmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<BTaggingSelectionTool*> : new vector<BTaggingSelectionTool*>;
   }
   static void *newArray_vectorlEBTaggingSelectionToolmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<BTaggingSelectionTool*>[nElements] : new vector<BTaggingSelectionTool*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEBTaggingSelectionToolmUgR(void *p) {
      delete (static_cast<vector<BTaggingSelectionTool*>*>(p));
   }
   static void deleteArray_vectorlEBTaggingSelectionToolmUgR(void *p) {
      delete [] (static_cast<vector<BTaggingSelectionTool*>*>(p));
   }
   static void destruct_vectorlEBTaggingSelectionToolmUgR(void *p) {
      typedef vector<BTaggingSelectionTool*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class vector<BTaggingSelectionTool*>

namespace ROOT {
   static TClass *maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_Dictionary();
   static void maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_TClassManip(TClass*);
   static void *new_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p = nullptr);
   static void *newArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(Long_t size, void *p);
   static void delete_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p);
   static void deleteArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p);
   static void destruct_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,vector<THnSparseT<TArrayD>*> >*)
   {
      map<string,vector<THnSparseT<TArrayD>*> > *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,vector<THnSparseT<TArrayD>*> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,vector<THnSparseT<TArrayD>*> >", -2, "map", 100,
                  typeid(map<string,vector<THnSparseT<TArrayD>*> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,vector<THnSparseT<TArrayD>*> >) );
      instance.SetNew(&new_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR);
      instance.SetNewArray(&newArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR);
      instance.SetDelete(&delete_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR);
      instance.SetDestructor(&destruct_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,vector<THnSparseT<TArrayD>*> > >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,vector<THnSparseT<TArrayD>*> >","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::vector<THnSparseT<TArrayD>*, std::allocator<THnSparseT<TArrayD>*> >, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, std::vector<THnSparseT<TArrayD>*, std::allocator<THnSparseT<TArrayD>*> > > > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,vector<THnSparseT<TArrayD>*> >*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,vector<THnSparseT<TArrayD>*> >*>(nullptr))->GetClass();
      maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<THnSparseT<TArrayD>*> > : new map<string,vector<THnSparseT<TArrayD>*> >;
   }
   static void *newArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,vector<THnSparseT<TArrayD>*> >[nElements] : new map<string,vector<THnSparseT<TArrayD>*> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p) {
      delete (static_cast<map<string,vector<THnSparseT<TArrayD>*> >*>(p));
   }
   static void deleteArray_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p) {
      delete [] (static_cast<map<string,vector<THnSparseT<TArrayD>*> >*>(p));
   }
   static void destruct_maplEstringcOvectorlETHnSparseTlETArrayDgRmUgRsPgR(void *p) {
      typedef map<string,vector<THnSparseT<TArrayD>*> > current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,vector<THnSparseT<TArrayD>*> >

namespace ROOT {
   static TClass *maplEstringcOfloatgR_Dictionary();
   static void maplEstringcOfloatgR_TClassManip(TClass*);
   static void *new_maplEstringcOfloatgR(void *p = nullptr);
   static void *newArray_maplEstringcOfloatgR(Long_t size, void *p);
   static void delete_maplEstringcOfloatgR(void *p);
   static void deleteArray_maplEstringcOfloatgR(void *p);
   static void destruct_maplEstringcOfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,float>*)
   {
      map<string,float> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,float>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,float>", -2, "map", 100,
                  typeid(map<string,float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,float>) );
      instance.SetNew(&new_maplEstringcOfloatgR);
      instance.SetNewArray(&newArray_maplEstringcOfloatgR);
      instance.SetDelete(&delete_maplEstringcOfloatgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOfloatgR);
      instance.SetDestructor(&destruct_maplEstringcOfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,float> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,float>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, float, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, float> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,float>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,float>*>(nullptr))->GetClass();
      maplEstringcOfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,float> : new map<string,float>;
   }
   static void *newArray_maplEstringcOfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,float>[nElements] : new map<string,float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOfloatgR(void *p) {
      delete (static_cast<map<string,float>*>(p));
   }
   static void deleteArray_maplEstringcOfloatgR(void *p) {
      delete [] (static_cast<map<string,float>*>(p));
   }
   static void destruct_maplEstringcOfloatgR(void *p) {
      typedef map<string,float> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,float>

namespace ROOT {
   static TClass *maplEstringcOTHnSparseTlETArrayIgRmUgR_Dictionary();
   static void maplEstringcOTHnSparseTlETArrayIgRmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p = nullptr);
   static void *newArray_maplEstringcOTHnSparseTlETArrayIgRmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p);
   static void deleteArray_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p);
   static void destruct_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,THnSparseT<TArrayI>*>*)
   {
      map<string,THnSparseT<TArrayI>*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,THnSparseT<TArrayI>*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,THnSparseT<TArrayI>*>", -2, "map", 100,
                  typeid(map<string,THnSparseT<TArrayI>*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTHnSparseTlETArrayIgRmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,THnSparseT<TArrayI>*>) );
      instance.SetNew(&new_maplEstringcOTHnSparseTlETArrayIgRmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTHnSparseTlETArrayIgRmUgR);
      instance.SetDelete(&delete_maplEstringcOTHnSparseTlETArrayIgRmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTHnSparseTlETArrayIgRmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTHnSparseTlETArrayIgRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,THnSparseT<TArrayI>*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,THnSparseT<TArrayI>*>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, THnSparseT<TArrayI>*, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, THnSparseT<TArrayI>*> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,THnSparseT<TArrayI>*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTHnSparseTlETArrayIgRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,THnSparseT<TArrayI>*>*>(nullptr))->GetClass();
      maplEstringcOTHnSparseTlETArrayIgRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTHnSparseTlETArrayIgRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,THnSparseT<TArrayI>*> : new map<string,THnSparseT<TArrayI>*>;
   }
   static void *newArray_maplEstringcOTHnSparseTlETArrayIgRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,THnSparseT<TArrayI>*>[nElements] : new map<string,THnSparseT<TArrayI>*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p) {
      delete (static_cast<map<string,THnSparseT<TArrayI>*>*>(p));
   }
   static void deleteArray_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p) {
      delete [] (static_cast<map<string,THnSparseT<TArrayI>*>*>(p));
   }
   static void destruct_maplEstringcOTHnSparseTlETArrayIgRmUgR(void *p) {
      typedef map<string,THnSparseT<TArrayI>*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,THnSparseT<TArrayI>*>

namespace ROOT {
   static TClass *maplEstringcOTHnSparseTlETArrayDgRmUgR_Dictionary();
   static void maplEstringcOTHnSparseTlETArrayDgRmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p = nullptr);
   static void *newArray_maplEstringcOTHnSparseTlETArrayDgRmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p);
   static void deleteArray_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p);
   static void destruct_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,THnSparseT<TArrayD>*>*)
   {
      map<string,THnSparseT<TArrayD>*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,THnSparseT<TArrayD>*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,THnSparseT<TArrayD>*>", -2, "map", 100,
                  typeid(map<string,THnSparseT<TArrayD>*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTHnSparseTlETArrayDgRmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,THnSparseT<TArrayD>*>) );
      instance.SetNew(&new_maplEstringcOTHnSparseTlETArrayDgRmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTHnSparseTlETArrayDgRmUgR);
      instance.SetDelete(&delete_maplEstringcOTHnSparseTlETArrayDgRmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTHnSparseTlETArrayDgRmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTHnSparseTlETArrayDgRmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,THnSparseT<TArrayD>*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,THnSparseT<TArrayD>*>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, THnSparseT<TArrayD>*, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, THnSparseT<TArrayD>*> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,THnSparseT<TArrayD>*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTHnSparseTlETArrayDgRmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,THnSparseT<TArrayD>*>*>(nullptr))->GetClass();
      maplEstringcOTHnSparseTlETArrayDgRmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTHnSparseTlETArrayDgRmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,THnSparseT<TArrayD>*> : new map<string,THnSparseT<TArrayD>*>;
   }
   static void *newArray_maplEstringcOTHnSparseTlETArrayDgRmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,THnSparseT<TArrayD>*>[nElements] : new map<string,THnSparseT<TArrayD>*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p) {
      delete (static_cast<map<string,THnSparseT<TArrayD>*>*>(p));
   }
   static void deleteArray_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p) {
      delete [] (static_cast<map<string,THnSparseT<TArrayD>*>*>(p));
   }
   static void destruct_maplEstringcOTHnSparseTlETArrayDgRmUgR(void *p) {
      typedef map<string,THnSparseT<TArrayD>*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,THnSparseT<TArrayD>*>

namespace ROOT {
   static TClass *maplEstringcOTH3FmUgR_Dictionary();
   static void maplEstringcOTH3FmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTH3FmUgR(void *p = nullptr);
   static void *newArray_maplEstringcOTH3FmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTH3FmUgR(void *p);
   static void deleteArray_maplEstringcOTH3FmUgR(void *p);
   static void destruct_maplEstringcOTH3FmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TH3F*>*)
   {
      map<string,TH3F*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TH3F*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TH3F*>", -2, "map", 100,
                  typeid(map<string,TH3F*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTH3FmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TH3F*>) );
      instance.SetNew(&new_maplEstringcOTH3FmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTH3FmUgR);
      instance.SetDelete(&delete_maplEstringcOTH3FmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTH3FmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTH3FmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TH3F*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,TH3F*>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, TH3F*, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, TH3F*> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,TH3F*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTH3FmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,TH3F*>*>(nullptr))->GetClass();
      maplEstringcOTH3FmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTH3FmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTH3FmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH3F*> : new map<string,TH3F*>;
   }
   static void *newArray_maplEstringcOTH3FmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH3F*>[nElements] : new map<string,TH3F*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTH3FmUgR(void *p) {
      delete (static_cast<map<string,TH3F*>*>(p));
   }
   static void deleteArray_maplEstringcOTH3FmUgR(void *p) {
      delete [] (static_cast<map<string,TH3F*>*>(p));
   }
   static void destruct_maplEstringcOTH3FmUgR(void *p) {
      typedef map<string,TH3F*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,TH3F*>

namespace ROOT {
   static TClass *maplEstringcOTH2FmUgR_Dictionary();
   static void maplEstringcOTH2FmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTH2FmUgR(void *p = nullptr);
   static void *newArray_maplEstringcOTH2FmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTH2FmUgR(void *p);
   static void deleteArray_maplEstringcOTH2FmUgR(void *p);
   static void destruct_maplEstringcOTH2FmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TH2F*>*)
   {
      map<string,TH2F*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TH2F*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TH2F*>", -2, "map", 100,
                  typeid(map<string,TH2F*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTH2FmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TH2F*>) );
      instance.SetNew(&new_maplEstringcOTH2FmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTH2FmUgR);
      instance.SetDelete(&delete_maplEstringcOTH2FmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTH2FmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTH2FmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TH2F*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,TH2F*>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, TH2F*, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, TH2F*> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,TH2F*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTH2FmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,TH2F*>*>(nullptr))->GetClass();
      maplEstringcOTH2FmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTH2FmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTH2FmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH2F*> : new map<string,TH2F*>;
   }
   static void *newArray_maplEstringcOTH2FmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH2F*>[nElements] : new map<string,TH2F*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTH2FmUgR(void *p) {
      delete (static_cast<map<string,TH2F*>*>(p));
   }
   static void deleteArray_maplEstringcOTH2FmUgR(void *p) {
      delete [] (static_cast<map<string,TH2F*>*>(p));
   }
   static void destruct_maplEstringcOTH2FmUgR(void *p) {
      typedef map<string,TH2F*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,TH2F*>

namespace ROOT {
   static TClass *maplEstringcOTH1FmUgR_Dictionary();
   static void maplEstringcOTH1FmUgR_TClassManip(TClass*);
   static void *new_maplEstringcOTH1FmUgR(void *p = nullptr);
   static void *newArray_maplEstringcOTH1FmUgR(Long_t size, void *p);
   static void delete_maplEstringcOTH1FmUgR(void *p);
   static void deleteArray_maplEstringcOTH1FmUgR(void *p);
   static void destruct_maplEstringcOTH1FmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,TH1F*>*)
   {
      map<string,TH1F*> *ptr = nullptr;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,TH1F*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,TH1F*>", -2, "map", 100,
                  typeid(map<string,TH1F*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOTH1FmUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,TH1F*>) );
      instance.SetNew(&new_maplEstringcOTH1FmUgR);
      instance.SetNewArray(&newArray_maplEstringcOTH1FmUgR);
      instance.SetDelete(&delete_maplEstringcOTH1FmUgR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOTH1FmUgR);
      instance.SetDestructor(&destruct_maplEstringcOTH1FmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,TH1F*> >()));

      instance.AdoptAlternate(::ROOT::AddClassAlternate("map<string,TH1F*>","std::map<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, TH1F*, std::less<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > const, TH1F*> > >"));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal(static_cast<const map<string,TH1F*>*>(nullptr)); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOTH1FmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal(static_cast<const map<string,TH1F*>*>(nullptr))->GetClass();
      maplEstringcOTH1FmUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOTH1FmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOTH1FmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1F*> : new map<string,TH1F*>;
   }
   static void *newArray_maplEstringcOTH1FmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,TH1F*>[nElements] : new map<string,TH1F*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOTH1FmUgR(void *p) {
      delete (static_cast<map<string,TH1F*>*>(p));
   }
   static void deleteArray_maplEstringcOTH1FmUgR(void *p) {
      delete [] (static_cast<map<string,TH1F*>*>(p));
   }
   static void destruct_maplEstringcOTH1FmUgR(void *p) {
      typedef map<string,TH1F*> current_t;
      (static_cast<current_t*>(p))->~current_t();
   }
} // end of namespace ROOT for class map<string,TH1F*>

namespace {
  void TriggerDictionaryInitialization_liblibSelectorMC_Impl() {
    static const char* headers[] = {
"libSelectorMC/BTagToolContainer.h",
"libSelectorMC/DilepChannel.h",
"libSelectorMC/HistsForFitContainer.h",
"libSelectorMC/JLCombination.h",
"libSelectorMC/JLMinMSubCombination.h",
"libSelectorMC/JLMinMSumCombination.h",
"libSelectorMC/JLdRCombination.h",
"libSelectorMC/JetContainer.h",
"libSelectorMC/PtBinMeanContainer.h",
"libSelectorMC/PtBinSingle.h",
"libSelectorMC/TreeReader.h",
"libSelectorMC/VariableContainer.h",
"libSelectorMC/VariablePtBinsContainer.h",
"libSelectorMC/VariablePtBinsCorrelationContainer.h",
"libSelectorMC/VariablePtContainer.h",
"libSelectorMC/XXContainer.h",
"libSelectorMC/finalSelectionMC.h",
nullptr
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/source/Selectors",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopAnalysis",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt//../../../../AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/RootUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopConfiguration",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/TopPhysUtils/TopDataPreparation",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopCorrections",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthToolSupport/AsgServices",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt//../../../../AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODMeasurementBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/ElectronPhotonID/ElectronEfficiencyCorrection",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Control/AthToolSupport/AsgDataHandles",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Trigger/TrigT1/TrigT1MuctpiBits",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/CalibrationDataInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/JetAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/MuonSpectrometer/MuonStationIndex",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/PMGAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/AnalysisCommon/PMGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TauID/TauAnalysisTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODTau",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/tauRecTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODParticleEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODMissingET",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODEventShape",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/MVAUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopPartons",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/Jet/JetReclustering",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/Jet/JetInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/Jet/JetRec",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/EventShapes/EventShapeInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Reconstruction/Jet/JetEDM",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/Interfaces/TriggerAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopEventSelectionTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/JetMissingEtID/JetSelectorTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/TopPhys/xAOD/TopParticleLevel",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODCutFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODMetaData",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/PhysicsAnalysis/JetTagging/JetTagPerformanceCalibration/xAODBTaggingEfficiency",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt//../../../../AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/include/onnxruntime",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/src/Tools/PathResolver",
"&{dirincdirs}",
"/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/include/",
"/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/Selectors/CMakeFiles/makelibSelectorMCCintDict.DeFjAy/",
nullptr
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "liblibSelectorMC dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Variable_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Variable_ptBins_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Variable_ptBins_Correlation_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  TreeReader;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  jet_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  BtagTool_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Pt_bin_single;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Pt_bin_mean_container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Hist_for_fit_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  XX_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  JL_Combination;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  JL_min_m_sum_Combination;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  JL_min_m_sub_Combination;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  JL_closest_dR_Combination;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  Variable_pt_Container;
class __attribute__((annotate("$clingAutoload$libSelectorMC/finalSelectionMC.h")))  DiLepChannel;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "liblibSelectorMC dictionary payload"

#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ATLAS_PACKAGE_NAME
  #define ATLAS_PACKAGE_NAME "Selectors"
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "libSelectorMC/BTagToolContainer.h"
#include "libSelectorMC/DilepChannel.h"
#include "libSelectorMC/HistsForFitContainer.h"
#include "libSelectorMC/JLCombination.h"
#include "libSelectorMC/JLMinMSubCombination.h"
#include "libSelectorMC/JLMinMSumCombination.h"
#include "libSelectorMC/JLdRCombination.h"
#include "libSelectorMC/JetContainer.h"
#include "libSelectorMC/PtBinMeanContainer.h"
#include "libSelectorMC/PtBinSingle.h"
#include "libSelectorMC/TreeReader.h"
#include "libSelectorMC/VariableContainer.h"
#include "libSelectorMC/VariablePtBinsContainer.h"
#include "libSelectorMC/VariablePtBinsCorrelationContainer.h"
#include "libSelectorMC/VariablePtContainer.h"
#include "libSelectorMC/XXContainer.h"
#include "libSelectorMC/finalSelectionMC.h"
#include "libSelectorMC/finalSelectionMC.h"
#include "libSelectorMC/DilepChannel.h"
#include "libSelectorMC/TreeReader.h"
#include "libSelectorMC/HistsForFitContainer.h"
#include "libSelectorMC/BTagToolContainer.h"
#include "libSelectorMC/PtBinMeanContainer.h"
#include "libSelectorMC/PtBinSingle.h"
#include "libSelectorMC/JetContainer.h"
#include "libSelectorMC/VariableContainer.h"
#include "libSelectorMC/VariablePtBinsContainer.h"
#include "libSelectorMC/VariablePtBinsCorrelationContainer.h"
#include "libSelectorMC/VariablePtContainer.h"
#include "libSelectorMC/XXContainer.h"
#include "libSelectorMC/JLCombination.h"
#include "libSelectorMC/JLMinMSubCombination.h"
#include "libSelectorMC/JLMinMSumCombination.h"
#include "libSelectorMC/JLdRCombination.h"


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

//for loading the object selection at run time                                                                                                                                                                      
#pragma link C++ class finalSelectorMC+;
#pragma link C++ class DiLepChannel+;
#pragma link C++ class TreeReader+;
#pragma link C++ class Hist_for_fit_Container+;
#pragma link C++ class Pt_bin_mean_container+;
#pragma link C++ class Pt_bin_single+;
#pragma link C++ class BtagTool_Container+;
#pragma link C++ class XX_Container+;
#pragma link C++ class jet_Container+;
#pragma link C++ class Variable_ptBins_Container+;
#pragma link C++ class Variable_ptBins_Correlation_Container+;
#pragma link C++ class Variable_Container+;
#pragma link C++ class Variable_pt_Container+;
#pragma link C++ class JL_Combination+;
#pragma link C++ class JL_min_m_sub_Combination+;
#pragma link C++ class JL_min_m_sum_Combination+;
#pragma link C++ class JL_closest_dR_Combination+;


#endif

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"BtagTool_Container", payloadCode, "@",
"DiLepChannel", payloadCode, "@",
"Hist_for_fit_Container", payloadCode, "@",
"JL_Combination", payloadCode, "@",
"JL_closest_dR_Combination", payloadCode, "@",
"JL_min_m_sub_Combination", payloadCode, "@",
"JL_min_m_sum_Combination", payloadCode, "@",
"Pt_bin_mean_container", payloadCode, "@",
"Pt_bin_single", payloadCode, "@",
"TreeReader", payloadCode, "@",
"Variable_Container", payloadCode, "@",
"Variable_ptBins_Container", payloadCode, "@",
"Variable_ptBins_Correlation_Container", payloadCode, "@",
"Variable_pt_Container", payloadCode, "@",
"XX_Container", payloadCode, "@",
"jet_Container", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("liblibSelectorMC",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_liblibSelectorMC_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_liblibSelectorMC_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_liblibSelectorMC() {
  TriggerDictionaryInitialization_liblibSelectorMC_Impl();
}
