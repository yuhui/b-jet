# This file will be configured to contain variables for CPack. These variables
# should be set in the CMake list file of the project before CPack module is
# included. The list of available CPACK_xxx variables and their associated
# documentation may be obtained using
#  cpack --help-variable-list
#
# Some variables are common to all generators (e.g. CPACK_PACKAGE_NAME)
# and some are specific to a generator
# (e.g. CPACK_NSIS_EXTRA_INSTALL_COMMANDS). The generator specific variables
# usually begin with CPACK_<GENNAME>_xxxx.


set(CPACK_BUILD_SOURCE_DIRS "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/source;/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build")
set(CPACK_CMAKE_GENERATOR "Unix Makefiles")
set(CPACK_COMPONENTS_ALL "Debug;Main;Unspecified")
set(CPACK_COMPONENT_UNSPECIFIED_HIDDEN "TRUE")
set(CPACK_COMPONENT_UNSPECIFIED_REQUIRED "TRUE")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_FILE "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.27.5/Linux-x86_64/share/cmake-3.27/Templates/CPack.GenericDescription.txt")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_SUMMARY "FTAGPDFCALIB built using CMake")
set(CPACK_DMG_SLA_USE_RESOURCE_FILE_LICENSE "ON")
set(CPACK_GENERATOR "RPM")
set(CPACK_IGNORE_FILES "/CVS/;/\\.svn/;/\\.bzr/;/\\.hg/;/\\.git/;\\.swp\$;\\.#;/#")
set(CPACK_INNOSETUP_ARCHITECTURE "x64")
set(CPACK_INSTALLED_DIRECTORIES "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/source;/")
set(CPACK_INSTALL_CMAKE_PROJECTS "")
set(CPACK_INSTALL_PREFIX "usr/FTAGPDFCALIB/1.0/InstallArea/x86_64-el9-gcc13-opt")
set(CPACK_INSTALL_SCRIPT "/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/cmake/modules/scripts/cpack_install.cmake")
set(CPACK_MODULE_PATH "/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/cmake/modules;/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBaseExternals/24.2.27/InstallArea/x86_64-el9-gcc13-opt/cmake/modules")
set(CPACK_NSIS_DISPLAY_NAME "FTAGPDFCALIB/1.0/InstallArea/x86_64-el9-gcc13-opt")
set(CPACK_NSIS_INSTALLER_ICON_CODE "")
set(CPACK_NSIS_INSTALLER_MUI_ICON_CODE "")
set(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES")
set(CPACK_NSIS_PACKAGE_NAME "FTAGPDFCALIB/1.0/InstallArea/x86_64-el9-gcc13-opt")
set(CPACK_NSIS_UNINSTALL_NAME "Uninstall")
set(CPACK_OBJCOPY_EXECUTABLE "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.40-acaab/x86_64-el9/bin/objcopy")
set(CPACK_OBJDUMP_EXECUTABLE "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.40-acaab/x86_64-el9/bin/objdump")
set(CPACK_OUTPUT_CONFIG_FILE "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CPackConfig.cmake")
set(CPACK_PACKAGE_CONTACT "atlas-sw-core@cern.ch")
set(CPACK_PACKAGE_DEFAULT_LOCATION "/usr")
set(CPACK_PACKAGE_DESCRIPTION "FTAGPDFCALIB - 1.0")
set(CPACK_PACKAGE_DESCRIPTION_FILE "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CMakeFiles/README.txt")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "FTAGPDFCALIB - 1.0")
set(CPACK_PACKAGE_FILE_NAME "FTAGPDFCALIB-1.0-Source")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "FTAGPDFCALIB/1.0/InstallArea/x86_64-el9-gcc13-opt")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "FTAGPDFCALIB/1.0/InstallArea/x86_64-el9-gcc13-opt")
set(CPACK_PACKAGE_NAME "FTAGPDFCALIB")
set(CPACK_PACKAGE_RELOCATABLE "true")
set(CPACK_PACKAGE_VENDOR "ATLAS Collaboration")
set(CPACK_PACKAGE_VERSION "1.0")
set(CPACK_PACKAGE_VERSION_MAJOR "1")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PROJECT_CONFIG_FILE "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CMakeFiles/CPackOptions.cmake")
set(CPACK_READELF_EXECUTABLE "/cvmfs/sft.cern.ch/lcg/releases/binutils/2.40-acaab/x86_64-el9/bin/readelf")
set(CPACK_RESOURCE_FILE_LICENSE "/cvmfs/atlas.cern.ch/repo/sw/software/24.2/AnalysisBase/24.2.27/InstallArea/x86_64-el9-gcc13-opt/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CMakeFiles/README.txt")
set(CPACK_RESOURCE_FILE_WELCOME "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.27.5/Linux-x86_64/share/cmake-3.27/Templates/CPack.GenericWelcome.txt")
set(CPACK_RPM_PACKAGE_ARCHITECTURE "noarch")
set(CPACK_RPM_PACKAGE_AUTOREQ " no")
set(CPACK_RPM_PACKAGE_AUTOREQPROV " no")
set(CPACK_RPM_PACKAGE_GROUP "ATLAS Software")
set(CPACK_RPM_PACKAGE_LICENSE "Apache License Version 2.0")
set(CPACK_RPM_PACKAGE_NAME "FTAGPDFCALIB_1.0_x86_64-el9-gcc13-opt")
set(CPACK_RPM_PACKAGE_PROVIDES "/bin/sh")
set(CPACK_RPM_PACKAGE_REQUIRES "AnalysisBase_24.2.27_x86_64-el9-gcc13-opt")
set(CPACK_RPM_PACKAGE_SOURCES "ON")
set(CPACK_RPM_PACKAGE_VERSION "1.0")
set(CPACK_RPM_SPEC_MORE_DEFINE "
%global __os_install_post %{nil}
%define _unpackaged_files_terminate_build 0
%define _binaries_in_noarch_packages_terminate_build 0
%define _source_payload w2.xzdio
%define _binary_payload w2.xzdio
%undefine __brp_mangle_shebangs")
set(CPACK_SET_DESTDIR "OFF")
set(CPACK_SOURCE_GENERATOR "RPM")
set(CPACK_SOURCE_IGNORE_FILES "/CVS/;/\\.svn/;/\\.bzr/;/\\.hg/;/\\.git/;\\.swp\$;\\.#;/#")
set(CPACK_SOURCE_INSTALLED_DIRECTORIES "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/source;/")
set(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CPackSourceConfig.cmake")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "FTAGPDFCALIB-1.0-Source")
set(CPACK_SOURCE_TOPLEVEL_TAG "Linux-Source")
set(CPACK_STRIP_FILES "")
set(CPACK_SYSTEM_NAME "Linux")
set(CPACK_THREADS "1")
set(CPACK_TOPLEVEL_TAG "Linux-Source")
set(CPACK_WIX_SIZEOF_VOID_P "8")

if(NOT CPACK_PROPERTIES_FILE)
  set(CPACK_PROPERTIES_FILE "/afs/cern.ch/user/y/yuhui/private/online22/wp4off/FinalSelection/TSelectors/build/CPackProperties.cmake")
endif()

if(EXISTS ${CPACK_PROPERTIES_FILE})
  include(${CPACK_PROPERTIES_FILE})
endif()
