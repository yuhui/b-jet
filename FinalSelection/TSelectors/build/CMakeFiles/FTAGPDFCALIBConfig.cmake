# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
# Configuration file for an ATLAS CMake release installation.
# It defines the following variables:
#
#  FTAGPDFCALIB_INSTALL_DIR  - Base install directory for the release
#  FTAGPDFCALIB_INCLUDE_DIR  - Include directory for the release
#  FTAGPDFCALIB_LIBRARY_DIR  - Library directory for the release
#  FTAGPDFCALIB_BINARY_DIR   - Runtime directory for the release
#  FTAGPDFCALIB_CMAKE_DIR    - Directory holding CMake files
#  FTAGPDFCALIB_PYTHON_DIR   - Directory holding python code
#  FTAGPDFCALIB_VERSION      - The version number of the release
#  FTAGPDFCALIB_TARGET_NAMES - The names of the targets provided by the
#                                      release
#  FTAGPDFCALIB_PLATFORM     - The name of the platform of the release
#
# Note however that most of the time you should not be using any of these
# variables, but the imported targets of the project instead. Even more, in most
# cases you will want to use atlas_project(...) to handle the imported targets
# in a "smart way".

# Require CMake 3.11 for aliasing global imported targets.
cmake_minimum_required( VERSION 3.11 )

# Add the CMake provided initialisation code.

####### Expanded from @PACKAGE_INIT@ by configure_package_config_file() #######
####### Any changes to this file will be overwritten by the next CMake run ####
####### The input file was ProjectConfig.cmake.in                            ########

get_filename_component(PACKAGE_PREFIX_DIR "${CMAKE_CURRENT_LIST_DIR}/../" ABSOLUTE)

####################################################################################

# Set various directory variables.
set( FTAGPDFCALIB_INSTALL_DIR "${PACKAGE_PREFIX_DIR}/" )
set( FTAGPDFCALIB_INCLUDE_DIR "${PACKAGE_PREFIX_DIR}/include" )
set( FTAGPDFCALIB_LIBRARY_DIR "${PACKAGE_PREFIX_DIR}/lib" )
set( FTAGPDFCALIB_BINARY_DIR "${PACKAGE_PREFIX_DIR}/bin" )
set( FTAGPDFCALIB_CMAKE_DIR "${PACKAGE_PREFIX_DIR}/cmake" )
set( FTAGPDFCALIB_PYTHON_DIR "${PACKAGE_PREFIX_DIR}/python" )

# Set the version of the project.
set( FTAGPDFCALIB_VERSION "1.0" )

# Set the names of the targets that the project exported.
set( FTAGPDFCALIB_TARGET_NAMES "libSelectorMC;RunTSelectorMC" )

# Print what project/release was found just now.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( FTAGPDFCALIB
   REQUIRED_VARS CMAKE_CURRENT_LIST_FILE
   VERSION_VAR FTAGPDFCALIB_VERSION )

# The base projects that this project was built on top of:
set( FTAGPDFCALIB_BASE_PROJECTS AnalysisBase;24.2.27 )

# The platform name used for the build.
set( FTAGPDFCALIB_PLATFORM "x86_64-el9-gcc13-opt" )
set( ATLAS_PLATFORM "x86_64-el9-gcc13-opt" )

# Include the project-specific pre-include file, if it exists:
include( "${FTAGPDFCALIB_CMAKE_DIR}/PreConfig.cmake" OPTIONAL )

# Include the base projects, in the order in which they were given to
# the atlas_project call.
set( _baseProjectsFTAGPDFCALIB
   ${FTAGPDFCALIB_BASE_PROJECTS} )
while( _baseProjectsFTAGPDFCALIB )
   # Extract the release name and version, and then remove these entries
   # from the list:
   list( GET _baseProjectsFTAGPDFCALIB 0
      _base_projectFTAGPDFCALIB )
   list( GET _baseProjectsFTAGPDFCALIB 1
      _base_versionFTAGPDFCALIB )
   list( REMOVE_AT _baseProjectsFTAGPDFCALIB 0 1 )
   # Make sure that the project version is a regular version number:
   if( NOT _base_versionFTAGPDFCALIB MATCHES "^[0-9]+[0-9.]*" )
      # Let's not specify a version in this case...
      message( STATUS "Using base project "
         "\"${_base_projectFTAGPDFCALIB}\" without "
         "its \"${_base_versionFTAGPDFCALIB}\" version name/number" )
      set( _base_versionFTAGPDFCALIB )
   endif()
   # Find the base release:
   if( FTAGPDFCALIB_FIND_QUIETLY )
      find_package( ${_base_projectFTAGPDFCALIB}
         ${_base_versionFTAGPDFCALIB} EXACT QUIET )
   else()
      find_package( ${_base_projectFTAGPDFCALIB}
         ${_base_versionFTAGPDFCALIB} EXACT )
   endif()
endwhile()
unset( _baseProjectsFTAGPDFCALIB )
unset( _base_projectFTAGPDFCALIB )
unset( _base_versionFTAGPDFCALIB )

# Make CMake find the release's installed modules. Optionally append the module
# library instead of prepending it. To allow the user to override the
# modules packaged with the release.
if( ATLAS_DONT_PREPEND_PROJECT_MODULES )
   list( APPEND CMAKE_MODULE_PATH "${FTAGPDFCALIB_CMAKE_DIR}/modules" )
else()
   list( INSERT CMAKE_MODULE_PATH 0 "${FTAGPDFCALIB_CMAKE_DIR}/modules" )
endif()
list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )

# Pull in the ATLAS code:
include( AtlasFunctions )

# Include the file listing all the imported targets and options:
include(
   "${FTAGPDFCALIB_CMAKE_DIR}/FTAGPDFCALIBConfig-targets.cmake"
   OPTIONAL )

# Check what build mode the release was built with. And set CMAKE_BUILD_TYPE
# to that value by default. While there should only be one build mode in
# a given install area, provide an explicit preference order to the different
# build modes:
foreach( _type "Debug" "RelWithDebInfo" "Release" "MinSizeRel" "Default" )
   string( TOLOWER "${_type}" _typeLower )
   set( _fileName "${FTAGPDFCALIB_CMAKE_DIR}/" )
   set( _fileName "${_fileName}FTAGPDFCALIBConfig-targets" )
   set( _fileName "${_fileName}-${_typeLower}.cmake" )
   if( EXISTS "${_fileName}" )
      # Set the build type forcefully. CMake (at least at the time of writing
      # [3.19]) needs to be handled in this weird way. (One would think that if
      # the variable is deemed not-yet-set, then you wouldn't need to use the
      # FORCE keyword. But you do...)
      if( NOT CMAKE_BUILD_TYPE )
         set( CMAKE_BUILD_TYPE "${_type}"
            CACHE STRING "Build mode for the release" FORCE )
      endif()
      break()
   endif()
   unset( _fileName )
   unset( _typeLower )
endforeach()

# If the libraries need to be set up...
if( FTAGPDFCALIB_FIND_COMPONENTS )

   # A sanity check.
   if( NOT ${FTAGPDFCALIB_FIND_COMPONENTS} STREQUAL "INCLUDE" )
      message( WARNING "Only the 'INCLUDE' component is understood. "
         "Continuing as if 'INCLUDE' would have been specified..." )
   endif()

   # Tell the user what's happening.
   message( STATUS "Including the packages from project "
      "FTAGPDFCALIB - 1.0..." )

   # Loop over the targets that this project has.
   foreach( _target ${FTAGPDFCALIB_TARGET_NAMES} )
      # If the target exists already, then don't do aything else.
      if( TARGET ${_target} )
         continue()
      endif()
      # Check whether the target in question is known in this release.
      if( NOT TARGET FTAGPDFCALIB::${_target} )
         message( SEND_ERROR
            "Target with name FTAGPDFCALIB::${_target} not found" )
         continue()
      endif()
      # And now create a deep copy of it.
      atlas_copy_target( FTAGPDFCALIB ${_target} )
   endforeach()

   # Include the base projects, in reverse order. So that the components from
   # the end of the list would get precedence over the components from the
   # front.
   set( _baseProjectsFTAGPDFCALIB
      ${FTAGPDFCALIB_BASE_PROJECTS} )
   while( _baseProjectsFTAGPDFCALIB )
      # Get the last project from the list:
      list( LENGTH _baseProjectsFTAGPDFCALIB
         _lengthFTAGPDFCALIB )
      math( EXPR _projNameIdxFTAGPDFCALIB
         "${_lengthFTAGPDFCALIB} - 2" )
      math( EXPR _projVersIdxFTAGPDFCALIB
         "${_lengthFTAGPDFCALIB} - 1" )
      list( GET _baseProjectsFTAGPDFCALIB
         ${_projNameIdxFTAGPDFCALIB}
         _base_projectFTAGPDFCALIB )
      list( GET _baseProjectsFTAGPDFCALIB
         ${_projVersIdxFTAGPDFCALIB}
         _base_versionFTAGPDFCALIB )
      list( REMOVE_AT _baseProjectsFTAGPDFCALIB
         ${_projNameIdxFTAGPDFCALIB}
         ${_projVersIdxFTAGPDFCALIB} )
      # Find the base release:
      find_package( ${_base_projectFTAGPDFCALIB}
         ${_base_versionFTAGPDFCALIB} EXACT COMPONENTS INCLUDE QUIET )
   endwhile()
   unset( _baseProjectsFTAGPDFCALIB )
   unset( _projNameIdxFTAGPDFCALIB )
   unset( _projVersIdxFTAGPDFCALIB )
   unset( _base_projectFTAGPDFCALIB )
   unset( _base_versionFTAGPDFCALIB )
   unset( _lengthFTAGPDFCALIB )

endif()

# Only do this if necessary:
if( NOT ATLAS_DONT_PREPEND_PROJECT_MODULES )
   # Make sure that after all of this, we still have this release's module
   # directory at the front of the list:
   list( INSERT CMAKE_MODULE_PATH 0 "${FTAGPDFCALIB_CMAKE_DIR}/modules" )
   list( REMOVE_DUPLICATES CMAKE_MODULE_PATH )
endif()

# Include the project-specific post-include file, if it exists:
include( "${FTAGPDFCALIB_CMAKE_DIR}/PostConfig.cmake" OPTIONAL )
