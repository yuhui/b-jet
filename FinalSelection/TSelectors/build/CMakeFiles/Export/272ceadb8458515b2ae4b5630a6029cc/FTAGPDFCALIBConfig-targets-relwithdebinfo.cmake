#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "FTAGPDFCALIB::libSelectorMC" for configuration "RelWithDebInfo"
set_property(TARGET FTAGPDFCALIB::libSelectorMC APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(FTAGPDFCALIB::libSelectorMC PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/liblibSelectorMC.so"
  IMPORTED_SONAME_RELWITHDEBINFO "liblibSelectorMC.so"
  )

list(APPEND _cmake_import_check_targets FTAGPDFCALIB::libSelectorMC )
list(APPEND _cmake_import_check_files_for_FTAGPDFCALIB::libSelectorMC "${_IMPORT_PREFIX}/lib/liblibSelectorMC.so" )

# Import target "FTAGPDFCALIB::RunTSelectorMC" for configuration "RelWithDebInfo"
set_property(TARGET FTAGPDFCALIB::RunTSelectorMC APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(FTAGPDFCALIB::RunTSelectorMC PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/RunTSelectorMC"
  )

list(APPEND _cmake_import_check_targets FTAGPDFCALIB::RunTSelectorMC )
list(APPEND _cmake_import_check_files_for_FTAGPDFCALIB::RunTSelectorMC "${_IMPORT_PREFIX}/bin/RunTSelectorMC" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
