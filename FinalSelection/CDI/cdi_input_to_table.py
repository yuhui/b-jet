import math
    
wp="85"
fname= "cdi-input-files/dataRun3/cdi-input-files-combined_fit/btag_ttbarPDF_v1.0_24-2-02_DL1dv01_FixedCutBEff_"+wp+".txt"
#fname= "cdi-input-files/d151617/cdi-input-files-combined_fit/btag_ttbarPDF_mc16ad_v2.0_21-2-53_MV2c10_FixedCutBEff_"+ wp +".txt"
pt_bins = {}
pt_bin_names= []
this_name_list=[];
anna_top_names_to_replace={
    "pileup" : "PRW_DATASF",
    "jvt" : "JET_JvtEfficiency",
    "FT_EFF_e_b_stat" : "Data stat. unc.",
    "FT_EFF_MC_stat_nominal": "MC stat. unc.",
    "FT_EFF_FSR":"ttbar FSR",
    "ttbar_PDF4LHC":"ttbar PDF4LHC",
    "FT_EFF_correctFakes": "fake leptons",
    "FT_EFF_Light_systematics": "light-flavour jet mistag rate", 

}
with open(fname) as f:
    for line in f:
        if "bin(" in line:
            this_bin_name= line.replace("	bin(","").replace(",0<abseta<2.5)","").replace("\n","").replace("<pt<", " - ") 
            print "found bin:",  this_bin_name
            this_unc_map={}
            pt_bins[this_bin_name] =this_unc_map
            pt_bin_names.append(this_bin_name)
        if "central_value(" in line:
            this_unc_map["central_value"]=line.replace("		central_value(","").replace(",0)", "").replace("\n","")
        if "sys(" in line:
            parts=line.replace("		sys(","").replace("%)\n","").split(",")
            this_unc_map[parts[0]]=parts[1]

#summarizing uncertainty

def summarize_unc(unc_map, names_to_summarize, new_name ="" ):
    if new_name=="":
        new_name=names_to_summarize
    for pt_name in pt_bin_names:
        this_unc=0
        unc_map=pt_bins[pt_name]
        for unc in unc_map.keys():
            if names_to_summarize in unc:
                np=float(unc_map[unc])
                this_unc=this_unc+np*np
                del unc_map[unc]
        unc_map[new_name] = str(round(math.sqrt(this_unc),2)) 

#stat
summarize_unc(this_unc_map, "FT_EFF_e_b_"+ wp +"_stat", "FT_EFF_e_b_stat")

#PDF4LHC
summarize_unc(this_unc_map, "ttbar_PDF4LHC")
summarize_unc(this_unc_map, "singletop_PDF4LHC")

#light and c calibration:
summarize_unc(this_unc_map, "FT_EFF_Eigen_Light")
summarize_unc(this_unc_map, "FT_EFF_Eigen_C")

#light and c calibration:
summarize_unc(this_unc_map, "FT_EFF_Eigen_Light")
summarize_unc(this_unc_map, "FT_EFF_Eigen_C")
summarize_unc(this_unc_map, "FT_EFF_Diboson_mc_shower", "Diboson_scale")
summarize_unc(this_unc_map, "FT_EFF_Zjets_mc_shower", "Zjets_scale")



#printing all remeining uncertainties
for unc in this_unc_map.keys():    
    print unc


def print_table_and_get_sum(unc_names_orderd, wp="70", short_description="Lepton-related", s_label = "lepton_unc" ):
    #printing
    unc_tot=[]
    print "\\begin{sidewaystable}"
    print "\\begin{tabular}{ | l",
    for pt_name in pt_bin_names:
        print "| l",
        unc_tot.append(0)
    print "| }"
    print "\\hline"
    print "\\pT [GeV]",
    for pt_name in pt_bin_names:
        print "&", pt_name,
    print "\\\\"
    print "\\hline"
    print "\\hline"
    for unc in unc_names_orderd:
        print unc.replace("FT_EFF_","").replace("_"," "),
        i_pt_bin=0
        for pt_name in pt_bin_names:
            print "&", (pt_bins[pt_name])[unc],
            unc_val=float((pt_bins[pt_name])[unc])
            unc_tot[i_pt_bin]=unc_tot[i_pt_bin] + unc_val*unc_val
            i_pt_bin=i_pt_bin+1
        print "\\\\"
    print "\\hline"
    print "Total",
    i_pt_bin=0
    for unc in unc_tot:
        unc_tot[i_pt_bin]=math.sqrt(unc)
        print "&", str(round(unc_tot[i_pt_bin],2)), 
        i_pt_bin=i_pt_bin+1
    print "\\\\"
    print "\\hline"
    print "\\end{tabular}"
    print "\\caption{" + short_description+ " uncertainties for the "+ wp +"\\% MV2 $b$-tagging working point (in \\%). The sums in quadrature of all numbers reported on this table are also shown (Total).}"
    print "\\label{tab:" + s_label + "_" + wp + "}"
    print "\\end{sidewaystable}"
    return unc_tot;


#the print order
jet_JES_unc=[
    "JET_BJES_Response",
    "JET_EffectiveNP_Detector1",
    "JET_EffectiveNP_Mixed1",
    "JET_EffectiveNP_Mixed2",
    "JET_EffectiveNP_Mixed3",
    "JET_EffectiveNP_Modelling1",
    "JET_EffectiveNP_Modelling2",
    "JET_EffectiveNP_Modelling3",
    "JET_EffectiveNP_Modelling4",
    "JET_EffectiveNP_Statistical1",
    "JET_EffectiveNP_Statistical2",
    "JET_EffectiveNP_Statistical3",
    "JET_EffectiveNP_Statistical4",
    "JET_EffectiveNP_Statistical5",
    "JET_EffectiveNP_Statistical6",
    "JET_EtaIntercalibration_Modelling",
    "JET_EtaIntercalibration_NonClosure_highE",
    "JET_EtaIntercalibration_NonClosure_negEta",
    "JET_EtaIntercalibration_NonClosure_posEta",
    "JET_EtaIntercalibration_TotalStat",
    "JET_Flavor_Composition",
    "JET_Flavor_Response",
    "JET_JvtEfficiency",
    "JET_Pileup_OffsetMu",
    "JET_Pileup_OffsetNPV",
    "JET_Pileup_PtTerm",
    "JET_Pileup_RhoTopology",
    "JET_PunchThrough_MC16",
]
jet_JES_sum = print_table_and_get_sum(jet_JES_unc, wp, "Jet Energy Scale", "JES_unc" )

jet_JER_unc=[
    "JET_JER_DataVsMC",
    "JET_JER_EffectiveNP_1",
    "JET_JER_EffectiveNP_2",
    "JET_JER_EffectiveNP_3",
    "JET_JER_EffectiveNP_4",
    "JET_JER_EffectiveNP_5",
    "JET_JER_EffectiveNP_6",
    "JET_JER_EffectiveNP_7",
    "JET_JER_EffectiveNP_8",
    "JET_JER_EffectiveNP_9",
    "JET_JER_EffectiveNP_10",
    "JET_JER_EffectiveNP_11",
    "JET_JER_EffectiveNP_12restTerm",

]
jet_JER_sum = print_table_and_get_sum(jet_JER_unc, wp, "Jet Energy Resolution", "JER_unc" )


lepton_unc = [
    "EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR",
    "EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR",
    "EG_RESOLUTION_ALL",
    "EG_SCALE_ALL",
    "MUON_EFF_ISO_STAT",
    "MUON_EFF_ISO_SYS",
    "MUON_EFF_RECO_STAT",
    "MUON_EFF_RECO_STAT_LOWPT",
    "MUON_EFF_RECO_SYS",
    "MUON_EFF_RECO_SYS_LOWPT",
    "MUON_EFF_TrigStatUncertainty",
    "MUON_EFF_TrigSystUncertainty",
    "MUON_EFF_TTVA_STAT",
    "MUON_EFF_TTVA_SYS",
    "MUON_ID",
    "MUON_MS",
    "MUON_SAGITTA_RESBIAS",
    "MUON_SAGITTA_RHO",
    "MUON_SCALE",
]
lep_sum = print_table_and_get_sum(lepton_unc)

ttbar_unc = [
    "FT_EFF_ttbar_PhPy8Rad",
    "FT_EFF_ttbar_PowHW7",
    "FT_EFF_ttbar_PhPy8FSR",
    "ttbar_PDF4LHC",
]
ttbar_sum = print_table_and_get_sum(ttbar_unc, wp, "$t\\bar{t}$ modeling ", "ttbar_unc" )


singletop_unc = [
    "FT_EFF_singletop_PhPy8Rad",
    "FT_EFF_singletop_PowHW7",
    "FT_EFF_singletop_PhPy8FSR",
    "singletop_PDF4LHC",
    "singletop_QCD",
    "FT_EFF_singletop_PowPy8_DS",
]
singletop_sum = print_table_and_get_sum(singletop_unc, wp, "singletop modeling", "singletop_unc")

Diboson_unc = [
    "Diboson_scale",
    "FT_EFF_Diboson_crossec_6per",
]
Diboson_sum = print_table_and_get_sum(Diboson_unc, wp, "Diboson modeling", "Diboson_unc")

Zjets_unc = [
    "Zjets_scale",
    "FT_EFF_Zjets_crossec_5per",
]
Zjets_sum = print_table_and_get_sum(Zjets_unc, wp, "Z+jets modeling", "Zjets_unc")


#adding in unc sum
i_pt_bin=0
for pt_name in pt_bin_names:
    this_unc_map=pt_bins[pt_name]
    this_unc_map["Total_(jet_JES)"] = str(round((jet_JES_sum[i_pt_bin]),2)) 
    this_unc_map["Total_(jet_JER)"] = str(round((jet_JER_sum[i_pt_bin]),2)) 
    this_unc_map["Total_(lepton)"] = str(round((lep_sum[i_pt_bin]),2)) 
    this_unc_map["Total_(ttbar)"] = str(round((ttbar_sum[i_pt_bin]),2))
    this_unc_map["Total_(singletop)"] = str(round((singletop_sum[i_pt_bin]),2))
    this_unc_map["Total_(Diboson)"] = str(round((Diboson_sum[i_pt_bin]),2))
    this_unc_map["Total_(Zjets)"] = str(round((Zjets_sum[i_pt_bin]),2))
    i_pt_bin=i_pt_bin+1


stat_unc=[
    "FT_EFF_e_b_stat",
    "FT_EFF_MC_stat_nominal",
]    
sums=[
    "Total_(lepton)",
    "Total_(jet_JES)",
    "Total_(jet_JER)",
    "Total_(ttbar)",
    "Total_(singletop)",
    "Total_(Diboson)",
    "Total_(Zjets)",

]
# Cross check: reading total relative syst. error for bin 0 from file: 6.76007802409%
# Cross check: reading total relative stat. error for bin 0 from file: 3.69639072325%
# Cross check: reading total relative error for bin 0 from file: 7.70467126299%

# Cross check: reading total relative syst. error for bin 1 from file: 2.86421719541%
# Cross check: reading total relative stat. error for bin 1 from file: 1.72020540574%
# Cross check: reading total relative error for bin 1 from file: 3.34108467124%

# Cross check: reading total relative syst. error for bin 2 from file: 1.2378268082%
# Cross check: reading total relative stat. error for bin 2 from file: 0.740534663188%
# Cross check: reading total relative error for bin 2 from file: 1.44243086298%


# Cross check: reading total relative syst. error for bin 3 from file: 0.959827303362%
# Cross check: reading total relative stat. error for bin 3 from file: 0.548972201546%
# Cross check: reading total relative error for bin 3 from file: 1.10573004407%

# Summed up total relative syst. error for bin 6:	0.212184183966%
# Summed up total relative stat. error for bin 6:	0.828887831793%
# Total error for bin 6:	0.85561508029%
# Cross check: reading total relative syst. error for bin 6 from file: 1.04209252836%
# Cross check: reading total relative stat. error for bin 6 from file: 0.828887831793%
# Cross check: reading total relative error for bin 6 from file: 1.33154492052%
other_unc=[
    "FT_EFF_correctFakes",
    "FT_EFF_Eigen_Light",
    "misstagLight_up",
    "FT_EFF_Eigen_C",
    "PRW_DATASF",
]
def print_last_table_and_get_sum(stat_unc,other_unc,sums, wp):
    #printing
    unc_tot=[]
    print "\\begin{sidewaystable}"
    print "\\begin{tabular}{ | l",
    for pt_name in pt_bin_names:
        print "| l",
        unc_tot.append(0)
    print "| }"
    print "\\hline"
    print "\\pT [GeV]",
    for pt_name in pt_bin_names:
        print "&", pt_name,
    print "\\\\"
    print "\\hline"
    print "\\hline"
    for unc in stat_unc:
        if unc in anna_top_names_to_replace:
            unc_name=anna_top_names_to_replace[unc]
        else:
            unc_name = unc.replace("FT_EFF_","").replace("_"," ")
        print unc_name,
        i_pt_bin=0
        for pt_name in pt_bin_names:
            print "&", (pt_bins[pt_name])[unc],
            unc_val=float((pt_bins[pt_name])[unc])
            unc_tot[i_pt_bin]=unc_tot[i_pt_bin] + unc_val*unc_val
            i_pt_bin=i_pt_bin+1
        print "\\\\"
        print "\\hline"
    for unc in other_unc:
        if unc in anna_top_names_to_replace:
            unc_name=anna_top_names_to_replace[unc]
        else:
            unc_name = unc.replace("FT_EFF_","").replace("_"," ")
        print unc_name,
        i_pt_bin=0
        for pt_name in pt_bin_names:
            print "&", (pt_bins[pt_name])[unc],
            unc_val=float((pt_bins[pt_name])[unc])
            unc_tot[i_pt_bin]=unc_tot[i_pt_bin] + unc_val*unc_val
            i_pt_bin=i_pt_bin+1
        print "\\\\"
    print "\\hline"
    print "\\hline"    
    print "Total (stat, fakes, ftag, pileupSF)",
    i_pt_bin=0
    for unc in unc_tot:
        unc=math.sqrt(unc)
        print "&", str(round(unc,2)), 
        i_pt_bin=i_pt_bin+1
    print "\\\\"
    print "\\hline"
    print "\\hline"  
    for unc in sums:
        print unc.replace("FT_EFF_","").replace("_"," "),
        i_pt_bin=0
        for pt_name in pt_bin_names:
            print "&", (pt_bins[pt_name])[unc],
            unc_val=float((pt_bins[pt_name])[unc])
            unc_tot[i_pt_bin]=unc_tot[i_pt_bin] + unc_val*unc_val
            i_pt_bin=i_pt_bin+1
        print "\\\\"
    print "\\hline"
    print "\\hline"    
    print "Total (all)",
    i_pt_bin=0
    for unc in unc_tot:
        unc=math.sqrt(unc)
        print "&", str(round(unc,2)), 
        i_pt_bin=i_pt_bin+1
    print "\\\\"
    

    print "\\hline"
    print "\\end{tabular}"

    print "\\caption{Summary of all uncertainties for the " + wp + "\\% MV2 $b$-tagging working point (in \\%). The sums in quadrature of all numbers reported on this table are also shown. More details on the jet- and lepton-related uncertainties are available in the previous tables. }"
    print "\\label{tab:tot_unc_"+ wp  +"}"
    print "\\end{sidewaystable}"
total_sum=print_last_table_and_get_sum(stat_unc,other_unc,sums, wp)

