import ROOT
import numpy as np
import os,sys,math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *

import pandas as pd

dataName=options.data_name
inputdir=options.output_dir
outputdir=options.plot_dir
ttb=options.ttb_sample
systematic_name="nominal" #"weight_pileup_DOWN"

option="INT"
lumi=options.data_lumi


def NormalStyle():

    ROOT.gStyle.SetHistLineWidth(2);
    ROOT.gStyle.SetHistFillColor(0);
        
    ROOT.gStyle.SetLabelSize(0.04);
    ROOT.gStyle.SetLabelOffset(0.01);
    
    ROOT.gStyle.SetTitleXOffset(0.1);
    ROOT.gStyle.SetTitleXSize(0.05);
    ROOT.gStyle.SetTitleYOffset(0.1);
    ROOT.gStyle.SetTitleYSize(0.05);
    
    ROOT.gStyle.SetLegendBorderSize(0);
    ROOT.gStyle.SetLegendFillColor(0);
    ROOT.gStyle.SetLegendTextSize(0.04);
    ROOT.gStyle.SetLegendFont(42);
        
    ROOT.gStyle.SetPadColor(0);
    ROOT.gStyle.SetPadTickX(1);
    ROOT.gStyle.SetPadTickY(1);
    ROOT.gStyle.SetPadLeftMargin(0.2);
    ROOT.gStyle.SetPadBottomMargin(0.2);
    
    ROOT.gStyle.SetPalette(57);

doData1516=False
doData17=False
doData18=False
if "1516" in dataName:
    doData1516=True
elif "17" in dataName:
    doData17=True
elif "18" in dataName:
    doData18=True

legendLabels={
    'FTAG2_ttV_aMCnlo' : 'ttV',
    'FTAG2_Zjets_Sherpa221' : 'Z+jets',
    'FTAG2_Wjets_Sherpa221' : 'W+jets',
    'FTAG2_singletop_PowPy8' : 'Single Top',
    'FTAG2_Diboson_Sherpa222' : "Diboson",
    'FTAG2_ttbar_PhPy8' : "t#bar{t}",
    'data1516' : "Data 2015 + 2016",
    'data17' : "Data 2017",
    'data18' : "Data 2018"
}


flavours={'bb' : ROOT.kBlue, 'bc':ROOT.kRed-3, 'cb':ROOT.kRed+2,'cc':ROOT.kRed,'bl':ROOT.kOrange+3,'lb':ROOT.kOrange-3,'ll':ROOT.kOrange,'cl':ROOT.kGreen-3,'lc':ROOT.kGreen+3 }
syst_global=[y.name for y in options.inner_systematics_in_nominal_tree]
syst_global.extend([y.name for y in options.tree_systematics])
syst_samples={
    'ttbar' : [y.name for y in options.syst_samples],
    'Zjets' : [y.name for y in options.syst_samples_ZJets],
    'Wjets' : [],#y.name for y in options.syst_samples_WJets],
    'singletop' : [y.name for y in options.syst_samples_singletop],
    'diboson' : [y.name for y in options.syst_samples_Diboson],
}

#syst_samples['ttbar'].remove('FTAG2_ttbar_aMcPy8')
#syst_samples['ttbar'].remove('FTAG2_ttbar_PowHW7')
#syst_samples['ttbar'].remove("FTAG2_ttbar_Sherpa221")
syst_samples['ttbar'].remove('FTAG2_ttbar_PhPy8_AF2')
syst_samples['ttbar'].remove('FTAG2_ttbar_PhPy8_hdamp3mtop')

other_ttbar_systs=options.pdf_systematics
other_ttbar_systs.extend(["weight_mc_fsr_DOWN","weight_mc_fsr_UP","weight_mc_rad_UP","weight_mc_rad_DOWN"])

samples={ 'ttbar' : ['FTAG2_ttbar_PhPy8',"t#bar{t}", ROOT.kBlue],
          'Zjets' : ['FTAG2_Zjets_Sherpa221',"Z+jets", ROOT.kRed],
          'Wjets' : ['FTAG2_Wjets_Sherpa221',"W+jets", ROOT.kGreen+2],
          'singletop' : ['FTAG2_singletop_PowPy8',"Single Top", ROOT.kOrange-3],
          'diboson' : ['FTAG2_Diboson_Sherpa222',"Diboson", ROOT.kViolet],
          'data1516' : ['data1516',"Data 2015 + 2016", ROOT.kBlack],
          'data17' : ['data17',"Data 2017", ROOT.kBlack],
          'data18' : ['data18',"Data 2018", ROOT.kBlack]
          }

#folders=['emu_OS_J2']
folders=['emu_OS_J2','emu_OS_J2_m_lj_cut']#,'emu_OS_J2_CR_lb','emu_OS_J2_CR_bl','emu_OS_J2_CR_ll']


def getFullHist(histo_name,myfile):
    histo=None
    for flav in flavours.keys():
        if not histo:
            histo=myfile.Get(histo_name+"_"+flav)
        else:
            histo.Add(myfile.Get(histo_name+"_"+flav))
    print(" INFO : Retrieving the histogram %s from file %s "%(histo_name,myfile.GetName()))
#    histo.SetDirectory(0)
    return histo

    
class histogram:
    def __init__(self,name,sample,histo_name,folder,flavour="all",isData=False):
        self.name=name
        self.sample=sample
        self.histo_name=folder+"/h_"+folder+"_"+histo_name
        if isData:
            self.histo_name+="_data"

        self.process=''
        if self.sample not in samples.keys():
            sys.exit('Sample %s is not known.'%self.sample)
        self.process=samples[self.sample][0]
        if self.sample not in syst_samples.keys() and not isData:
            sys.exit('No systematics for sample %s.'%self.sample)

        sys_sample=None
        nominal_file=None
        if not isData:
            sys_sample=syst_samples[self.sample]
        if isData:
            nominal_file=ROOT.TFile(inputdir+'/'+self.process+"/"+self.process+"_data_combination.root", "read")
        else:
            nominal_file=ROOT.TFile(inputdir+'/'+self.process+"/"+self.process+"_nominal_combination.root", "read")

        self.histo_nominal=None
        if isData:
            self.histo_nominal=nominal_file.Get(self.histo_name)
        elif flavour == "all":
            self.histo_nominal=getFullHist(self.histo_name,nominal_file)
        else:
            self.histo_nominal=nominal_file.Get(self.histo_name+"_"+flavour)
            
        self.hist_nominal.SetName(self.sample+"_"+self.histo_name)
        self.histo_nominal.SetName(self.sample+"_"+self.histo_name)
        self.histo_nominal.SetDirectory(0)
#        print(" INFO : Retrieving the histogram %s from nominal file %s "%(self.histo_nominal.GetName(),nominal_file.GetName()))
        nominal_file.Close()
        self.xbins=[]
        self.ybins=[]
        self.xerrorup=[]
        self.xerrordown=[]
        self.yerrorup=[]
        self.yerrordown=[]
        self.systematics={}
        self.errors={}

        if not isData:
            for i in range(self.histo_nominal.GetNbinsX()):
                self.xbins.append(self.histo_nominal.GetBinCenter(i+1))
                self.ybins.append(self.histo_nominal.GetBinContent(i+1))
                if math.fabs(self.histo_nominal.GetBinContent(i+1)) > 0: 
                    self.yerrorup.append(self.histo_nominal.GetBinError(i+1)/self.histo_nominal.GetBinContent(i+1))
                    self.yerrordown.append(self.histo_nominal.GetBinError(i+1)/self.histo_nominal.GetBinContent(i+1))
                else:
                    self.yerrorup.append(0)
                    self.yerrordown.append(0)

                self.xerrorup.append(self.histo_nominal.GetBinWidth(i+1)/2)
                self.xerrordown.append(self.histo_nominal.GetBinWidth(i+1)/2)
        
            for syst in syst_global:
                self.systematics[syst]=self.histo_nominal.Clone(self.histo_nominal.GetName()+"_"+syst)
                # if 'weight' in syst:
                #     syst=syst.split('weight_')[-1]
                if not os.path.isfile(inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root"):
                    print("  WARNING :  "+inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root")
                    continue
                syst_file=ROOT.TFile(inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root", "read")
                if syst_file.IsZombie():
                    print("   ERROR :   File "+inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root"+" but it could not be opened")
                    syst_file.Close()
                    continue
                syst_histo=None
 #               print(" INFO : Retrieving the histogram %s from file %s "%(self.histo_name,syst_file.GetName()))
                if flavour=="all":
                    syst_histo=getFullHist(self.histo_name,syst_file)
                    self.systematics[syst].Add(syst_histo,-1)
                else:
                    syst_histo=syst_file.Get(self.histo_name+"_"+flavour)

                self.addErrors(syst_histo)
#                    self.systematics[syst].Add(syst_histo,-1)
                else:
                    syst_histo=syst_file.Get(self.histo_name+"_"+flavour)

                self.systematics[self.name+"_"+syst]=self.addErrors(syst_histo)
                syst_file.Close()

            for syst in sys_sample:
                if 'weight' in syst:
                    syst=syst.split('weight_')[-1]
                if not os.path.isfile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"):
                    print("  WARNING :  "+inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"+"_combination.root")
                    continue
                syst_file=ROOT.TFile(inputdir+'/'+syst+"/"+syst+"_nominal_combination.root", "read")
                if syst_file.IsZombie():
                    print("   ERROR :   File "+inputdir+'/'+syst+"/"+syst+"_nominal_combination.root"+" but it could not be opened")
                    syst_file.Close()
                    continue
                syst_histo=None

                if flavour=="all":
                    syst_histo=getFullHist(self.histo_name,syst_file)
                    self.systematics[syst].Add(syst_histo,-1)
                else:
                    syst_histo=syst_file.Get(self.histo_name+"_"+flavour)
                self.addErrors(syst_histo)
#                    self.systematics[syst].Add(syst_histo,-1)
                else:
                    syst_histo=syst_file.Get(self.histo_name+"_"+flavour)
                if "PowWH7" in syst:
                    nom_alt_file=ROOT.TFile(inputdir+'/FTAG2_ttbar_PhPy8_AF2/FTAG2_ttbar_PhPy8_AF2_nominal_combination.root', "read")
                    nom_alt_sample=getFullHist(self.histo_name,nom_alt_file)
                    self.systematics[self.name+syst]=self.addErrors(syst_histo,nom_alt_sample)
                    nom_alt_file.Close()
                else:
                    self.systematics[self.name+syst]=self.addErrors(syst_histo)
                syst_file.Close()

            if self.name == "ttbar":
                for syst in other_ttbar_systs:
                    if 'weight' in syst:
                        syst=syst.split('weight_')[-1]
                    if not os.path.isfile(inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root"):
                        print("  WARNING :  "+inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root")
                        continue
                    syst_file=ROOT.TFile(inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root", "read")
                    if syst_file.IsZombie():
                        print("   ERROR :   File "+inputdir+'/'+self.process+"/"+self.process+"_"+syst+"_combination.root"+" but it could not be opened")
                        syst_file.Close()
                        continue

                    syst_histo=None
  #                  print(" INFO : Retrieving the histogram %s from file %s "%(self.histo_name,syst_file.GetName()))
                    if flavour=="all":
                        syst_histo=getFullHist(self.histo_name,syst_file)
                        self.systematics[syst].Add(syst_histo,-1)
                    else:
                        syst_histo=syst_file.Get(self.histo_name+"_"+flavour)
                    self.addErrors(syst_histo)
#                        self.systematics[syst].Add(syst_histo,-1)
                    else:
                        syst_histo=syst_file.Get(self.histo_name+"_"+flavour)
                    if "weight_mc" in syst and "weight_mc_shower_np_11" not in syst:
                        nom_alt_file=ROOT.TFile(inputdir+'/FTAG2_ttbar_PhPy8/FTAG2_ttbar_PhPy8_weight_mc_shower_np_11_combination.root', "read")
                        nom_alt_sample=getFullHist(self.histo_name,nom_alt_file)
                        self.systematics[self.name+"_"+syst]=self.addErrors(syst_histo,nom_alt_sample)
                        nom_alt_file.Close()
                    else:
                        self.systematics[self.name+"_"+syst]=self.addErrors(syst_histo)
                    syst_file.Close()
                    
        self.histo_nominal.SetStats(0)
        if not isData:
            if flavour != "all":
                self.histo_nominal.SetFillColor(flavours[flavour])
            else:
                self.histo_nominal.SetFillColor(samples[self.sample][2])
            self.histo_nominal.SetFillStyle(1001)
            self.histo_nominal.SetLineColor(ROOT.kBlack)
            self.histo_nominal.SetLineWidth(2)
            self.histo_nominal.SetMarkerStyle(20)
        else:
            self.histo_nominal.SetMarkerStyle(20)
            self.histo_nominal.SetMarkerSize(2)
            self.histo_nominal.SetLineWidth(2)
            self.histo_nominal.SetMarkerColor(samples[self.sample][2])
            self.histo_nominal.SetLineColor(samples[self.sample][2])

        if flavour != "all":            
            self.histo_nominal.SetLineColor(flavours[flavour])
        else:
            self.histo_nominal.SetLineColor(samples[self.sample][2])

    def addErrors(self,histo):
            
        for i in range(histo.GetNbinsX()):
            if math.fabs(self.ybins[i]) > 0:
                if self.ybins[i] > histo.GetBinContent(i+1):
                    self.yerrordown[i]=math.sqrt(math.pow(self.yerrordown[i],2)+math.pow((histo.GetBinContent(i+1)-self.ybins[i])/self.ybins[i],2))
                else:
                    self.yerrorup[i]=math.sqrt(math.pow(self.yerrorup[i],2)+math.pow((histo.GetBinContent(i+1)-self.ybins[i])/self.ybins[i],2))
        self.systematics={}

    def addErrors(self,histo,nomhisto=None):
        
        errorsup=[]
        errorsdown=[]

        y_bins=self.ybins
        if nomhisto:
            for i in range(nomhisto.GetNbinsX()):
                y_bins[i]=nomhisto.GetBinContent(i+1)
            
        for i in range(histo.GetNbinsX()):
            if math.fabs(self.ybins[i]) > 0:
                if self.ybins[i] > histo.GetBinContent(i+1):
                    self.yerrordown[i]=math.sqrt(math.pow(self.yerrordown[i],2)+math.pow((histo.GetBinContent(i+1)-self.ybins[i])/self.ybins[i],2))
                    errorsdown.append(self.ybins[i]-histo.GetBinContent(i+1))
                    errorsup.append(0)
                else:
                    self.yerrorup[i]=math.sqrt(math.pow(self.yerrorup[i],2)+math.pow((histo.GetBinContent(i+1)-y_bins[i])/y_bins[i],2))
                    errorsup.append(histo.GetBinContent(i+1)-y_bins[i])
                    errorsdown.append(0)
        return [errorsdown,errorsup]

    def Add(self, other):
        self.histo_nominal.Add(other.histo_nominal)
        for i in range(self.histo_nominal.GetNbinsX()):
            if math.fabs(other.yerrordown[i]*other.histo_nominal.GetBinContent(i+1) + self.histo_nominal.GetBinContent(i+1)) > 0 :
                self.yerrordown[i]=math.sqrt(math.pow(self.yerrordown[i]*self.histo_nominal.GetBinContent(i+1),2)+math.pow( other.yerrordown[i]*other.histo_nominal.GetBinContent(i+1),2))/(other.histo_nominal.GetBinContent(i+1) + self.histo_nominal.GetBinContent(i+1))
                self.yerrorup[i]=math.sqrt(math.pow(self.yerrorup[i]*self.histo_nominal.GetBinContent(i+1),2)+math.pow( other.yerrorup[i]*other.histo_nominal.GetBinContent(i+1),2))/(other.histo_nominal.GetBinContent(i+1) + self.histo_nominal.GetBinContent(i+1))

class Stacker:
    def __init__(self,name,folder,histo_name,xlabel="",ylabel="Events"):
        self.name=name
        self.histo_name=histo_name
        self.folder=folder
        self.histos=[]
        self.histos_flavour={}
        for flav in flavours.keys():
            self.histos_flavour[flav]=None
        if doData1516:
            self.data=histogram("data1516"+self.histo_name,"data1516",self.histo_name,self.folder,isData=True)
        elif doData17:
            self.data=histogram("data17"+self.histo_name,"data17",self.histo_name,self.folder,isData=True)
        elif doData18:
            self.data=histogram("data18"+self.histo_name,"data18",self.histo_name,self.folder,isData=True)

        tmp_flavours={}
        for sample in samples.keys():
            if "data" in sample:
                continue
            
            self.histos.append(histogram(sample+self.histo_name,sample,self.histo_name,self.folder))
            for flav in flavours.keys():
                if self.histos_flavour[flav] == None:
                    self.histos_flavour[flav]=histogram(sample+self.histo_name,sample,self.histo_name,self.folder,flavour=flav)
                else:
                    self.histos_flavour[flav].Add(histogram(sample+self.histo_name+"_"+flav,sample,self.histo_name,self.folder,flavour=flav))


        self.canvas=ROOT.TCanvas("canvas","canvas",1200,800)
        self.leg=ROOT.TLegend(0.5,0.55,0.85,0.85)
        self.leg_flav=ROOT.TLegend(0.5,0.55,0.85,0.85)
        self.leg_ratio=ROOT.TLegend(0.25,0.8,0.35,0.85)
        self.stack_samples=ROOT.THStack("hs"+self.name,"")
        self.stack_samples_flav=ROOT.THStack("hs"+self.name,"")
        self.total_histo=None
        self.xlabel=xlabel
        self.ylabel=ylabel

    def setPads(self):

        self.canvas.cd()
        self.pad1=ROOT.TPad("pad1","",0.0,0.05,1.0,0.3)
        self.pad1.SetTopMargin(0)
        self.pad1.SetBottomMargin(0.3)
        self.pad1.Draw()
        self.pad2=ROOT.TPad("pad2","",0.0,0.3,1.0,1.0)
        self.pad2.SetBottomMargin(0)
        self.pad2.Draw()
        self.pad2.SetLogy(1)

    def setLegends(self):
        self.leg.SetBorderSize(0)
        self.leg.SetFillColor(0)
        self.leg.SetNColumns(2)
        self.leg_flav.SetBorderSize(0)
        self.leg_flav.SetFillColor(0)
        self.leg_flav.SetNColumns(2)
        self.leg_ratio.SetBorderSize(0)
        self.leg_ratio.SetFillColor(0)
        self.leg_ratio.SetTextSize(0.08)

    def setStackers(self):
        self.histos=sorted(self.histos,key=lambda hist : hist.histo_nominal.GetMaximum(),reverse=False)
        self.histos_flavour=sorted(self.histos_flavour.items(),key=lambda hist : hist[1].histo_nominal.GetMaximum() , reverse=False)
        hists_flav=[hist[1] for hist in self.histos_flavour]

        for hist in self.histos:
            if self.total_histo == None:
                self.total_histo=hist.histo_nominal.Clone("Sum"+self.name)
            else:
                self.total_histo.Add(hist.histo_nominal)
            self.stack_samples.Add(hist.histo_nominal)
            self.leg.AddEntry(hist.histo_nominal,legendLabels[hist.process],"f")

        for hist in self.histos_flavour:
            hist[1].histo_nominal.SetFillColor(flavours[hist[0]])
            self.stack_samples_flav.Add(hist[1].histo_nominal)
            self.leg_flav.AddEntry(hist[1].histo_nominal,hist[0],"f")
        
    def defineMaximum(self,stacker,pad,log):
        
        binmax=stacker.GetStack().Last().GetMaximumBin()
        maximum=stacker.GetStack().Last().GetBinContent(binmax)
        
        pad.cd()
        # if binmax < stacker.GetStack().Last().GetNbinsX()/2 :
        #     print("Pixel stacker %d and pixel v %d"%(pad.YtoPixel(maximum),pad.VtoPixel(0.7)))
            
        #     while pad.YtoPixel(maximum) < pad.VtoPixel(0.7) :
        #         if log:
        #             stacker.SetMaximum(stacker.GetMaximum()*10)
        #         else:
        #             stacker.SetMaximum(stacker.GetMaximum()*1.5)
        # else:
        #     while pad.YtoPixel(maximum ) < pad.VtoPixel(0.55) :
        if log:
            stacker.SetMaximum(stacker.GetMaximum()*1000)
        else:
            stacker.SetMaximum(stacker.GetMaximum()*1.5)
        pad.Modified()
        pad.Update()
        stacker.Modified()

    def getUncertainties(self,useSyst=True):
        yerrorup=[]
        yerrordown=[]
        ratioup=[]
        ratiodown=[]

        for hist in self.histos:
            for i in range(len(hist.yerrorup)):
                if len(yerrorup) < len(hist.yerrorup):
                    yerrorup.append(hist.yerrorup[i]*hist.ybins[i])
                    yerrordown.append(hist.yerrordown[i]*hist.ybins[i])
                else:
                    yerrorup[i]=math.sqrt(math.pow(yerrorup[i],2)+math.pow(hist.yerrorup[i]*hist.ybins[i],2))
                    yerrordown[i]=math.sqrt(math.pow(yerrordown[i],2)+math.pow(hist.yerrordown[i]*hist.ybins[i],2))

        ytotal=[]
        yones=[]

        for i in range(self.total_histo.GetNbinsX()):
            yones.append(1.)
            ytotal.append(self.total_histo.GetBinContent(i+1))
            if math.fabs(self.total_histo.GetBinContent(i+1)) > 0:
                ratioup.append(yerrorup[i]/self.total_histo.GetBinContent(i+1))
                ratiodown.append(yerrordown[i]/self.total_histo.GetBinContent(i+1))
            else:
                ratioup.append(1)
                ratiodown.append(1)

        ## Total uncertainty 
        self.graph_errors=ROOT.TGraphAsymmErrors(len(yerrorup),np.array(self.histos[0].xbins),np.array(ytotal),np.array(self.histos[0].xerrordown),np.array(self.histos[0].xerrorup),np.array(yerrordown),np.array(yerrorup))
        self.graph_errors.SetFillStyle(3004)
        self.graph_errors.SetFillColor(ROOT.kBlack)
#        graph_errors.SetLineWidth(1)

        ## Ratio plot
        self.graph_errors_ratio=ROOT.TGraphAsymmErrors(len(yerrorup),np.array(self.histos[0].xbins),np.array(yones),np.array(self.histos[0].xerrordown),np.array(self.histos[0].xerrorup),np.array(ratiodown),np.array(ratioup))
        self.graph_errors_ratio.SetFillStyle(1001)
        self.graph_errors_ratio.SetFillColorAlpha(kGreen+3,0.4)
        self.graph_errors_ratio.SetLineColor(ROOT.kBlack)

    def plotStyle(self,pad1,option,lumino):

        latex=ROOT.TLatex()
        latex.SetTextFont(42)
        latex.SetTextSize(0.06)
        pad1.cd()
        if option =="INT":
            latex.DrawLatexNDC(0.27,0.8,"#bf{#it{ATLAS}} Internal")
        elif option =="SIMINT":
            latex.DrawLatexNDC(0.23,0.8,"#bf{#it{ATLAS}} Simulation Internal")
        elif option =="SIM":
            latex.DrawLatexNDC(0.27,0.8,"#bf{#it{ATLAS}} Simulation")
        elif option =="WIP":
            latex.DrawLatexNDC(0.23,0.8,"#bf{#it{ATLAS}} Work in progress")
        elif option =="OFFICIAL":
            latex.DrawLatexNDC(0.3,0.8,"#bf{#it{ATLAS}}")
        elif option == "PREL":
            latex.DrawLatexNDC(0.27,0.8,"#bf{#it{ATLAS}} Preliminary")
            
        latex.SetTextSize(0.04)
        latex.DrawLatexNDC(0.23,0.7,"#sqrt{s} = 13 TeV, #int Ldt = %.2f fb^{-1}"%(lumino/1000.))

    def Draw(self,output):

        ### Build the stacks and the plots
        self.setPads()
        self.setStackers()
        self.getUncertainties()
        self.setLegends()
        self.setRatio()

        line=ROOT.TLine(self.ratio.GetXaxis().GetBinLowEdge(1),1.,self.ratio.GetXaxis().GetBinUpEdge(self.ratio.GetNbinsX()),1.)
        line.SetLineColor(ROOT.kRed)
        line.SetLineWidth(ROOT.kBlack)
        line.SetLineStyle(10)

        ## Histogram separated by samples 
        self.leg.AddEntry(self.data.histo_nominal,legendLabels[self.data.process],"lp")
        self.pad2.cd()
        self.defineMaximum(self.stack_samples,self.pad2,True)
        self.stack_samples.SetMinimum(0.01)
        self.stack_samples.Modified()        
        self.stack_samples.Draw("hist")
        self.stack_samples.GetYaxis().SetTitle(self.ylabel)
        self.stack_samples.GetYaxis().SetTitleOffset(0.5)
        self.stack_samples.GetYaxis().SetTitleSize(0.07)
#        self.stack_samples.Modified()
        self.pad2.Update()
        self.data.histo_nominal.Draw("SAME")
        self.leg.AddEntry(self.graph_errors,"Uncertainty","f")
        self.graph_errors.Draw("2""SAME")
        self.leg.Draw("SAME")
        self.plotStyle(self.pad2,"INT",lumi)
        self.pad1.cd()

        ## Ratio plot
        self.leg_ratio.AddEntry(self.graph_errors_ratio,"stat.+syst.","f")
        self.ratio.Draw()
        self.leg_ratio.Draw("SAME")
        self.graph_errors_ratio.Draw("2""SAME")
        line.Draw("SAME")
        self.canvas.SaveAs(output,"RECREATE")

        ## Plotting the flavour histogram
        self.pad2.Clear()
        self.leg_flav.AddEntry(self.data.histo_nominal,legendLabels[self.data.process],"lp")
        self.pad2.cd()
        self.defineMaximum(self.stack_samples_flav,self.pad2,True)
        self.stack_samples_flav.SetMinimum(0.01)
        self.stack_samples_flav.Modified()
        self.stack_samples_flav.Draw("hist")
        self.stack_samples_flav.GetYaxis().SetTitle(self.ylabel)
        self.stack_samples_flav.GetYaxis().SetTitleOffset(0.5)
        self.stack_samples_flav.GetYaxis().SetTitleSize(0.07)
#        self.stack_samples_flav.Modified()
        self.pad2.Update()
        self.data.histo_nominal.Draw("SAME")
        self.leg_flav.AddEntry(self.graph_errors,"Uncertainty","f")
        self.graph_errors.Draw("2""SAME")
        self.leg_flav.Draw("SAME")
        self.plotStyle(self.pad2,"INT",lumi)
        self.canvas.SaveAs(output.replace('.pdf','_flav.pdf'),"RECREATE")

        del self.stack_samples
        del self.stack_samples_flav
        del self.ratio

    def getErrorTable(self):
        
        total_y=[]
        for bin in range(self.total_histo.GetNbinsX()):
            total_y.append(self.total_histo.GetBinContent(bin+1))
        errup={}
        errdown={}
        for hist in self.histos:
            for systematic,impl in hist.systematics.items():
                sysname=systematic.split('___')[0]
                if not sysname in errdown.items():
                    errdown[sysname]=np.array(impl[0])
                else:
                    errdown[sysname]=np.sqrt(errordown[sysname]**2+impl[0]**2)
                if not sysname in errup.items():
                    errup[sysname]=np.array(impl[1])
                else:
                    errup[sysname]=np.sqrt(errorup[sysname]**2+impl[1]**2)

        for key in errup.keys():
            errup[key]=errup[key]/np.array(total_y)
            errdown[key]=errdown[key]/np.array(total_y)
        print(errup)
        print(errdown)
        total_errup=np.array([x.to_list() for x in errup.values()])
        total_errdown=np.array([x.to_list() for x in errdown.values()])
        listsys=errup.keys()

        print(total_errup)
        print(total_errup.shape)
        print(total_errdown)
        print(total_errdown.shape)

        dfup=pd.DataFrame(data=total_errup,index=listsys,columns=[("bin_%d"%(x+1)) for x in range(len(total_y))])
        dfdown=pd.DataFrame(data=total_errdown,index=listsys,columns=[("bin_%d"%(x+1)) for x in range(len(total_y))])
        return [dfup,dfdown]
        
    def setRatio(self):
        self.ratio=self.data.histo_nominal.Clone("ratio_"+self.name)
        self.ratio.SetStats(0)
        self.ratio.SetTitle("")
        self.ratio.GetXaxis().SetTitle(self.xlabel)
        self.ratio.GetYaxis().SetTitle("Data/MC")
        self.ratio.Divide(self.total_histo)
        self.ratio.GetYaxis().SetRangeUser(0.7,1.3)
        self.ratio.GetYaxis().SetNdivisions(5)
        self.ratio.GetYaxis().SetTitleSize(0.2)
        self.ratio.GetYaxis().SetTitleOffset(0.2)
        self.ratio.GetXaxis().SetTitleSize(0.25)
        self.ratio.GetXaxis().SetTitleOffset(0.5)
        self.ratio.GetYaxis().SetLabelSize(0.125)
        self.ratio.GetXaxis().SetLabelSize(0.15)
        self.ratio.GetXaxis().SetTickSize(0.1)
        self.ratio.GetYaxis().SetTickSize(0.03)


if __name__ == '__main__':
    
    NormalStyle()
    histo_list={
        'jet1_pt': 'Leading jet p_{T} [GeV]',
        'jet2_pt': 'Subleading jet p_{T} [GeV]',
        'jet1_DL1':'DL1 of leading jet',
        'jet2_DL1':'DL1 of subleading jet',
        'jet1_mv2c10':'MV2c10 of leading jet',
        'jet2_mv2c10':'MV2c10 of subleading jet',
#        'met':'E_{T}^{miss} [GeV]'
        }

    for folder in folders:
        os.system("mkdir -p %s/plots/%s/%s/"%(os.getcwd(),dataName,folder) )
        for hist,xlabel in histo_list.items():
            stacking=Stacker("stack",folder,hist,xlabel,"Events")
            stacking.Draw("%s/plots/%s/%s/%s.pdf"%(os.getcwd(),dataName,folder,hist))

    plotFolder='plots'
    for folder in folders:
        os.system("mkdir -p %s/%s/%s/%s/"%(os.getcwd(),plotFolder,dataName,folder) )
        for hist,xlabel in histo_list.items():
            stacking=Stacker("stack",folder,hist,xlabel,"Events")
            stacking.Draw("%s/%s/%s/%s/%s.pdf"%(os.getcwd(),plotFolder,dataName,folder,hist))
#            frames=stacking.getErrorTable()
#            print()
#    class Stacker(self,name,histo_name,xlabel,ylabel):
