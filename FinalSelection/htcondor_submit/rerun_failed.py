import os,glob
import sys
import subprocess
import shutil
sys.path.insert(0, '../')
from options_file import *


samples_to_run = (
    options.nominal_samples +
    options.singletop_rad_samples +
    options.singletop_fsr_samples +
    options.singletop_syst_samples +
    options.ZJets_syst_samples +
    options.Diboson_syst_samples +
    options.ttbar_syst_samples +
    options.ttbar_rad_samples +
    options.ttbar_fsr_samples
)

submissionFiles=[]
for sample in samples_to_run:
    sampleName=sample.name
    list_of_folders=glob.glob(os.getcwd()+"/condor_logs/"+options.data_name+sampleName+"*")
    for folder  in list_of_folders:
        syst=folder.split("/")[-1].split(sampleName)[-1]
        systname = "nominal"
        if syst=="":
            pass
        elif "weight_mc_fsr" in syst:
            systname=syst[1:]
        elif "weight_mc_rad" in syst:
            systname=syst[1:]
        elif "_nominal" in syst:
            systname=syst[1:]
        else:
            systname=syst
        outputFolder=options.output_dir+"/"+sampleName+"/"
        intermediateFilesExist=any( systname in infile.split("/")[-1] for infile in glob.glob(options.output_dir+"/"+sampleName+"/"+"/*") if not "combination.root" in infile)
        if os.path.isfile(outputFolder+"/"+sampleName+"_"+systname+"_combination.root") and not intermediateFilesExist:
            print("\033[94m Systematic %s for sample %s seems to have been processed correctly \033[0m"%(systname,sampleName))
        elif os.path.isfile(outputFolder+"/"+sampleName+"_"+systname+"_combination.root") and intermediateFilesExist:
            print("\033[93m WARNING: Systematic %s for sample %s seems to have intermediate files ! \033[0m "%(systname,sampleName))
            submissionFiles.extend(glob.glob(folder+"/*submit"))
        elif not os.path.isfile(outputFolder+"/"+sampleName+"_"+systname+"_combination.root"):
            print("\033[91m ERROR: Systematic %s for sample %s was not processed correctly ! \033[0m "%(systname,sampleName))
            submissionFiles.extend(glob.glob(folder+"/*submit"))


resubmitter=open("resubmit.sh","w+")
for submitFile in submissionFiles:
    resubmitter.write("condor_submit %s\n"%submitFile)
resubmitter.close()
