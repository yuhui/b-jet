import os
import sys
import subprocess
import shutil, socket
sys.path.insert(0, '../')
from options_file import *
import sys,glob,getpass,socket

run_nominal=True # nominal
run_detector_syst=True # experimental systematics
run_syst_samples=True # alternative samples + QCD ISR, FSR variations
run_syst_samples_boot_strap=False
run_Pdf_syst=True # PDF uncertainties
run_QCD_syst=True # muF and muR variations
run_weight_syst=False
run_systs_for_fake_plots=False
run_data = True # data
run_MC = True # MC

#run_options_for_all_jobs="--rrS --save_fit_input --nominal_only" #--combine_datasets " #--correctFakes"  --log
run_options_for_all_jobs="--rrS --save_fit_input --runParallel" #--combine_datasets " #--correctFakes"  --log
#--runParallel 
run_options_for_MC=run_options_for_all_jobs + " --correctFakes"  # + " --correctFakes" + " --apply_lf_calib"

#we have to create file structure first
def writeShellScript(job_dir,job_name,inFile,runOptions):
    username = getpass.getuser()
    fsh = open(job_dir+job_name+'.sh', 'w')
    fsh.write("#!/bin/bash\n")
    fsh.write('cd '+os.getcwd()+'\n')
    fsh.write('cd ..\n')
    fsh.write("export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase\n")
    fsh.write("source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh\n")
    if "alopezs" in username and 'zeuthen.desy.de' in socket.gethostname():
        fsh.write('source  setup_desy.sh\n')
    else:
        fsh.write('source  setup.sh\n')
    #fsh.write('export LD_LIBRARY_PATH='+os.path.dirname(os.getcwd())+'/TSelectors/build/x86_64-centos7-gcc11-opt/lib:$LD_LIBRARY_PATH\n')
    fsh.write('lsetup \"emi -w\" \n')
    fsh.write('python finalSelection.py '+inFile+ ' ' + runOptions +' \n')
    fsh.close()

def writeJobSubmit(job_dir,job_name, boot_strap):
    fsubmit = open(job_dir+job_name+'.submit', 'w')
    cwd=os.getcwd();
    fsubmit.write("executable     = "+cwd+"/"+job_dir+job_name+".sh\n")
    #fsubmit.write("output         = "+cwd+"/"+job_dir+job_name+".out\n")
    fsubmit.write("error          = "+cwd+"/"+job_dir+job_name+".error\n")
    fsubmit.write("log            = "+cwd+"/"+job_dir+job_name+".log\n")
    fsubmit.write("RequestCpus = 4\n")
    fsubmit.write("use_x509userproxy = true\n")
    fsubmit.write("x509userproxy=" + options.proxy_path +"\n")
    fsubmit.write("universe       = vanilla\n")
    fsubmit.write("when_to_transfer_output = ON_EXIT\n")
    fsubmit.write("+JobFlavour = \"tomorrow\" \n")
    fsubmit.write("queue 1 \n")
    fsubmit.close()


def submitFinalSelection(job_name,inFile,runOptions, boot_strap=False):
    job_dir='condor_logs/'+job_name+'/'
    cwd=os.getcwd();
    if not os.path.exists(job_dir):
        os.makedirs(job_dir)
    else:
        shutil.rmtree(job_dir)
        os.makedirs(job_dir)
        print("already submitted: "+ 'python finalSelection.py '+inFile+ ' ' + runOptions +' \n')

    if boot_strap:
        runOptions=runOptions+" --boot_strap"
    writeShellScript(job_dir,job_name,inFile,runOptions)
    writeJobSubmit(job_dir, job_name, boot_strap)
    subprocess.check_call(["chmod", "u+x", cwd+"/"+job_dir+job_name+".sh"])
    subprocess.check_call(["chmod", "u+x", cwd+"/"+job_dir+job_name+".submit"])
    subprocess.check_call(["condor_submit", cwd+"/"+job_dir+job_name+".submit"])
    print("submitted: "+ 'python finalSelection.py '+inFile+ ' ' + runOptions +' \n')

input_folder=options.input_selection_dir
#let's start with submitting data
if run_nominal and run_data:
    input_file = input_folder + options.data_name +".txt"
    #runOptions=run_options_for_all_jobs + " --save_bin_means_for_sr_histo --data --run_fakes"
    runOptions=run_options_for_all_jobs + " --save_bin_means_for_sr_histo --data"
    job_name=options.data_name
    submitFinalSelection(job_name,input_file,runOptions)


if run_MC:
    for sample in options.nominal_samples:
        if run_nominal:
            input_file = input_folder + sample.name +".txt"
            #runOptions=run_options_for_MC + " --save_bin_means_for_sr_histo --run_fakes --hadronization " + str(sample.hadronization)
            runOptions=run_options_for_MC + " --save_bin_means_for_sr_histo --hadronization " + str(sample.hadronization)
            job_name=options.data_name+sample.name
            submitFinalSelection(job_name,input_file,runOptions, run_syst_samples_boot_strap)
        if run_detector_syst:
            for tree_systematic in options.tree_systematics:
                input_file = input_folder + sample.name +".txt"
                job_name=options.data_name+sample.name+tree_systematic.systematic_name
                runOptions="-t "+tree_systematic.systematic_name + " " + run_options_for_MC + " --run_fakes --hadronization " + str(sample.hadronization)
                boot_strap=False
                if tree_systematic.do_bootstrap:
                    if run_syst_samples_boot_strap:
                        boot_strap=True
                submitFinalSelection(job_name,input_file,runOptions, boot_strap)
                
            for inner_systematic in options.inner_systematics_in_nominal_tree:
                input_file = input_folder + sample.name +".txt"
                job_name=options.data_name+sample.name+inner_systematic.systematic_name
                runOptions="-w "+inner_systematic.systematic_name + " " + run_options_for_MC + " --hadronization " + str(sample.hadronization) + " --output_cmd " + inner_systematic.systematic_command
                if not "FT_EFF" in inner_systematic.systematic_name:
                    runOptions=runOptions+ " --run_fakes"
                boot_strap=False
                if inner_systematic.do_bootstrap:
                    if  run_syst_samples_boot_strap:
                        boot_strap=True
                submitFinalSelection(job_name,input_file,runOptions, boot_strap)


    systsamples_to_run = (
        # options.singletop_rad_samples + 
        # options.singletop_fsr_samples +
        # options.singletop_syst_samples+ 
        # options.ZJets_syst_samples +
        # options.Diboson_syst_samples + 
        # options.ttbar_syst_samples 
        # options.ttbar_rad_samples+
        # options.ttbar_fsr_samples
        options.singletop_rad_samples + 
        options.singletop_fsr_samples +
        options.singletop_syst_samples +
        #options.ZJets_syst_samples +
        #options.Diboson_syst_samples +
        options.ttbar_syst_samples +
        options.ttbar_rad_samples+
        options.ttbar_fsr_samples
        )
    

    if run_syst_samples:
        for sample in systsamples_to_run:
            runOptions = run_options_for_MC + " --run_fakes --hadronization " + str(sample.hadronization) + " -w " + sample.systematic + " --output_cmd " + sample.systematic_command
            boot_strap=False
            if sample.boot_strap_available and run_syst_samples_boot_strap:
                boot_strap=True
            job_name=options.data_name+sample.name+"_"+sample.systematic
            input_file = input_folder + sample.name +".txt"
            submitFinalSelection(job_name,input_file,runOptions, boot_strap)

    if run_Pdf_syst:
        for pdf_systematic in options.ttbar_pdf_systematics:
            job_name=options.data_name+options.ttb_sample.name+pdf_systematic.systematic_command
            input_file = input_folder + options.ttb_sample.name +".txt"
            runOptions="-w "+pdf_systematic.systematic_name + " " +run_options_for_MC + " --hadronization " + str(options.ttb_sample.hadronization) + " --output_cmd " + pdf_systematic.systematic_command
            #runOptions="-w "+pdf_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.ttb_sample.hadronization)
            submitFinalSelection(job_name,input_file,runOptions, pdf_systematic.do_bootstrap)
        for pdf_systematic in options.singletop_pdf_systematics:
            job_name=options.data_name+options.singleTop_sample.name+pdf_systematic.systematic_command
            input_file = input_folder + options.singleTop_sample.name +".txt"
            runOptions="-w "+pdf_systematic.systematic_name + " " +run_options_for_MC + " --hadronization " + str(options.singleTop_sample.hadronization) + " --output_cmd " + pdf_systematic.systematic_command
            #runOptions="-w "+pdf_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.singleTop_sample.hadronization)
            submitFinalSelection(job_name, input_file, runOptions, pdf_systematic.do_bootstrap)

    if run_weight_syst:
        for weight_systematic in options.Diboson_weight_mc_systematics:
            job_name=options.data_name+options.Diboson_sample.name+weight_systematic.systematic_command
            input_file = input_folder + options.Diboson_sample.name +".txt"
            runOptions="-w "+weight_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.Diboson_sample.hadronization)
            submitFinalSelection( job_name, input_file, runOptions, weight_systematic.do_bootstrap)
        for weight_systematic in options.ZJets_weight_mc_systematics:
            job_name=options.data_name+options.ZJets_sample.name+weight_systematic.systematic_command
            input_file = input_folder + options.ZJets_sample.name +".txt"
            runOptions="-w "+weight_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.ZJets_sample.hadronization)
            submitFinalSelection( job_name, input_file,runOptions, weight_systematic.do_bootstrap)

    if run_QCD_syst:
        for qcd_systematic in options.ttbar_qcd_systematics:
            job_name=options.data_name+options.ttb_sample.name+qcd_systematic.systematic_command
            input_file = input_folder + options.ttb_sample.name +".txt"
            runOptions="-w "+qcd_systematic.systematic_name + " " +run_options_for_MC + " --hadronization " + str(options.ttb_sample.hadronization) + " --output_cmd " + qcd_systematic.systematic_command
            #runOptions="-w "+qcd_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.ttb_sample.hadronization)
            submitFinalSelection(job_name,input_file,runOptions, qcd_systematic.do_bootstrap)
        for qcd_systematic in options.singletop_qcd_systematics:
            job_name=options.data_name+options.singleTop_sample.name+qcd_systematic.systematic_command
            input_file = input_folder + options.singleTop_sample.name +".txt"
            runOptions="-w "+qcd_systematic.systematic_name + " " +run_options_for_MC + " --hadronization " + str(options.singleTop_sample.hadronization) + " --output_cmd " + qcd_systematic.systematic_command
            #runOptions="-w "+qcd_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.singleTop_sample.hadronization)
            submitFinalSelection(job_name, input_file, runOptions, qcd_systematic.do_bootstrap)
        #for weight_systematic in options.ZJets_qcd_systematics:
        #    job_name=options.data_name+options.ZJets_sample.name+weight_systematic.systematic_command
        #    input_file = input_folder + options.ZJets_sample.name +".txt"
        #    runOptions="-w "+weight_systematic.systematic_command + " " +run_options_for_MC + " --hadronization " + str(options.ZJets_sample.hadronization)
        #    submitFinalSelection( job_name, input_file,runOptions, weight_systematic.do_bootstrap)

    if run_systs_for_fake_plots:
        boot_strap=False
        for sample in options.special_samples_for_fake_estimation:
            for tree_systematic in options.tree_systematics:
                input_file = input_folder + sample.name +".txt"
                job_name=options.data_name+sample.name+tree_systematic.systematic_name
                runOptions="-t "+tree_systematic.systematic_name + " --rrS --log " 
                submitFinalSelection(job_name,input_file,runOptions, boot_strap)
            for inner_systematic in options.inner_systematics_in_nominal_tree:
                input_file = input_folder + sample.name +".txt"
                job_name=options.data_name+sample.name+inner_systematic.systematic_name
                runOptions="-w "+inner_systematic.systematic_name + " --rrS --log "
                if not "FT_EFF" in inner_systematic.systematic_name: 
                    submitFinalSelection(job_name,input_file,runOptions, boot_strap)
