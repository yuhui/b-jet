# 
# PLEASE READ BEFORE USE
# 
# This is based off: cutflows-to-table.py for orgiinal code
# Chnages (compared to cutflows-to-table.py):
# OG code had samples = options.samples so had to change to options.nominal_samples
# Therefore the data file must have _nominal_ before combination.root
# Usage:
# Outputs yield table to a txt file (yieldtable.txt) which can be simply copied and pasted into latex
# To change the name of the file search/find: self.YT_filename and change accordingly
# 
# Next Changes to be made: Add export final tables to latex from existing functions.

import ROOT
import os
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *
import pandas as pd
import numpy as np

def writeFileHeader(file):
	file.write("% Please add the following required packages to your document preamble:\n")
	file.write("% \\usepackage{graphicx}\n")
	file.write("% \\booktabs\n\n")

def writeTableHeader(file, channel):
	file.write("Channel: " + channel + "\n")
	file.write("\\begin{table}[]\n")
	file.write("\\centering\n")
	file.write("\\resizebox{\\textwidth}{!}{%\n")
	file.write("\\begin{tabular}{lllllll}\n")
	file.write("\\toprule\n")

def writeTableFooter(file):
	# Final table commands
	file.write("\\bottomrule\n")
	file.write("\\end{tabular}%\n")
	file.write("}\n")
	file.write("\\caption{My caption}\n")
	file.write("\\label{my-label}\n")
	file.write("\\end{table}\n")
	file.write("\n")

dataName=options.data_name
outdir=options.output_dir

flavours={'bb':0,'bc':0,'bl':0,'cb':0,'cc':0,'cl':0,'lb':0,'lc':0,'ll':0}
flavourskeys=flavours.keys()
samples=options.nominal_samples
samples.insert(0,sample(dataName))
channels=options.channels

class CutFlowAndFlavCompAnalysis():
	def __init__(self):
		pass

	def getInfo4AllChannels(self):
		self.yieldtable_filename = "yieldtable.txt"
		f = open(self.yieldtable_filename,"w+")
		self.arr_of_Channel_names = []
		for c in channels:
			print('Channel: ', c.name)
			self.arr_of_Channel_names.append(c.name)
			snames="Samples &"
			lines=0
			flavlines=0
			for s in samples:
				snames = snames + ' ' + s.name + " &"
                                # infile=None
                                if "data" in s.name:
                                        infile= ROOT.TFile(outdir+s.name+"/"+s.name+"_data_combination.root", "read")
                                else:
                                        infile= ROOT.TFile(outdir+s.name+"/"+s.name+"_nominal_combination.root", "read")
				hist=infile.Get(c.name + "/h_"+c.name+"_CutFlow").Clone()
				if lines==0:
					lines=[]
					flavlines={}
					for bin in xrange(1,hist.GetNbinsX()):
						if not bin == (hist.GetNbinsX() - 1):
							lines.append(str(hist.GetXaxis().GetBinLabel(bin)) + " &")
					for flav in flavourskeys:
						flavlines[flav]=flav+' &'

				for ci in xrange(0,len(lines)):
					lines[ci]=lines[ci]+" "+str("%.1f" % hist.GetBinContent(ci+1))+' &'
				for flav in flavourskeys:
					if not s.name==dataName:
						hist=infile.Get(c.name + "/h_"+c.name+"_jet1_phi_"+flav).Clone()
						flavlines[flav]=flavlines[flav]+' '+str("%.1f" % hist.Integral())+" &"
					else:
						flavlines[flav]=flavlines[flav]+' - &'
		    # Put sample names line into just the sample names (not FTAG2_ttbar but just ttbar... etc)
			snames = snames.split("&")[:-1]
			i = 2
			for sname in snames[2:]:
				sname = sname.split("_")[1]
				snames[i] = " " + sname + " "
				i = i + 1
			snames = "&".join(snames)
			f.write(snames + "\\midrule \n")
			print(snames)
			for line in lines:
				line = "&".join(line.split("&")[:-1])
				print(line)
				f.write(line + "\n")
			print("")
			i = 1
			for flav in flavourskeys:
				flavlines[flav] = "&".join(flavlines[flav].split("&")[:-1])
				print(flavlines[flav])
				f.write(flavlines[flav] + "\n")
				i = i + 1
			print("\n")
		print("Has been outputted to: " + self.yieldtable_filename + "\n")
		f.close()

	def createArrayFromTxtfile(self):
		f = open(self.yieldtable_filename,"r")
		arr = []
		# Create initially a huge array of each element being a line in the txt file
		for line in f:
			l = list(line)
			n = len(line)
			l[n-1:n] = []
			x = "".join(l)
			arr.append(x)
		return arr

	def cleanArray4DFArr(self, file_array):
		# Here we remove clean the data: remove excess sapces, "& to ," (for numpy array), etc
		for i in range(0,len(file_array)):
			# Split the line first
			line = file_array[i].split(" & ")
			# Then loop over elements in the line
			for j in range(0,len(line)):
				# print(line[i])
				if (j+1) == len(line):
					line[j] = line[j][:-1]				#Remove final space in last element
				if "-" in line[j]:
					line[j] = "0.0"						#Change data column in flav comp to 0.0
				if "\\midrule" in line[j]:
					line[j] = line[j][:5]				#Change to ttbar
			file_array[i] = ",".join(line)
		return file_array


	def createHugeArray(self, file_array):
		# Create an array of DFs, one DF for each channel
		DF_arr = []
		sub_arr = []
		# First we create an element in the DF (super) array filled with info for each channel
		for i in range(0,len(file_array)):
			sub_arr.append(file_array[i])
			if "cl" in file_array[i]:
				sub_arr.append(file_array[i])
				DF_arr.append(sub_arr)
				sub_arr = []
		return DF_arr

	def createDFs(self,DF_arr):	
		# Now to create assosciated DFs of these channels, first loop over each channel
		for i in range(0,len(DF_arr)):
			# For each channel
			header = pd.DataFrame(DF_arr[i][0].split(",")).T
			new_df = pd.concat([header])
			for j in range(1,len(DF_arr[i])):
				# For each line in the channel from the txt file
				b = pd.DataFrame(DF_arr[i][j].split(",")).T
				new_df = pd.concat([new_df,b])
			# Modificaiton to initial DF here
			new_df = new_df.set_index([0])				#Change 1st col to index
			new_df.columns = new_df.iloc[0]				#Change 1st row to cols
			new_df = new_df[1:-1]
			# Finally insert it into the previous DF array
			DF_arr.append(new_df)
		DF_arr = DF_arr[(len(DF_arr)/2):]				#Remove previous lists
		# Next to split the DFs into CutFlow and Flavour Composition
		for i in range(0,len(DF_arr)):
			split_DF_arr = np.split(DF_arr[i], [9], axis=0)#Each split_DF_arr contains two DFs
			# Repeat last loop and insert and remove old elements
			DF_arr.append(split_DF_arr)
		DF_arr = DF_arr[(len(DF_arr)/2):]
		return DF_arr

	def getTables(self,file_array,channel):
		# First find the channel no corresponding the to the channel interested in
		for i in range(0,len(self.arr_of_Channel_names)):
			if channel == self.arr_of_Channel_names[i]:
				channel_no = i
				print("The channel no is: " + str(channel_no))
		return file_array[channel_no]

	def beautifyTables(self,DFs):
		cutflow_DF, flavComp_DF = DFs[0], DFs[1]
		# CutFlow First:
		cutflow_DF = cutflow_DF[:-1]					#Remove 0.0 last row
		cutflow_DF = cutflow_DF.astype(np.float64)		#Change col types to floats for summation
		cutflow_DF['Total-MC'] =  cutflow_DF.ix[:,1:].sum(axis=1)			#Add Total MC col
		print(cutflow_DF)

		# FlavComposition Next:
		flavComp_DF = flavComp_DF.drop(flavComp_DF.columns[0], axis=1)		#Drop Data Col
		flavComp_DF = flavComp_DF.astype(np.float64)						#Change col types to floats
		flavComp_DF['ttbar/total%'] =  (flavComp_DF.ix[:,"ttbar"]/flavComp_DF.ix[:,:].sum(axis=1))*100
		flavComp_DF = flavComp_DF.round(2)									#Round off to 2 d.p
		flavComp_DF = flavComp_DF.sort_values(by='ttbar/total%')			#Order by ttbar%
		print(flavComp_DF)

		# Purity table is Last:
		purity_DF = flavComp_DF
		purity_DF = purity_DF.drop(purity_DF.columns[-1], axis=1)		#Drop ttbar% Col
		purity_DF['Total-MC'] =  purity_DF.ix[:,1:].sum(axis=1)			#Add Total MC col
		total_MC_events = purity_DF.ix[:,"Total-MC"].sum()				#Need the total MC events for next calc
		purity_DF['Purity (%)'] =  (purity_DF.ix[:,"Total-MC"]/total_MC_events)*100
		purity_DF = purity_DF.drop(purity_DF.columns[:-1], axis=1)		#Drop every col but purity
		print(purity_DF)

		return cutflow_DF, flavComp_DF, purity_DF

# Main
main_instance = CutFlowAndFlavCompAnalysis()
main_instance.getInfo4AllChannels()
file_array = CutFlowAndFlavCompAnalysis.createArrayFromTxtfile(main_instance)
file_array = CutFlowAndFlavCompAnalysis.cleanArray4DFArr(main_instance, file_array)
huge_array_of_channels = main_instance.createHugeArray(file_array)
array_of_DFs = main_instance.createDFs(huge_array_of_channels)
table_DFs = main_instance.getTables(array_of_DFs, "emu_OS_J2")
#table_DFs = main_instance.getTables(array_of_DFs, "emu_OS_J2_m_lj_cut")

# Personally change the next function to suit you:
print(" =======   emu_OS_J2 ====== ")
cutflow_DF, flavComp_DF, purity_DF = main_instance.beautifyTables(table_DFs)
cutflow_DF.to_latex(buf="Yieldtables/"+"cutflow_DF_"+str(options.data_name)+".tex")
flavComp_DF.to_latex(buf="Yieldtables/"+"flavComp_DF_"+str(options.data_name)+".tex")
purity_DF.T.to_latex(buf="Yieldtables/"+"purity_DF_"+str(options.data_name)+".tex")
print("Finished")
