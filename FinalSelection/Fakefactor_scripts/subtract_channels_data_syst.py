import ROOT
import os
import subprocess
import sys
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
sys.path.insert(0, '../')
from options_file import *

for syst in options.inner_systematics_in_nominal_tree+options.tree_systematics:
    p=subprocess.Popen("python subtract_channels_data.py -s "+syst.name, shell=True)
    p.wait()

subprocess.Popen("python subtract_channels_data.py -s correctFakes", shell=True)

print
print "subtract_channels_data.py ran over all systematic channels - Done!"
