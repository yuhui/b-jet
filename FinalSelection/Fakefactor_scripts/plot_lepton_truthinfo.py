import ROOT
import os
import sys
import math
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
sys.path.insert(0, '../')
from options_file import *

outdir=options.output_dir
MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singeTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)
for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_nominal_combination.root"

Channels=[	'emu_OS_J2',
		'emu_SS_J2',
		#'emu_SS_J3',
		#'ee_SS_J2_2lepcut',
		#'ee_SS_J3_aCuts',
		#'mumu_SS_J2_2lepcut',
		#'mumu_SS_J3_aCuts'
]
Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]
styles = [ 20,23,22,28,25,31,24,27 ] #https://root.cern.ch/doc/v608/classTAttMarker.html
colours = [ 1,28,6,5,8,42,4,2 ]
#_lastFillColor =0
#_fillColors=[ROOT.kRed+1, ROOT.kGreen-8, ROOT.kBlue+1, ROOT.kGray+1,1,2,3,4,5,6,7,8,9,10]
outdir += "LeptonTruthinfo/"
if not os.path.exists(outdir):
    os.makedirs(outdir)

#Setup AtlasStyle for plots
import atlas_labels as al
AtlasStyle = al.atlasStyle()
AtlasStyle.SetErrorX(0.5)
AtlasStyle.SetEndErrorSize( 0 )
AtlasStyle.SetPadTopMargin(0.05)
AtlasStyle.SetPadRightMargin(  0.05)
AtlasStyle.SetPadBottomMargin( 0.16)
AtlasStyle.SetPadLeftMargin(   0.16)
gROOT.SetStyle("ATLAS")
gROOT.ForceStyle()
TH1.SetDefaultSumw2()

def gethistlist(histlist, histname, channelname, var):
	for sample in MCsamples:
		MCfile = ROOT.TFile(sample.root_file_path, "read")
		hist=MCfile.Get(histname + "_" + Flavourlabels[0]).Clone()
		hist.SetDirectory(0)
		hist.Reset()
		hist.SetStats(0)
		hist.SetName("h_" + sample.name + "_" + channelname + "_" + var)
		hist.SetLineWidth(1)
		hist.SetMarkerSize(0.5)
		hist.GetXaxis().SetTitle(var)
		hist.GetYaxis().SetTitle("Number of events")
		hist.GetXaxis().SetTitleSize(1.5)
		hist.GetXaxis().SetLabelSize(0.08)
		hist.GetYaxis().SetTitleSize(0.05)
		hist.GetYaxis().SetTitleOffset(1.1)
		for flavour in Flavourlabels:
			hist.Add(MCfile.Get(histname + "_" + flavour))
		sample.setHistOptions(hist)
		histlist.append(hist)
		MCfile.Close()

def fillcanvas(stackhist, canvas, legend, channelname):
	mainPad  = TPad("mainPad", "mainPad", 0.0, 0.0, 1.0, 1.0)
	mainPad.Draw()
	mainPad.cd()

	stackhist.Draw("E")
	stackhist.Draw("HIST E SAME")
	legend.Draw("SAME")

	l1 = ROOT.TLatex()
	l1.SetTextSize(0.04)
	l1.SetTextColor(kBlack)
	l1.SetNDC()
	l1.DrawLatex(0.60,0.65,"#bf{ATLAS} Internal")
	l1.DrawLatex(0.60,0.60, "#sqrt{s} = 13 TeV")
	l1.DrawLatex(0.60,0.55, channelname)

def plotleptruthinfo(channelname,var):
	outputfile_name = outdir + "LeptonTruthinfo_" + channelname + "_"+ var +".root"
	outputfile = ROOT.TFile(outputfile_name,"recreate")

	sign = ""
	if "OS" in channelname:
		sign = "OS"
	else:
		sign = "SS"
	### LeptonTruthinfo determination ###
	MCStack_SS = ROOT.THStack("h_MCStack_" + channelname + "_" + var, "Stacked MC " + sign + " events in " + channelname + " for " + var + ";" + var + "; Number of events")
	MCStack_SS_NPel = ROOT.THStack("h_MCStack_" + channelname + "_NPel_" + var, "Stacked MC " + sign + " NPel events in " + channelname + " for " + var + ";" + var + "; Number of events")
	MCStack_SS_NPmu = ROOT.THStack("h_MCStack_" + channelname + "_NPmu_" + var, "Stacked MC " + sign + " NPmu events in " + channelname + " for " + var + ";" + var + "; Number of events")

	histname_MC_SS = channelname + "/h_" + channelname + "_" + var
	histname_MC_SS_NPel = histname_MC_SS.replace(sign,sign+"_NPel")
	histname_MC_SS_NPmu = histname_MC_SS.replace(sign,sign+"_NPmu")

	Histograms_SS=[]
	Histograms_SS_NPel=[]
	Histograms_SS_NPmu=[]

        gethistlist(Histograms_SS, histname_MC_SS, channelname, var)
        gethistlist(Histograms_SS_NPel, histname_MC_SS_NPel, channelname, var)
        gethistlist(Histograms_SS_NPmu, histname_MC_SS_NPmu, channelname, var)

	legendlabels=[]
	for sample in MCsamples:
		legendlabels.append(sample.name[6:])

	###Plot histograms below
	#Setup legend
	leg = ROOT.TLegend(0.55,0.70,0.75,0.90)
	leg.SetTextSize(0.033)
	leg.SetShadowColor(0)
	leg.SetTextFont(42)
	leg.SetFillColor(0)
	leg.SetLineColor(0)
	leg.SetFillStyle(0)
	leg.SetBorderSize(0)
	for i in xrange(0,len(Histograms_SS)):
		MCStack_SS.Add(Histograms_SS[i])
		MCStack_SS_NPel.Add(Histograms_SS_NPel[i])
		MCStack_SS_NPmu.Add(Histograms_SS_NPmu[i])
		leg.AddEntry(Histograms_SS[i],legendlabels[i],"F")

	### Save histograms to file
	outputfile.cd()
	MCStack_SS.Write()
	MCStack_SS_NPel.Write()
	MCStack_SS_NPmu.Write()

	#Setup TCanvas and TPads
	c1 = TCanvas("c1", "LeptonTruthinfo in " + channelname, 900, 900)
	c1.Divide(2,2)
	c1.cd(1)
	fillcanvas(MCStack_SS, c1, leg, channelname)
	c1.cd(2)
	fillcanvas(MCStack_SS_NPel, c1, leg, channelname + "_NPel")
	c1.cd(3)
	fillcanvas(MCStack_SS_NPmu, c1, leg, channelname + "_NPmu")

	#Write plots to file
	outname = outdir + channelname + "_"+ var
	c1.SaveAs(outname + ".root")
	c1.SaveAs(outname + ".pdf")
	c1.SaveAs(outname + ".png")
	c1.Close()

	outputfile.Close()
	print "MC variable " + var + " in channel " + channelname + " written to file " + outputfile_name

#Loop over all channels and calculate SS 1PL distribution for each individually
print '####################################################'
print 'Running over the following samples:'
for sample in MCsamples:
	print sample.name
for channel in Channels:
	Variables = []
	if "ee" in channel:
		Variables.append('el1_true_type')
		Variables.append('el1_true_origin')
		Variables.append('el2_true_type')
		Variables.append('el2_true_origin')
	elif "mumu" in channel:
		Variables.append('mu1_true_type')
		Variables.append('mu1_true_origin')
		Variables.append('mu2_true_type')
		Variables.append('mu2_true_origin')
	else:
		Variables.append('el1_true_type')
		Variables.append('el1_true_origin')
		Variables.append('mu1_true_type')
		Variables.append('mu1_true_origin')
	for variable in Variables:
		print '####################################################'
		print 'Plotting variable ' + variable + ' in channel ' + channel
		plotleptruthinfo(channel,variable)
print "Done."
