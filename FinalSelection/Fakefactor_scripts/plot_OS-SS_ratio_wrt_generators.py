import ROOT
import os
import sys
import math
import array
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from array import array
sys.path.insert(0, '../')
from options_file import *

outdir=options.output_dir
dataName=options.data_name

MCsamples=[]
MCsamples.append(options.Diboson_sample)
MCsamples.append(options.singeTop_sample)
MCsamples.append(options.Wjets_sample)
MCsamples.append(options.ZJets_sample)
MCsamples.append(options.ttb_sample)
for sample in MCsamples:
    sample.root_file_path=outdir + sample.name+"/" + sample.name + "_nominal_combination.root"

Channels=[
		'emu_OS_NPel_J2',
		'emu_OS_NPmu_J2',
		'emu_SS_NPel_J2',
		'emu_SS_NPmu_J2',
]

if not os.path.exists(outdir+"NPLeptons/"):
    os.makedirs(outdir+"NPLeptons/")
outputfile_name=outdir+"NPLeptons/Ratio_wrt_pT_nominal.root"
outputfile=ROOT.TFile(outputfile_name,"recreate")

datafile = ROOT.TFile(outdir+dataName+"/"+dataName+"_data_combination.root", "read")

Flavourlabels=[	'bb','bc','bl','cb','cc','cl','lb','lc','ll' ]
new_binedges_el = [0.,150.,300.,600.]
new_binedges_mu = [0.,60.,80.,600.]
new_ngroup = len(new_binedges_el)-1

def ComputeRatio(sample):

	hist_OS_NPel_pt = ROOT.TH1D("hist_"+sample.name+"_OS/SS_NPel_pt","hist_"+sample.name+"_OS/SS_NPel_pt",60, 0, 600)
	hist_OS_NPel_pt.Sumw2()
	hist_OS_NPmu_pt = ROOT.TH1D("hist_"+sample.name+"_OS/SS_NPmu_pt","hist_"+sample.name+"_OS/SS_NPmu_pt",60, 0, 600)
	hist_OS_NPmu_pt.Sumw2()
	hist_SS_NPel_pt = ROOT.TH1D("hist_"+sample.name+"_SS_NPel_pt","hist_"+sample.name+"_SS_NPel_pt",new_ngroup, array('d', new_binedges_el))
	hist_SS_NPel_pt.Sumw2()
	hist_SS_NPmu_pt = ROOT.TH1D("hist_"+sample.name+"_SS_NPmu_pt","hist_"+sample.name+"_SS_NPmu_pt",60, 0, 600)
	hist_SS_NPmu_pt.Sumw2()

	for channelname in Channels:
		histname = channelname + "/h_" + channelname + "_CutFlow"
		histptname = channelname + "/h_" + channelname
		if "el" in channelname:
			histptname += "_el1_pt"
		else:
			histptname += "_mu1_pt"
		MCInputfile = ROOT.TFile(sample.root_file_path, "read")
		if "OS" in channelname:
			if "el" in channelname:
				for flavour in Flavourlabels:
					hist_OS_NPel_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
			else:
				for flavour in Flavourlabels:
					hist_OS_NPmu_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
		else:
			if "el" in channelname:
				for flavour in Flavourlabels:
					hist_SS_NPel_pt.Add(MCInputfile.Get(histptname + "_" + flavour))
			else:
				for flavour in Flavourlabels:
					hist_SS_NPmu_pt.Add(MCInputfile.Get(histptname + "_" + flavour))

	hist_OS_NPel_pt.GetXaxis().SetTitle("Non-prompt lepton p_{T} [GeV]")
	hist_OS_NPel_pt.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
	hist_OS_NPmu_pt.GetXaxis().SetTitle("Non-prompt lepton p_{T} [GeV]")
	hist_OS_NPmu_pt.GetYaxis().SetTitle("OS/SS ratio [a.u.]")
	hist_OS_NPel_rebinned_pt = hist_OS_NPel_pt.Rebin(new_ngroup,"hist_"+sample.name+"_OS/SS_NPel_rebinned_pt",array('d', new_binedges_el))
	hist_OS_NPmu_rebinned_pt = hist_OS_NPmu_pt.Rebin(new_ngroup,"hist_"+sample.name+"_OS/SS_NPmu_rebinned_pt",array('d', new_binedges_mu))
	hist_SS_NPel_rebinned_pt = hist_SS_NPel_pt.Rebin(new_ngroup,"hist_"+sample.name+"_SS_NPel_rebinned_pt",array('d', new_binedges_el))
	hist_SS_NPmu_rebinned_pt = hist_SS_NPmu_pt.Rebin(new_ngroup,"hist_"+sample.name+"_SS_NPmu_rebinned_pt",array('d', new_binedges_mu))
	hist_OS_NPel_rebinned_pt.Divide(hist_SS_NPel_rebinned_pt) 
	hist_OS_NPmu_rebinned_pt.Divide(hist_SS_NPmu_rebinned_pt)
	#hist_OS_NPel_pt.Divide(hist_SS_NPel_pt) #does not work currently, because different binning between OS and SS
	#hist_OS_NPmu_pt.Divide(hist_SS_NPmu_pt)
	outputfile.cd()
	#hist_OS_NPel_pt.Write()
	hist_OS_NPel_rebinned_pt.Write()
	#hist_OS_NPmu_pt.Write()
	hist_OS_NPmu_rebinned_pt.Write()

#Loop over all channels and compute yields for each individually
for sample in MCsamples:
	print '####################################################'
	print 'Compute OS/SS ratio for sample: ' + sample.name
	ComputeRatio(sample)
	print

outputfile.Close()
print "Histograms written to file " + outputfile_name
print "Done"
