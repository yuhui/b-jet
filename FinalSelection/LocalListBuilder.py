import ROOT
import os
import glob
from options_file import *
import argparse

# Write the folder you want to create here
folderToCreate = options.version_tag

parser = argparse.ArgumentParser(
    description='Processes the final selection and plots some cuts.')
parser.add_argument('-f',action='store',dest="folders",
                    help='Folders to search for the samples')
args = parser.parse_args()

folderToCreate=options.input_selection_dir

nominal={
    'FTAG2_Zjets_Sherpa221' : ["*36410*Sherpa*","*36411*Sherpa*","*36412*Sherpa*","*36413*Sherpa*","*364140*Sherpa*","*364141*Sherpa*","*364198*Sherpa*","*364199*Sherpa*","*36420*Sherpa*","*36421*Sherpa*"],
    'FTAG2_Zjets_PowPy8' : ["*361106*","*361107*","*361108*"],
    'FTAG2_Zjets_MGPy8' : [".*3631*","*.36151*"],
    'FTAG2_Wjets_Sherpa221' : ["*36415*Sherpa*","*36416*Sherpa*","*36417*Sherpa*","*36418*Sherpa*","*36419*Sherpa*"],
    'FTAG2_Singletop_PowPy8' : ["*410646*s3681*","*410647*s3681*","*410644*s3681*","*410645*s3681*","*410658*s3681*","*410659*s3681*"],
    'FTAG2_Singletop_PowPy8_Full' : ["*410646*s3681*","*410647*s3681*","*410644*s3681*","*410645*s3681*","*410658*s3681*","*410659*s3681*"],
    'FTAG2_Singletop_PowPy8_AF' : ["*410646*a875*","*410647*a875*","*410644*a875*","*410645*a875*","*410658*a875*","*410659*a875*"],
    'FTAG2_Singletop_PowPy8_AF2' : ["*410646*a875*","*410647*a875*","*410644*a875*","*410645*a875*","*410658*a875*","*410659*a875*"],
    'FTAG2_Singletop_aMcPy8' : ["*412005*","*412002*"],
    'FTAG2_Singletop_PowHW7' : ["*411034*","*411035*","*411036*","*411037*"],
    'FTAG2_Singletop_PowPy8_DS' : ["*410654*","*410655*","*410644*s3681*","*410645*s3681*","*410658*s3681*","*410659*s3681*"],
    'FTAG2_Singletop_PowPy8_DS_AF2' : ["*410654*","*410655*","*410644*a875*","*410645*a875*","*410658*a875*","*410659*a875*"],
    'FTAG2_Diboson_Sherpa222' : ["*364250*","*364253*","*364254*","*36335*","*363360*","*363489*","*364255*","*36428*","*364290*","*345705*","*345706*","*345715*","*345723*","*364302*","*364304*","*364305*"],
    'FTAG2_Diboson_PowPy8' : ["*.3616*"],
    'FTAG2_ttbar_PhPy8_nominal' : ["*410472.PhPy8EG.*_s3681*"],
    'FTAG2_ttbar_PhPy8_AF2' : ["*410472.PhPy8EG.*_a875*"],
    'FTAG2_ttbar_PhPy8_hdamp3mtop' : ["*410482.*"],
    'FTAG2_ttbar_Sherpa221' : ["*410252.*"],
    #    'FTAG2_ttbar_Sherpa221' : ["*410250.*","*410251.*","*410252.*"],
    'FTAG2_ttbar_aMcPy8' : ["*410465.*"],
    'FTAG2_ttbar_PowHW7' : ["*410558.*"],
    'FTAG2_ttbar_PowHW7_alt' : ["*411085*","*411086*","*411087*"],
    'data1516' : ["*grp15*","*grp16*"],
    'data17' : ["*grp17*"],
    'data18' : ["*grp18*"]
}
input_container=args.folders.split(",")
mctag=""
datatag=["grp15","grp16","grp17","grp18"]
if options.data_name == "data1516":
    mctag="r9364"
    datatag=["grp15","grp16"]
elif options.data_name == "data17":
    mctag="r10201"
    datatag=["grp17"]
elif options.data_name == "data18":
    mctag="r10724"
    datatag=["grp18"]
else:
    mctag = "" 

os.system("mkdir -p %s "%(folderToCreate))

for name,folders in nominal.items():
    os.system("mkdir -p %s "%(folderToCreate+"/"+name))
    globbedFolders=[]
    for folder in folders:
        for inFolder in input_container:
            if not "data" in name:
                globbedFolders.extend(glob.glob(inFolder+"/"+folder+"*%s*"%mctag))
            else:
                if any( x in folder for x in datatag):
                    globbedFolders.extend(glob.glob(inFolder+"/"+folder))
                else:
                    continue

    print("Sample %s has the following files "%name)
    fileIn=open(folderToCreate+"/"+name+".txt",'w')
    for myfile in globbedFolders:
        print("     %s"%myfile)
        fileIn.write(myfile+"\n")
        fileIn2=open(folderToCreate+"/"+name+"/"+myfile.split('/')[-1]+".txt",'w')
        listofFiles=glob.glob(myfile+"/*")
        for myfile2 in listofFiles:
            fileIn2.write(myfile2+"\n")
        fileIn2.close()
    fileIn.close()
