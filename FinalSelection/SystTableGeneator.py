import ROOT
import numpy as np
import os,sys,math
ROOT.PyConfig.IgnoreCommandLineOptions = True
ROOT.gROOT.SetBatch(True)
from ROOT import *
from options_file import *
import pandas as pd

# Import options file info
dataName=options.data_name
inputdir=options.output_dir
outputdir=options.plot_dir
ttb=options.ttb_sample
lumi=options.data_lumi
Taggers = options.taggers
Cut = options.cuts
WPs = options.WPs

def CheckForBootStrap(Directory):
        ListOfBootStrapSysts = []
        for Key in Directory.GetListOfKeys():
            # Check if the key is a smoothed directory
            if "smoothing_" in Key.GetName():
            	syst_name = Key.GetName().split("smoothing_")[-1]
                ListOfBootStrapSysts.append(syst_name)
        return ListOfBootStrapSysts

def getGroupedPandas(Folder,SubFolder,Cut,Tagger,WorkingPoint):
	NominalFile_Name = options.plot_dir+"/FitPlots_3SB/FinalFitPlots_r21_"+dataName+"_"+Tagger+"_"+Cut+"_emu_OS_J2.root"
	NominalFile=ROOT.TFile(NominalFile_Name,"read")
	print("Opened up nominal file:\t" + NominalFile_Name)
	syst_dir = NominalFile.Get(Folder+"/"+SubFolder)
	print("Working in systematic folder: " + Folder)
	# Create a DF to store all the systs from syst folder per WP
	data = {}
	bootstrap_files_in_folder = False
	bootstrap_systs = CheckForBootStrap(syst_dir)
	for key in syst_dir.GetListOfKeys():
		if "c_e_b_" in key.GetName():
			# This skips the canvas' that are in the folders
			continue
		elif "_unused" in key.GetName():
			# This skips the alt generator systs
			continue
		elif "Pileup" in key.GetName() or "pileup" in key.GetName():
			# This makes sure the next line doesnt remove pileup systs
			pass
		elif "Scaledown" in key.GetName():
			# Theres a MET syst
			pass
		elif "up" in key.GetName() or "down" in key.GetName():
			# This skips up and down variations and only takes the Error_rel hist
			continue
		elif "smoothing_" in key.GetName():
			# This skips smoothing folders and deals with them later
			bootstrap_files_in_folder = True
			continue
		Hist = syst_dir.Get(key.GetName())
		print("Getting relative errors for systematic:\t" + str(key.GetName()))
		ErrorArr = GetErrors(Hist)
		data.update({key.GetName():ErrorArr})
	# Now need to check if any of them need to be replaced with the bootstrap version
	if bootstrap_files_in_folder == True:
		for syst in bootstrap_systs:
			new_syst_dir = NominalFile.Get(Folder+"/"+SubFolder+"/smoothing_"+syst)
			non_bootstrap_name = "e_b_"+WorkingPoint+"_"+syst+"_syst_Error_rel"
			if data.get(non_bootstrap_name) == None:
				# Skip since its not used in systs
				pass
			else:
				# Now change to the quadrature error from bootstrap
				UpHist_name = "e_b_"+WorkingPoint+"_"+syst+"_syst_up_Error_rel"
				DownHist_name = "e_b_"+WorkingPoint+"_"+syst+"_syst_down_Error_rel"
				UpHist = new_syst_dir.Get(UpHist_name)
				DownHist = new_syst_dir.Get(DownHist_name)
				data[non_bootstrap_name] = GetBootstrapErrors(UpHist,DownHist)
	NominalFile.Close()
	print("Closed file.")
	DF=pd.DataFrame(data=data.values(),index=data.keys(),columns=[("ptbin_%d"%(x+1)) for x in range(9)],dtype=np.float64)
	return DF

def GetBootstrapErrors(UpHistogram,DownHistogram):
	UpErrorArray = []
	DownErrorArray = []
	ErrorArray = []
	for another_bin in range(0,UpHistogram.GetNbinsX()):
		# Get the errors for the up variation
		UpErrorArray.append(UpHistogram.GetBinContent(another_bin+1))
	for a_different_bin in range(0,DownHistogram.GetNbinsX()):
		# Get the errors for the down variation
		DownErrorArray.append(DownHistogram.GetBinContent(a_different_bin+1))
	if len(UpErrorArray) != len(DownErrorArray):
		print("Unequal up and down arrays! Come to this print statement and figure out why!")
		raw_input()
	else:
		for element in range(0,len(UpErrorArray)):
			# Loop over one of the arrays since we know they are the same size
			new_value_squared = math.pow(UpErrorArray[element],2) + math.pow(DownErrorArray[element],2)
			ErrorArray.append(math.pow(new_value_squared,0.5))
	return ErrorArray

def GetErrors(histogram):
    ErrorArray = []
    for bin in range(0,histogram.GetNbinsX()):
        ErrorArray.append(histogram.GetBinContent(bin+1))
    return ErrorArray

def CreatedGroupedSystDF(DataFrame,SystNameDict):
    GroupedDFneg=pd.DataFrame(0,index=SystNameDict.keys(),columns=[("ptbin_%d"%(x+1)) for x in range(9)],dtype=np.float64)
    GroupedDFpos=pd.DataFrame(0,index=SystNameDict.keys(),columns=[("ptbin_%d"%(x+1)) for x in range(9)],dtype=np.float64)
    for group_name, syst_names in SystNameDict.items():
        # Loop over the group and syst names to group them together
        # print(group_name, syst_names)
        for syst, row_of_values in DataFrame.iterrows():
            # Loop over each indiv syst and its pt values
            for syst_name in syst_names:
                # Can be multiple names in a group so need another loop
                if syst_name in syst:
					# Now we've found a syst that belongs to a group, put it into pos and neg syst tables
					for pt_bin, value in row_of_values.iteritems():
						# Loop over each value per pt bin and check for pos or neg
						if value >= 0.0:
							# Add the square value for summing in quadrature
							GroupedDFpos.loc[[group_name],[pt_bin]] = GroupedDFpos.loc[[group_name],[pt_bin]] + math.pow(value,2)
						elif value < 0.0:
							GroupedDFneg.loc[[group_name],[pt_bin]] = GroupedDFneg.loc[[group_name],[pt_bin]] + math.pow(value,2)
    return GroupedDFpos,GroupedDFneg

if __name__ == '__main__':
    FinalFitPlotFolders=["ttbar_pdf_systematics", "other_systematics", "JET_JES_systematics", "JET_JER_systematics", "FT_EFF_Eigen_C_systematics", "FT_EFF_Eigen_Light_systematics", "ttbar_systematics","Diboson_systematics","singletop_systematics"]
    grouped_syst_names={
        'Powheg+Herwig7' : ['ttbar_PowHW7'],
        'Jet Resolution' : ['JET_JER', "JET_EffectiveNP"],
        'Other JES': ['JET_JES',"JET_BJES"],
        'Egamma'         : ['EG_','leptonSF_EL'],
        'Muon'           : ['MUON_','leptonSF_MU'] ,
        'Jet $\eta$ InterCalib.': ['JET_EtaIntercalibration'],
        'Jet flavour'    : ['JET_Flavor'],
		'Other JER': ['JET_PunchThrough',"JET_SingleParticle"],
        'PDF systematics': ['mc_shower_np'],
        'Jet Pileup'     : ['JET_Pileup'],
        'Flavour Tagging': ['FT_EFF','missLight'],
        'MET'            : ['MET_SoftTrk'],
        'Pileup Data SF' : ['weight_pileup'],
        'FSR'            : ['weight_mc_fsr'],
        'ISR'            : ['mc_rad'],
        "JVT Efficiency" : ["weight_jvt"],
        "MC Stat."       : ["MCstat"],
        "Fakes":["correctFakes"],
            "Singletop Theory":["singletop"]
        }
    OutputDict = {}
    OutputGroupedDict = {}
    OutputGroupedRelDict = {}
    # Create output directory
    cwd = os.getcwd() + "/"
    outdir = cwd + "Systematics/"
    if not os.path.exists(outdir):
        # Create output directory  if it doesn't exist
        os.makedirs(outdir)
    for tagger in Taggers:
        # Loop over tagggers
        for cut in Cut:
            # Loop over cuts - this is only FixedcutBEff now
            for wp in WPs:
                # Now loop over the fixed cut operating points
                print("Getting systematics for the working point:\t" + str(wp) + "\%")
                DictOfDFSysts = {}
                for syst_folder in FinalFitPlotFolders:
                    new_DF = getGroupedPandas(syst_folder, "e_b_wp_"+wp, cut, tagger, wp)
                    DictOfDFSysts.update({syst_folder:new_DF})
                MergedDF = pd.concat(DictOfDFSysts.values())
                # Round all the nos to 6 sig fig
                # MergedDF = MergedDF.round(6)
                print("Finalised changes to the dataframe for:\t" +tagger+"_"+cut+"_"+wp)
                # Create grouped version of the dataframe using the dict grouped_syst_names
                GroupedDFPos,GroupedDFNeg = CreatedGroupedSystDF(MergedDF,grouped_syst_names)
                # Now sqaure root to complete summing in quadrature
                GroupedDFPos = np.power(GroupedDFPos,0.5)
                GroupedDFNeg = np.power(GroupedDFNeg,0.5)
                GroupedDFPos = GroupedDFPos.sort_values(by='ptbin_1',ascending=False)
                GroupedDFNeg = GroupedDFNeg.sort_values(by='ptbin_1',ascending=False)
                # Create a final DF that is the grouped in %
                GroupedRelDFPos = GroupedDFPos * 100.0
                GroupedRelDFNeg = GroupedDFNeg * 100.0
                GroupedRelDF = np.power(np.power(GroupedRelDFPos,2) + np.power(GroupedRelDFNeg,2),0.5)
                GroupedRelDF = GroupedRelDF.sort_values(by='ptbin_1',ascending=False)
                # If you want to see your dataframe, uncomment:
                # print(GroupedRelDF)
                # Output everything we need
                MergedDF.to_latex(buf="Systematics/"+dataName+"_systematics_"+tagger+"_"+cut+"_"+wp+".tex")
                # GroupedRelDFPos.to_latex(buf="Systematics/"+dataName+"_systematics_"+tagger+"_"+cut+"_"+wp+"_grouped_percent_pos"+".tex")
                # GroupedRelDFNeg.to_latex(buf="Systematics/"+dataName+"_systematics_"+tagger+"_"+cut+"_"+wp+"_grouped_percent_neg"+".tex")
                GroupedRelDF.to_latex(buf="Systematics/"+dataName+"_systematics_"+tagger+"_"+cut+"_"+wp+"_grouped_percent_joined"+".tex")
                OutputDict.update({tagger+"_"+cut+"_"+wp:MergedDF})
                OutputGroupedRelDict.update({tagger+"_"+cut+"_"+wp+"_grouped_percent_pos":GroupedRelDFPos})
                OutputGroupedRelDict.update({tagger+"_"+cut+"_"+wp+"_grouped_percent_neg":GroupedRelDFNeg})
                OutputGroupedRelDict.update({tagger+"_"+cut+"_"+wp+"_grouped_percent_joined":GroupedRelDF})
    print("Done.")
