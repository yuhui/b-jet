setupATLAS
lsetup git
if [[ ! -d TSelectors/build ]];
then
    mkdir TSelectors/build
    cd TSelectors/build
    acmSetup AnalysisBase,24.2.4,here
    acm compile
else
    cd TSelectors/build
    acmSetup AnalysisBase,24.2.4,here

fi

cd ../../
#source x86_64-centos7-gcc11-opt/setup.sh
# export LD_LIBRARY_PATH
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/TSelectors/build/lib
