When your grid jobs are finished you can run the final selection locally. This is done in order to be able to do small changes fast without rerunning the full event selection.


#### sample collection
Your first steps should be to gather all the files you ran over on the grid per sample. The sample folders can be created by *RucioListBuilder.py* , which takes the list of files you ran over and creates a folder for each sample.

You can create a list of the files with a command such as:
```bash
source setup.sh
rucio ls --short "user.USERNAME:user.USERNAME.*.DAOD_FTAG2.*p-tag*output.root" > my_files.txt
```

Now run *RucioListBuilder.py* in order to get all files of a sample on your local-group-disk, and LocalFolderBuilder.py to get everything in the correct format for the final selection.

```
python RucioListBuilder.py -o <out_dir> <my_files.txt> ( -a <RSE> in case that your ntuples are stored in a RSE that is not at CERN)
python LocalFolderBuilder.py -f <out_dir>
```

Insert your sample file names and other options into (you can look how this is done in the example option files.)
*options_file.py*. The names in the option file have to be the same as the names of the "high-level-txt" files! 
There are many systematic options fo the systematic uncertainties here. 

#### event selection
afterward you can run final selection over the found files:
```bash
python finalSelection.py --rrS rl21-21-2-4-171009/input_selection/FTAG2_Diboson_PowPy8_21-2-4_old.txt
```

The final selection can be ran over all the samples and different systematics options via htcondor:
```bash
cd htcondor_submit
python submit_to_condor.py
```

*finalSelection.py* is a python script which calculates the lumiweight and runs the */TSelectors/source/Selectors/macros/runTSelectorMC.cxx* on it. The input files are gatherd in txt files by *RucioListBuilder.py* for each sample separately. 
don't forget the flag  *--data* when you run over data files!
```
positional arguments:

  input_file            input file for example: ../selecting_inputs/user.jschm
                        oec.410000.PowhegPythiaEvtGen.DAOD_FTAG2.e3698_s2608_s
                        2183_r7725_r7676_p2669.anaTopStyle-17-06-06_output.roo
                        t.txt



optional arguments:

  -h, --help            show this help message and exit

  --rrS                 rerun the Event Selection (default: false)

  --data                the input is data (default: false)

  --test                only run on two files for testing (default: false)

  --dPlots              draw Comparison Plots, takes a while. (default: false)

  --sFile               The input is a single File and not a file with Folders
                        as usual (default: false)

  -t TSYST, --tsyst TSYST

                        name of systematic tree to run over. default=nominal.

  -w ISYST, --isyst ISYST
                        only valid if you run on nominal. You can specify
                        inner systematics here like
                        weight_leptonSF_EL_SF_Trigger_UP(weights)

  --log                 Create log files, nice option for batch submission!
                        (default: false)

  --runParallel         if you want to run all jubs in parallel. (default:
                        false)

  --runFakes            Includes SS channels relevant for fake estimation
                        (default: false)

  --correctFakes        Scale factors are applied to correct weights of OS events
                        with a non-prompt electron - have to be generated first!
                        (default: false)
			To generate them, first run with option --run_fakes
			Then, use Fakefactor_scripts/compare_1PL_2PL_Data.py

  --run_on_VR           It will run the full final selection on Variable-R track jets.			
```

The real selection is done by the Tselector *finalSelectionMC.C*.
*TTbar-b-calib-final-selection/TSelectors/source/Selectors/libSelectorsMC/finalSelectionMC.cxx*

You also need to make sure you run the nominal with save_bin_mean option turned on in the TSelectors (HistForFitContainer.h with variable name save_bin_something_sr_hists). Then do the same thing again when you use combinehists_for_fit_systs in the likelihood section. Its used for the final calculate_all_fits so you'll be mad if you dont run it as you'll have to redo it all again anyway so make sure its turned on for both stages for the nominal SM samples.

and if you processed all samples you can run:
*python combinePlots.py*
which will produce a bunch of control plots.
this is a little outdated.
Ir oder to be able to plot systematic uncertainty bands the root script stack_plot.C was developed. Its in a not good state, with hard coded pathes. Needs clean up!

In order to merge the output of mc16a/d/e and data15-16/17/18, you can run the merge_productions.py script in each you need to change the path of combi_dir_1(2,3) to the output directory of the final selection for each dataset. The script will hadd all the files and save them under the path defined as "merged_dir".

then go to [likelihood-fit](../likelihood-fit/).


#### other scripts:

 - cutflows-to-table.py gets numbers of events from different processes and flavours from the nominal MC samples and data. These tables can be inseretd to google-sheets. 

 - writeCDI.py and writeCDI_pseudocontinous.py write the test input files for the next generation of CDI files. They can be run after you performed all the fits. Check out https://gitlab.cern.ch/atlas-ftag-calibration/inputs_CDI to upload your files to the cdi building.

 - cdi_input_to_table.py use this script to write a table with all the uncertainies which ended up in you cdi-text input files. It prints the table in a latex format. 

#### Run with Bootstrap Weights:
- You'll need use *pflow_submit_to_condor.py*
- Turn on all options except *run_Pdf_syst*
- Turn on /uncomment a makeshift flag on the first lines of the submitfinalselction method something relating to:
    ```
    if not "data15161718FTAG2_ttbar_PhPy8" in job_name:
       return
       ```
- Then go into your respective options file year and turn off all inner tree systematics (by commenting them out) as well as all tree systematics except those that have the do_boostrap attribute set to True (they are the ones we'll be running on).
- You can now run *pflow_submit_to_condor.py*. This should give you give a high purity list of jobs with a dozen or so that aren't run on bootstrap weights. It's not perfect but if you can be bothered to think of an idea to implement, be our guest!
- Next you need to merge these files together you have created. To do this use *htcondor_submit/merger_boostrap.py* (to use on condor). You can also do this locally but it uses lots of RAM so if you're doing full Run 2 data, use condor. Beware though, it takes a lot of RAM so your system admin will not be happy so do this out-of-hours so they don't notice.
- If you have run on condor, your files will be in your output directory (check options file if you can't remember) + "/bootstrap_merged/". We now need to replace these files with our existing ones created without bootstrap weights. For this *ReplaceNominalWBootStrap.py* was created and you can just run this but first follow the instructions at the top of file and secondly, if you're including or missing an files you can uncomment the section it loops over, for example, FTAG2_ttbar_Sherpa221.

